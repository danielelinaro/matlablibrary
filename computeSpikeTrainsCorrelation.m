function c = computeSpikeTrainsCorrelation(y_i,y_j,tau,L,dt,nu_i,nu_j)
% c = computeSpikeTrainsCorrelation(y_i,y_j,tau,L,dt,nu_i,nu_j)
tau_i = round(abs(tau)/dt);
if tau > 0
    c = mean(sum(y_i(:,1:end-tau_i).*y_j(:,tau_i+1:end),2) / ((L-abs(tau))*dt) - nu_i*nu_j);
else
    c = mean(sum(y_j(:,1:end-tau_i).*y_i(:,tau_i+1:end),2) / ((L-abs(tau))*dt) - nu_i*nu_j);
end
