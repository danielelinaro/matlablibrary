function [I,f] = extractfIPoints(files, options)
% [I,f] = extractfIPoints(files, options)

if isstruct(files(1))
    files =  arrayfun(@(x) x.name, files, 'UniformOutput', 0);
end

if strcmp(options.mode, 'current')
    if isfield(options, 'Ke')
        Ke = options.Ke;
    else
        Ke = [];
    end
    if isfield(options, 'thresh')
        thresh = options.thresh;
    else
        thresh = -20;
    end
    [I,f] = computefICurveFromFiles(files,Ke,thresh,0,1);
elseif strcmp(options.mode, 'current_staircase')
    n = length(files);
    [t,V,Iinj,stim] = loadH5Traces(files);
    dt = t(2)-t(1);
    if isfield(options, 'Ke')
        V = AECoffline(V,Iinj,options.Ke);
    end
    spks = extractSpikes(t,V,-20,1);
    I = cell(1,size(V,1));
    f = cell(1,size(V,1));
    for ii=1:n
        idx = find(stim{ii}(:,3) > 0);
        I{ii} = zeros(1,length(idx));
        f{ii} = zeros(1,length(idx));
        stimtimes = cumsum(stim{ii}(:,1));
        for jj=1:length(idx)
            t0 = stimtimes(idx(jj)-1) + 0.5;
            t1 = stimtimes(idx(jj));
            nspks = length(find(spks{ii} > t0 & spks{ii} < t1));
            I{ii}(jj) = stim{ii}(idx(jj),3);
            f{ii}(jj) = nspks / (t1 - (t0+0.5));
        end
    end
    I = flattenCellArray(I);
    f = flattenCellArray(f);
    [I,idx] = sort(I);
    f = f(idx);
elseif strcmp(options.mode, 'frequency_ramp')
    n = length(files);
    I = [];
    f = [];
    for k=1:n
        out = loadH5Trace(files{k});
        I = [I ; out.data.ds5];
        f = [f ; out.data.ds4];
    end
    [I,idx] = sort(I);
    f = f(idx);
elseif strcmp(options.mode, 'frequency_staircase')
    n = length(files)-1;
    Vm = cell(n,1);
    Iinj = cell(n,1);
    spks = cell(n,1);
    for k=1:n
        out = loadH5Trace(files{k});
        t = (0:length(out.data.ds1)-1) * out.dt;
        Vm{k} = out.data.ds1';
        Iinj{k} = out.data.ds5';
        spks(k) = extractSpikes(t,Vm{k},-20,1);
    end
    isi = cellfun(@(x) diff(x), spks, 'UniformOutput', 0);
    f = cellfun(@(x) 1./x, isi, 'UniformOutput', 0);
    idx = cellfun(@(x) round(x/out.dt), spks, 'UniformOutput', 0);
    I = [];
    for k=1:n
        I = [I ; Iinj{k}(idx{k}(2:end))'];
    end
    [I,idx] = sort(I);
    f = flattenCellArray(f);
    f = f(idx);
end


