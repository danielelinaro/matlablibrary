function [rho,c_ij,c_ii,c_jj] = computeRho(y_i,y_j,L,dt,delta,nu_i,nu_j,c_ii,c_jj)
% [rho,c_ij,c_ii,c_jj] = computeRho(y_i,y_j,L,dt,delta,nu_i,nu_j,[c_ii, [c_jj]])
if size(y_i,1) ~= size(y_j,1)
    error('y_i and y_j must contain the same number of trials.');
end
N = size(y_i,1);
if length(nu_i) ~= N || length(nu_j) ~= N
    error('nu_i and nu_j must have as many elements as the number of trials.');
end
tau = -delta : dt : delta;
n_tau = length(tau);
c_ij = zeros(N,n_tau);
for i=1:N
    for j=1:n_tau
        c_ij(i,j) = computeSpikeTrainsCorrelation(y_i(i,:),y_j(i,:),...
            tau(j),L,dt,nu_i(i),nu_j(i));
    end
end
c_ij = sum(mean(c_ij));
if all(all(y_i == y_j))
    c_ii = c_ij;
    c_jj = c_ij;
else
    if ~exist('c_ii','var') || isnan(c_ii)
        c_ii = zeros(N,n_tau);
        for i=1:N
            for j=1:n_tau
                c_ii(i,j) = computeSpikeTrainsCorrelation(y_i(i,:),y_i(i,:),...
                    tau(j),L,dt,nu_i(i),nu_i(i));
            end
        end
        c_ii = sum(mean(c_ii));
    end
    if ~exist('c_jj','var') || isnan(c_jj)
        c_jj = zeros(N,n_tau);
        for i=1:N
            for j=1:n_tau
                c_jj(i,j) = computeSpikeTrainsCorrelation(y_j(i,:),y_j(i,:),...
                    tau(j),L,dt,nu_j(i),nu_j(i));
            end
        end
        c_jj = sum(mean(c_jj));
    end
end
rho = c_ij/sqrt(c_ii*c_jj);
