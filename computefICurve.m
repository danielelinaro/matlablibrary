function [I,f,gain,rheobase,CV] = computefICurve(t,V,Iinj,stim,thresh,ttran)
% computefICurve analyses data from steps injections to compute a f-I
% curve.
% 
% [I,f,gain,rheobase,CV] = computefICurve(t,V,Iinj,stim,thresh,ttran)
% 
% Parameters:
%       t - (1xN) time vector.
%       V - (MxN) voltage matrix, where each row contains a measurement.
%    Iinj - (MxN) current matrix, can be empty if stim is specified.
%    stim - (Mx1) cell array containing the stimulus matrixes.
%  thresh - threshold for spike detection.
%   ttran - transient duration for spike frequency adaptation (values < 0
%           instruct the function to compute the frequency as the inverse
%           of the first ISI).
% 
% Returns:
%         I - the values of injected current.
%         f - the corresponding firing rate, after ttran seconds.
%      gain - the gain of the f-I curve in Hz/pA.
%  rheobase - the current threshold for sustained firing in pA.
%        CV - coefficient of variation of the ISIs.
% 
% See also computefICurveFromFiles.
% 

% Author: Daniele Linaro

t0 = cellfun(@(x) sum(x(1:end-2,1)), stim);
dur = cellfun(@(x) x(end-1,1), stim);
n = size(V,1);
if ~ isempty(Iinj)
    I = zeros(n,1);
else
    I = cellfun(@(x) x(end-1,3), stim);
end
f = zeros(n,1);
CV = nan(n,1);

p = defaultParameters(loadCellType);
for k=1:n
    idx = find(t > t0(k)+ttran & t < t0(k)+dur(k));
    if ~ isempty(Iinj)
        I(k) = mean(Iinj(k,idx));
    end
    spks = extractAPPeak(t(idx),V(k,idx),thresh,p.min_isi);
    f(k) = length(spks{1}) / (dur(k)-ttran);
    isi = diff(spks{1});
    CV(k) = std(isi) / mean(isi);
    if ttran < 0 && length(spks{1}) > 1
        f(k) = 1/isi(1);
    end
end
[I,idx] = sort(I);
f = f(idx);
CV = CV(idx);

idx = find(f > 0);
if length(idx) < 2
    model = fit(I,f,fittype('a*x+b'),'StartPoint',[0,0]);
else
    model = fit(I(idx),f(idx),fittype('a*x+b'),'StartPoint',polyfit(I(idx),f(idx),1));
end
try
    ci = confint(model);
    gain = struct('value',model.a,'confidence',ci(:,1));
    rheobase = struct('value',-model.b/model.a,'confidence',sort(-ci(:,2)./ci(:,1)));
catch
    ci = [];
    gain = struct('value',model.a);
    rheobase = struct('value',-model.b/model.a);
end
