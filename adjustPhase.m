function phi_out = adjustPhase(F,phi_in,tolerance,do_plot)
% phi_out = adjustPhase(F,phi_in,tolerance,do_plot)
n = length(phi_in);
phi_out = phi_in;
for k=2:n
    while abs(phi_out(k)-phi_out(k-1)) >= pi
        if phi_out(k) > phi_out(k-1)
            phi_out(k) = phi_out(k)-2*pi;
        else
            phi_out(k) = phi_out(k)+2*pi;
        end
    end
    if F(k) > 100 && phi_out(k) > phi_out(k-1)+pi/10
        while phi_out(k) > phi_out(k-1)
            phi_out(k) = phi_out(k) - 2*pi;
        end
    end
end
idx = find(F >= 100);
start = find(phi_out(idx) < 0, 1);
for k=start+1:length(idx)
    while phi_out(idx(k)) > phi_out(idx(k-1))+tolerance
        phi_out(idx(k)) = phi_out(idx(k)) - 2*pi;
    end
end
if do_plot
    figure;
    semilogx(F,phi_in,'ko-','MarkerFaceColor','k');
    hold on;
    semilogx(F,phi_out,'ro-','MarkerFaceColor','r');
    box off;
    xlabel('Frequency (Hz)');
    ylabel('Phase (rad/s)');
    set(gcf,'Color','w');
end
end