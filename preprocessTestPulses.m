function preprocessTestPulses(window)
% preprocessTestPulses(window)

if ~ exist('window','var') || isempty(window)
    window = 20;
end

files = listH5Files;
n = length(files);
stim = load('stimuli/step_01.stim');
dV = diff(stim(1:2,3));
t0 = stim(1,1);
dur = stim(2,1);
[~,Vh] = loadH5TracesBis(files(1),2);
[t,I] = loadH5TracesBis(files,1);
idx = t < t0;

Ra = nan(n,1);
ds = 1;
figure;
subplot(11,1,1:5);
hold on;
for i=1:n-window-1
    II = I(i:i+window,:);
    I0 = mean(mean(II(:,idx)));
    Im = mean(II);
    Imax = max(Im);
    dI = Imax - I0;
    Ra(i) = dV/dI * 1e3;
    if i == 10
        plot(t(1:ds:end),Im(1:ds:end),'k','LineWidth',1);
    end
end
plot(t(1:ds:end),Im(1:ds:end),'r','LineWidth',1);
axis tight;
axis off;
yl = ylim;
if yl(2) < 50
    dy = 20;
else
    dy = floor(yl(2)/2/50)*50;
end
placeScaleBars(0.1*t0,yl(2)-dy,t0/2,dy,sprintf('%.0f ms',t0/2*1e3),...
    sprintf('%.0f pA',dy),'k','LineWidth',1);
subplot(11,1,6);
hold on;
% plot(t,Vh,'Color',[.6,.6,.6],'LineWidth',1);
plot([0,t0,t0,t0+dur,t0+dur,t(end)],[0,0,dV,dV,0,0],'Color',[.6,.6,.6],'LineWidth',1);
placeScaleBars(0.1*t0,dV/2,[],dV/2,'',sprintf('%.0f mV',dV/2),'k','LineWidth',1);
axis tight;
axis off;

T = cumsum(arrayfun(@(y) seconds(y), diff(datetime(cellfun(@(x) x(1:end-3), ...
    files, 'UniformOutput', 0),'InputFormat','yyyyMMddHHmmss'))));
T = [0 ; T];
idx = find(~isnan(Ra));
T = T(idx);
Ra = Ra(idx);
save('test_pulses.mat','T','Ra');
subplot(11,1,7:11);
hold on;
hold on;
plot(T(10)/60+[0,0],[0,max(Ra)*1.1],'k','LineWidth',1);
plot(T(end)/60+[0,0],[0,max(Ra)*1.1],'r','LineWidth',1);
plot(T/60,Ra,'bo-','LineWidth',1,'MarkerFaceColor','w','MarkerSize',3);
set(gca,'TickDir','Out','LineWidth',0.8,'YLim',[0,max(Ra)*1.1]);
sz = [7,5];
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz,'Color','w');
box off;
xlabel('Time (min)');
ylabel('Access resistance (MOhm)');
print('-dpdf','test_pulses.pdf');
print('-dpng','-r300','-painters','test_pulses.png');

