function covT_theory = computeCov_theory3(cell1,cell2,var_in,c,T,nu1,nu2)
% covT_theory = computeCov_theory3(cell1,cell2,var_in,c,T,nu1,nu2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% compute linear response prediction for spike count covariance in long windows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S_OU_2 = @(w,tau,sigma) 2*tau.*sigma.^2 ./ (1+w.^2*tau.^2);

%%% or fit the f-I curve with a power-law
data_in = load(strcat(cell1,'/fI_curves/fI_curves.mat'));
I = data_in.Iu{2};
f = data_in.fm{2};
[a,b,~,Itest,ftest] = fitFICurve(I,f,data_in.rheobase(2,:));
[~,ind] = min(abs(ftest - nu1));
fIgain1 = a*b*Itest(ind)^(b-1);

data_in = load(strcat(cell2,'/fI_curves/fI_curves.mat'));
I = data_in.Iu{2};
f = data_in.fm{2};
[a,b,~,Itest,ftest] = fitFICurve(I,f,data_in.rheobase(2,:));
[~,ind] = min(abs(ftest - nu2));
fIgain2 = a*b*Itest(ind)^(b-1);

%%% estimate the spike count covariance
%%% assume the transfer function is constanct from 0 to 1/T Hz
freq = -100:.001:100; % Hz
k0 = find(freq==0);
freq = freq([1:k0-1 k0+1:end]);
% df = freq(2) - freq(1);

%%%%% The following formula is wrong, there's an additional 1/T that should
%%%%% not be there
% kT = (sin(pi.*freq.*T).^2)./(T*pi^2.*freq.^2);
%%%%% Corrected version:
kT = (sin(pi.*freq.*T).^2)./(pi^2.*freq.^2);
covT_theory = zeros(size(var_in));

%%% input power spectrum
% Pss_AMPA = 0.005./(1+(2*pi*0.005*freq).^2); %OU noise with timescale 5 ms and unit variance
% Pss_GABA = 0.01./(1+(2*pi*0.01*freq).^2); %OU noise with timescale 10 ms and unit variance
% Pss_NMDA = 0.1./(1+(2*pi*0.1*freq).^2);
% Pss0 = Pss_AMPA + Pss_GABA;
% Pss0 = 0.1./(1+(2*pi*0.1*freq).^2);

% integral = trapz(freq,Pss0);

for i=1:length(var_in)

	%%% normalize total input power to what it should be
% 	Pss = Pss0./(integral/var_in(i));
    Pss = S_OU_2(2*pi*freq,100e-3,sqrt(var_in(i)));
	covT_theory(i) = c*fIgain1*fIgain2*trapz(freq,kT.*Pss);
end
