function [a,b,c,varargout] = fitFICurve(I,f,rheobase)
% [a,b,c] = fitFICurve(I,f,rheobase)
% [a,b,c,II,ff,rheobase,rss] = fitFICurve(I,f,rheobase)
% 
% rheobase must be a three-element vector containing the actual value of
% rheobase and the confidence interval.
% 

% Author: Daniele Linaro - May 2015

I = I(:)';
f = f(:)';

if rheobase(1) < min(I(f > 0))
    I = [rheobase(1), I];
    f = [0, f];
end
[I,idx] = sort(I);
f = f(idx);
f = f(I>0);
I = I(I>0);
try
    p = polyfit(I,f,1);
    g = fittype('a*x^b+c');
    model = fit(I(:),f(:),g,'StartPoint',[p(1),1,p(2)]);
%     model = fit(I,f,'power2');
    rss = sum((f-model(I)).^2);
%     if model.c > 1 && rheobase(1) > 100
%         Izero = (1:floor(rheobase(2)))';
%         II = [Izero ; I];
%         ff = [zeros(size(Izero)) ; f];
%         model = fit(II,ff,'power2');
%     end
    a = model.a;
    b = model.b;
    c = model.c;
    if nargout > 3
        varargout{1} = 0:.1:I(end)*1.1;
        varargout{2} = a*varargout{1}.^b + c;
        if c < 0
            varargout{3} = (-c/a)^(1/b); % the rheobase computed from the power-law
        else
            varargout{3} = (c/a)^(1/b);
        end
        varargout{4} = rss;
    end
catch
    a = nan;
    b = nan;
    c = nan;
    if nargout > 3
        varargout{1} = nan;
        varargout{2} = nan;
        varargout{3} = rheobase(1);
        varargout{4} = nan;
    end
end
    