function p = defaultParameters(cell_type)

keys = struct('PYR',1,'PV',2,'SOM',3,'CULT',4,'GC',5);

%%% pyramidal cells
par(1).max_ahp_dur = 5e-3;
par(1).max_adp_dur = 20e-3;
par(1).min_isi = 5e-3;
par(1).min_dV_thresh = 100;      % used in extractAPRapidness
par(1).min_dV_thresh_coeff = 10; % used in extractAPEnd
par(1).dVdtThresh = 50;          % used in extractAPAcceleration
par(1).max_peak_to_end = 5e-3;
par(1).max_threshold_to_peak = 5e-3;
par(1).rapidness_window = [3e-3,5e-3];

%%% PV+ cells
par(2).max_ahp_dur = 2e-3;
par(2).max_adp_dur = 7e-3;
par(2).min_isi = 3e-3;
par(2).min_dV_thresh = 100;
par(2).min_dV_thresh_coeff = 2;
par(2).dVdtThresh = 50;
par(2).max_peak_to_end = 3e-3;
par(2).max_threshold_to_peak = 2e-3;
par(2).rapidness_window = [2e-3,3e-3];

%%% SOM+ cells
par(3).max_ahp_dur = 3e-3;
par(3).max_adp_dur = 10e-3;
par(3).min_isi = 5e-3;
par(3).min_dV_thresh = 100;
par(3).min_dV_thresh_coeff = 5;
par(3).dVdtThresh = 50;
par(3).max_peak_to_end = 3e-3;
par(3).max_threshold_to_peak = 3e-3;
par(3).rapidness_window = [3e-3,5e-3];

%%% Cultured cells
par(4).max_ahp_dur = 30e-3;
par(4).max_adp_dur = 50e-3;
par(4).min_isi = 10e-3;
par(4).min_dV_thresh = 10;
par(4).min_dV_thresh_coeff = 2;
par(4).dVdtThresh = 50;
par(4).max_peak_to_end = 15e-3;
par(4).max_threshold_to_peak = 7e-3;
par(4).rapidness_window = [7e-3,10e-3];

%%% granule cells
par(5).max_ahp_dur = 5e-3;
par(5).max_adp_dur = 20e-3;
par(5).min_isi = 3e-3;
par(5).min_dV_thresh = 100;      % used in extractAPRapidness
par(5).min_dV_thresh_coeff = 10; % used in extractAPEnd
par(5).dVdtThresh = 150;          % used in extractAPAcceleration
par(5).max_peak_to_end = 5e-3;
par(5).max_threshold_to_peak = 5e-3;
par(5).rapidness_window = [3e-3,5e-3];

p = par(keys.(cell_type));

