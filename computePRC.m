function [T,CV,phi,dphi,cphi,cdphi] = computePRC(data,method)
% COMPUTEPRC Computes the phase-response curve of an oscillator, using
% various possible methods.
% 
% [T,CV,phi,dphi,cphi,cdphi] = computePRC(data, method)
% 
% Arguments:
%   data - a (cell array of) structure(s) obtained from readPRCData,
%          containing the spike and perturbation times.
% method - the name of a method for computing the PRC. Available options
%          are (traditional,corrected).
%
% Returns:
%      T - the mean ISI.
%     CV - the mean coefficient of variation.
%    phi - The phase, in the range [0,1].
%   dphi - The variation of phase.
%   cphi,cdphi - Control phi and dphi.
% 

if ~ exist('method','var')
    method = 'traditional';
end

if ~ strcmp(method, 'traditional') && ~ strcmp(method, 'corrected')
    disp(['Unknown method: ', method, '.']);
end

% if ~ iscell(data)
%     data = {data};
% end

n = length(data);
T = zeros(n,1);
CV = zeros(n,1);
phi = cell(n,1);
dphi = cell(n,1);
if nargout == 6
    cphi = cell(n,1);
    cdphi = cell(n,1);
end

for k=1:length(data)
    
    Ttrans = data{k}.Tdelay;
%     idx = find(data{k}.spikes > Ttrans & data{k}.spikes < data{k}.perturb(1));
    idx = find(data{k}.spikes > Ttrans);
    T(k) = mean(diff(data{k}.spikes(idx)));
    CV(k) = std(diff(data{k}.spikes(idx))) / T(k);
%     if CV(k) > 0.1
%         CV(k)
%         error('The CV of the ISIs must be <= 0.1');
%     end

    if strcmp(method,'traditional')
        
        [phi{k},dphi{k}] = computePRCtraditional(data{k}.spikes, data{k}.perturb, T(k));
        if nargout == 6
            ipi = diff(data{k}.perturb);
            n = 1;
            tmp_phi = zeros(n,3*(length(ipi)+1));
            tmp_dphi = zeros(n,3*(length(ipi)+1));
            for i=1:n
                perturb = cumsum([data{k}.perturb(1); ipi(randperm(length(ipi)))]);
                [tmp_phi(i,:),tmp_dphi(i,:)] = computePRCtraditional(data{k}.spikes, perturb, T(k));
                [tmp_phi(i,:),idx] = sort(tmp_phi(i,:));
                tmp_dphi(i,:) = tmp_dphi(i,idx);
            end
            cphi{k} = mean(tmp_phi);
            cdphi{k} = mean(tmp_dphi);
        end
        
    elseif strcmp(method,'corrected')
        
        [phi{k},dphi{k}] = computePRCcorrected(data{k}.spikes, data{k}.perturb, T(k));
        if nargout == 6
            ipi = diff(data{k}.perturb);
            n = 1;
            tmp_phi = zeros(n,3*(length(ipi)+1));
            tmp_dphi = zeros(n,3*(length(ipi)+1));
            for i=1:n
                perturb = cumsum([data{k}.perturb(1); ipi(randperm(length(ipi)))]);
                try
                [tmp_phi(i,:),tmp_dphi(i,:)] = computePRCcorrected(data{k}.spikes, perturb, T(k));
                catch
                    keyboard
                end
                [tmp_phi(i,:),idx] = sort(tmp_phi(i,:));
                tmp_dphi(i,:) = tmp_dphi(i,idx);
            end
            cphi{k} = mean(tmp_phi);
            cdphi{k} = mean(tmp_dphi);
        end
        
    end

end

T = mean(T);
CV = mean(CV);

phi = cell2mat(phi);
dphi = cell2mat(dphi);
if nargout == 6
    cphi = cell2mat(cphi);
    cdphi = cell2mat(cdphi);
end

% sort the points
[phi,idx] = sort(phi);
dphi = dphi(idx);
if nargout == 6
    [cphi,idx] = sort(cphi);
    cdphi = cdphi(idx);
end

% take only points in the range [0,1]
idx = find(phi >= 0 & phi <= 1);
phi = phi(idx);
dphi = dphi(idx);
if nargout == 6
    idx = find(cphi >= 0 & cphi <= 1);
    cphi = cphi(idx);
    cdphi = cdphi(idx);
end

end

%%% Traditional method
function [phi,dphi] = computePRCtraditional(spikes, perturb, T)
trials = length(perturb);
phi = zeros(trials,1);
dphi = zeros(trials,1);
for k=1:trials
    prev = find(spikes < perturb(k), 1, 'Last');
    next = prev+1;
    tau = perturb(k) - spikes(prev);
    phi(k) = tau / T;
    dphi(k) = 1 - (spikes(next) - spikes(prev)) / T;
end
end

%%% Corrected method
function [phi,dphi] = computePRCcorrected(spikes, perturb, T)
trials = length(perturb);
phi = nan(trials,3);
dphi = nan(trials,3);
for k=1:trials
    prev = find(spikes < perturb(k), 1, 'Last');
    if prev+2 <= length(spikes)
        next = prev+1;
        tau = perturb(k) - spikes(prev);
        Ti = spikes(next) - spikes(prev);
        Tim1 = spikes(prev) - spikes(prev-1);
        Tip1 = spikes(next+1) - spikes(next);
        phi(k,1) = tau / T;
        phi(k,2) = (Tim1 + tau) / T;
        phi(k,3) = (tau - Ti) / T;
        dphi(k,1) = 1 - Ti / T;
        dphi(k,2) = 1 - Tim1 / T;
        dphi(k,3) = 1 - Tip1 / T;
    end
end
idx = find(~isnan(phi(:,1)));
phi = phi(idx,:);
dphi = dphi(idx,:);
phi = phi(:);
dphi = dphi(:);
end
