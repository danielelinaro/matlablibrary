function [y,idx,z] = outliers(x,w)
% [y,idx,z] = outliers(x,w)
if ~ exist('w','var') || isempty(w)
    w = 1.5;
end
n = size(x,2);
y = cell(1,n);
idx = cell(1,n);
z = cell(1,n);
for i=1:n
    q = prctile(x(:,i),[25,75]);
    dq = diff(q);
    jdx = x(:,i) > q(2)+w*dq | x(:,i) < q(1)-w*dq;
    y{i} = x(jdx,i);
    idx{i} = find(jdx);
    z{i} = x(~jdx,i);
end
if n == 1
    y = y{1};
    idx = idx{1};
    z = z{1};
end
