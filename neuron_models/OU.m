function eta = OU(tend, dt, mu, sigma, tau, eta0, seed)
% eta = OU(tend, dt, mu, sigma, tau, eta0, seed)
if ~ exist('eta0','var')
    eta0 = 0;
end
if ~ exist('seed','var')
    seed = randi(5061983);
end
stream = RandStream('mt19937ar','Seed',seed);
n = round(tend/dt) + 1;
eta = zeros(1,n);
eta(1) = eta0;
if isscalar(mu)
    mu = repmat(mu, size(eta));
end
if isscalar(sigma)
    sigma = repmat(sigma, size(eta));
end
x = exp(-dt/tau);
one_minus_x2 = 1-x*x;
tau_over_2 = tau/2;
noise = stream.randn(size(eta));
c = 2*sigma.^2./tau;
for i=1:n-1
    eta(i+1) = mu(i) + x*(eta(i)-mu(i)) + sqrt(c(i)*tau_over_2*one_minus_x2)*noise(i);
end
