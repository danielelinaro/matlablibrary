function [V,w] = aEIF(tend, dt, C, gL, EL, deltaT, VT, tauw, a, b, Vr, tarp, Iext, varargin)
% [V,w] = aEIF(tend, dt, C, gL, EL, deltaT, VT, tauw, a, b, Vr, tarp, Iext)
% [V,w] = aEIF(tend, dt, C, gL, EL, deltaT, VT, tauw, a, b, Vr, tarp, Iext, g_1, g_2, ...)
% g_i is a struct with fields:
%   E - the reversal potential
%   G - the conductance

% Author - Daniele Linaro, May 2013

tend = tend*1e3;
dt = dt*1e3;
tauw = tauw*1e3;
tarp = tarp*1e3;

n = round(tend/dt) + 1;
if isscalar(Iext)
    Iext = repmat(Iext,[1,n]);
end
for k=14:nargin
    if isscalar(varargin{k-13}.G)
        varargin{k-13}.G = repmat(varargin{k-13}.G,[1,n]);
    end
end

V = zeros(1,n);
w = zeros(1,n);

V(1) = -65;
w(1) = 0;

dt_over_C = dt/C;
dt_over_tauw = dt/tauw;
gL_times_deltaT = gL*deltaT;
one_over_deltaT = 1/deltaT;

refractory_period = floor(tarp/dt);

t = 2;
while t <= n
    I = Iext(t-1);
    for k=14:nargin
        I = I + varargin{k-13}.G(t-1) * (varargin{k-13}.E - V(t-1));
    end
    V(t) = V(t-1) + dt_over_C * (gL*(EL-V(t-1)) + ...
        gL_times_deltaT*exp(one_over_deltaT*(V(t-1)-VT)) + I - w(t-1));
    w(t) = w(t-1) + dt_over_tauw * (a*(V(t-1)-EL) - w(t-1));
    if V(t) > 20
        V(t:t+refractory_period) = Vr;
        w(t:t+refractory_period) = w(t)+b;
        t = t+refractory_period;
    end
    t = t+1;
end
