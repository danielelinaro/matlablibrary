function [V,x] = EIF_Kv7(tend, dt, C, gL, EL, deltaT, VT, Vre, tau_ref, Iext, varargin)
% [V,x] = EIF_Kv7(tend, dt, C, gL, EL, deltaT, VT, Vre, tau_ref, Iext)
% [V,x] = EIF_Kv7(tend, dt, C, gL, EL, deltaT, VT, Vre, tau_ref, Iext, g_1, g_2, ...)
% g_i is a struct with fields:
%   E - the reversal potential
%   G - the conductance

% Author - Daniele Linaro, March 2014

tend = tend*1e3;
dt = dt*1e3;
tau_ref = tau_ref*1e3;
Vth = 30;

gx = 0.3;   % [mS/cm^2]
taux = 200; % [ms]
Vxh = -40;  % [mV]
Dx = 8;     % [mV]
Ex = -80;   % [mV]

n = round(tend/dt) + 1;
if isscalar(Iext)
    Iext = repmat(Iext,[1,n]);
end
for k=14:nargin
    if isscalar(varargin{k-13}.G)
        varargin{k-13}.G = repmat(varargin{k-13}.G,[1,n]);
    end
end

V = zeros(1,n);
x = zeros(1,n);

V(1) = -65;
x(1) = 0;

dt_over_C = dt/C;
dt_over_taux = dt/taux;
gL_times_deltaT = gL*deltaT;
one_over_deltaT = 1/deltaT;

refractory_period = floor(tau_ref/dt);

reset = Vth - (0:dt:tau_ref)/tau_ref * (Vth-Vre);

t = 2;
while t <= n
    I = Iext(t-1);
    for k=14:nargin
        I = I + varargin{k-13}.G(t-1) * (varargin{k-13}.E - V(t-1));
    end
    V(t) = V(t-1) + dt_over_C * (gL*(EL-V(t-1)) + gx*x(t-1)*(Ex-V(t-1)) + ...
        gL_times_deltaT*exp(one_over_deltaT*(V(t-1)-VT)) + I);
    x(t) = x(t-1) + dt_over_taux * (xinf(V(t-1),Vxh,Dx) - x(t-1));
    if V(t) > Vth
        V(t:t+refractory_period) = reset;
        for i=0:refractory_period
            x(t+i) = x(t+i-1) + dt_over_taux * (xinf(V(t+i-1),Vxh,Dx) - x(t+i-1));
        end
        t = t+refractory_period;
    end
    t = t+1;
end

end

function x = xinf(V,Vxh,Dx)
x = 1 ./ (1 + exp(-(V-Vxh)/Dx));
end
