function V = LIF(tend, dt, C, tau, tarp, Er, E0, Vth, Iext, varargin)
% V = LIF(tend, dt, C, tau, tarp, Er, E0, Vth, Iext)
% V = LIF(tend, dt, C, tau, tarp, Er, E0, Vth, Iext, g_1, g_2, ...)
% 
% g_i is a struct with fields:
%   E - the reversal potential
%   G - the conductance
% 

% Author - Daniele Linaro, May 2013

n = round(tend/dt) + 1;
if isscalar(Iext)
    Iext = repmat(Iext,[1,n]);
end
for k=10:nargin
    if isscalar(varargin{k-9}.G)
        varargin{k-9}.G = repmat(varargin{k-9}.G,[1,n]);
    end
end

V = zeros(1,n);

lambda = -1/tau;    % inverse of the time constant
coeff = exp(lambda*dt);
Rl = tau/C;         % leak resistance

t_prev_spike = -1000;

refractory_period = floor(tarp/dt);

V(1) = Er;

for t=2:n
    if (t - t_prev_spike) < refractory_period
        V(t) = Er;
    else
        I = 0;
        for k=10:nargin
            I = I + varargin{k-9}.G(t-1) * (varargin{k-9}.E - V(t-1));
        end
        Vinf = Rl*(I+Iext(t-1)) + E0;
        V(t) = Vinf - (Vinf-V(t-1)) * coeff;
        if V(t) > Vth
            V(t) = 20;
            t_prev_spike = t;
        end
    end
end

