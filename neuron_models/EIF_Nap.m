function [V,m] = EIF_Nap(tend, dt, C, gnap, gL, EL, deltaT, VT, Vre, tau_ref, Iext, varargin)
% [V,m] = aEIF_Nap(tend, dt, C, gnap, gL, EL, deltaT, VT, Vre, tau_ref, Iext)
% [V,m] = aEIF_Nap(tend, dt, C, gnap, gL, EL, deltaT, VT, Vre, tau_ref, Iext, g_1, g_2, ...)
% g_i is a struct with fields:
%   E - the reversal potential
%   G - the conductance

% Author - Daniele Linaro, March 2014

tend = tend*1e3;
dt = dt*1e3;
tau_ref = tau_ref*1e3;
Vth = 30;

Vnaph = -48;  % [mV]
Dnap = 10;     % [mV]
Ena = 55;   % [mV]

n = round(tend/dt) + 1;
if isscalar(Iext)
    Iext = repmat(Iext,[1,n]);
end
for k=14:nargin
    if isscalar(varargin{k-13}.G)
        varargin{k-13}.G = repmat(varargin{k-13}.G,[1,n]);
    end
end

V = zeros(1,n);
m = zeros(1,n);

V(1) = -65;
m(1) = 0;

dt_over_C = dt/C;
gL_times_deltaT = gL*deltaT;
one_over_deltaT = 1/deltaT;

refractory_period = floor(tau_ref/dt);

reset = Vth - (0:dt:tau_ref)/tau_ref * (Vth-Vre);

t = 2;
while t <= n
    I = Iext(t-1);
    for k=14:nargin
        I = I + varargin{k-13}.G(t-1) * (varargin{k-13}.E - V(t-1));
    end
    V(t) = V(t-1) + dt_over_C * (gL*(EL-V(t-1)) + gnap*m(t-1)*(Ena-V(t-1)) + ...
        gL_times_deltaT*exp(one_over_deltaT*(V(t-1)-VT)) + I);
    m(t) = m(t-1) + dt/taum(V(t-1)) * (minf(V(t-1),Vnaph,Dnap) - m(t-1));
    if V(t) > Vth
        stop = min(t+refractory_period,length(V));
        V(t:stop) = reset(1:stop-t+1);
        for i=0:stop-t
            m(t+i) = m(t+i-1) + dt/taum(V(t+i-1)) * (minf(V(t+i-1),Vnaph,Dnap) - m(t+i-1));
        end
        t = t+refractory_period;
    end
    t = t+1;
end

end

function m = minf(V,Vxh,Dx)
m = 1 ./ (1 + exp(-(V-Vxh)/Dx));
end

function tau = taum(V)
if V < -40
    tau = 0.025+0.14*exp((V+40)/10);
else
    tau = 0.02+0.145*exp(-(V+40)/10);
end
end
