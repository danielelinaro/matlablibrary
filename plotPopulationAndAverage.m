function plotPopulationAndAverage(x,mode,yLabel,yLim,cmap)
% plotPopulationAndAverage plots a set of values with their average
% and error bar.
%
% plotPopulationAndAverage(x,mode,yLabel,yLim,cmap)
%
% Parameters:
%       x - an array containing the values to be plotted.
%    mode - either 'std' or 'sem', which is the default value.
%  yLabel - the label for the y-axis.
%    yLim - the limit for the y-axis. If not specified, 3 standard
%           deviations from the mean are used.
%

% Author: Daniele Linaro - April 2013.

if ~ exist('mode','var')
    mode = 'sem';
end

if ~ iscell(x)
    x = {x};
end

for i=1:length(x)
    n = sum(~isnan(x{i}));
    m = nanmean(x{i});
    s = nanstd(x{i});
    if strcmpi(mode,'std')
        e = s;
    elseif strcmpi(mode,'sem')
        e = s / sqrt(n);
    else
        error('Supported modes are STD and SEM.');
    end
    
    if ~ exist('yLim','var') || isempty(yLim)
        yLim = [m-3*s,m+3*s];
        if min(cellfun(@(y) min(y), x)) > 0
            yLim(1) = max(yLim(1),0);
        end
    end
    
    hold on;
    P = sobolset(1);
    if ~exist('cmap','var')
        if length(x) == 1
            cmap = [0,0,0];
        else
            cmap = lines(length(x));
        end
    end
    for j=1:length(x{i})
        plot(i-0.25+rand/2, x{i}(j), 'o', 'Color', 'k', ...
            'MarkerFaceColor', [1,.75,.5], 'MarkerSize', 6);
    end
    errorbar(i,m,e,'s','Color','k','MarkerFaceColor',[0,.5,1], 'MarkerSize', 8);
end

axis([0,i+1,yLim]);
set(gca,'XTick',[],'TickDir','Out');
if exist('yLabel','var') && ~isempty('yLabel')
    ylabel(yLabel);
end
title(sprintf('n = %d', n));
