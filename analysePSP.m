function [weight,deflection,err] = analysePSP(files, ntrials, target, window, type, doPlot)
% ANALYSEPSP performs a simple analysis of the data obtained with a PSP
% optimization procedure.
% 
% [weight,deflection,error] = analysePSP(files, ntrials, target, window, type, doPlot)
% 
% Parameters:
%       files - a cell array containing the list of files to analyse.
%     ntrials - the number of trials performed for each weight in the
%               optimization procedure.
%      target - the target deflection of the optimization procedure.
%      window - a 2-elements vector containing the times before and after
%               the pulse to plot (default [0.1,0.3]).
%        type - 'epsp' or 'ipsp' (the latter is the default).
%      doPlot - whether to plot the results (default is 1).
% 
% Returns:
%      weight - the weights used during the optimization.
%  deflection - the corresponding (mean) deflections.
%       error - the associated error.
% 

% Author: Daniele Linaro - January 2013

if ~ exist('window','var')
    window = [0.1,0.3];
end

if ~ exist('type','var')
    type = 'ipsp';
end

if ~ exist('doPlot','var')
    doPlot = 1;
end

if ~ strcmpi(type,'ipsp') && ~ strcmpi(type,'epsp')
    error('Type must be either ''epsp'' or ''ipsp'' (the latter one being the default).');
end

nfiles = length(files);
groups = nfiles / ntrials;

% load all the data
[t,Vpre,Vpost,Isyn,Ipulse] = loadH5TracesBis(files,1,3,2,4);
dt = diff(t(1:2));

% find the time when the stimulation was on
idx = find(Ipulse(1,:) ~= 0);
tstim = (idx(1)-1) * dt;
stimdur = length(idx) * dt;
preamble = round((idx(1)-1) * 0.9);

if doPlot
    figure;
    %%%
    threshold = str2double(extractEntityParameterFromXMLFile( ...
            ['.dclamp/',files{1}(1:end-3),'/psp.xml'], ...
            'LIFNeuron','Vth'));
    idx = find(t > tstim-5e-3 & t < tstim+stimdur+100e-3);
    T = t(idx) - t(idx(1));
    subplot(2,1,1);
    hold on;
    plot(T, Vpre(1,idx), 'k');
    plot(T, -80 + Isyn(1,idx)/20, 'r');
    plot(T, Vpost(1,idx), 'b');
    plot(T, -90 + Ipulse(1,idx)/400, 'm');
    plot(T([1,end]),threshold+[0,0],'--','Color',[.6,.6,.6]);
    axis([0,0.1,-100,-45]);
    placeScaleBars(0, -100, 0.01, 10, '10 ms', ...
        {'10 mV', 'k'; '10 mV', 'b'; '200 pA', 'r'; '4 nA', 'm'}, 'k', 'LineWidth', 2);
    axis off;
end

idx = find(t > tstim-window(1) & t < tstim+stimdur+window(2));
T = t(idx) - t(idx(1));
weight = zeros(groups,1);
deflection = zeros(groups,1);
err = zeros(groups,1);

if doPlot
    subplot(2,1,2);
    hold on;
end

for k=1:groups
    block = (k-1)*ntrials+1:k*ntrials;
    [Vm,V] = meanWithOffset(Vpost(block(max(Vpost(block,:),[],2) < -60),:),preamble);
    deflection(k) = max(abs(Vm(t > tstim & t < tstim+0.1)));
    err(k) = (deflection(k) - target)^2;
    weight(k) = str2double(extractEntityParameterFromXMLFile( ...
        ['.dclamp/',files{k*ntrials}(1:end-3),'/psp.xml'], ...
        'SynapticConnection','weight'));
    if doPlot
        plot(T+(k-1)*(sum(window)+0.1),V(:,idx),'Color',[.6,.6,.6],'LineWidth',1);
        plot(T+(k-1)*(sum(window)+0.1),Vm(idx),'Color','k','LineWidth',2);
        text((k-1)*(sum(window)+0.1), 2, sprintf('w = %.1f', weight(k)));
        text((k-1)*(sum(window)+0.1), 1.5, sprintf('|d| = %.1f mV', deflection(k)));
        text((k-1)*(sum(window)+0.1), 1, sprintf('e = %.2f mV^2', err(k)));
    end
end
if doPlot
    axis tight
    yl = ylim;
    if strcmpi(type,'ipsp')
        axis([xlim,yl(1)-0.2,2]);
    else
        axis([xlim,-2,yl(2)+0.2]);
    end
    placeScaleBars(0, yl(1), 0.1, 1, '100 ms', '1 mV', 'k', 'LineWidth', 2);
    axis off;
    
    set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,10,10]);
    print('-depsc2','psp.eps');
end
