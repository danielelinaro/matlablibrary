function [c,lags] = computeCorrelation(x,y,maxlag,lagstep)
% COMPUTECORRELATION Estimates the correlation function between two time series.
% 
% [c,lags] = computeCorrelation(x,y,maxlag,lagstep)
% 
% Parameters:
%      x,y - the time series
%   maxlag - the maximum lag
%  lagstep - the step among lags.
% 
% Returns:
%      c - the estimated autocorrelation
%   lags - the corresponding lags
% 

% Author: Daniele Linaro - February 2012

if ~exist('lagstep','var') || isempty(lagstep)
    lagstep = 1;
end
if lagstep < 1
    error('lagstep must be > 0');
end

lags = -maxlag:lagstep:maxlag;
c = zeros(size(lags));
n = (length(lags)-1)/2;
for k=1:n
    c(k) = corr(x(-lags(k)+1:end),y(1:end+lags(k)));
end
c(n+1) = corr(x,y);
for k=1:n
    c(k+n+1) = corr(x(1:end-lags(k+n+1)),y(lags(k+n+1)+1:end));
end
