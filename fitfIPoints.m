function [lin_model,nlin_model] = fitfIPoints(I,f,fthresh)
% FITFIPOINTS fits data from a f-I curve experiment with a PID controller.
%
% The returned model is linear (f(I) = f_0 + k*I) and constitutes a fit
% to the linear part of the f-I curve.
% 
% model = fitfIPoints(I,f,fthresh)
% 
% Parameters:
%          I - an array containing the values of current.
%          f - an array containing the values of frequency.
%    fthresh - the maximum value of frequency to consider (default 100 Hz)
% 
% Returns:
%  lin_model - a linear model of the form f(I) = offset + gain*I
% nlin_model - a non-linear model of the form f(I) = A * sqrt(I-I0)
% 

% Author: Daniele Linaro - February 2013

if ~ exist('fthresh','var')
    fthresh = 100;  % default firing rate threshold, reasonable for pyramidal neurons
end

% a linear model
lin = fittype('offset+gain*I', 'coeff', {'offset','gain'}, 'indep', 'I');
% a non-linear one
nlin = fittype('A*sqrt(I-I0)', 'coeff', {'A','I0'}, 'indep', 'I');

% remove all points above the threshold
idx = find(f < fthresh);
I = I(idx);
f = f(idx);

% remove the outliers
m = mean(f);
s = std(f);
idx = find(f > m-2*s & f < m+2*s);
I = I(idx);
f = f(idx);

lin_model = fit(I, f, lin, 'StartPoint', [0,0.03]);
nlin_model = fit(I, f, nlin, 'StartPoint', [1,min(I)-30], 'Upper', [inf,min(I)]);
