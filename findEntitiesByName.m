function idx = findEntitiesByName(entities, name)
% idx = findEntitiesByName(entities, name)

idx = [];
n = length(entities);
for k=1:n
    if strcmp(entities(k).name,name)
        idx = [idx, k];
    end
end
