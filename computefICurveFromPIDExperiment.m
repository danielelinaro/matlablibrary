function [I,f,models] = computefICurveFromPIDExperiment(t,V,I_pid,doPlot)

if ~ exist('doPlot','var')
    doPlot = 0;
end

%%% compute the instantaneous firing frequency
n = size(V,1);
dt = diff(t(1:2));
spks = extractAPPeak(t,V,-20);
f = cellfun(@(x) (1./diff(x))', spks, 'UniformOutput', 0);
idx = cellfun(@(x) round(x(2:end)/dt), spks, 'UniformOutput', 0);
I = cell(n,1);
for k=1:n
    I{k} = I_pid(k,idx{k})';
end

%%% fit the data
models = cell(n,2);
for k=1:n
    [models{k,1},models{k,2}] = fitfIPoints(I{k},f{k},50);
end

if doPlot
    %%% plot the data
    figure;

    v_off   = 0.1;
    h_off   = 0.1;
    v_space = 0.1;
    h_space = 0.1;
    sub = 5;

    height = (1 - v_off*2 - (n-1)*v_space) / n;
    fntsize = 8;

    for k=1:n
        width = 0.5;
        ypos = v_off + (k-1)*(height+v_space);
        h = axes('Position', [h_off, ypos, width, height], 'NextPlot', 'Add');
        plot(t,V(k,:),'Color',[.4,.4,.4],'LineWidth',1);
        placeScaleBars(-2,min(V(k,:))-10,5,20,'5 s','20 mV','k');
        c = get(h,'Children');
        axis tight
        axis([-2,t(end),ylim]);
        axis off;
        for ii=1:length(c)
            try
                set(c(ii), 'FontName', 'Arial', 'FontSize', fntsize);
            catch
            end
        end

        orange = [1,.5,0];
        blue = [0,.5,1];
        
        axes('Position', [h_off+width+h_space, ypos, 0.25, height], ...
            'NextPlot', 'Add', 'FontName', 'Arial', 'FontSize', fntsize);
        
        plot(I{k}(1:sub:end), f{k}(1:sub:end), 'o', ...
            'Color', [.5,.5,.5], 'MarkerSize', 5);

        x = linspace(models{k,2}.I0, 1.1*max(I{k}), 1000);
        y = feval(models{k,2}, x);
        plot(x, y, '--', 'Color', orange, 'LineWidth', 3);
        
        x = [min(I{k}), max(I{k})];
        y = feval(models{k,1}, x);
        plot(x, y, '--', 'Color', blue, 'LineWidth', 2);
        
        axis([models{k,2}.I0-20, 1.1*max(I{k}), 0, y(end)]);
        
        set(gca, 'XTick', 0:200:max(I{k}), 'YTick', 0:10:max(f{k}));
        xlabel('I (pA)');
        ylabel('Frequency (Hz)');
        text(1.1*max(I{k}), 6, sprintf('Gain: %.1f Hz/nA', models{k,1}.gain*1000), ...
            'HorizontalAlignment', 'Right', ...
            'FontName', 'Arial', 'FontSize', fntsize);
    end

    set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,9,6]);
    print('-depsc2', 'fI_PID.eps');
end
