function preprocessImpedanceExperiment(M,N,input_resistance)
% preprocessImpedanceExperiment(M,N,input_resistance)

DEBUG = 0;

if ~exist('N','var') || isempty(N)
    N = 2;
end
if ~exist('M','var') || isempty(M)
    M = 1;
end

% kernel for active electrode compensation
kernels = dir('*_kernel.dat');
Ke = load(kernels(1).name);

% the files that contain the data
files = listH5Files;
nfiles = length(files);

% the model used in fitting the oscillatory voltage trace
g = fittype('A*sin(2*pi*f*t+phi)','indep','t','problem','f','coeff',{'A','phi'});

% tested frequency
f = zeros(nfiles,1);
% measured impedance
R = zeros(nfiles,1);
% measured phase
phase = zeros(nfiles,1);
keep = true(nfiles,1);
V0 = nan(nfiles,1);
if DEBUG
    figure;
    hold on;
    cmap = jet(nfiles);
    hndl = zeros(nfiles,1);
    lgnd = cell(nfiles,1);
end
for i=1:nfiles
    [entities,info] = loadH5Trace(files{i});
    t0 = entities(2).metadata(1,1);
    dur = entities(2).metadata(2,1);
    f(i) = entities(2).metadata(2,4);
    T = 1/f(i);
    samples_per_period = ceil(T/info.dt);
    if samples_per_period ~= T/info.dt || f(i) == 30 || f(i) == 80
        % avoid messing up the fitting of the phase
        keep(i) = false;
        continue
    end
    number_of_periods = floor(dur/info.dt/samples_per_period);
    idx = t0/info.dt+1:t0/info.dt+samples_per_period*number_of_periods;
    t = linspace(0,T,samples_per_period)';
    V = mean( reshape( ...
        AECoffline(entities(1).data(idx),entities(2).data(idx),Ke),...
        [samples_per_period,length(idx)/samples_per_period]), 2);
    V0(i) = mean(V);
    V = V-V0(i);
    [A,j] = max(V);
    phi = t(j) - T/4;
    model = fit(t,V,g,'problem',f(i),'StartPoint',[A,phi]);
    R(i) = abs(model.A) / entities(2).metadata(2,3) * 1e3;
    phase(i) = model.phi;
    if model.A < 0
        phase(i) = phase(i) - pi;
    end
    if DEBUG
        x = linspace(t(1),t(end),100);
        y = abs(model.A) * sin(2*pi*model.f*x + phase(i));
        hndl(i) = plot(t/t(end),V/max(y),'Color',cmap(i,:));
        lgnd{i} = sprintf('%.1f Hz', f(i));
        plot(x/x(end),y/max(y),'Color',min(cmap(i,:)+[0.3,0.3,0.3],[1,1,1]),'LineWidth',2);
    end
end
if DEBUG
    plot(linspace(0,1,100),sin(2*pi*linspace(0,1,100)),'k','LineWidth',2);
    axis tight;
    grid on;
    legend(hndl(keep),lgnd(keep),'Location','SouthEastOutside');
end
V0 = nanmean(V0);

% remove useless frequencies
f = f(keep);
R = R(keep);
phase = phase(keep);

% adjust the phases
for i=2:length(phase)
    if f(i) > 10
        while phase(i) < phase(i-1)-2*pi
            phase(i) = phase(i)+2*pi;
        end
        while phase(i-1) < phase(i)
            phase(i) = phase(i)-2*pi;
        end
    end
end

% sort everything
[f,idx] = sort(f);
R = R(idx);
phase = phase(idx);

% if the user provided the input resistance, we add it to the data
if exist('input_resistance','var')
    f = [f(1)/10; f];
    R = [input_resistance; R];
    phase = [phase(1); phase];
end

phase = adjustPhase(f,phase,pi/2,0);

% fit the rational transfer function with M zeros and M poles
[A,Z,P,Dt,offset] = ...
            fitTransferFunction(f, R, phase, M, N, [], [1;1], 0);

% evaluate the computed transfer function on more points
[w,mag,phi] = evalTransferFunction(A,Z*2*pi,P*2*pi,Dt,0.5*f(1)*2*pi,2*f(end)*2*pi,100);

save('impedance.mat','f','R','phase','M','N','A','Z','P','Dt','offset','V0');
if exist('input_resistance','var')
    save('impedance.mat','input_resistance','-append');
end

% do some plotting
fnt = 11;
green = [0,.6,0];
blue = [0,0,.6];

both = 0;

if both
    figure;
    axes('Position',[0.15,0.13,0.8,0.38],'NextPlot','Add');
    set(gca,'FontSize',fnt);
    plot(w/(2*pi),phi,'k','LineWidth',1);
    plot(f,phase,'ko','MarkerFaceColor',[1,0,.5],'LineWidth',1,'MarkerSize',6);
    xlabel('Frequency (Hz)');
    ylabel('Phase (rad)');
    axis([w([1,end])/(2*pi),ylim]);
    set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8,'YTick',-2:1);
    
    axes('Position',[0.15,0.56,0.8,0.38],'NextPlot','Add');
    set(gca,'FontSize',fnt);
    plot(w/(2*pi),mag,'k','LineWidth',1);
    plot(f,R,'ko','MarkerFaceColor',[1,0,.5],'LineWidth',1,'MarkerSize',6);
    if exist('input_resistance','var')
        plot(f(1),R(1),'ks','MarkerFaceColor',[1,.5,0],'MarkerSize',8);
        text(10^log10(0.8*f(1)),0.95*R(1),sprintf('R_{in} = %.0f MOhm', input_resistance),...
            'VerticalAlignment','Top','FontSize',fnt);
    end
    text(f(end),max(R),sprintf('V_m = %.1f mV', mean(V0)),'HorizontalAlignment','Right',...
        'FontSize',fnt);
    ylabel('Impedance (MOhm)');
    axis([w([1,end])/(2*pi),ylim]);
    set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8,'XTickLabel',[]);
    
    axes('Position',[0.15,0.13,0.8,0.81],'NextPlot','Add');
    for i=1:M
        plot(Z(i)+[0,0],[0,1],'Color',blue,'LineWidth',1);
        text(10^log10(0.85*Z(i)),0.01,sprintf('Z_%d = %.2f Hz', i, Z(i)),'Rotation',90,...
            'Color',blue,'FontSize',fnt);
    end
    for i=1:N
        plot(P(i)+[0,0],[0,1],'Color',green,'LineWidth',1);
        text(10^log10(1.5*P(i)),0.01,sprintf('P_%d = %.2f Hz', i, P(i)),'Rotation',90,...
            'Color',green,'FontSize',fnt);
    end
    set(gca,'XScale','Log');
    axis([w([1,end])/(2*pi),0,1]);
    axis off;
    
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,5],'PaperSize',[6,5]);
else
    figure;
    axes('Position',[0.1,0.2,0.85,0.75],'NextPlot','Add');
    set(gca,'FontSize',fnt);
    plot(w/(2*pi),mag,'k','LineWidth',1);
    plot(f,R,'ko','MarkerFaceColor',[1,0,.5],'LineWidth',1,'MarkerSize',6);
    if exist('input_resistance','var')
        plot(f(1),R(1),'ks','MarkerFaceColor',[1,.5,0],'MarkerSize',8);
        text(10^log10(0.8*f(1)),0.95*R(1),sprintf('R_{in} = %.0f MOhm', input_resistance),...
            'VerticalAlignment','Top','FontSize',fnt);
    end
    for i=1:M
        plot(Z(i)+[0,0],ylim,'Color',blue,'LineWidth',1);
        text(10^log10(0.85*Z(i)),0.01,sprintf('Z_%d = %.2f Hz', i, Z(i)),'Rotation',90,...
            'Color',blue,'FontSize',fnt);
    end
    for i=1:N
        plot(P(i)+[0,0],ylim,'Color',green,'LineWidth',1);
        text(10^log10(1.5*P(i)),0.01,sprintf('P_%d = %.2f Hz', i, P(i)),'Rotation',90,...
            'Color',green,'FontSize',fnt);
    end
    text(f(end),max(R),sprintf('V_m = %.1f mV', mean(V0)),'HorizontalAlignment','Right',...
        'FontSize',fnt);
    xlabel('Frequency (Hz)');
    ylabel('Impedance (MOhm)');
    axis([w([1,end])/(2*pi),ylim]);
    set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8);
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,3],'PaperSize',[6,3]);
end

print('-dpdf','impedance.pdf');
