function [R,C] = extractRinAndCFromVoltageStep(t,I,dV,t0,dur)
% [R,C] = extractRinAndCFromVoltageStep(t,I,dV,t0,dur)

Im = mean(I(:,t<=t0+dur));
dI = mean(Im(t>t0+dur-50e-3 & t<=t0+dur)) - mean(Im(t<t0));   % [mV]
R = dV/dI*1e3;                       % [MOhm]
[Imax,i] = max(Im(t>t0+1e-3 & t<t0+100e-3));
Im = Im - Imax;
Im(Im>0) = 0;
idx = round(t0/diff(t(1:2))) + (0:i);
Q = trapz(t(idx),Im(idx));           % [coul]
C = Q/dV*1e3;                        % [pF]
