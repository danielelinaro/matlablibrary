function preprocessImpedances(varargin)

if nargin == 0
    dirs = arrayfun(@(x) x.name, dir('0*'), 'UniformOutput', 0);
elseif nargin == 1
    if ischar(varargin{1})
        dirs = {varargin{1}};
    else
        dirs = varargin{1};
    end
    if isstruct(dirs(1))
        dirs = arrayfun(@(x) x.name, dirs);
    end
elseif nargin == 2 && strcmp(varargin{1},'ignore')
    dirs = arrayfun(@(x) x.name, dir('0*'), 'UniformOutput', 0);
    if ischar(varargin{2})
        to_ignore = {varargin{2}};
    else
        to_ignore = varargin{2};
    end
    for i=1:length(to_ignore)
        idx = find(cellfun(@(x) strcmp(x,to_ignore{i}), dirs));
        if isempty(idx)
            fprintf(1, '%s: no such directory.\n', to_ignore{i});
        end
        jdx = setdiff(1:length(dirs), idx);
        dirs = dirs(jdx);
    end
else
    dirs = varargin;
end

dirs = dirs(:);
n = length(dirs);
for i=1:n
    if ~ exist(dirs{i},'dir')
        error([dirs{i},': no such directory.']);
    end
end

N = 100;
w = zeros(n,N);
mag = zeros(n,N);
phi = zeros(n,N);
Z = zeros(n,1);
P = zeros(n,2);
R = zeros(n,1);
V0 = zeros(n,1);
lgnd = cell(n,1);
for i=1:n
    data = load([dirs{i},'/impedance.mat']);
    [w,mag(i,:),phi(i,:)] = evalTransferFunction(data.A,data.Z*2*pi,data.P*2*pi,data.Dt,...
        0.5*data.f(1)*2*pi,2*data.f(end)*2*pi,100);
    V0(i) = data.V0;
    Z(i) = data.Z;
    P(i,:) = data.P';
    R(i) = data.A;
    lgnd{i} = sprintf('V_0 = %.1f mV', V0(i));
end

fnt = 11;
green = [0,.6,0];
blue = [0,0,.6];

cmap = lines(n);
figure;
axes('Position',[0.15,0.13,0.5,0.38],'NextPlot','Add');
set(gca,'FontSize',fnt);
hndl = zeros(n,1);
for i=1:n
    hndl(i) = plot(w/(2*pi),phi(i,:),'Color',cmap(i,:),'LineWidth',1);
end
xlabel('Frequency (Hz)');
ylabel('Phase (rad)');
axis([w([1,end])/(2*pi),ylim]);
set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8,'YTick',-4:1);

axes('Position',[0.15,0.56,0.5,0.38],'NextPlot','Add');
set(gca,'FontSize',fnt);
for i=1:n
    hndl(i) = plot(w/(2*pi),mag(i,:),'Color',cmap(i,:),'LineWidth',1);
end
h = legend(hndl,lgnd,'Location','NorthEast');
set(h, 'Box', 'Off');
ylabel('Impedance (MOhm)');
axis([w([1,end])/(2*pi),ylim]);
set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8);

axes('Position',[0.75,0.56,0.2,0.38],'NextPlot','Add');
set(gca,'FontSize',fnt);
plot(V0,R,'ko','MarkerFaceColor','w','LineWidth',2);
axis([min(V0)-2,max(V0)+2,floor(min(R))-5,ceil(max(R))+5]);
set(gca,'TickDir','Out','LineWidth',0.8);
ylabel('Input resistance (MOhm)');

axes('Position',[0.75,0.13,0.2,0.38],'NextPlot','Add');
set(gca,'FontSize',fnt);
plot(V0,Z,'bo','MarkerFaceColor','w','LineWidth',2);
plot(V0,P(:,1),'r^','MarkerFaceColor','w','LineWidth',2);
plot(V0,P(:,2),'kv','MarkerFaceColor','w','LineWidth',2);
set(gca,'TickDir','Out','LineWidth',0.8);
xlabel('V_0 (mV)')
ylabel('Frequency (Hz)');
axis([min(V0)-2,max(V0)+2,floor(min(Z))-1,ceil(max(max(P)))+1]);

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,5],'PaperSize',[8,5]);
print('-dpdf','impedances.pdf');
