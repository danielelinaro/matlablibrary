function x = hhbetah(v)
x = 1.0 ./ (exp(-(v+35.)/10.) + 1.);
