function updateInDatabase(db, cell_id, columns, values, experimenter_id)
% updateInDatabase updates an entry into the database.
% 
% updateInDatabase(db, table, columns, values)
% 
% Parameters:
%       db - a string containing the name of the database.
%    table - a string containing the name of the table where the search will
%            be performed.
%  columns - a cell array with the names of the columns to be inserted.
%   values - a cell array with the corresponding values.
% 
% Example:
% 
% updateInDatabase('electrophysiology','experimenters',{'name','surname'},{'Daniele','Linaro'});
% 
% See also findInDatabase, addToDatabase.
%

% Author: Daniele Linaro - January 2015

isint = @(x) abs(x-floor(x)) < 1e-20;

if ~ exist('experimenter','var')
    experimenter = struct('name','Daniele','surname','Linaro');
end

if ~ iscell(columns)
    columns = {columns};
end
if ~ iscell(values)
    values = {values};
end

if length(columns) ~= length(values)
    error('Columns and values must have the same length.');
end
n = length(columns);

try
    hndl = mysql('open','localhost','daniele','ilenia');
catch err
    error('Unable to connect to the database [%s].', err.identifier);
end

foo = mysql(sprintf('use %s', db));

query = 'UPDATE neurons SET ';

for k=1:n
    query = [query, columns{k}, '='];
    if ischar(values{k})
        query = [query, '''', values{k}, ''''];
    elseif isfloat(values{k})
        if isint(values{k})
            query = [query, num2str(values{k})];
        else
            query = [query, num2str(values{k},'%.10f')];
        end
    else
        query = [query, num2str(values{k})];
    end
    if k < n
        query = [query, ', '];
    end
end
query = sprintf('%s WHERE id = ''%s'' AND experimenter_id = %d;', ...
                query, cell_id, experimenter_id);
query = strrep(query,'=,','=NULL,');
query = strrep(query,'NaN','NULL');
try
    foo = mysql(query);
catch
    keyboard
end

mysql('close');
