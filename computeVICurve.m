function [I,V,R,V0,sag,sag_ratio,rebound,tau] = computeVICurve(t,Vm,stim,Irange,Vrange)
% computeVICurve analyses data from steps injections to compute a V-I
% curve.
% 
% [I,V,R,V0,sag,sag_ratio,rebound] = computeVIcurve(t,Vm,stim,Irange,Vrange)
% 
% Parameters:
%     t - (1xN) time vector.
%    Vm - (MxN) voltage matrix, where each row contains a measurement.
%  stim - (Mx1) cell array containing the stimulus matrixes.
% 
% Returns:
%        I - the values of injected current.
%        V - the corresponding mean voltage at steady state.
%        R - input resistance (in MOhm), as a linear fit to the (I,V) points.
%       V0 - resting membrane potential, the point (0,V0) on the (I,V) curve.
%      sag - the overshoot of membrane potential when applying a current.
%  rebound - number of rebound spikes present after the end of the stimulus.
%      tau - the time constant of the cell.
% 
% See also computeVICurveFromFiles.
% 

% Author - Daniele Linaro

if ~ exist('Vrange','var') || isempty(Vrange)
    Vrange = [-1000,1000];
end

if ~ exist('Irange','var') || isempty(Irange)
    Irange = [-10000,10000];
end

N = size(Vm,1);
I = zeros(N,1);
V = zeros(N,1);
sag = zeros(N,1);
sag_ratio = nan(N,1);
rebound = zeros(N,1);
ok = zeros(N,1);
tp = extractAPPeak(t,Vm);

for k=1:N
    t0 = sum(stim{k}(1:end-2,1));
    tstim = stim{k}(end-1,1);
    I(k) = stim{k}(end-1,3);
    V(k) = mean(Vm(k,t > t0+tstim-0.1 & t < t0+tstim));
    V0 = mean(Vm(k,t>t0-0.2 & t<t0));
    if max(Vm(k,t>t0 & t<t0+tstim)) < -35
        ok(k) = 1;
        ss_dV = V0 - V(k);
        if I(k) < 0
            peak_dV = V0 - min(Vm(k,t > t0 & t <= t0+tstim));
            sag(k) = V(k) - min(Vm(k,t > t0 & t <= t0+tstim));
            sag_ratio(k) = ss_dV / peak_dV;
            if sag_ratio(k) < 0
                sag_ratio(k) = nan;
            end
        elseif I(k) > 0
            peak_dV = V0 - max(Vm(k,t > t0 & t <= t0+tstim));
            sag(k) = max(Vm(k,t > t0 & t <= t0+tstim)) - V(k);
            sag_ratio(k) = ss_dV / peak_dV;
        end
    end
    if ~ isempty(tp{k})
        rebound(k) = length(find(tp{k} > t0+tstim & tp{k} < t0+tstim+0.3));
    end
end
[I,idx] = sort(I);
Vm = Vm(idx,:);
V = V(idx);
sag = sag(idx);
sag_ratio = sag_ratio(idx);
rebound = rebound(idx);
ok = ok(idx);
ok(V < Vrange(1) | V > Vrange(2)) = 0;
ok(I < Irange(1) | I > Irange(2)) = 0;
warning('off','MATLAB:polyfit:RepeatedPointsOrRescale');
p = polyfit(I(ok==1)*1e-12,V(ok==1,1)*1e-3,1);
warning('on','MATLAB:polyfit:RepeatedPointsOrRescale');
model = fit(I(ok==1), V(ok==1,1), fittype('a*x+b'), 'StartPoint', [p(1)*1e-9,p(2)]);
R = struct('value', model.a * 1e3);    % [MOhm]
V0 = struct('value', model.b);         % [mV]
try
    ci = confint(model);
    R.confidence = ci(:,1) * 1e3;
    V0.confidence = ci(:,2);
catch
    R.confidence = R.value + [0,0];
    V0.confidence = V0.value + [0,0];
end

idx = find(I <= max(I(I<0)));
tend = cellfun(@(x) sum(x(1:end-1,1)), stim);
taus = zeros(length(idx),2);
srate = 1/diff(t(1:2));
rmse = zeros(length(idx),1);
for i=1:length(idx)
    out = fitExp(Vm(idx(i),t>tend(i)), srate, 2);
    taus(i,:) = sort([out.F.b0, out.F.b1]);

    T = t(t>tend(i))-tend(i);
    x = Vm(idx(i),t>tend(i))';
    y = out.F(T)+min(Vm(idx(i),t>tend(i)));
    rmse(i) = sqrt(sum((x-y).^2))/diff(y([1,end]));
    
%     figure;
%     hold on;
%     plot(T,Vm(idx(i),t>tend(i)),'k');
%     plot(T,out.F(T)+min(Vm(idx(i),t>tend(i))),'r');
%     title(sprintf('RMSE = %g',rmse(i)));
end
tau = mean(taus(rmse<5,:),1);
