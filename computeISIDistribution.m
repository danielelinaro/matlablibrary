function [edges,cnt] = computeISIDistribution(spikes,isibin,stimtime)
% [edges,cnt] = computeISIDistribution(spikes,isibin,stimtime)

if ~ iscell(spikes)
    spikes = {spikes};
end

ntrials = length(spikes);
isi = cell(ntrials,1);
isimin = 1e6;
isimax = -1e6;
for k=1:ntrials
    spikes{k} = spikes{k}(:)';
    ndx = find(spikes{k} >= stimtime(1) & spikes{k} <= stimtime(2));
    isi{k} = diff(spikes{k}(ndx));
    tmp = min(isi{k});
    if tmp < isimin
        isimin = tmp;
    end
    tmp = max(isi{k});
    if tmp > isimax
        isimax = tmp;
    end
end

edges = isimin : isibin : isimax;

cnt = zeros(size(edges));

for k=1:ntrials
    cnt = cnt + histc(isi{k},edges);
end
cnt = cnt / ntrials;

