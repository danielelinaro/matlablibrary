function [tahp,vahp] = extractAPAHP(T,V,max_ahp_dur,thresh,tp,tth,tadp)
% EXTRACTAPAHP finds the value of the AHP.
% 
% [tahp,vahp] = extractAPAHP(T,V,max_ahp_dur,thresh,tp,tth,tadp)
% 
% Parameters:
%            T - time vector, with size (1xN) where N is the number of samples.
%            V - voltage trace(s), with size (MxN), where M is the number of 
%                traces and N is the number of samples.
%  max_ahp_dur - maximal duration of the AHP, from the spike peak (in seconds, default 5e-3).
%       thresh - threshold for spike detection: it can either be an (Mx1) array or a scalar.
% 
% Return values:
%  tahp - a cell array [of size (Mx1)] with the times of the AHP voltages.
%  vahp - a cell array [of size (Mx1)] with the AHP voltages.
%

% Daniele Linaro - May 2013

if ~ exist('max_ahp_dur','var') || isempty(max_ahp_dur)
    pars = defaultParameters(loadCellType());
    max_ahp_dur = 5e-3;
end
if ~ exist('thresh','var')
    thresh = [];
end
if ~ exist('tth','var') || isempty(tth)
    tth = extractAPThreshold(T,V,thresh);
end
if ~ exist('tp','var') || isempty(tp)
    tp = extractAPPeak(T,V,thresh);
end
if ~ exist('tadp','var') || isempty(tadp)
    got_adp_times = 0;
else
    got_adp_times = 1;
end 

nexp = size(V,1);
tahp = cell(nexp,1);
vahp = cell(nexp,1);
nspks = cellfun(@(x)length(x), tp);

if ~ all(cellfun(@(x) length(x), tth) == cellfun(@(x) length(x), tp))
    keyboard
end

for ii=1:nexp

    if nspks(ii) == 0
        continue;
    end
    
    tahp{ii} = zeros(1,nspks(ii));
    vahp{ii} = zeros(1,nspks(ii));

    for jj=1:nspks(ii)-1
        stop = min(tth{ii}(jj+1)-tp{ii}(jj),max_ahp_dur);
        if got_adp_times
            stop = min(stop,tadp{ii}(jj)-tp{ii}(jj));
        end
        idx = find(T > tp{ii}(jj) & T < tp{ii}(jj)+stop);
        try
        [vahp{ii}(jj),k] = min(V(ii,idx));
        catch
            keyboard
        end
        tahp{ii}(jj) = T(idx(k));
    end
    if got_adp_times
        idx = find(T > tp{ii}(end) & T < tadp{ii}(end));
    else
        idx = find(T > tp{ii}(end) & T < tp{ii}(end)+max_ahp_dur);
    end
    [vahp{ii}(end),k] = min(V(ii,idx));
    tahp{ii}(end) = T(idx(k));
end
