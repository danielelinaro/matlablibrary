function idx = findEntitiesByUnits(entities, units)
% idx = findEntitiesByUnits(entities, units)

idx = [];
n = length(entities);
for k=1:n
    if strcmp(entities(k).units,units)
        idx = [idx, k];
    end
end
