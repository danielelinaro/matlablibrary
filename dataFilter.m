function dataf = dataFilter(varargin) 
%   dataf = dataFilter(varargin)
%       Applies a filter to the data
%       Example: tmpdataf=dataFilter(tmpdata.ch1,'srate',tmpdata.srate,'BandType','low','FilterType','butter','fmax',5000);
%       for a notch filter 
%          freq=[50,100,150,200,250,300,350,400,450,500];
%          for ii=1:length(freq)
%            data=dataFilter(data,'srate',srate,'BandType','stop','fmax',freq(ii)+0.5,'fmin',freq(ii)-0.5);
%          end
%           
    p=inputParser;
    p = inputParser;
    
    p.addRequired('data'); % data vector of doubles with voltage values.
    p.addOptional('srate',25000,@isnumeric); % sampling rate (in Hz). %changed name of var from sr to srate
    p.addOptional('FilterType','butter',@(x)strcmpi(x,'ellip')...
                                    || strcmpi(x,'butter')); 
    p.addOptional('BandType','low',@(x)strcmpi(x,'bandpass') ...
                                    || strcmpi(x,'low')...
                                    || strcmpi(x,'stop')); 
    p.addOptional('fmin',300,@isnumeric);
    p.addOptional('fmax',3000,@isnumeric);
    
    p.parse(varargin{:});
    data=p.Results.data;
    srate=p.Results.srate;
    FilterType=p.Results.FilterType;
    BandType=p.Results.BandType;
    fmin=p.Results.fmin;
    fmax=p.Results.fmax;
    n=3;
switch FilterType
    case 'ellip';
        switch BandType
            case 'bandpass';
                [B,A]=ellip(2,0.1,40,[fmin,fmax]./(srate/2),'bandpass');
            otherwise 'low';
                [B,A]=ellip(2,0.1,40,fmax./(srate/2),'low');
        end
                
    case 'butter';
        switch BandType
            case 'bandpass';
                [B,A]=butter(n,[fmin,fmax]./(srate/2),'bandpass');
            case 'stop';
                [B,A]=butter(n,[fmin,fmax]./(srate/2),'stop');
            otherwise 'low';
                [B,A]=butter(n,fmax./(srate/2),'low');
        end
    otherwise
        disp('---> Filter not defined...yet..')
end
        
dataf=filtfilt(B,A,data);
%done!

