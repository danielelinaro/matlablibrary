function data = readLatencyData(filename)
% data = readLatencyData(filename)

data = struct;

if ~ exist(filename, 'file')
    disp([filename, ': no such file.']);
    return;
end

info = hdf5info(filename);

details = hdf5read(filename, '/Details');
for k=1:length(details.MemberNames)
    if isnumeric(details.Data{k})
        data.(details.MemberNames{k}) = details.Data{k};
        if strcmp(details.MemberNames{k},'dt') || ...
           strcmp(details.MemberNames{k},'delay') || ...
           strcmp(details.MemberNames{k},'dur')
            data.(details.MemberNames{k}) = data.(details.MemberNames{k}) / 1000;
        end
    else
        data.(details.MemberNames{k}) = details.Data{k}.Data;
    end
end

for k=1:2
    name = info.GroupHierarchy.Groups.Datasets(k).Name;
    [base,key] = strread(name(2:end), '%s%s', 'delimiter', '/');
    data.(key{1}) = hdf5read(filename, name);
    if strcmp(key{1},'spikes')
        data.(key{1}) = data.(key{1}) / 1000;
    end
end

T = 1/data.F1;
data.stimuli = data.delay : T : data.delay+(data.trials1-1)*T;
T = 1/data.F2;
data.stimuli = [data.stimuli, data.stimuli(end)+1/data.F1 : T : data.stimuli(end)+1/data.F1+(data.trials2-1)*T];
T = 1/data.F1;
data.stimuli = [data.stimuli, data.stimuli(end)+1/data.F2 : T : data.stimuli(end)+1/data.F2+(data.trials1-1)*T];
data.stimuli = data.stimuli(:);
