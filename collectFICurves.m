function [I,f,Im,fm,fstd,fsem,N,Iall,Fall,a,b,c] = ...
    collectFICurves(directories,subdirectories,binsize,Ilim)
% [I,f,Im,fm,fstd,fsem,N,Iall,Fall,a,b,c] = ...
%     collectFICurves(directories,subdirectories,binsize,Ilim)

if ~ exist('binsize','var')
    binsize = 50;
end

func = @mean;

ncells = length(directories);
if ischar(subdirectories)
    subdirectories = repmat({subdirectories},[ncells,1]);
end
I = [];
f = [];
a = [];
b = [];
c = [];
Iall = cell(ncells,1);
Fall = cell(ncells,1);
count = 0;
for i=1:ncells
    try
        data = load(sprintf('%s/steps/%s/fI_curve.mat', directories{i}, subdirectories{i}));
        [II,~,jdx] = unique(data.I);
        ff = zeros(size(II));
        for j=jdx'
            ff(j) = func(data.f(jdx==j));
        end
        df = diff(ff)./ff(1:end-1);
        stop = min([find(df < -0.2,1,'first'),length(ff)]);
        ff = ff(1:stop);
        II = II(1:stop);
        if any(ff>0 & ff<2)
            idx = find(ff>0 & ff<2, 1, 'last');
            rheobase = II(idx);
        elseif ff(1) == 0
            idx = find(ff == 0, 1, 'last');
            rheobase = func(II(idx:idx+1));
        elseif isfield(data.rheobase,'value')
            rheobase = data.rheobase.value;
        else
            rheobase = data.rheobase;
        end
        I = [I ; data.I - rheobase];
        f = [f ; data.f];
        if ~ isfield(data,'a')
            [data.a,data.b,data.c] = fitFICurve(data.I,data.f,rheobase);
        end
        a = [a, data.a];
        b = [b, data.b];
        c = [c, data.c];
%         Iall{i} = data.I - rheobase;
        Iall{i} = data.I;
        idx = find(Iall{i} >= 0);
        [Iall{i},~,jdx] = unique(Iall{i}(idx));
        Iall{i} = Iall{i}(:)';
        Fall{i} = zeros(size(Iall{i}));
        for j=1:max(jdx)
            Fall{i}(j) = mean(data.f(idx(jdx==j)));
        end
%         Iall{i} = unique(data.I(data.I>0));
%         Fall{i} = data.a * Iall{i} .^ data.b + data.c;
%         Iall{i} = Iall{i} - rheobase;
        count = count+1;
    catch
%         keyboard
        continue;
    end
end

[I,idx] = sort(I);
f = f(idx);

if exist('Ilim','var')
    edges = Ilim(1) : binsize : Ilim(2);
else
    edges = floor(min(I)/binsize)*binsize : binsize : ceil(max(I)/binsize)*binsize;
end
edges = edges - binsize/2;
[~,bin] = histc(I,edges);
Im = edges(1:end-1) + diff(edges(1:2))/2;
fm = zeros(1,length(edges)-1);
fstd = zeros(1,length(edges)-1);
fsem = zeros(1,length(edges)-1);
for k=1:length(edges)-1
    fm(k) = func(f(bin==k));
    if count > 1
        fstd(k) = std(f(bin==k));
        fsem(k) = std(f(bin==k)) / sqrt(count);
    end
end

N = count;
