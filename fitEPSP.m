function [tau_rise,tau_decay,amplitude,slope,delay] = fitEPSP(t,V,t0,duration,slope_window,hndl,DEBUG)
% fitEPSP fits an excitatory post-synaptic potential with a
% double-exponential function.
% 
% [tau_rise,tau_decay,amplitude,slope] = fitEPSP(t,V,t0,duration,slope_window)
% 
% Parameters:
%             t - time vector, size (1xN)
%             V - voltage matrix, size (MxN)
%            t0 - the initial time of the stimulation used to elicit the
%                 EPSP.
%      duration - the time window to use for the fit.
%  slope_window - the time window to use for the computation of the slope (in ms).
% 
% Returns:
%    tau_rise - the rise time of the EPSP.
%   tau_decay - the decay time of the EPSP.
%   amplitude - the amplitude of the EPSP.
%       slope - the initial slope of the EPSP.
%       delay - the delay before the onset of the EPSP.
% 

% Author: Daniele Linaro - March 2013

if ~ exist('slope_window','var') || isempty(slope_window)
    slope_window = 2.5;
end

if exist('hndl','var')
    do_plot = true;
end

if ~exist('DEBUG','var')
    DEBUG = false;
end

% baseline = mean(V(:, t>t0-0.05 & t<t0-0.01), 2);
baseline = mean(V(:, t>t0+duration-50e-3 & t<t0+duration), 2);
ndx = find(t>t0 & t<t0+duration);

EPSP = V(:,ndx);
[nEPSP,nSamples] = size(EPSP);
% set the baseline to zero
EPSP = EPSP - repmat(baseline, [1,nSamples]);
[amplitude,i] = max(EPSP, [], 2);
% normalise the amplitude
EPSP = EPSP ./ repmat(amplitude, [1,nSamples]);
% start EPSP time at zero
tEPSP = t(ndx) - t(ndx(1));

tau_rise = zeros(nEPSP,1);
tau_decay = zeros(nEPSP,1);
slope = zeros(nEPSP,1);
delay = zeros(nEPSP,1);

if do_plot
    set(hndl,'NextPlot','Add');
end

for k=1:nEPSP
    % initial conditions for the rise time, the
    % decay time, and the axonal propagation delay
    nchar = fprintf(1, '[%03d/%03d] ', k, nEPSP);
%     EPSPpars  = [1e-3,10e-3,1e-3];
    EPSPpars  = [tEPSP(i)/5,(duration-tEPSP(i))/10,tEPSP(i)/10];
    
    opt = optimset('TolX', 1e-12, 'TolFun', 1e-12, 'Display', 'off');
    optVal = 1e12;
    for ii=1:2                     % repeat a few times
        [x,fval] = fminsearch(@(x) EPSPCost(x,tEPSP,EPSP(k,:)), ...
            EPSPpars .* (1 + 0.1*rand(size(EPSPpars))), opt);
        if fval < optVal
            EPSPpars = x;
            optVal = fval;
        end
    end
    m = min(EPSPpars(1:2));
    M = max(EPSPpars(1:2));
    EPSPpars(1) = m;
    EPSPpars(2) = M;
    if do_plot
        if ~isnan(amplitude(k)) && amplitude(k) >= 0.5 && amplitude(k) < 10 && ...
                EPSPpars(2) > 1e-3 && EPSPpars(1) > 0.001e-3 && ...
                EPSPpars(3) < 20e-3 && (EPSPpars(2)/EPSPpars(1) >= 7 || ...
                (EPSPpars(1) > 10e-3 && EPSPpars(2)/EPSPpars(1) >= 3))
            col = 'g';
        else
            col = 'r';
        end
        if DEBUG
            figure;
            hold on;
            plot(tEPSP,EPSP(k,:)*amplitude(k),'Color',[.6,.6,.6],'LineWidth',0.5);
            plot(tEPSP,doubleexp(EPSPpars(1),EPSPpars(2),EPSPpars(3),tEPSP)*...
                amplitude(k),col,'LineWidth',2);
            axis tight;
            title(sprintf('tau_r = %.3f ms tau_d = %.1f ms d = %.1f ms', ...
                EPSPpars(1)*1e3, EPSPpars(2)*1e3, EPSPpars(3)*1e3));
            pause
            close(gcf);
        end
        plot(hndl,tEPSP,EPSP(k,:)*amplitude(k),'Color',[.6,.6,.6],'LineWidth',0.5);
        plot(hndl,tEPSP,doubleexp(EPSPpars(1),EPSPpars(2),EPSPpars(3),tEPSP)*...
            amplitude(k),col,'LineWidth',2);
    end
    tau_rise(k) = EPSPpars(1);
    tau_decay(k) = EPSPpars(2);
    delay(k) = EPSPpars(3);
    idx = find(t>t0+EPSPpars(3) & t<t0+EPSPpars(3)+slope_window*1e-3);
    p = polyfit(t(idx),V(k,idx),1); % [mV/s]
    slope(k) = p(1);
    fprintf(1, repmat('\b',1,nchar));
end
if do_plot
    xlabel(hndl,'Time (s)');
    ylabel(hndl,'Membrane voltage (mV)');
    axis(hndl,'tight');
    yl = ylim(hndl);
    set(hndl,'XLim',[0,duration],'YLim',[-2,1.1*yl(2)]);
end
