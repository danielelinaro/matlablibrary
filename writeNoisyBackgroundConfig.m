function writeNoisyBackgroundConfig(filename, mode, Rm, Vm, R_exc, interval, tau_exc, tau_inh, E_exc, E_inh)
% Writes an XML configuration file for usage with the noisy background
% protocol of the dynamic clamp.
% 
% Usage:
%   writeNoisyBackgroundConfig(filename, mode, Rm, Vm, R_exc, interval, tau_exc, tau_inh, E_exc, E_inh)
%
% Parameters:
% filename - output file name.
%     mode - either ''bio'' or ''relative''.
%       Vm - resting membrane potential (mV).
%       Rm - resting membrane resistance (MOhm).
%    R_exc - rate of excitatory input (Hz), default 7000.
% interval - the time interval at which the synaptic input should be
%            active. It must be a 2-elements array.
%  tau_exc - time constant of excitatory inputs (msec), default 5.
%  tau_inh - time constant of inhibitory inputs (msec), default 10.
%    E_exc - reversal potential of excitatory inputs (mV), default 0.
%    E_inh - reversal potential of inhibitory inputs (mV), default -80.
% 

% Author: Daniele Linaro - March 2012

if strcmp(mode,'bio') == 0 && strcmp(mode,'relative') == 0
    error('type must either be ``bio'''' or ``relative''''.');
end

fid = fopen(filename, 'w');
if fid == -1
    error(['Unable to open [', filename, '].']);
end

if ~ exist('Vm','var') || isempty(Vm)
	Vm = -57.6;
end
if ~ exist('R_exc','var')
    R_exc = 7000;
end
if ~ exist('interval','var')
    interval = [2.61,7.61];
end
if ~ exist('tau_exc','var')
    tau_exc = 5;
end
if ~ exist('tau_inh','var')
    tau_inh = 10;
end
if ~ exist('E_exc','var')
    E_exc = 0;
end
if ~ exist('E_inh','var')
    E_inh = -80;
end

ratio = computeRatesRatio(mode, Vm, Rm, tau_exc, tau_inh, E_exc, E_inh);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = computeSynapticBackgroundCoefficients(...
                                mode, Rm, R_exc, ratio, tau_exc, tau_inh);

fprintf(fid, '<dynamicclamp>\n<entities>\n');
fprintf(fid, '<ou>\n');
fprintf(fid, '<G0>%g</G0>\n', Gm_exc);
fprintf(fid, '<sigma>%g</sigma>\n', Gs_exc);
fprintf(fid, '<tau>%g</tau>\n', tau_exc*1e-3);
fprintf(fid, '<E>%g</E>\n', E_exc);
fprintf(fid, '<interval>%g,%g</interval>', interval(1), interval(2));
fprintf(fid, '</ou>\n');
fprintf(fid, '<ou>\n');
fprintf(fid, '<G0>%g</G0>\n', Gm_inh);
fprintf(fid, '<sigma>%g</sigma>\n', Gs_inh);
fprintf(fid, '<tau>%g</tau>\n', tau_inh*1e-3);
fprintf(fid, '<E>%g</E>\n', E_inh);
fprintf(fid, '<interval>%g,%g</interval>', interval(1), interval(2));
fprintf(fid, '</ou>\n');
fprintf(fid, '</entities>\n</dynamicclamp>\n');

fclose(fid);


