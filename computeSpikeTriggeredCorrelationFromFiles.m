function [stc_V,stc_I] = computeSpikeTriggeredCorrelationFromFiles(directories, window)
% [stc_V,stc_I] = computeSpikeTriggeredCorrelation(directories, window)

DEBUG = 0;

narginchk(1,2);

if ~exist('window','var')
    window = [5e-3,30e-3];
end

if ~iscell(directories) || length(directories) ~= 2
    error('directories must be a 2-element cell array');
end

if ~isnumeric(window) || length(window) ~= 2
    error('window must be a 2-element vector');
end

Ee = 0;
Ei = -80;

V = cell(2,1);
I = cell(2,1);
gampa = cell(2,1);
gampac = cell(2,1);
ggaba = cell(2,1);
ggabac = cell(2,1);

ttran = 0.1;

for i=1:2
    files = listH5Files(directories{i});
    entities = loadH5Trace(files{1});
    t0 = entities(2).metadata(1,1);
    dur = entities(2).metadata(2,1);
    [t,V{i},gampa{i},gampac{i},ggaba{i},ggabac{i}] = loadH5TracesBis(files,1,2,4,3,5);
    I{i} = (gampa{i}+gampac{i}) .* (Ee - V{i}) + (ggaba{i}+ggabac{i}) .* (Ei - V{i});
    features(i) = load([directories{i},'/spike_features.mat']);
end

[stc_V,stc_I] = computeSpikeTriggeredCorrelation(t,V,I,features,t0,ttran,dur,window);

% %%
% dt = diff(t(1:2));
% T = -window(1):dt:window(2);
% stc_V = zeros(2,length(T));
% stc_I = zeros(2,length(T));
% for ref = [1,2]
%     other = 1 + mod(ref,2);
%     X = cell(2,1);
%     Y = cell(2,1);
%     events = cell(2,1);
%     for i=1:length(features(ref).tp)
%         idx = find(features(ref).tp{i} > t0+ttran+window(1) & ...
%             features(ref).tp{i} < t0+dur-window(2));
%         tp = features(ref).tp{i}(idx);
%         tth = features(ref).tth{i}(idx);
%         tend = features(ref).tend{i}(idx);
%         if isempty(tp)
%             continue;
%         elseif isscalar(tp)
%             events{ref} = tp;
%         elseif tth(2)-tp(1) > window(2)
%             events{ref} = tp(1);
%         else
%             events{ref} = [];
%         end
%         for j=2:length(tp)-1
%             if tp(j)-tend(j-1) > window(1) && tth(j+1)-tp(j) > window(2)
%                 events{ref} = [events{ref} , tp(j)];
%             end
%         end
%         idx = find(features(other).tp{i} > t0+ttran & ...
%             features(other).tp{i} < t0+dur);
%         tp = features(other).tp{i}(idx);
%         tth = features(other).tth{i}(idx);
%         tend = features(other).tend{i}(idx);
%         idx = [];
%         for j=1:length(events{ref})
%             before = find(tp <= events{ref}(j), 1, 'last');
%             after = find(tp >= events{ref}(j), 1, 'first');
%             if (isempty(before) || isempty(after) || before ~= after) && ...
%                     (isempty(before) || events{ref}(j)-tend(before) > window(1)) && ...
%                     (isempty(after) || tth(after)-events{ref}(j) > window(2))
%                 idx = [idx ; j];
%             end
%         end
%         if ~ isempty(idx)
%             for j=1:2
%                 [~,x] = ETSA(t,V{j}(i,:),events{ref}(idx),window);
%                 X{j} = [X{j} ; x];
%                 [~,y] = ETSA(t,I{j}(i,:),events{ref}(idx),window);
%                 Y{j} = [Y{j} ; y];
%             end
%         end
%     end
% 
%     stc_V(ref,:) = diag(corr(X{1},X{2}))';
%     stc_I(ref,:) = diag(corr(Y{1},Y{2}))';
%     
%     if DEBUG
%         XX = mean(X{ref});
%         figure;
%         hold on;
%         plot(T,stc_V(ref,:),'k');
%         plot(T,(XX-min(XX))/(max(XX)-min(XX))/2,'r');
%     end
%     
% end
% 
