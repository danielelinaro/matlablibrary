function varargout = StimEdit(varargin)
% STIMEDIT M-file for StimEdit.fig
%      STIMEDIT, by itself, creates a new STIMEDIT or raises the existing
%      singleton*.
% Last Modified by GUIDE v2.5 21-Dec-2012 09:46:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @StimEdit_OpeningFcn, ...
    'gui_OutputFcn',  @StimEdit_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before StimEdit is made visible.
function StimEdit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to StimEdit (see VARARGIN)

% Choose default command line output for StimEdit
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% USER Initialization of the GUI STARTS HERE !!!
global cwd;
cwd = pwd;
initialize_gui(hObject, eventdata, handles);

% UIWAIT makes StimEdit wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = StimEdit_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function fname_edit_Callback(hObject, eventdata, handles)
global fname;
fname = get(handles.fname_edit, 'String');

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
function refresh_button_Callback(hObject, eventdata, handles)
global nlines;
set(handles.add_button, 'Visible', 'off');
if (~exist('/Users/daniele/bin/create_stimulus', 'file'))
    errordlg('Executable <create_stimulus> not found!', 'Stimulus Editor')
elseif (nlines>0)
    tableData = get(handles.uitable1,'data');
    xtcks = [];
    fp = fopen('temp.stim', 'w');
    for i=1:100,
        if (~isempty(tableData{i,1}))
            if (i>1),  fprintf(fp, '\n'); end;
            for j=1:12,
                fprintf(fp, '%s\t', tableData{i,j});
            end
            if (str2num(tableData{i,1}) > 0),
                xtcks = [xtcks, str2num(tableData{i,1})];
            end
        end
    end
    
    for k=1:length(xtcks)-1,
        xtcks(k+1) = xtcks(k+1) + xtcks(k);
    end
    xtcks = [0 xtcks];
    
    fclose(fp);
    srate = str2num(get(handles.srate, 'String'));
    [status,result] = system(sprintf('/Users/daniele/MatlabLibrary/create_stimulus/create_stimulus 1 1 %f temp.stim', srate));
%     [srate N M time data] = load_binary_mex('stimulus.dat');
    out = loadTrace('stimulus.dat');
    srate = out.srate;
    N = length(fieldnames(out)) - 1;
    M = length(out.ch1);
    time = (0:M-1) / srate;
    data = zeros(M,N);
    for k=1:length(fieldnames(out))-1
        data(:,k) = out.(sprintf('ch%d',k));
    end
    Q = plot(handles.axes1, time, data);
    set(Q, 'Color', [1 0 0], 'LineWidth', 1);
    set(handles.axes1, 'FontName', 'Arial', 'FontSize', 10);
    xlabel(handles.axes1,'time [s]', 'FontName', 'Arial', 'FontSize', 10);
    MAX = max(abs(data)); if (MAX==0), MAX = 1; end;
    set(handles.axes1, 'YLim', [-1.1 1.1]*MAX);
    set(handles.axes1, 'XGrid', 'on', 'YGrid', 'on');
    set(handles.axes1, 'XTick', xtcks);
end
set(handles.add_button, 'Visible', 'on');
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

function load_button_Callback(hObject, eventdata, handles)
global fname nlines cwd;
initialize_gui(hObject, eventdata, handles);

[filename, pathname] = uigetfile('*.stim', 'Pick a stimulus-file', cwd);
if isequal(filename,0) || isequal(pathname,0)
    ;
else
    cwd   = pathname;
    fname = fullfile(pathname, filename);
    set(handles.fname_edit, 'String', fname);
    
    tableData = cell(100, 12);
    fp = fopen(fname, 'r');
    nlines = 0;
    while (~feof(fp))
        pat = '\s+';
        s = regexp(fgetl(fp), pat, 'split');
        if (~isempty(s{1}))
            nlines = nlines + 1;
            for k=1:12, tableData{nlines, k} = s{k}; end;
        end
    end
    fclose(fp);
    set(handles.uitable1,'data',tableData);
    refresh_button_Callback(hObject, eventdata, handles);
end

function save_button_Callback(hObject, eventdata, handles)
global fname;
refresh_button_Callback(hObject, eventdata, handles);
if (exist('temp.stim', 'file'))
    copyfile('temp.stim',fname,'f');
end

function saveas_button_Callback(hObject, eventdata, handles)
global fname cwd;
[filename, pathname] = uiputfile('*.stim', 'Save as a stimulus-file', cwd);
if isequal(filename,0) || isequal(pathname,0)
    ;
else
    cwd   = pathname;
    fname = fullfile(pathname, filename);
    set(handles.fname_edit, 'String', fname);
    refresh_button_Callback(hObject, eventdata, handles);
    if (exist('temp.stim', 'file'))
        copyfile('temp.stim',fname,'f');
    end
end

function clear_button_Callback(hObject, eventdata, handles)
initialize_gui(hObject, eventdata, handles);

function update_pars(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
T  = str2num(get(handles.T, 'String'));
P1 = str2num(get(handles.p1, 'String'));
P2 = str2num(get(handles.p2, 'String'));
P3 = str2num(get(handles.p3, 'String'));
P4 = str2num(get(handles.p4, 'String'));
P5 = str2num(get(handles.p5, 'String'));
SUBCODE = 0;
OPERATOR = 0;
MYSEED  = str2num(get(handles.seed, 'String'));
FIXSEED = get(handles.fixseed, 'Value');
EXPON   = str2num(get(handles.expon, 'String'));

function add_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global nlines;
nlines = nlines + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = num2str(T);
tableData{nlines, 2} = num2str(code);
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(SUBCODE);
tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines, 12} = num2str(EXPON);
set(handles.uitable1,'data',tableData);
refresh_button_Callback(hObject, eventdata, handles);
%-----------

function subwavetype_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;

popup_sel_index = get(handles.subwavetype, 'Value');
%'DC', 'Noise (O.U.)', 'Sine', 'Square', 'Saw',
%'Chirp', 'Ramp', 'Monophasic pulse-train', 'Exponential pulse-train',
%'Biphasic pulse-train', 'Alpha function'
switch popup_sel_index
    case 1  %DC
        code = 1;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '0');
        set(handles.p3, 'String', '0');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'off');
        set(handles.p2, 'Visible', 'off');
        set(handles.p3_txt, 'Visible', 'off');
        set(handles.p3, 'Visible', 'off');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 2  %Noise
        code = 2;
        set(handles.p1_txt, 'String', 'Mean');
        set(handles.p2_txt, 'String', 'Std dev');
        set(handles.p3_txt, 'String', 'Time const');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '0');
        set(handles.p3, 'String', '1');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'on');
        set(handles.seed, 'Visible', 'on');
    case 3 % Sine
        code = 3;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Phase [rad]');
        set(handles.p4_txt, 'String', 'Offset');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '0');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'on');
        set(handles.p4, 'Visible', 'on');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 4 % Square
        code = 4;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Duty cycle [%]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '50');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 5  % Saw
        code = 5;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Duty cycle [%]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '100');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 6 % Chirp
        code = 6;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Initial Freq. [Hz]');
        set(handles.p3_txt, 'String', 'Final Freq. [Hz]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '1');
        set(handles.p3, 'String', '100');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 7 % Ramp
        code = 7;
        set(handles.p1_txt, 'String', 'Final amplitude');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '0');
        set(handles.p3, 'String', '0');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'off');
        set(handles.p2, 'Visible', 'off');
        set(handles.p3_txt, 'Visible', 'off');
        set(handles.p3, 'Visible', 'off');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'off');
        set(handles.seed, 'Visible', 'off');
    case 8 % Monophasic
        code = 8;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Pulse width [ms]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '2');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'on');
        set(handles.seed, 'Visible', 'on');
    case 9 % Exponential
        code = 9;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Exp decay [ms]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '2');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'on');
        set(handles.seed, 'Visible', 'on');
    case 10 % Biphasic
        code = 10;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Frequency [Hz]');
        set(handles.p3_txt, 'String', 'Pulse width [ms]');
        set(handles.p1, 'String', '0');
        set(handles.p2, 'String', '5');
        set(handles.p3, 'String', '2');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'off');
        set(handles.p4, 'Visible', 'off');
        set(handles.p5_txt, 'Visible', 'off');
        set(handles.p5, 'Visible', 'off');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'on');
        set(handles.seed, 'Visible', 'on');
    case 11 % Alpha function
        code = 12;
        set(handles.p1_txt, 'String', 'Amplitude');
        set(handles.p2_txt, 'String', 'Rise time [ms]');
        set(handles.p3_txt, 'String', 'Decay time [ms]');
        set(handles.p4_txt, 'String', 'Delay [s]');
        set(handles.p5_txt, 'String', 'Offset');
        set(handles.p1, 'String', '1');
        set(handles.p2, 'String', '1');
        set(handles.p3, 'String', '5');
        set(handles.p4, 'String', '0');
        set(handles.p5, 'String', '0');
        
        set(handles.p1_txt, 'Visible', 'on');
        set(handles.p1, 'Visible', 'on');
        set(handles.p2_txt, 'Visible', 'on');
        set(handles.p2, 'Visible', 'on');
        set(handles.p3_txt, 'Visible', 'on');
        set(handles.p3, 'Visible', 'on');
        set(handles.p4_txt, 'Visible', 'on');
        set(handles.p4, 'Visible', 'on');
        set(handles.p5_txt, 'Visible', 'on');
        set(handles.p5, 'Visible', 'on');
        set(handles.fixseed, 'Value', 0);
        set(handles.fixseed, 'Visible', 'on');
        set(handles.seed, 'Visible', 'on');
end

function [srate N M time data] = load_binary(fname)
%
% [srate N M time data] = load_binary(file_name)
%
% srate [Hz] : sampling rate
% N          : number of simultaneous waves (channels)
% M          : number of samples per wave (channel)
%
% time   [s] : vector M x 1, containing the time axis
% data       : vector M x N, containing the data
%
%
%
% Nov 4th 2009 - Michele Giugliano, PhD - mgiugliano@gmail.com
%
srate = -1;                     % default value
N     = -1;                     % default value
M     = -1;                     % default value
time  = [];                     % default value
data  = [];                     % default value
if ~exist(fname, 'file')
    disp(sprintf('load_binary() - error: file <%s> not found!', fname));
    return;
end

fp = fopen(fname, 'r');        % file is open for read-only

srate = fread(fp, 1, 'double');% sampling rate [Hz]
dt    = 1./srate;              % sampling interval [s]
N     = fread(fp, 1, 'ulong'); % number of simultaneous waves (channels)
M     = fread(fp, 1, 'ulong'); % number of samples per wave (channel)
maxT  = (M-1)*dt;              % largest sampling time [s]

time  = 0:dt:maxT;
data  = zeros(M,N);            % initialization, improves performances
for i=1:N,
    data(:,i) = fread(fp, M, 'double');
end


fclose(fp);                     % file is released

function initialize_gui(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global fname nlines composite_count;
cla(handles.axes1);
nlines = 0;
composite_count = 0;
fname = fullfile(pwd, 'default.stim');
set(handles.fname_edit, 'String', fname);
set(handles.subwavetype, 'Value', 1);
subwavetype_Callback(hObject, eventdata, handles);

set(handles.uitable1, 'data', cell(100, 12));
set(handles.add_button, 'Visible', 'on');
set(handles.eq_button, 'Visible', 'off');
set(handles.subwavetype, 'Value', 1);
set(handles.T, 'String', '1.5');
set(handles.p1, 'String', '0');
set(handles.p2, 'String', '0');
set(handles.p3, 'String', '0');
set(handles.p4, 'String', '0');
set(handles.p5, 'String', '0');
SUBCODE = 0;
OPERATOR = 0;
set(handles.seed, 'String', '3532765');
set(handles.fixseed, 'Value', 0);
set(handles.expon, 'String', '1');

set(handles.p1_txt, 'String', 'Amplitude');
set(handles.p1_txt, 'Visible', 'on');
set(handles.p1, 'Visible', 'on');
set(handles.p2_txt, 'Visible', 'off');
set(handles.p2, 'Visible', 'off');
set(handles.p3_txt, 'Visible', 'off');
set(handles.p3, 'Visible', 'off');
set(handles.p4_txt, 'Visible', 'off');
set(handles.p4, 'Visible', 'off');
set(handles.p5_txt, 'Visible', 'off');
set(handles.p5, 'Visible', 'off');
if (exist('./temp.stim', 'file')), delete('./temp.stim'); end;
if (exist('./stimulus.dat', 'file')), delete('./stimulus.dat'); end;
update_pars(hObject, eventdata, handles);

function plus_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global nlines;
global composite_count;
composite_count = composite_count + 1;
nlines = nlines + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = num2str(T);
tableData{nlines, 2} = '?';
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(code);
%tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines+1, 11} = '1';
tableData{nlines, 12} = num2str(EXPON);
set(handles.uitable1,'data',tableData);
set(handles.add_button, 'Visible', 'off');
set(handles.eq_button, 'Visible', 'on');

function minus_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global nlines;
global composite_count;
composite_count = composite_count + 1;
nlines = nlines + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = num2str(T);
tableData{nlines, 2} = '?';
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(code);
%tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines+1, 11} = '3';
tableData{nlines, 12} = num2str(EXPON);
set(handles.uitable1,'data',tableData);
set(handles.add_button, 'Visible', 'off');
set(handles.eq_button, 'Visible', 'on');


function times_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global nlines;
global composite_count;
composite_count = composite_count + 1;
nlines = nlines + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = num2str(T);
tableData{nlines, 2} = '?';
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(code);
%tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines+1, 11} = '2';
tableData{nlines, 12} = num2str(EXPON);
set(handles.uitable1,'data',tableData);
set(handles.add_button, 'Visible', 'off');
set(handles.eq_button, 'Visible', 'on');


function div_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global nlines;
global composite_count;
composite_count = composite_count + 1;
nlines = nlines + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = num2str(T);
tableData{nlines, 2} = '?';
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(code);
%tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines+1, 11} = '4';
tableData{nlines, 12} = num2str(EXPON);
set(handles.uitable1,'data',tableData);
set(handles.add_button, 'Visible', 'off');
set(handles.eq_button, 'Visible', 'on');


function eq_button_Callback(hObject, eventdata, handles)
global code T P1 P2 P3 P4 P5 FIXSEED MYSEED SUBCODE OPERATOR EXPON;
global composite_count;
global nlines;
nlines = nlines + 1;
composite_count = composite_count + 1;
update_pars(hObject, eventdata, handles);
tableData = get(handles.uitable1,'data');
tableData{nlines, 1} = '0';
tableData{nlines, 2} = num2str(-composite_count);
tableData{nlines, 3} = num2str(P1);
tableData{nlines, 4} = num2str(P2);
tableData{nlines, 5} = num2str(P3);
tableData{nlines, 6} = num2str(P4);
tableData{nlines, 7} = num2str(P5);
tableData{nlines, 8} = num2str(FIXSEED);
tableData{nlines, 9} = num2str(MYSEED);
tableData{nlines, 10} = num2str(code);
%tableData{nlines, 11} = num2str(OPERATOR);
tableData{nlines, 12} = num2str(EXPON);

for k=1:composite_count
    tableData{nlines-k +1, 2} = num2str(-composite_count);
end
for k=1:(composite_count-1)
    tableData{nlines-k + 1, 1} = num2str(0);
end
tableData{nlines-composite_count + 1, 11} = num2str(0);
composite_count = 0;
set(handles.uitable1,'data',tableData);
refresh_button_Callback(hObject, eventdata, handles)
set(handles.add_button, 'Visible', 'on');
set(handles.eq_button, 'Visible', 'off');

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

function T_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p1_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p2_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p3_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p4_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p5_txt_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function T_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p4_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function p5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function seed_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function subwavetype_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {'DC', 'Noise (O.U.)', 'Sine', 'Square', 'Saw', 'Chirp', ...
    'Ramp', 'Monophasic pulse-train', 'Exponential pulse-train', 'Biphasic pulse-train', ...
    'Alpha function'});
function expon_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function fname_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function srate_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function T_Callback(hObject, eventdata, handles)
function p1_Callback(hObject, eventdata, handles)
function p2_Callback(hObject, eventdata, handles)
function p3_Callback(hObject, eventdata, handles)
function p4_Callback(hObject, eventdata, handles)
function p5_Callback(hObject, eventdata, handles)
function seed_Callback(hObject, eventdata, handles)
function fixseed_Callback(hObject, eventdata, handles)
function expon_Callback(hObject, eventdata, handles)
function srate_Callback(hObject, eventdata, handles)
