function preprocessCorrelationsDirectory(threshold, ttran, binWidth, windowSize)
% preprocessCorrelationsDirectory(threshold, ttran, binWidth, windowSize)

if ~ exist('threshold','var')
    threshold = -20;
end

if ~ exist('ttran','var')
    ttran = 0.1;
end

if ~ exist('binWidth','var')
    binWidth = 1.2e-3;
end

if ~ exist('windowSize','var')
    windowSize = 40e-3;
end

if isempty(listH5Files)
    dirs = listDirectories;
else
    dirs = {'.'};
end
ndirs = length(dirs);

SPIKE_FEATURES_FILE = 'spike_features.mat';

y = cell(ndirs,1);
edges = cell(ndirs,1);
n = cell(ndirs,1);
spiketimes = cell(ndirs,1);
isi = cell(ndirs,1);
rates = cell(ndirs,1);
baseDir = pwd;
Ee = 0;
Ei = -80;
cclamp = 0;

for i=1:ndirs

    cd(dirs{i});

    if ~ exist(SPIKE_FEATURES_FILE, 'file')
        code = input('Enter cell type:\n   [1] Pyramidal\n   [2] FS interneuron\n ');
        switch code
            case 1
                extractSpikeFeaturesFromFiles('pyramidal');
            case 2
                extractSpikeFeaturesFromFiles('fs');
            otherwise
                error(sprintf('Unknown cell code: %g.',code));
        end
    end
    
    data = load(SPIKE_FEATURES_FILE, 'tp');

    files = listH5Files;
    entities = loadH5Trace(files{1});
    try
        t0 = entities(2).metadata(1);
        dur = entities(2).metadata(2);
        [t,V] = loadH5TracesBis(files,1);
    catch
        [t,Vr,I,info] = loadH5Traces(files);
        t0 = info{1}(1);
        dur = info{1}(2);
        kernels = dir('*_kernel.dat');
        nk = length(kernels);
        V = zeros(size(Vr));
        for j=1:nk
            idx = (j-1)*size(V,1)/nk+1:j*size(V,1)/nk;
            Ke = load(kernels(j).name);
            V(idx,:) = AECoffline(Vr(idx,:),I(idx,:),Ke);
        end
        cclamp = 1;
    end
    
    idx = find(t >= t0+ttran & t <= t0+dur);
    idx = idx(2:end-1);
    if isempty(idx)
        keyboard
    end
    
    %%%% extract spike times, binarize the voltage trace and compute spike
    %%%% counts
    
    VV = V(:,idx)';
    tp = extractAPPeak((0:numel(VV)-1) * diff(t(1:2)),VV(:)',threshold);
    clear VV;
    isi{i} = diff(tp{1});
    spiketimes{i} = cellfun(@(x) x(x>=t0+ttran)-(t0+ttran), data.tp, 'UniformOutput', 0);
    rates{i} = cellfun(@(x) length(x)/(dur-ttran), spiketimes{i});
    [y{i},edges{i},n{i}] = binarizeSpikeTrain(spiketimes{i},binWidth,[0,dur-ttran],windowSize);

    cd(baseDir);
end

cv = mean(cellfun(@(x) std(x)/mean(x), isi));
nu = mean(cellfun(@(x) mean(x), rates));

L = dur-ttran;
T = windowSize;

%%%% better safe than sorry
if exist('correlations.mat','file')
    eval(sprintf('!cp correlations.mat correlations_%s.mat',datestr(now,'yyyymmddHHMMSS')));
end

if cclamp
    save('correlations.mat','y','edges','n','spiketimes','isi','cv',...
        'rates','nu','t0','dur','ttran','binWidth','windowSize','L','T');
else
    save('correlations.mat','y','edges','n','spiketimes','isi','cv',...
        'rates','nu','t0','dur','ttran','binWidth','windowSize','L','T','Ee','Ei');
end
