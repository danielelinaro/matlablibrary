function [Ithresh,Vthresh] = analyseRamp(t,V,I,isi_thresh,ap_thresh)
% [Ithresh,Vthresh] = analyseRamp(t,V,I,isi_thresh,ap_thresh)

% Author: Daniele Linaro - September 2012

if ~ exist('ap_thresh','var')
    ap_thresh = [];
end

if ~ exist('isi_thresh','var') || isempty(isi_thresh)
    isi_thresh = 0.5;
end

if size(I,1) == 1
    I = repmat(I,[size(V,1),1]);
end

tthresh = extractAPThreshold(t,V,ap_thresh);
tfirst = zeros(length(tthresh),1);
for i=1:length(tthresh)
    j = 1;
    while j+1 <= length(tthresh{i}) && tthresh{i}(j+1) - tthresh{i}(j) > isi_thresh
        fprintf(1, 'Discarding spike #%d, ISI greater than %g sec.\n', j, isi_thresh);
        j = j+1;
    end
    try
        tfirst(i) = tthresh{i}(j);
    catch
        tfirst(i) = nan;
    end
end
idx = arrayfun(@(x) find(t == x), tfirst, 'UniformOutput', 0);
Vthresh = nan(length(idx),1);
Ithresh = nan(length(idx),1);
for k=1:length(idx)
    if ~ isempty(idx{k})
        Vthresh(k) = V(k,idx{k});
        Ithresh(k) = I(k,idx{k});
    end
end
