function accel = extractAPAcceleration(dt,V,dVdtThresh)
% accel = extractAPAcceleration(dt,V,dVdtThresh)
if ~ exist('dVdtThresh','var') || isempty(dVdtThresh)
    dVdtThresh = 50;
end
N = size(V,1);
accel = cell(N,1);
for i=1:N
    v = V(i,2:end-1);
    dvdt = (v(3:end)-v(1:end-2))/(2*dt*1e3);
    v = v(2:end-1);
    dV = [dvdt(1:end-1); dvdt(2:end)];
    idx = find(dV(1,:) < dVdtThresh & dV(2,:) > dVdtThresh);
    accel{i} = zeros(size(idx));
    for j=1:length(idx)
        try
        accel{i}(j) = 1./polyval(polyder(polyfit(dvdt(idx(j)-1:idx(j)+2),...
            v(idx(j)-1:idx(j)+2),2)),dVdtThresh);
        catch
            keyboard
        end
    end
end