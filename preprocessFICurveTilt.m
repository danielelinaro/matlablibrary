function preprocessFICurveTilt(varargin)

if nargin == 0
    dirs = arrayfun(@(x) x.name, dir('0*'), 'UniformOutput', 0);
elseif nargin == 1
    if ischar(varargin{1})
        dirs = {varargin{1}};
    else
        dirs = varargin{1};
    end
    if isstruct(dirs(1))
        dirs = arrayfun(@(x) x.name, dirs);
    end
elseif nargin == 2 && strcmp(varargin{1},'ignore')
    dirs = arrayfun(@(x) x.name, dir('0*'), 'UniformOutput', 0);
    if ischar(varargin{2})
        to_ignore = {varargin{2}};
    else
        to_ignore = varargin{2};
    end
    for i=1:length(to_ignore)
        idx = find(cellfun(@(x) strcmp(x,to_ignore{i}), dirs));
        if isempty(idx)
            fprintf(1, '%s: no such directory.\n', to_ignore{i});
        end
        jdx = setdiff(1:length(dirs), idx);
        dirs = dirs(jdx);
    end
else
    dirs = varargin;
end

dirs = dirs(:);
n = length(dirs);
for i=1:n
    if ~ exist(dirs{i},'dir')
        error([dirs{i},': no such directory.']);
    end
end

I = cell(n,1);
Iu = cell(n,1);
f = cell(n,1);
fm = cell(n,1);
fs = cell(n,1);
p = zeros(n,2);
gain = zeros(n,3); % actual value and confidence interval
rheobase = zeros(n,3); % actual value and confidence interval
for i=1:n
    try
        data = load([dirs{i},'/fI_curve.mat'],'I','f','p','gain','rheobase');
    catch
        error(['No fI_curve.mat file in ', dirs{i}, '.']);
    end
    I{i} = data.I;
    f{i} = data.f;
    [Iu{i},~,jdx] = unique(I{i});
    fm{i} = zeros(size(Iu{i}));
    fs{i} = zeros(size(Iu{i}));
    for j=1:length(Iu{i})
        fm{i}(j) = mean(f{i}(jdx == j));
        fs{i}(j) = std(f{i}(jdx == j)) / sqrt(sum(jdx == j));
    end
    p(i,:) = data.p;
    gain(i,:) = [data.gain.value, data.gain.confidence'];
    rheobase(i,:) = [data.rheobase.value, data.rheobase.confidence'];
end

save('fI_curve_tilt.mat','dirs','I','Iu','f','fm','fs','p','gain','rheobase');

cmap = [0,0,0 ; 1,.5,0 ; 0,1,.5 ; 1,0,.5];
figure;
hold on;
for i=1:n
    errorbar(Iu{i},fm{i},fs{i},'ks','MarkerFaceColor',cmap(i,:));
    x = [-p(i,2)/p(i,1),Iu{i}(end)];
    y = polyval(p(i,:),x);
    plot(x,y,'Color',cmap(i,:),'LineWidth',2);
    text(1.02*x(2),1.02*y(2),sprintf('Gain = %.0f Hz/nA',p(i,1)*1e3),...
        'Color',cmap(i,:),'FontName','Arial','FontSize',10,...
        'HorizontalAlignment','Left');
end
Imax = ceil(max(cellfun(@(x) max(x), Iu))/100)*100 + 100;
Imin = floor(min(cellfun(@(x) min(x), Iu))/100)*100 - 100;
fmax = round(max(cellfun(@(x) max(x), fm))/5)*5 + 5;
axis([Imin,Imax,0,fmax]);

set(gca,'TickDir','Out','XTick',0:100:Imax,'YTick',0:10:fmax,'LineWidth',0.8);
xlabel('Current (pA)');
ylabel('Firing rate (spikes/s)');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,6]);
print('-depsc2','fI_curve_tilt.eps');
