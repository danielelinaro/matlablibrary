function out = doubleexp(t1, t2, d, t)
%
% out = doubleexp(t1, t2, d, t)
%
% t1 [s] : rising time constant
% t2 [s] : decaying time constant
% d  [s] : delay
% t  [s] : time axis
%
% out --> double exponential, with max amplitude == 1,
% regardless of the combination {t1, t2}..
% 
% Sep 2007 - Michele Giugliano (mgiugliano@gmail.com)
%

% Let's consider the function f(t) = (exp(-t/t1)-exp(-t/t2))/(t1-t2)
% I compute the time 'tt' at which the first derivative of f(t) is zero
tt = (t1*t2/(t2-t1)) * log(t2/t1);          

% I then compute f(tt)
aa = (exp(-tt/t1)-exp(-tt/t2))/(t1-t2);

% I appropriately prepare a normalization prefactor.. (so it has unitary
% peak amplitude).
A  = 1. / (aa * (t1 - t2));

% Then, the actual function as an output
out =  A * ( exp(-(t-d)/t1) - exp(-(t-d)/t2) ) .* (t >= d);
