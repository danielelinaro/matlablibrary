function fileList = findFiles(dirName, endsWith)
% fileList = findFiles(dirName, endsWith)

if ~ exist('dirName','var')
    dirName = '.';
elseif ~ exist(dirName,'dir')
    error('%s: no such directory.', dirName);
end

if ~ exist('endsWith','var')
    endsWith = '.h5';
end

dirData = dir(dirName);      %# Get the data for the current directory
dirIndex = [dirData.isdir];  %# Find the index for directories
fileList = {dirData(~dirIndex).name}';  %'# Get a list of the files
fileList = fileList(strcmp(cellfun(@(x) x(max(length(x)-length(endsWith)+1,1):end), ...
    fileList, 'UniformOutput', false), endsWith));
if ~isempty(fileList)
    fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
        fileList,'UniformOutput',false);
end
subDirs = {dirData(dirIndex).name};  %# Get a list of the subdirectories
validIndex = ~ismember(subDirs,{'.','..'});  %# Find index of subdirectories
                                             %#   that are not '.' or '..'
for iDir = find(validIndex)                  %# Loop over valid subdirectories
    nextDir = fullfile(dirName,subDirs{iDir});    %# Get the subdirectory path
    if isempty(strfind(nextDir,'.lcg'))
        fileList = [fileList; findFiles(nextDir,endsWith)];  %# Recursively call getAllFiles
    end
end
