function preprocessCorrelationsExperiment(threshold)
% preprocessCorrelationsExperiment(threshold)

if ~ exist('threshold','var')
    threshold = -20;
end

PREPROCESSED_DATA_FILE = 'correlations.mat';

dirs = listDirectories('.','0*');
ndirs = length(dirs);

cmap = lines(ndirs);
figure;
hold on;

baseDir = pwd;

for i=1:ndirs
    
    cd(dirs{i});
    
    if ~ exist('DO_NOT_USE','file')
        if ~ exist(PREPROCESSED_DATA_FILE, 'file')
            fprintf(1, 'Preprocessing data in [%s]... ', dirs{i});
            preprocessCorrelationsDirectory(threshold);
            fprintf(1, 'done.\n');
        end
        
        data = load(PREPROCESSED_DATA_FILE,'rates','nu');
        nreps = length(data.rates);
        
        for j=1:nreps
            c = min(cmap(i,:) + [.5,.5,.5], [1,1,1]);
            plot(data.rates{j},'o','Color',c,'MarkerFaceColor',c,'MarkerSize',4);
        end
        plot([0,100],data.nu+[0,0],'Color',cmap(i,:),'LineWidth',3);
    end
    
    cd(baseDir);
end
xlabel('Trial #');
ylabel('Firing rate (AP/sec)');
axis tight;
yl = ylim;
axis([0,100,yl]);
set(gca,'XTick',0:50:100,'YTick',0:10:yl(2));

set(gcf,'Color','w','PaperPosition',[0,0,6,5],'PaperUnits','Inch');
print('-depsc2','correlations.eps');
