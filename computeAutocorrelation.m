function [c,lags] = computeAutocorrelation(x,maxlag,lagstep)
% COMPUTEAUTOCORRELATION Estimates the autocorrelation function.
% 
% [c,lags] = computeAutocorrelation(x,maxlag,lagstep)
% 
% Parameters:
%        x - the time series
%   maxlag - the maximum lag
%  lagstep - the step among lags.
% 
% Returns:
%      c - the estimated autocorrelation
%   lags - the corresponding lags
% 

% Author: Daniele Linaro - February 2010

lags = 0:lagstep:maxlag;
c = zeros(size(lags));
for k=1:length(lags)
    c(k) = corr(x(1:end-lags(k)),x(lags(k)+1:end));
end
