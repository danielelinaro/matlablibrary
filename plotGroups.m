function plotGroups(ax, Y, col, labels, ylimits, yticks, ylbl, groups_idx, markers_color, markers_color_index)
% plotGroups plots groups of values and their mean and standard error.
% 
% plotGroups(ax, Y, col, labels, ylimits, yticks, ylbl, groups_idx)
% 
% Arguments:
%          ax - the axes where the plot will be shown. Can be empty, in which
%               case gca is used.
%           Y - the data to plot. It must be a Nx1 cell array, where N is the
%               number of groups that will be displayed.
%         col - the colors to use. It must be a Nx3 matrix, where each row
%               contains the color for a group.
%      labels - the labels to use for each group on the x-axis. It must be a
%               Nx1 cell array.
%     ylimits - the limits for the y-axis. It must be a 1x2 matrix, with
%               lower and upper bound.
%      yticks - the ticks for the y-axis.
%        ylbl - the label for the y-axis.
%  groups_idx - the indexes of the groups in Y to consider for ANOVA.
% 

% Author: Daniele Linaro - March 2016.

if isempty(ax)
    ax = gca;
end

if ~ exist('groups_idx','var') || isempty(groups_idx)
    groups_idx = 1:length(Y);
end

if length(Y) > 2
    X = flattenCellArray(Y(groups_idx),'full');
    groups = [];
    for i=1:length(groups_idx)
        groups = [groups ; i+zeros(length(Y{groups_idx(i)}),1)];
    end
    [~,~,s] = anova1(X,groups,'off');
    comparison = multcompare(s,'alpha',0.05,'display','off');
    significant = comparison(comparison(:,3).*comparison(:,5)>0,1:2);
    if ~ isempty(significant)
        [~,idx] = sort(diff(significant,[],2), 'descend');
        significant = significant(idx,:);
        [~,idx] = sort(significant(:,1),'ascend');
        significant = significant(idx,:);
        out = {significant(1,:)};
        for i=2:size(significant,1)
            for j=1:size(out,1)
                add = 1;
                for k=1:size(out,2)
                    z = intersection(significant(i,:),out{j,k});
                    if ~ isempty(z) && diff(z) ~= 0
                        add = 0;
                        break;
                    end
                end
                if add
                    out{j,length(out(j))+1} = significant(i,:);
                    break;
                end
            end
            if ~ add
                out{size(out,1)+1,1} = significant(i,:);
            end
        end
        significant = out;
    end
else
    isnormal = @(x) ~ kstest((x-mean(x))/std(x),[],1e-3);
    n = length(Y);
    cnt = 1;
    P = ones(n*(n-1)/2,1);
    pairs = zeros(n*(n-1)/2,2);
    alpha = 0.05;
    for i=1:n
        for j=i+1:n
            pairs(cnt,:) = [i,j];
            try
                if isnormal(Y{i}(~isnan(Y{i}))) && isnormal(Y{j}(~isnan(Y{j})))
                    fprintf(1, 't-test: ');
                    [h,P(cnt)] = ttest2(Y{i}(~isnan(Y{i})),Y{j}(~isnan(Y{j})),alpha,'Both','Unequal');
                else
                    [h,P(cnt)] = kstest2(Y{i}(~isnan(Y{i})),Y{j}(~isnan(Y{j})),alpha);
                    fprintf(1, 'KS-test: ');
                end
                fprintf(1, '(%d,%d) H = %d P = %f\n', i, j, h, P(cnt));
            catch
            end
            cnt = cnt+1;
        end
    end
    if any(P < alpha)
        % significant p-values
        Psig = sort(P(P<alpha));
        % their rank
        x = (1:length(Psig))';
        % critical p-values
        Pcrit = alpha*x/length(x);
        % corrected alpha
        alpha_corr = Psig(find(Psig<Pcrit,1,'last'));
        significant = pairs(P<=alpha_corr,:);
        [~,idx] = sort(diff(significant,[],2), 'descend');
        significant = significant(idx,:);
        [~,idx] = sort(significant(:,1),'ascend');
        significant = significant(idx,:);
        out = {significant(1,:)};
        for i=2:size(significant,1)
            for j=1:size(out,1)
                add = 1;
                for k=1:size(out,2)
                    z = intersection(significant(i,:),out{j,k});
                    if ~ isempty(z) && diff(z) ~= 0
                        add = 0;
                        break;
                    end
                end
                if add
                    out{j,length(out(j))+1} = significant(i,:);
                    break;
                end
            end
            if ~ add
                out{size(out,1)+1,1} = significant(i,:);
            end
        end
        significant = out;
    else
        significant = [];
    end
end

%
if exist('markers_color_index','var')
    with_index = true;
else
    with_index = false;
end

set(ax,'NextPlot','Add');
axes(ax);
for i=1:length(Y)
%     n = length(Y{i});
    n = sum(~isnan(Y{i}));
    for j=1:length(Y{i})
        if with_index
            plot(ax,i+0.25+0.25*rand,Y{i}(j),'o','Color',markers_color(markers_color_index{i}(j),:),...
                'MarkerFaceColor',markers_color(markers_color_index{i}(j),:),'MarkerSize',5,'LineWidth',1);
        else
            plot(ax,i+0.25+0.25*rand,Y{i}(j),'o','Color',col(i,:),...
                'MarkerFaceColor','w','MarkerSize',5,'LineWidth',1);
        end
    end
    plot(ax,i+0.75+[0,0],nanmean(Y{i})+nanstd(Y{i})/sqrt(n).*[-1,1],...
        'Color',col(i,:),'LineWidth',2);
%     plot(ax,i+0.75,nanmean(Y{i}),'o','Color',col(i,:),'MarkerFaceColor',...
%         col(i,:),'LineWidth',2,'MarkerSize',7);
    plot(ax,i+0.75+0.1*[-1,-1,1,1],[ylimits(1),nanmean(Y{i})+[0,0],ylimits(1)],...
        'Color',col(i,:),'LineWidth',2);
%     text(i+0.1,nanmean(Y{i}),sprintf('n=%d',sum(~isnan(Y{i}))),...
%         'HorizontalAlignment','Center','VerticalAlignment','Middle',...
%         'Rotation',90,'FontSize',10,'Color',col(i,:));
    text(i+0.75,ylimits(2),sprintf('n=%d',sum(~isnan(Y{i}))),...
        'HorizontalAlignment','Center','VerticalAlignment','Top',...
        'FontSize',10,'Color',col(i,:));
end
set(ax,'XTick',1.75:i+0.75,'XLim',[1,i+1],'XTickLabel',labels,...
    'YLim',ylimits,'YTick',yticks,'TickDir','Out','LineWidth',0.8);
ylabel(ax,ylbl);


if 0 && ~ isempty(significant)
    xl = xlim;
    pos = get(ax, 'Position');
    pos(2) = pos(2)+pos(4);
    pos(4) = 0.05;
    axes('Position',pos,'NextPlot','Add');
    n = 1;
%     if P > 0.01
%         n = 1;
%     elseif P > 0.001
%         n = 2;
%     else
%         n = 3;
%     end
    for i=1:length(significant)
        if size(significant,2) == 1
            plotSignificance(significant{i}(1)+0.8, i, diff(significant{i})-0.05, 0, n);
        else
            for j=1:length(significant{i})
                if ~ isempty(significant{i,j})
                    plotSignificance(significant{i,j}(1)+0.8, i, diff(significant{i,j})-0.05, 0, n);
                end
            end
        end
    end
    axis tight;
    axis([xl,ylim]);
    axis off
end
