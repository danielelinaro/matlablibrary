function varargout = findInDatabase(db, table, columns, keys, values)
% findInDatabase queries a database for specific columns, corresponding to
% key-value pairs.
% 
% [...] = findInDatabase(db, table, columns, keys, values)
% 
% Parameters:
%       db - a string containing the name of the database.
%    table - a string containing the name of the table where the search will
%            be performed.
%  columns - a cell array with the names of the columns to be returned.
%     keys - a cell array with the keys.
%   values - a cell array with the corresponding values.
% 
% Returns:
%     As many elements as the number of columns.
% 
% Example:
% 
% id = findInDatabase('electrophysiology','experimenters','id',{'name','surname'},{'Daniele','Linaro'});
% 
% See also addToDatabase.
%

% Author: Daniele Linaro - February 2013

if ~ iscell(columns)
    columns = {columns};
end
if ~ iscell(keys)
    keys = {keys};
end
if ~ iscell(values)
    values = {values};
end

if length(keys) ~= length(values)
    error('Keys and values must have the same length.');
end

try
    hndl = mysql('open','localhost','daniele','ilenia');
catch err
    error('Unable to connect to the database [%s].', err.identifier);
end

foo = mysql(sprintf('use %s', db));
query = 'SELECT ';
for k=1:length(columns)
    query = [query, columns{k}];
    if k < length(columns)
        query = [query, ', '];
    end
end
query = [query, ' FROM ', table, ' WHERE '];
for k=1:length(keys)
    query = [query, keys{k}, ' = ''', values{k}, ''''];
    if k < length(keys)
        query = [query, ' AND '];
    end
end
query = [query, ';'];

varargout = cell(1,length(columns));
expression = '[';
for k=1:length(columns)
    expression = [expression, sprintf('varargout{%d}',k)];
    if k < length(columns)
        expression = [expression,','];
    end
end
expression = [expression, '] = mysql(query);'];

eval(expression);

mysql('close');
