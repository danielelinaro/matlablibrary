function extractSpikeFeaturesFromFiles(varargin)
% 
% extractSpikeFeaturesFromFiles(max_ahp_dur,max_peak_to_end)
% 
% extractSpikeFeaturesFromFiles('pyramidal')
%      defaults =>  max_ahp_dur = 15e-3, max_peak_to_end = 10e-3
% 
% extractSpikeFeaturesFromFiles('fs')
%      defaults =>  max_ahp_dur = 15e-3, max_peak_to_end = 2e-3
% 

defaults = defaultParameters(loadCellType);

p = inputParser;
p.addParameter('threshold', [], @isnumeric);
p.addParameter('maxAHPDur', defaults.max_ahp_dur, @(x) isnumeric(x) && x>0);
p.addParameter('maxADPDur', defaults.max_adp_dur, @(x) isnumeric(x) && x>0);
p.addParameter('maxPeakToEndDur', defaults.max_peak_to_end, @(x) isnumeric(x) && x>0);
p.addParameter('maxThresholdToPeakDur', defaults.max_threshold_to_peak, @(x) isnumeric(x) && x>0);
p.addParameter('tstart', [], @(x) isnumeric(x) && all(x)>0);
p.addParameter('stepdur', [], @(x) isnumeric(x) && all(x)>0);
p.addParameter('stepamp', [], @isnumeric);
p.addParameter('minISI', defaults.min_isi, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('APEndDVThresholdCoeff', defaults.min_dV_thresh_coeff, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('rapidnessDVThreshold', defaults.min_dV_thresh, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('rapidnessWindow', defaults.rapidness_window, @(x) isnumeric(x) && all(x>0));
p.addParameter('maxNoSpikes', [], @(x) isnumeric(x) && isscalar(x));
p.addParameter('deriv', 3, @(x) x==1 || x==3);
parse(p, varargin{:});

max_ahp_dur = p.Results.maxAHPDur;
max_adp_dur = p.Results.maxADPDur;
max_peak_to_end = p.Results.maxPeakToEndDur;
max_threshold_to_peak = p.Results.maxThresholdToPeakDur;
min_ISI = p.Results.minISI;
rapidness_window = p.Results.rapidnessWindow;
min_dV_threshold = p.Results.rapidnessDVThreshold;
min_dV_threshold_coeff = p.Results.APEndDVThresholdCoeff;
deriv = p.Results.deriv;

files = listH5Files;
if ~ isempty(files)
    if exist('gampa.stim','file') || exist('gexc.stim','file')
        [t,V] = loadH5TracesBis(files,1);
    else
        allFiles = listH5Files('.',0);
        kernels = dir('*_kernel.dat');
        [t,Vr,I] = loadH5Traces(files);
        V = zeros(size(Vr));
        nk = length(kernels);
        if nk == 0
            error('There are no kernels in this directory.');
        end
        stop = 0;
        for i=1:nk
            Ke = load(kernels(i).name);
            start = stop+1;
            if i == nk
                stop = length(files);
            else
                next_kernel = find(cellfun(@(x) strcmp(x(1:14),kernels(i+1).name(1:14)), allFiles));
                stop = next_kernel - 1 - i;
            end
            if stop < start
                stop = start-1;
            else
                V(start:stop,:) = AECoffline(Vr(start:stop,:),I(start:stop,:),Ke);
            end
        end
    end
elseif exist('recording.mat','file')
    data = load('recording.mat');
    t = (0:size(data.V,2)-1) * data.dt;
    V = data.V;
end

Fc = 5000;
stop = 2*round(1/Fc/diff(t(1:2)));
padding = repmat(V(:,1),[1,stop]);
V = filterTrace(V,1/diff(t(1:2)),Fc,2);
V(:,1:stop) = padding;


if max(max(V)) > p.Results.threshold
    idx = t > p.Results.tstart(1) & t < p.Results.tstart(1)+p.Results.stepdur(1);
    [tth,Vth,tp,Vp,tend,Vend,tahp,Vahp,tadp,Vadp,thalf,Vhalf,halfwidth,rapidness] = ...
        extractSpikeFeatures(t(idx),V(:,idx),p.Results.threshold,max_ahp_dur,max_adp_dur,...
        max_peak_to_end,max_threshold_to_peak,min_ISI,min_dV_threshold_coeff,min_dV_threshold,...
        rapidness_window,p.Results.maxNoSpikes,deriv);

    save('spike_features.mat','tth','Vth','tp','Vp','tend','Vend','tahp','Vahp',...
        'tadp','Vadp','thalf','Vhalf','halfwidth','max_ahp_dur','max_adp_dur',...
        'max_peak_to_end','rapidness','deriv');

    if ~ isempty(p.Results.tstart)
        tstart = p.Results.tstart;
        if isscalar(tstart)
            tstart = tstart + zeros(size(V,1),1);
        end
        save('spike_features.mat','tstart','-append');
    end
    if ~ isempty(p.Results.stepdur)
        stepdur = p.Results.stepdur;
        save('spike_features.mat','stepdur','-append');
    end
    if ~ isempty(p.Results.stepdur)
        stepamp = p.Results.stepamp;
        save('spike_features.mat','stepamp','-append');
    end
    
    n = size(V,1);
    AP_amplitude = nan(n,2);
    AP_duration = nan(n,2);
    AP_half_width = nan(n,2);
    AP_rise_time = nan(n,2);
    AP_fall_time = nan(n,2);
    AP_ahp_time = nan(n,2);
    AP_rise_rate = nan(n,2);
    AP_fall_rate = nan(n,2);
    AP_fast_ahp = nan(n,2);
    AP0_ahp_to_AP1_th_time = nan(n,1);
    for k=1:n
        if length(tp{k}) > 1
            AP_amplitude(k,:) = Vp{k}(1:2) - Vth{k}(1:2);
            AP_duration(k,:) = tend{k}(1:2) - tth{k}(1:2);
            AP_half_width(k,:) = halfwidth{k}(1:2);
            AP_rise_time(k,:) = tp{k}(1:2) - tth{k}(1:2);
            AP_fall_time(k,:) = tend{k}(1:2) - tp{k}(1:2);
            AP_ahp_time(k,:) = tahp{k}(1:2) - tend{k}(1:2);
            AP_rise_rate(k,:) = AP_amplitude(k,:) ./ AP_rise_time(k,:);
            AP_fall_rate(k,:) = AP_amplitude(k,:) ./ AP_fall_time(k,:);
            AP_fast_ahp(k,:) = Vend{k}(1:2) - Vahp{k}(1:2);
            AP0_ahp_to_AP1_th_time(k) = tahp{k}(1) - tth{k}(2);
        elseif length(tp{k}) == 1
            AP_amplitude(k,1) = Vp{k}(1) - Vth{k}(1);
            AP_duration(k,1) = tend{k}(1) - tth{k}(1);
            AP_half_width(k,1) = halfwidth{k}(1);
            AP_rise_time(k,1) = tp{k}(1) - tth{k}(1);
            AP_fall_time(k,1) = tend{k}(1) - tp{k}(1);
            AP_ahp_time(k,1) = tahp{k}(1) - tend{k}(1);
            AP_rise_rate(k,1) = AP_amplitude(k,1) ./ AP_rise_time(k,1);
            AP_fall_rate(k,1) = AP_amplitude(k,1) ./ AP_fall_time(k,1);
            AP_fast_ahp(k,1) = Vend{k}(1) - Vahp{k}(1);
        end
    end
    AP_amplitude_change = diff(AP_amplitude,[],2)./AP_amplitude(:,2);
    AP_duration_change = diff(AP_duration,[],2)./AP_duration(:,2);
    AP_half_width_change = diff(AP_half_width,[],2)./AP_half_width(:,2);
    AP_ahp_time_change = diff(AP_ahp_time,[],2)./AP_ahp_time(:,2);
    AP_rise_rate_change = diff(AP_rise_rate,[],2)./AP_rise_rate(:,2);
    AP_fall_rate_change = diff(AP_fall_rate,[],2)./AP_fall_rate(:,2);
    AP_fast_ahp_change = diff(AP_fast_ahp,[],2)./AP_fast_ahp(:,2);
    AHP_symmetry = AP_ahp_time ./ (AP_ahp_time+repmat(AP0_ahp_to_AP1_th_time,[1,2]));
    
    save('spike_features.mat','AP_amplitude','AP_duration','AP_half_width','AP_rise_time',...
        'AP_fall_time','AP_ahp_time','AP_rise_rate','AP_fall_rate','AP_fast_ahp',...
        'AP_amplitude_change','AP_duration_change','AP_half_width_change','AP_ahp_time_change',...
        'AP_rise_rate_change','AP_fall_rate_change','AP_fast_ahp_change','AHP_symmetry',...
        'AP0_ahp_to_AP1_th_time','-append');

end
