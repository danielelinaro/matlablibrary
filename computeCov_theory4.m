function covT_theory = computeCov_theory4(cell1,cell2,freq,Pss,T,nu1,nu2)
% covT_theory = computeCov_theory3(cell1,cell2,freq,Pss,T,nu1,nu2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% compute linear response prediction for spike count covariance in long windows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% fit the f-I curve with a power-law
data_in = load(strcat(cell1,'/fI_curves/fI_curves.mat'));
I = data_in.Iu{2};
f = data_in.fm{2};
[a,b,~,Itest,ftest] = fitFICurve(I,f,data_in.rheobase(2,:));
[~,ind] = min(abs(ftest - nu1));
fIgain1 = a*b*Itest(ind)^(b-1);

data_in = load(strcat(cell2,'/fI_curves/fI_curves.mat'));
I = data_in.Iu{2};
f = data_in.fm{2};
[a,b,~,Itest,ftest] = fitFICurve(I,f,data_in.rheobase(2,:));
[~,ind] = min(abs(ftest - nu2));
fIgain2 = a*b*Itest(ind)^(b-1);

% %%%%% modified, Daniele
% data = load(['../',cell1,'/correlations/01/psd.mat']);
% freq = [fliplr(-data.f(2:end)),data.f];
% Pss = [fliplr(data.S_I(2:end)),data.S_I]; 
% %%%%%

%%%%% Corrected version:
kT = (sin(pi.*freq.*T).^2)./(pi^2.*freq.^2);
kT(freq==0) = T^2;
n = size(Pss,1);
covT_theory = zeros(n,1);

for i=1:n
	covT_theory(i) = fIgain1*fIgain2*trapz(freq,kT.*Pss(i,:));
end
