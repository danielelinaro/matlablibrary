function [out] = loadTrace(varargin)
    % [out]=loadTrace(filename,start=0,stop=0)
    p = inputParser;
    p.addRequired('fname',@ischar) %filename
    p.addOptional('start',0); %spikes detection threshold
    p.addOptional('stop',0);
    p.parse(varargin{:});
    fname=p.Results.fname;
    Tstart=p.Results.start;
    Tstop=p.Results.stop;
    
    % is the file there??
    if ~exist(fname, 'file')
        disp(['Warning --> error: file ',fname ,' not found!']);
        out.ch1=[];
        out.srate=-1;
        return;
    end
    
    fp = fopen(fname, 'r');        
    srate = fread(fp, 1, 'double');
    N     = fread(fp, 1, 'ulong');
    M     = fread(fp, 1, 'ulong');
    M     = fread(fp, 1, 'uint64'); % number of samples per wave (channel)
    
    if ~(Tstart==0)
        Tstart=int32(Tstart*srate)+1;
    end
    if Tstop==0
        Tstop=M;
    else
        Tstop=int32(Tstop*srate)+1;
    end
    if Tstop>M
        Tstop=M;
    end
    
    recorded  = zeros((Tstop)-Tstart,N);
    zeroPosition=ftell(fp);
    for ii=1:N
        fseek(fp,zeroPosition+(Tstart)*8,'bof'); % move to the correct position (8 is the number of bytes in a double)
        recorded(:,ii)=fread(fp,(Tstop)-Tstart,'double');
        zeroPosition=zeroPosition+(int32(M)*8);
    end
    fclose(fp);
    %export stuff
    out.srate=srate;
    for ii=1:size(recorded,2)
        out.(['ch',num2str(ii)])=recorded(:,ii);
    end
    