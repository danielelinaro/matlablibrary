clear all;
close all;
clc;

%%
kernels = dir('*_kernel.dat');
files = listH5Files;
if ~ isempty(files)
    Ke = load(kernels(1).name);
    [t,Vr,I,stim] = loadH5Traces(files);
    V = AECoffline(Vr,I,Ke);
    clear Vr Ke
else
    [t,V,I,stim] = loadH5Traces(files);
end
V = filterTrace(V, 1/diff(t(1:2)), 500, 2);
[dur,i] = max(stim{1}(:,1));
t0 = sum(stim{1}(1:i-1,1));
idx = find(t > t0);
ndx = all(I(:,idx)==0,2);
t = t(idx) - t(idx(1));
I = I(ndx,idx);
V = V(ndx,idx);
clear files stim

%%
[tau_rise,tau_decay,amplitudes] = extractEPSPs(t,V,[5e-3,100e-3],1);
save('epsp.mat','tau_rise','tau_decay','amplitudes');
