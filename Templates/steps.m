clear all;
close all;
clc;

%%
if ~ exist('recording.mat','file')
    files = listH5Files;
    [t,Vr,Iinj,stim] = loadH5Traces(files);
    t0 = cellfun(@(x) sum(x(1:end-2,1)), stim);
    stimdur = cellfun(@(x) x(end-1,1), stim);
    stimamp = cellfun(@(x) x(end-1,3), stim);
    kernels = dir('*_kernel.dat');
    if ~ isempty(kernels)
        Ke = load(kernels(1).name);
        Vm = AECoffline(Vr,Iinj,Ke);
    else
        Vm = Vr;
    end
else
    data = load('recording.mat');
    Vm = data.V;
    t = (0:size(Vm,2)-1) * data.dt;
    t0 = data.tstart;
    stimdur = data.dur;
    stimamp = data.I(:,end/2);
end

% thresh = max(Vm,[],2) - 20;
% thresh(thresh < -40) = 0;
thresh = 0;
Vm = filterTrace(Vm,1/diff(t(1:2)),1000,2);
features = analyseDCPulse(t,Vm,t0,stimdur,stimamp,thresh);
save('features.mat','features','stimdur');
