clear all;
close all;
clc;

%%
kernels = dir('*_kernel.dat');
files = listH5Files;
if ~ isempty(files)
    Ke = load(kernels(1).name);
    [t,Vr,I,stim] = loadH5Traces(files);
    V = AECoffline(Vr,I,Ke);
    clear Vr Ke
else
    [t,V,I,stim] = loadH5Traces(files);
end
V = filterTrace(V, 1/diff(t(1:2)), 500, 2);
idx = find(t > 50e-3 & t < stim{1}(1));
t = t(idx) - t(idx(1));
I = I(:,idx);
V = V(:,idx);
clear files stim

%%
[tau_rise,tau_decay,amplitudes] = extractEPSPs(t,V,[5e-3,100e-3],1);
save('epsp.mat','tau_rise','tau_decay','amplitudes');
