clear all;
close all;
clc;

%%
files = listH5Files;
[t,Vm,tau_rise,tau_decay,amplitude,pp_ratio] = analysePulsesTrainFromFiles(files);
save('extracellular.mat','t','Vm','tau_rise','tau_decay','amplitude','pp_ratio');

