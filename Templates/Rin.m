% clear all;
% close all;
% clc;

%%
files = listH5Files;
kernels = dir('*_kernel.dat');
Ke = load(kernels(1).name);
[t,Vr,I,info] = loadH5Traces(files(end));
Vm = AECoffline(Vr,I,Ke);
spike_thresh = loadThreshold(-20);
do_plot = 1;
R = computeInputResistanceFromPulses(t,Vm,I,spike_thresh,do_plot);
save('Rin.mat','R');
