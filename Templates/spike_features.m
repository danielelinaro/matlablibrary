% clear all;
% close all;
% clc;

%%
if ~ exist('recording.mat','file')
    files = listH5Files;
    [t,~,~,stim] = loadH5Traces(files);
    t0 = cellfun(@(x) sum(x(1:end-2,1)), stim);
    stimdur = cellfun(@(x) x(end-1,1), stim);
    stimamp = cellfun(@(x) x(end-1,3), stim);
else
    data = load('recording.mat');
    n = size(data.V,1);
    t0 = data.tstart + zeros(n,1);
    stimdur = data.dur + zeros(n,1);
    stimamp = data.I(:,end/2) + zeros(n,1);
end

[thresh,deriv] = loadThreshold(0);
extractSpikeFeaturesFromFiles('threshold',thresh,'tstart',t0,...
    'stepdur',stimdur','stepamp',stimamp,'maxNoSpikes',2,'deriv',deriv);
