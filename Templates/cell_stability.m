% clear all;
% close all;
% clc;

%%
[passive,kernels] = checkStability;
tend = round(max([passive.mins(end),kernels.mins(end)]));
save('cell_stability','passive','kernels','tend');
