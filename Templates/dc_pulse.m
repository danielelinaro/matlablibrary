clear all;
close all;
clc;

%%
files = listH5Files;
[t,V,I,gexc,ginh] = loadH5TracesBis(files{end}, 1, 2, 3, 4);
I0 = max(I);
idx = find(gexc ~= 0);
t0 = t(idx(1));
dur = t(idx(end)) - t0;
ndx = find(t > t0-0.5 & t < t0+dur+0.5);
spks = extractAPPeak(t,V,-20);
spks = spks{1};
rate = length(spks)/dur;
isi = diff(spks);
cv = std(isi)/mean(isi);

%%
wndw = [10e-3,10e-3];
[gexc_avg,gexc_wndw] = ETSA(t,gexc,spks,wndw);
[ginh_avg,ginh_wndw] = ETSA(t,ginh,spks,wndw);
gexc0 = mean(gexc(t>t0 & t<t0+dur));
ginh0 = mean(ginh(t>t0 & t<t0+dur));
dt = diff(t(1:2));
T = -wndw(1) : dt : wndw(2);

%%
figure;
axes('Position',[0.1,0.45,0.8,0.45],'NextPlot','Add');
plot(t(ndx),V(ndx),'Color',[.4,.4,.4]);
axis off;
placeScaleBars(t0-1, min(V(idx))-10, 2, 20, '2 s', '20 mV',...
    'k','LineWidth', 2);
axis([t0-1.1,t0+dur+0.5,min(V)-10,max(V)+30]);
text(t0+dur+0.5,max(V)+30,sprintf('Stimulation amplitude = %.0f pA', I0),...
    'FontName','Arial','FontSize',9,'HorizontalAlignment','Right');
text(t0+dur+0.5,max(V)+20,sprintf('Firing rate = %.1f Hz', rate),...
    'FontName','Arial','FontSize',9,'HorizontalAlignment','Right');
text(t0+dur+0.5,max(V)+10,sprintf('CV = %.2f', cv),...
    'FontName','Arial','FontSize',9,'HorizontalAlignment','Right');
axes('Position',[0.1,0.1,0.8,0.3],'NextPlot','Add');
plot(T, gexc_wndw(10:35,:)/gexc0,'Color',[1,.5,.5]);
plot(T, ginh_wndw(10:35,:)/ginh0,'Color',[.5,.5,1]);
plot(T, gexc_avg/gexc0,'r','LineWidth',2);
plot(T, ginh_avg/ginh0,'b','LineWidth',2);
axis tight;
axis off;
xl = xlim;
yl = ylim;
placeScaleBars(xl(1)-0.5e-3,yl(1)-0.1,3e-3,0.2,'3 ms','20%', ...
    'k', 'LineWidth', 2);
axis([xl(1)-0.51e-3,xl(2),yl(1)-0.1,yl(2)]);
title('Normalized STA','FontName','Arial','FontSize',9);
set(gcf, 'Color', [1,1,1], 'PaperPosition', [0,0,10,6], 'PaperUnits', 'Inch');
print('-depsc2','dc_pulse.eps');
