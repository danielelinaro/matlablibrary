% clear all;
% close all;
% clc;

%%
files = listH5Files;
entities = loadH5Trace(files{1});
t0 = entities(2).metadata(1,1);
dur = entities(2).metadata(2,2);
if strcmp(entities(1).units,'pA')
    [t,I,V] = loadH5TracesBis(files,1,2);
    VLED = max(V,[],2);
    [Ipeak,Iss,V] = extractPeakAndSteadyStateCurrents(t,I,VLED,t0,dur);
    save('ChR2_currents.mat','Ipeak','Iss','V','t0','stimdur');

    figure;
    hold on;
    plot(V,Ipeak,'ko-','LineWidth',1,'MarkerFaceColor','w','MarkerSize',5);
    plot(V,Iss,'rs-','LineWidth',1,'MarkerFaceColor','w','MarkerSize',5);
    xlabel('Command voltage (V)');
    ylabel('Current (pA)');
    h = legend('Peak current','Steady-state current','Location','NorthEast');
    set(h,'Box','Off');
    set(gca,'XTick',V,'TickDir','Out','LineWidth',0.8);
    sz = [5,3];
    set(gcf,'PaperUnits','Inch','PaperSize',sz,'PaperPosition',[0,0,sz],'Color','w');
    print('-dpdf','ChR2_currents.pdf');
else
    [VLED,f,gain,rheobase,a,b,c,VLEDint] = computefICurveFromFiles(files,[],0,0,1);
    save('f-light_curve.mat','VLED','f','gain','rheobase','a','b','c','VLEDint');
    !rm fI_curve.pdf
    for ax = get(gcf,'Children')'
        for c = [get(ax,'Children');get(ax,'XLabel');get(ax,'YLabel')]'
            try
                str = get(c,'String');
                if strcmp(str,'I (pA)')
                    set(c,'String','Command voltage (V)');
                else
                    pos = strfind(str,'pA');
                    if ~ isempty(pos)
                        set(c,'String',[str(1:pos-1),'V',str(pos+2:end)]);
                    end
                end
            catch
            end
        end
    end
    print('-dpdf','f-light_curve.pdf');
end
