% clear all;
% close all;
% clc;

%%
if ~ exist('recording.mat','file')
    try
        files = dir('*_kernel.dat');
        Ke = load(files(1).name);
    catch
        files = dir('../../steps/01/*_kernel.dat');
        Ke = load(['../../steps/01/',files(1).name]);
    end
    files = listH5Files;
    [t,Vr,I,stim] = loadH5Traces(files);
    Vm = AECoffline(Vr,I,Ke);
    t0 = sum(stim{1}(1:end-2,1))-0.5;
else
    data = load('recording.mat');
    Vm = data.V;
    I = data.I;
    t = (0:size(Vm,2)-1) * data.dt;
    t0 = data.tstart-0.5;
end
idx = find(t>t0);
thresh = loadThreshold;
[Ithresh,Vthresh] = analyseRamp(t(idx),Vm(:,idx),I(:,idx),0.5,thresh);
n = length(Ithresh);
fprintf(1, 'Spiking threshold: %.3f +- %.3f pA.\n', ...
    mean(Ithresh), std(Ithresh)/sqrt(n));
fprintf(1, 'Voltage threshold: %.3f +- %.3f mV.\n', ...
    mean(Vthresh), std(Vthresh)/sqrt(n));

save('ramp.mat','Ithresh','Vthresh');
