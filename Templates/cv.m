clear all;
close all;
clc;

%%
dirs = dir('0*');
ndirs = length(dirs);
CV = cell(ndirs,1);
ISI = cell(ndirs,1);
spikeTimes = cell(ndirs,1);

for ii=1:ndirs
    files = listH5Files(dirs(ii).name,1);
    nfiles = length(files);
    spikeTimes{ii} = cell(nfiles,1);
    CV{ii} = zeros(nfiles,1);
    ISI{ii} = zeros(nfiles,1);
    for jj=1:nfiles
        [entities,info] = loadH5Trace([dirs(ii).name,'/',files{jj}]);
        t = (0:length(entities(1).data)-1) * info.dt;
        spks = extractAPPeak(t,entities(1).data');
        spikeTimes{ii}{jj} = spks{1};
        isi = diff(spikeTimes{ii}{jj});
        CV{ii}(jj) = std(isi)/mean(isi);
        ISI{ii}(jj) = mean(isi);
    end
end

save('cv.mat', 'CV','ISI','spikeTimes');

%%
figure;
hold on;
col = [1,0.5,0.5; 0.5,1,0.5; 0.5,0.5,1];
symb = ['o','^','s'];
lgnd = cell(ndirs,1);
hndl = zeros(ndirs,1);
for ii=1:ndirs
    hndl(ii) = plot(ISI{ii},CV{ii},symb(ii),'Color',col(ii,:),...
        'MarkerFaceColor',col(ii,:));
    lgnd{ii} = sprintf('%dX', ii);
end
axis([0,2,0,1.5]);
set(gca,'XTick',0:.4:2,'YTick',0:.3:1.5);
xlabel('<ISI> (sec)');
ylabel('CV');
box off;
legend(hndl, lgnd, 'Location', 'SouthEast');
set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,6,4]);
print('-depsc2','cv.eps');
