clear all;
close all;
clc;

%%
kernels = dir('*.dat');
Ke = load(kernels(1).name);
files = listH5Files;
[t,Vpre,Vpost,I] = loadH5TracesBis(files,1,2,3);
Fs = 1/diff(t(1:2));
Fc = 5000;
N = 2;
Vpre = filterTrace(AECoffline(Vpre,I,Ke),Fs,Fc,N);
Vpost = filterTrace(Vpost,Fs,Fc,N);
analysePulses(t,Vpre,Vpost,I);
