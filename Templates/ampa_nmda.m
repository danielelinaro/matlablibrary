
folders = {'01','02'};
currents = {'AMPA+NMDA','NMDA'};
hold_values = [-70,40];

[nmda_to_ampa_ratio,taus,Ipeak,I,coeff,model] = ...
    preprocessAMPANMDAExperiment(folders, currents, hold_values);

save('ampa_nmda.mat','folders','currents','hold_values','nmda_to_ampa_ratio',...
    'taus','Ipeak','I','coeff','model');
