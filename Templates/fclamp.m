clear all;
close all;
clc;

%%
files = listH5Files;
[t,V,I,target,f] = loadH5TracesBis(files{end},1,2,3,4);
analyseFrequencyClampExperiment(t,V,I,f,target(end));
