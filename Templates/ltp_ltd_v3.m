
%% Load the data
if exist('recording.mat','file')
    load recording.mat;
    Vstim = struct('tstart',tstart_extra,'dur',extra_dur,'amplitude',extra_amp);
    Istim = struct('pulses',struct('tstart',Vstim.tstart+time_offset,'dur',pulse_dur,'amplitude',...
        pulse_amp,'n',2,'freq',1/pulse_interval),'step',struct('tstart',tstart_step,...
        'dur',step_dur,'amplitude',step_amp));
else
    for i=1:3
        if ~ exist(sprintf('%02d',i),'dir')
            return;
        end
    end
    
    %%%%
    T = load('stimulation_period');
    stim_freq = 1/T;
    
    fprintf(1, 'Loading data from directory [01]... ');
    cd('01');
    files = listH5Files;
    [entities,info] = loadH5Trace(files{1});
    dt = info.dt;
    if strcmp(entities(1).name,'RealNeuron')
        compensate = 0;
    else
        compensate = 1;
    end
    if strcmp(entities(2).units,'V')
        extra_id = 2;
        intra_id = 3;
    else
        extra_id = 3;
        intra_id = 2;
    end
    [~,Vbefore,I] = loadH5TracesBis(files,1,intra_id);
    if compensate
        kernels = dir('*_kernel.dat');
        Ke = load(kernels(1).name);
        Vbefore = AECoffline(Vbefore,I,Ke);
    end
    cd('..');
    fprintf(1, 'done.\n');
    
    %%%%
    fprintf(1, 'Loading data from directory [02]... ');
    cd('02');
    files = listH5Files;
    entities = loadH5Trace(files{1});
    [~,Vpairing,I] = loadH5TracesBis(files,1,intra_id);
    if compensate
        kernels = dir('*_kernel.dat');
        Ke = load(kernels(1).name);
        Vpairing = AECoffline(Vpairing,I,Ke);
    end
    Vstim = struct('tstart',entities(extra_id).metadata(1,1),...
        'dur',entities(extra_id).metadata(2,1),...
        'amplitude',entities(extra_id).metadata(2,3));
    Istim = struct('pulses',struct('tstart',entities(intra_id).metadata(1,1),...
        'dur',entities(intra_id).metadata(2,1),...
        'amplitude',entities(intra_id).metadata(2,3),...
        'n',length(find(entities(intra_id).metadata(:,3) == entities(intra_id).metadata(2,3))), ...
        'freq',1/sum(entities(intra_id).metadata(2:3,1))),'step',struct(...
        'tstart',sum(entities(intra_id).metadata(1:end-2,1)),...
        'dur',entities(intra_id).metadata(end-1,1),...
        'amplitude',entities(intra_id).metadata(end-1,3)));
    cd('..');
    fprintf(1, 'done.\n');
    
    %%%%
    fprintf(1, 'Loading data from directory [03]... ');
    cd('03');
    files = listH5Files;
    [~,Vafter,I] = loadH5TracesBis(files,1,intra_id);
    if compensate
        kernels = dir('*_kernel.dat');
        files_time = cellfun(@(x) str2double(x(9:10))*3600+...
            str2double(x(11:12))*60+str2double(x(13:14)), files);
        kernels_time = arrayfun(@(x) str2double(x.name(9:10))*3600+...
            str2double(x.name(11:12))*60+str2double(x.name(13:14)), kernels);
        kernels_time = [kernels_time ; files_time(end)+1];
        for i=2:length(kernels_time)
            idx = find(files_time > kernels_time(i-1) & files_time < kernels_time(i));
            Ke = load(kernels(i-1).name);
            Vafter(idx,:) = AECoffline(Vafter(idx,:),I(idx,:),Ke);
        end
    end
    cd('..');
    fprintf(1, 'done.\n');
    
end

%% Filter the traces
Fc = 1000;
N = 2;
Vm = {filterTrace(Vbefore,1/dt,Fc,N), ...
    filterTrace(Vpairing,1/dt,Fc,N), ...
	filterTrace(Vafter,1/dt,Fc,N)};
t = (0:size(Vm{1},2)-1) * dt;

idx = 1:round(2/(dt*Fc));
for i=1:length(Vm)
    Vm{i}(:,idx) = mean(mean(Vm{i}(:,t>5e-3 & t<15e-3)));
end

%% Perform the analysis
max_window = [1e-3,20e-3];
do_plot = 1;
[seconds,Rin,V0,slope,amplitude,area] = ...
    analyseLTPLTD3(t, Vm, Vstim, Istim, stim_freq, max_window, do_plot);

%%
alpha = 0.01;

X = amplitude{1};
idx = find(~isnan(amplitude{2}));
idx = idx(end-length(X)+1:end);
Y = amplitude{2}(idx);
normal_amplitudes = [lillietest(X),lillietest(Y)];
if normal_amplitudes(1)
    fprintf(1, 'The distribution of baseline amplitudes is normal.\n');
else
    fprintf(1, 'The distribution of baseline amplitudes is not normal.\n');
end
if normal_amplitudes(2)
    fprintf(1, 'The distribution of final amplitudes is normal.\n');
else
    fprintf(1, 'The distribution of final amplitudes is not normal.\n');
end
[h_amplitudes,p_amplitudes] = ttest2(X,Y,'alpha',0.01,'tail','left','vartype','unequal');
if h_amplitudes
    fprintf(1, ['The distribution of final amplitudes has greater mean ',...
        'than that of baseline amplitudes (p = %.03f < %.03f).\n'], p_amplitudes, alpha);
else
    fprintf(1, ['The distribution of final amplitudes has the same mean ',...
        'as that of baseline amplitudes (p = %.03f >= %.03f).\n'], p_amplitudes, alpha);
end
    
%%
X = area{1};
idx = find(~isnan(area{2}));
idx = idx(end-length(X)+1:end);
Y = area{2}(idx);
normal_areas = [lillietest(X),lillietest(Y)];
if normal_areas(1)
    fprintf(1, 'The distribution of baseline areas is normal.\n');
else
    fprintf(1, 'The distribution of baseline areas is not normal.\n');
end
if normal_areas(2)
    fprintf(1, 'The distribution of final areas is normal.\n');
else
    fprintf(1, 'The distribution of final areas is not normal.\n');
end
[h_areas,p_areas] = ttest2(X,Y,'alpha',0.01,'tail','left','vartype','unequal');
if h_areas
    fprintf(1, ['The distribution of final areas has greater mean ',...
        'than that of baseline areas (p = %.03f < %.03f).\n'], p_areas, alpha);
else
    fprintf(1, ['The distribution of final areas has the same mean ',...
        'as that of baseline areas (p = %.03f >= %.03f).\n'], p_areas, alpha);
end

%%
X = slope{1};
idx = find(~isnan(slope{2}));
idx = idx(end-length(X)+1:end);
Y = slope{2}(idx);
normal_slopes = [lillietest(X),lillietest(Y)];
if normal_slopes(1)
    fprintf(1, 'The distribution of baseline slopes is normal.\n');
else
    fprintf(1, 'The distribution of baseline slopes is not normal.\n');
end
if normal_slopes(2)
    fprintf(1, 'The distribution of final slopes is normal.\n');
else
    fprintf(1, 'The distribution of final slopes is not normal.\n');
end
[h_slopes,p_slopes] = ttest2(X,Y,'alpha',0.01,'tail','left','vartype','unequal');
if h_slopes
    fprintf(1, ['The distribution of final slopes has greater mean ',...
        'than that of baseline slopes (p = %.03f < %.03f).\n'], p_slopes, alpha);
else
    fprintf(1, ['The distribution of final slopes has the same mean ',...
        'as that of baseline slopes (p = %.03f >= %.03f).\n'], p_slopes, alpha);
end

%%
save('ltp_ltd.mat', 'Vstim', 'Istim', 'stim_freq', ...
    'seconds', 'Rin', 'V0', 'slope', 'amplitude', 'area', ...
    'h_*', 'p_*', 'normal_*', 'alpha');

print('-dpdf','ltp_ltd.pdf');

  
