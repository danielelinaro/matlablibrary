% clear all;
% close all;
% clc;

%%
if ~ exist('recording.mat','file')
    kernels = dir('*kernel.dat');
    try
        Ke = load(kernels(1).name);
    catch
        for kernel_folder = {'../../steps/01/','../../vi/01/','../../vi/02/'}
            kernels = dir([kernel_folder{1},'*kernel.dat']);
            if ~isempty(kernels)
                Ke = load([kernel_folder{1},kernels(1).name]);
                break
            end
        end
    end
    files = listH5Files;
    [t,Vr,I,stim] = loadH5Traces(files);
    dt = diff(t(1:2));
    t0 = stim{1}(1,1);
    t1 = sum(stim{1}(1:2,1));
    Vm = AECoffline(Vr,I,Ke);
else
    data = load('recording.mat');
    Vm = data.V;
    dt = data.dt;
    t0 = data.tstart;
    t1 = data.tstart+data.dur;
    t = (0:size(Vm,2)-1) * dt;
    Rseries = 20;
    Vm = data.V - Rseries*repmat(data.I,[size(data.V,1),1])*1e-3;
    idx = find(t>t1,1);
    Vm(:,idx) = mean(Vm(:,idx-1:2:idx+1),2);
    Vm = mean(Vm);
end
[spike_threshold,deriv] = loadThreshold;
Vm = Vm(max(Vm,[],2)>spike_threshold,:);
V0 = mean(mean(Vm(:,t<0.2)));
tp = extractAPPeak(t,Vm,spike_threshold);
idx = cellfun(@(x) ~isempty(x), tp);
Vm = Vm(idx,:);
tp = tp(idx,:);
before = 10e-3;
after = 25.5e-3;
for k=1:length(tp)
    if length(tp{k}) > 1 && diff(tp{k}(1:2)) < after
        after = diff(tp{k}(1:2));
    end
end
window = round([before,after-0.5e-3] / dt);
for i=1:length(tp)
    [~,j] = min(abs(tp{i}-t0));
    tp{i} = tp{i}(j);
end
peakpos = cellfun(@(x) round(x/dt), tp, 'UniformOutput', 0);
peakpos = cellfun(@(x) x(1), peakpos);
V = zeros(size(Vm,1),sum(window)+1);
for k=1:size(Vm,1)
    V(k,:) = Vm(k,peakpos(k)-window(1):peakpos(k)+window(2));
end
T = t(peakpos(1)-window(1):peakpos(1)+window(2));
figure;
features = APanalysis(T,V,spike_threshold,deriv,gca,[t0,t1]);
save('ap.mat','T','V','features','V0','spike_threshold','deriv');
