close all;

%%
ttran = 2;
nPeriods = 1;
nBins = 10;
saveSpikes = 1;
loadSpikes = 1;
doPlot = 1;
[F,r0,r1,phi,conf,r0_shuffled,r1_shuffled,phi_shuffled,TF] = ...
    analyseDCPlusSinusoidPlusStaticBGCondExp(ttran,nPeriods,nBins,...
    saveSpikes,loadSpikes,doPlot);

%% Adjust here wrong values of phi
% phi(end) = phi(end)-2*pi;
% w = repmat(r1./r0-r1_shuffled./r0_shuffled,[1,2]);
% w(w(:,1)<=0,1) = min(w(w(:,1)>0,1))/10;
% w(w(:,2)<=0,2) = 0;
% [A,Z,P,Dt] = fitTransferFunction(F, r1./r0, phi, 1, [2,3], w, [], doPlot);
% print('-depsc2','fit_transfer_function.eps');
% TF = struct('A',A,'Z',Z,'P',P,'Dt',Dt);

%%
save('modulation_phase_shift_ap_peak.mat','ttran','nPeriods','nBins',...
    'F','r0','r1','phi','conf','r0_shuffled','r1_shuffled','phi_shuffled','TF');
