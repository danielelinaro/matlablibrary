clear all;
close all;
clc;

% How many spikes are necessary to have a good estimate of the CV?

%%
files = listH5Files;
nfiles = length(files);
N = cell(nfiles,1);
CV = cell(nfiles,1);
ISI = zeros(nfiles,1);
for ii=1:nfiles
    [t,V] = loadH5TracesBis(files{ii},1);
    spks = extractAPPeak(t,V);
    if length(spks{1}) > 2
        N{ii} = 2:length(spks{1});
        CV{ii} = zeros(1,length(N{ii}));
        for jj=1:length(N{ii})
            isi = diff(spks{1}(1:N{ii}(jj)));
            CV{ii}(jj) = std(isi)/mean(isi);
        end
        ISI(ii) = mean(isi);
    end
end
idx = find(ISI ~= 0);
ISI = ISI(idx);
N = N(idx);
CV = CV(idx);
[ISI,idx] = sort(ISI);
N = N(idx);
CV = CV(idx);

%%
n = length(ISI);
cmap = jet(n);
hndl = zeros(n,1);
lgnd = cell(n,1);
figure;
hold on;
for k=1:n
    hndl(k) = plot(N{k},CV{k},'Color',cmap(k,:));
    lgnd{k} = sprintf('<ISI> = %.0f ms', ISI(k)*1e3);
end

box off;
xlabel('Number of spikes');
ylabel('CV');
legend(hndl, lgnd, 'Location', 'NorthEastOutside');
set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,6,4]);
print('-depsc2','cv_estimation.eps');