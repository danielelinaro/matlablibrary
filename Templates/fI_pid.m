% clear all;
% close all;
% clc;

%%
files = listH5Files;
[t,V,I_pid] = loadH5TracesBis(files, 1, 2);
[I,f,models] = computefICurveFromPIDExperiment(t,V,I_pid,1);
save('fI_curve.mat', 'I', 'f', 'models');
