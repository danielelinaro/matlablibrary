clear all;
close all;
clc;

%%
files = findFiles('.','epsp.mat');
if isempty(files)
    disp('No epsp.mat files found!');
else
    tau_rise = [];
    tau_decay = [];
    amplitudes = [];
    for k=1:length(files)
        data = load(files{k});
        tau_rise = [tau_rise, data.tau_rise];
        tau_decay = [tau_decay, data.tau_decay];
        amplitudes = [amplitudes, data.amplitudes];
    end
    coeff = sqrt(length(tau_rise));
    fprintf(1, 'Tau rise: %g +- %g ms.\n', ...
        mean(tau_rise)*1e3, std(tau_rise)*1e3/coeff);
    fprintf(1, 'Tau decay: %g +- %g ms.\n', ...
        mean(tau_decay)*1e3, std(tau_decay)*1e3/coeff);
    fprintf(1, 'EPSP amplitude: %g +- %g mV.\n', ...
        mean(amplitudes), std(amplitudes)/coeff);
end
