clear all;
close all;
clc;

%%
animal = 'Wistar';
age = 18;
region = 'Cortex';
area = 'S1';
location = 'L5';
type = 'Pyramidal';
project = 'Misc';
preparation = 'In vitro';
cwd = pwd;
idx = strfind(cwd,'/');
id = cwd(idx(end)+1:end);
date = id(1:8);
neuron = makeNeuronStruct('AnimalType',animal,'AnimalAge',age,...
    'BrainRegion',region,'BrainArea',area,'Location',location,...
    'CellId',id,'CellDate',date,...
    'NeuronType',type,'ProjectName',project,'Preparation',preparation);
addNeuronToDB(neuron);

