clear all;
close all;
clc;

%%
kernels = dir('*_kernel.dat');
Ke = load(kernels(1).name);
files = listH5Files;
[t,Vr,I,stim] = loadH5Traces(files);
Vm = AECoffline(Vr,I,Ke);
[I,f] = extractfICurveFromCurrentSteps(t,Vm,stim{1},0.5);
p = polyfit(I,f,1);
save('fI_curve','I','f','p','Ke');

%%
figure;
hold on;
plot(I(:),f(:),'.k');
xlabel('Current (pA)');
ylabel('Frequency (spks/sec)');
