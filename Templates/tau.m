% clear all;
% close all;
% clc;

%%
if ~ exist('recording.mat','file')
    try
        kernels = dir('*_kernel.dat');
        Ke = load(kernels(1).name);
    catch
        kernels = dir('../../ap/01/*_kernel.dat');
        Ke = load(['../../ap/01/',kernels(1).name]);
    end
    files = listH5Files;
else
    files = {'recording.mat'};
    Ke = [];
end
spike_thresh = -20;
window = 0.5;
do_plot = 1;
[taus,confidence] = computeTauFromFiles(files, Ke, spike_thresh, window, do_plot);
save('tau.mat','taus','confidence','Ke');
