clear all;
close all;
clc;

%%
files = listH5Files;
target_deflection = 2;
ntrials = 10;
window = [0.1,0.25];
[weight,deflection,err] = analysePSP(files, ntrials, target_deflection, window);
save('psp.mat','target_deflection','deflection','err','weight');
