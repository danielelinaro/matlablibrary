% clear all;
% close all;
% clc;

%%
if ~ exist('recording.mat','file')
    files = dir('*_kernel.dat');
    if ~ exist('gexc.stim','file') && ~ isempty(files)
        Ke = load(files(1).name);
    else
        Ke = [];
    end
    files = listH5Files;
else
    files = {'recording.mat'};
    Ke = [];
end
thresh = loadThreshold(0);
[I,f,gain,rheobase,a,b,c,Iint,CV,evoked_CV] = computefICurveFromFiles(files,Ke,thresh,0.1,1);
save('fI_curve.mat','I','f','gain','rheobase','a','b','c','Iint','CV','evoked_CV');
