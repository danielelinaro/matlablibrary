function [t,V,I,stim] = loadTraces(files)
% [t,V,I,stim] = loadTraces(files)

n = length(files);
data = loadTrace(files(1).name);
dt = 1/data.srate;
t = 0:dt:dt*(length(data.ch1)-1);
V = repmat(data.ch1',[n/2,1]);
if isfield(data,'ch2')
    I = repmat(data.ch2',[n/2,1]);
else
    data = loadTrace(files(2).name);
    I = repmat(data.ch1',[n/2,1]);
end
stim = cell(n/2,1);
stim{1} = load([files(1).name(1:end-4),'_prot.stim']);

for k=3:2:n
    data = loadTrace(files(k).name);
    V((k+1)/2,:) = data.ch1';
    if isfield(data,'ch2')
        I((k+1)/2,:) = data.ch2';
    else
        data = loadTrace(files(k+1).name);
        I((k+1)/2,:) = data.ch1';
    end
    stim{(k+1)/2} = load([files(k).name(1:end-4),'_prot.stim']);
end
