function [T,R] = computeMovingReliability(t,V,window_dur,sigma)
% [T,R] = computeMovingReliability(t,V,window_dur,sigma)

dt = diff(t(1:2));
window_length = round(window_dur/dt);
steps = ceil(t(end)/window_dur)*2-1;
T = zeros(steps,2);
R = zeros(steps,1);

offset = ceil(window_length/2);
idx = -offset+1;
for k=1:steps
    start = idx(1) + offset;
    stop = start + window_length - 1;
    if stop > length(t)
        stop = length(t);
    end
    idx = start:stop;
    T(k,:) = t(idx([1,end]));
    R(k) = computeReliabilityCorrelation(V(:,idx),sigma);
end

