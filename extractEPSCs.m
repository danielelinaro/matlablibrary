function [event_times, amplitudes, tau_decay, EPSC, RMSE, threshold, ...
    suspicious, Ifiltered, discarded_time] = extractEPSCs(t,I,threshold_factor,wndw,min_iei,verbose)
% [event_times, amplitudes, tau_decay, EPSC, RMSE, threshold, 
%   suspicious, Ifiltered, discarded_time] = extractEPSCs(t,I,threshold_factor,wndw,min_iei,verbose)

if ~exist('verbose','var') || isempty(verbose)
    verbose = 1;
end

warning('OFF', generatemsgid('noPeaks'));

if ~ exist('threshold_factor','var') || isempty(threshold_factor)
    threshold_factor = 5;
end

if ~ exist('wndw','var') || isempty(wndw)
    wndw = [10e-3,50e-3];
end

if ~ exist('min_iei','var') || isempty(min_iei)
    min_iei = 20e-3;
end

dt = diff(t(1:2));
n = size(I,1);

event_times = cell(n,1);
amplitudes = cell(n,1);
tau_decay = cell(n,1);
EPSC = cell(n,1);
RMSE = cell(n,1);
suspicious = cell(n,1);

N = 2;
Rp = 0.1;
Rs = 40;
% band = [0.2,1000];
band = [1,1000];
[b,a] = ellip(N,Rp,Rs,band*2*dt);
g = fittype('A*exp(-t/tau)','indep','t','coeff','tau','problem','A');

% Iold = I;
for i=1:n
    I(i,:) = filtfilt(b,a,detrend(I(i,:)'));
end
discarded_time = [50e-3,50e-3];
idx = t>discarded_time(1) & t<t(end)-discarded_time(2);
% discarded_time = 1/band(1);
% idx = t<t(end)-discarded_time;
I = I(:,idx);
t = t(idx);

threshold = zeros(n,1);

for i=1:n
    s = computeThreshold(I(i,:));
    threshold(i) = -threshold_factor*s;

    if min(I(i,:)) >= threshold(i)
        continue
    end
    try
%         [~,locs] = findpeaks(-I(i,:), 'MinPeakHeight', -threshold(i), 'MinPeakDistance', round(50e-3/diff(t(1:2))));
        [~,locs] = findpeaks(-I(i,:), 'MinPeakHeight', -threshold(i), 'MinPeakDistance', round(min_iei/diff(t(1:2))));
    catch
        continue
    end
    locs = locs(I(i,locs) > -1000);
    if isempty(locs)
        continue;
    end
    ncrossings = length(locs);
    if verbose
        if ncrossings == 1
            fprintf(1, 'There is 1 threshold crossing.\n');
        else
            fprintf(1, 'There are %d threshold crossings.\n', ncrossings);
        end
    end
    event_times{i} = t(locs);
    amplitudes{i} = I(i,locs);
    tau_decay{i} = nan(1,ncrossings);
    suspicious{i} = false(1,ncrossings);
    RMSE{i} = inf(1,ncrossings);
    [~,EPSC{i}] = ETSA(t,I(i,:),t(locs),wndw);
    T = (0:size(EPSC{i},2)-1) * dt - wndw(1);
    for j=1:ncrossings
        if j < ncrossings
            idx = find(T>=0 & T<=min(15e-3,t(locs(j+1))-2e-3-t(locs(j))));
        else
            idx = find(T>=0 & T<=min(15e-3,t(end)-t(locs(end))));
        end
        try
            model = fit(T(idx)',EPSC{i}(j,idx)',g,'StartPoint',2e-3,'problem',EPSC{i}(j,idx(1)));
            tau_decay{i}(j) = model.tau;
            RMSE{i}(j) = sqrt(sum((EPSC{i}(j,idx)'-model(T(idx))).^2)) / length(idx);
            if j == 1
                suspicious{i}(j) = abs(mean(EPSC{i}(j,T<-2e-3))) > 3*s;
            else
                suspicious{i}(j) = abs(mean(EPSC{i}(j,T<-2e-3))) > 3*s & diff(t(locs(j-1:j))) > wndw(1);
            end
        catch
        end
        if verbose
            if ~ suspicious{i}(j)
                fprintf(1, '.');
            else
                fprintf(1, '*');
            end
            if mod(j,50) == 0 || j == ncrossings
                fprintf(1, '\n');
            end
        end
        
%         if suspicious{i}(j)
% %         if suspicious{i}(j) || (abs(mean(EPSC{i}(j,T<-2e-3))) > 3*s && diff(t(locs(j-1:j))) <= wndw(1))
%         clf;
%         hold on;
%         plot(T+event_times{i}(j),EPSC{i}(j,:),'k','LineWidth',1);
%         plot(T(idx)+event_times{i}(j),model(T(idx)),'r','LineWidth',1);
%         plot(T([1,end])+event_times{i}(j),s*3+[0,0],'r--','LineWidth',1);
%         plot(T([1,end])+event_times{i}(j),-s*3+[0,0],'r--','LineWidth',1);
%         plot(event_times{i}(j)+[-wndw(1),0],mean(EPSC{i}(j,T<0))+[0,0],'b--','LineWidth',2);
%         title(sprintf('\\tau_{decay} = %.1f ms - suspicious = %d',model.tau*1e3,suspicious{i}(j)));
%         pause;        
%         keyboard
%         end

    end
end
warning('ON', generatemsgid('noPeaks'));
Ifiltered = I;
