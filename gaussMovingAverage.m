function z = gaussMovingAverage(x, y, sigma, dx)
% GAUSSMOVINGAVERAGE computes a moving average using a Gaussian kernel.
% 
% z = gaussMovingAverage(x, y, sigma, dx)
%
% Arguments:
%  x - the independent variable.
%  y - the dependent variable, usually corrupted with noise.
%  sigma - the width of the kernel.
%  dx - kernel sampling step.
% 
% Returns:
%  z - the smoothed version of y. It hase the same length as y.
% 
% Example:
%  N = 50000;
%  x = linspace(0,6*pi,N);
%  y = sin(x) + randn(size(x));
%  z = gaussMovingAverage(x,y,0.25);
%  plot(x,y,'.k',x,sin(x),'g',x,z,'r');

% Author: Daniele Linaro - October 2011

if ~ exist('dx','var')
    dx = diff(x(1:2));
end
xk = -10*sigma : dx : 10*sigma;
k = 1/(sqrt(2*pi)*sigma) * exp(-xk.^2/(2*sigma^2));
k = k / sum(k);
L = length(xk);
lag = floor(L/2);
z = conv(y,k);
z = z(lag+mod(L,2):end-lag);
