function [Ipeak,Iss,V] = extractPeakAndSteadyStateCurrents(t,I,VLED,t0,stimdur)
% [Ipeak,Iss,V] = extractPeakAndSteadyStateCurrents(t,I,VLED,t0,stimdur)

V = unique(VLED);
nV = length(V);
Ipeak = zeros(nV,1);
Iss = zeros(nV,1);

window = 0.2;
ss = find(t > t0+stimdur-window & t < t0+stimdur);
peak = find(t > t0 & t<t0+10e-3);
baseline = t < t0-window;
mpd = round(3e-3/diff(t(1:2)));

for i=1:nV
    Im = mean(I(VLED == V(i),:),1);
    Im0 = mean(Im(baseline));
    Iss(i) = Im0 - mean(Im(ss));
%     [~,locs] = findpeaks(-Im,'MinPeakHeight',-mean(Im(ss)),'MinPeakDistance',mpd);
%     Ipeak(i) = Im0 - Im(locs(1));
    [m,j] = min(Im(peak));
    Ipeak(i) = Im0 - m;
%     clf;
%     hold on;
%     plot(t,Im,'k');
%     plot(t(peak(j)),m,'mo');
%     plot(t(ss([1,end])),mean(Im(ss))+[0,0],'r--');
%     pause
end
