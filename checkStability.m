function varargout = checkStability(dirName,doPlot)
% [passive,kernels,spikes] = checkStability(dirName,doPlot)

if ~ exist('dirName','var')
    dirName = '.';
elseif ~ exist(dirName,'dir')
    error('%s: no such directory.', dirName);
end

if ~ exist('doPlot','var')
    doPlot = 1;
end

% whether to compute active properties
active = 0;
if nargout == 3
    active = 1;
end

h5Files = findFiles(dirName, '.h5');
nh5 = length(h5Files);
files = findFiles(dirName, '_kernel.dat');
kernelFiles = {};
for f=files(:)'
    try
        Ke = load(f{1});
        kernelFiles = [kernelFiles ; f];
    catch
    end
end
Ke = cellfun(@(x) load(x), kernelFiles, 'UniformOutput', false);
nk = length(kernelFiles);
kernels.secs = zeros(1,nk);
for k=1:nk
    [~,name] = fileparts(kernelFiles{k});
    kernels.secs(k) = datevec(name(1:14),'yyyymmddHHMMSS') * [0;0;0;3600;60;1];
end
kernels.Re = cellfun(@(x) sum(x), Ke) * 1e-6;

passive.secs = zeros(1,nh5);
passive.Vrest = zeros(1,nh5);
passive.tau.values = zeros(1,nh5);
passive.tau.confidence = zeros(2,nh5);
passive.Rin = zeros(1,nh5);
passive.has_rebound = zeros(1,nh5);
passive.cnt = 0;

if active
    spikes.secs = zeros(1,nh5);
    spikes.thresh.mean = zeros(1,nh5);
    spikes.thresh.std = zeros(1,nh5);
    spikes.thresh.sem = zeros(1,nh5);
    spikes.cnt = 0;
end

for k=1:nh5
    fprintf(1, '.');
    if mod(k,60) == 0
        fprintf(1, '\n');
    end

    if ~isempty(strfind(h5Files{k},'/correlations/'))
        continue;
    end

    [~,name] = fileparts(h5Files{k});
    secs = datevec(name(1:14),'yyyymmddHHMMSS') * [0;0;0;3600;60;1];

    % load the file
    [entities,info] = loadH5Trace(h5Files{k});
    stim = [];
    Vm = [];
    I = [];
    switch info.version
        case 0
            for ii=1:length(entities)
                if (entities(ii).id == 1 || entities(ii).id == 2) && ...
                    (isempty(entities(ii).metadata) || size(entities(ii).metadata,1) == 1)
                    Vm = entities(ii).data;
                    if isempty(entities(ii).metadata) % no kernel present
                        compensate = 1;
                    else
                        compensate = 0;
                    end
                elseif size(entities(ii).metadata,2) == 12 || ...
                        size(entities(ii).metadata,2) == 13
                    I = entities(ii).data;
                    stim = entities(ii).metadata;
                end
            end
        case 1
            for ii=1:length(entities)
                if strcmp(entities(ii).units,'mV')
                    Vm = entities(ii).data;
                    if isempty(entities(ii).metadata) % no kernel present
                        compensate = 1;
                    else
                        compensate = 0;
                    end
                elseif size(entities(ii).metadata,2) == 12 || ...
                        size(entities(ii).metadata,2) == 13
                    I = entities(ii).data;
                    stim = entities(ii).metadata;
                end
            end
        case 2
            for ii=1:length(entities)
                if strcmp(entities(ii).units,'mV') &&...
                        (strcmp(entities(ii).name,'AnalogInput') || ...
                        strcmp(entities(ii).name,'RealNeuron'))                        
                    Vm = entities(ii).data;
                    if isempty(entities(ii).metadata) % no kernel present
                        compensate = 1;
                    else
                        compensate = 0;
                    end
                elseif strcmp(entities(ii).name,'Waveform') && ...
                    (strcmp(entities(ii).units, 'pA') || ...
                     strcmp(entities(ii).units, 'N/A')) && ...
                    (size(entities(ii).metadata,2) == 12 || ...
                     size(entities(ii).metadata,2) == 13)
                    I = entities(ii).data;
                    stim = entities(ii).metadata;
                end
            end
        otherwise
            error('Version not supported.');
    end
    
    if ~isempty(I) && compensate
        [~,idx] = min(abs(kernels.secs - secs));
        Vm = AECoffline(Vm,I,Ke{idx});
    end
    t = (1:length(Vm)) * info.dt;

    %%% PASSIVE PROPERTIES - start
    
    % check whether it contains the stability preamble 
    if ~isempty(stim) && size(stim,1) >= 4 && size(stim,2) >= 3 && ...
            all(stim(2:4,1) == [0.01;0.5;0.6])
        stimtimes = cumsum(stim(1:4,1));
        stimamps = stim(1:4,3);
        passive.cnt = passive.cnt+1;
        passive.secs(passive.cnt) = secs;
        
        % resting membrane potential
        passive.Vrest(passive.cnt) = mean(Vm(t < stimtimes(1)));
        
        % membrane time constant
        try
            f = fitExp(Vm(t > stimtimes(2) & t < stimtimes(2)+0.15), 1/info.dt, 2);
        catch
            fprintf(1, 'Error with file [%s].\n', h5Files{k})
            return
        end
        taus = [f.F.b0,f.F.b1];
        ci = confint(f.F);
        ci = ci(:,3:4);
        [m,i] = max(taus);
        passive.tau.values(passive.cnt) = m * 1e3;
        passive.tau.confidence(:,passive.cnt) = ci(:,i) * 1e3;
        
        % input resistance
        passive.Rin(passive.cnt) = (mean(Vm(t > stimtimes(4)-0.3 & t < stimtimes(4))) - ...
            passive.Vrest(passive.cnt)) / stimamps(4) * 1e3;

        % see whether the cell had a rebound spike after the
        % hyperpolarizing step of current
        if max(Vm(t>stimtimes(4)&t<stimtimes(4)+0.3)) > -20
            passive.has_rebound(passive.cnt) = 1;
        end
    end
    
    %%% PASSIVE PROPERTIES - end
    
    %%% ``ACTIVE'' PROPERTIES - start
    if active
        [~,vthresh] = extractAPThreshold(t,Vm);
        if ~ isempty(vthresh{1})
            spikes.cnt = spikes.cnt + 1;
            spikes.secs(spikes.cnt) = secs;
            spikes.thresh.mean(spikes.cnt) = mean(vthresh{1});
            spikes.thresh.std(spikes.cnt) = std(vthresh{1});
            spikes.thresh.sem(spikes.cnt) = ...
                spikes.thresh.std(spikes.cnt) / sqrt(length(vthresh{1}));
        end
    end
    %%% ``ACTIVE'' PROPERTIES - end
end

fprintf(1, '\n');

%%% passive properties
passive.secs = passive.secs(1:passive.cnt);
passive.Vrest = passive.Vrest(1:passive.cnt);
passive.tau.values = passive.tau.values(1:passive.cnt);
passive.tau.confidence = passive.tau.confidence(:,1:passive.cnt);
passive.Rin = passive.Rin(1:passive.cnt);
passive.has_rebound = passive.has_rebound(1:passive.cnt);
[passive.secs,idx] = sort(passive.secs);
passive.Vrest = passive.Vrest(idx);
passive.tau.values = passive.tau.values(idx);
passive.tau.confidence = passive.tau.confidence(:,idx);
passive.Rin = passive.Rin(idx);
passive.has_rebound = passive.has_rebound(idx);
passive.Cm = (passive.tau.values*1e-3) ./ (passive.Rin*1e6) * 1e12;
passive.mins = passive.secs/60;
passive = rmfield(rmfield(passive,'secs'),'cnt');

%%% kernels
[kernels.secs,idx] = sort(kernels.secs);
kernels.mins = kernels.secs/60;
kernels.Re = kernels.Re(idx);
kernels = rmfield(kernels,'secs');

%%% active properties
if active
    spikes.secs = spikes.secs(1:spikes.cnt);
    spikes.thresh.mean = spikes.thresh.mean(1:spikes.cnt);
    spikes.thresh.std = spikes.thresh.std(1:spikes.cnt);
    spikes.thresh.sem = spikes.thresh.sem(1:spikes.cnt);
    [spikes.secs,idx] = sort(spikes.secs);
    spikes.thresh.mean = spikes.thresh.mean(idx);
    spikes.thresh.std = spikes.thresh.std(idx);
    spikes.thresh.sem = spikes.thresh.sem(idx);
    spikes.mins = spikes.secs/60;
    spikes = rmfield(rmfield(spikes,'secs'),'cnt');
end

min0 = min([kernels.mins(1),passive.mins(1)]);
if active
    min0 = min([min0,spikes.mins(1)]);
end
passive.mins = passive.mins - min0;
varargout{1} = passive;
kernels.mins = kernels.mins - min0;
varargout{2} = kernels;
if active
    spikes.mins = spikes.mins - min0;
    varargout{3} = spikes;
end

if doPlot
    tend = max([passive.mins(end),kernels.mins(end)]);
    if active
        tend = max([tend,spikes.mins(end)]);
    end
    %%% Passive properties
    nplots = 4;
    figure;
    subplot(nplots,1,1);
    set(gca,'TickDir','Out','LineWidth',0.8,'NextPlot','Add');
    Vmean = mean(passive.Vrest);
    Vstd = std(passive.Vrest);
    plot([0,tend],Vmean+[0,0],'r','LineWidth',2);
    plot(passive.mins,passive.Vrest,'ok','MarkerSize',4,'MarkerFaceColor','k');
    axis([0,tend,Vmean-5*Vstd,Vmean+5*Vstd]);
    ylabel('V_{rest} (mV)');
    subplot(nplots,1,2);
    set(gca,'TickDir','Out','LineWidth',0.8,'NextPlot','Add');
    Rmean = nanmean(passive.Rin);
    Rstd = nanstd(passive.Rin);
    plot([0,tend],Rmean+[0,0],'r','LineWidth',2);
    plot(passive.mins,passive.Rin,'ok','MarkerSize',4,'MarkerFaceColor','k');
    axis([0,tend,Rmean-5*Rstd,Rmean+5*Rstd]);
    ylabel('R_{in} (MOhm)');
    subplot(nplots,1,3);
    set(gca,'TickDir','Out','LineWidth',0.8,'NextPlot','Add');
    idx = find(passive.tau.values < 50);
    taumean = mean(passive.tau.values(idx));
    taustd = std(passive.tau.values(idx));
    plot([0,tend],taumean+[0,0],'r','LineWidth',2);
    plot(passive.mins(idx),passive.tau.values(idx),'ok','MarkerSize',4,'MarkerFaceColor','k');
    axis([0,tend,taumean-5*taustd,taumean+5*taustd]);
    ylabel('\tau (ms)');
    subplot(nplots,1,4);
    set(gca,'TickDir','Out','LineWidth',0.8,'NextPlot','Add');
    Rsmean = mean(kernels.Re);
    Rsstd = std(kernels.Re);
    plot([0,tend],40+[0,0],'m--','LineWidth',1);
    plot([0,tend],Rsmean+[0,0],'r','LineWidth',2);
    plot(kernels.mins,kernels.Re,'ok','MarkerSize',4,'MarkerFaceColor','k');
    axis([0,tend,max(0,Rsmean-3*Rsstd),max(Rsmean+3*Rsstd,45)]);
    ylabel('Rs (MOhm)');
    xlabel('Minutes');
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,6,5]);
    print('-depsc2','passive_properties.eps');

    %%% Active properties
    if active
        figure;
        hold on;
        threshmean = mean(spikes.thresh.mean);
        threshstd = std(spikes.thresh.mean);
        plot([0,tend], threshmean+[0,0], 'r', 'LineWidth', 2);
        errorbar(spikes.mins, spikes.thresh.mean, spikes.thresh.std, ...
            'ok','MarkerSize',4,'MarkerFaceColor','k');
        axis([0,tend,threshmean-5*threshstd,threshmean+5*threshstd]);
        ylabel('V_{thresh}, mean +- std (mV)');
        xlabel('Minutes');
        set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,8,4]);
        print('-depsc2','active_properties.eps');
    end
    
end
