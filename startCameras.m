vid=videoinput('linuxvideo', 1);
preview(vid)
vid2=videoinput('linuxvideo', 2);
preview(vid2)

%frame = getsnapshot(vid2);

%figure()
%image(frame)

%% cam 1
figure(99)
ax1 = axes();
ax2 = axes();
set(ax1,'position',[0,0,0.5,1])
set(ax2,'position',[0.5,0,0.5,1])

vid = videoinput('linuxvideo', 1);
vid.FramesPerTrigger = 1;
vid.ReturnedColorspace = 'rgb';
triggerconfig(vid, 'manual');
vidRes = get(vid, 'VideoResolution');
imWidth = vidRes(1);
imHeight = vidRes(2);
nBands = get(vid, 'NumberOfBands');
hImage = image(zeros(imHeight, imWidth, nBands), 'parent', ax1)
preview(vid, hImage);
 
% cam 2
vid2 = videoinput('linuxvideo', 2);
vid2.FramesPerTrigger = 1;
vid2.ReturnedColorspace = 'rgb';
triggerconfig(vid2, 'manual');
vidRes2 = get(vid2, 'VideoResolution');
imWidth2 = vidRes(1);
imHeight2 = vidRes(2);
nBands2 = get(vid2, 'NumberOfBands');
hImage2 = image(zeros(imHeight2, imWidth2, nBands2), 'parent', ax2)
preview(vid2, hImage2);
%
% prepare for capturing the image preview
start(vid); 
start(vid2);
% pause for 3 seconds to give our webcam a "warm-up" time
pause(3); 
% do capture!
trigger(vid);trigger(vid2);
%5 stop the preview
%stoppreview(vid);stoppreview(vid2);
%% get the captured image data and save it on capt1 variable
capt1 = getdata(vid);
% now write capt1 into file named captured.png
imwrite(capt1, 'captured.png');
% just dialog that we are done capturing
warndlg('Done!');
