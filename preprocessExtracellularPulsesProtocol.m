function preprocessExtracellularPulsesProtocol(directories, dur, labels)
% preprocessExtracellularPulsesProtocol(directories, dur, labels)

if ~ exist('directories','var') || isempty(directories)
    directories = arrayfun(@(x) x.name, dir('0*'),'UniformOutput',0);
    fprintf(1, 'Reading data from the following directories (in this order): [');
    for i=1:length(directories)-1
        fprintf(1, '%s], [', directories{i});
    end
    fprintf(1, '%s].\n', directories{end});
end
ndir = length(directories);

if ~ exist('dur','var') || isempty(dur)
    dur = [Inf,5*60,5*60,5*60];
end

if ~ exist('labels','var') || isempty(labels)
    if ndir == 4
        labels = {'','+ GABAzine','+ GABAzine + NBQX','+ GABAzine + NBQX + AP-V'};
    elseif ndir == 3
        labels = {'','+ GABAzine','+ GABAzine + NBQX + AP-V'};
    elseif ndir == 2
        labels = {'','+ CNQX + AP-V'};
    else
        labels = directories;
    end
end

cwd = pwd;
start = strfind(cwd,'201');
cell_id = cwd(start:start+10);

downsample_factor = 20;

Fc = 1000;      % cut-off frequency for the filter

% stimulus properties
stim = load([directories{1},'/pulses.stim']);
t0 = stim(1,1);
if stim(2,2) == 8
    stim_dur = stim(2,5)*1e-3;
    Tpulse = -1/stim(2,4);
    Npulses = stim(2,1)/Tpulse;
else
    stim_dur = stim(2,1)*1e-3;
    Tpulse = sum(stim(2:3,1));
    Npulses = length(find(stim(:,1) == stim_dur));
end
Fpulse = 1/Tpulse;

figure;
axes('Position',[0.05,0.05,0.9,0.9],'NextPlot','Add');
VV = [];
for i=1:ndir
    files = listH5Files(directories{i});
    T = str2double(files{2}(end-4:end-3)) - str2double(files{1}(end-4:end-3));
    if T < 0
        T = 60 + T;
    end
    n = max(20,ceil(dur(i)/T));
    if ~ isinf(n) && n < length(files)
        files = files(end-n:end);
    end
    [t,V,Vh] = loadH5TracesBis(files,1,2);
    if all(all(V>-10))
        tmp = Vh;
        Vh = V;
        V = tmp;
    end
    V0 = mean(V(:,t<t0),2);
%     V = V(V0 > -75 & V0 <= -60,:);
    Fs = 1/diff(t(1:2));
    N = Fs/Fc*2;
    padding = repmat(V(:,1),[1,N]);
    V = filterTrace(V,Fs,Fc,2);
    V(:,1:N) = padding;
    dt = diff(t(1:2));
    Vm = meanWithOffset(V(max(V,[],2) < -20,:),round(0.5/dt));
    Vm = removeArtifacts(Vm,Vh);
    VV = [VV ; Vm];
    max_V = max(Vm);
    if i == 1
        placeScaleBars(0.1,2,0.2,round(max_V),'200 ms',sprintf('%.0f mV',...
            round(max_V)),'k','LineWidth',2);
        text(t(end),max_V*1.2,cell_id,'HorizontalAlignment','Right','FontSize',13);
    else
        Vm = Vm - 1 + min_V - max_V;
    end
    plot(t(1:downsample_factor:end),Vm(1:downsample_factor:end),'k','LineWidth',1);
    min_V = min(Vm);
    text(t(end),max(Vm(end-1000:end))+0.05,labels{i},'HorizontalAlignment','Right','VerticalAlignment',...
        'Bottom','FontSize',10);
end

col = [.7,0,0];
for i=0:Npulses-1
    patch(t0+i*Tpulse+[0,stim_dur,stim_dur,0],min_V-1+[0,0,.5,.5],col,'EdgeColor',col);
end
if size(stim,1) == 5
    patch(sum(stim(1:3,1))+[0,stim_dur,stim_dur,0],min_V-3+[0,0,.5,.5],col,'EdgeColor',col);
end
axis tight;
axis off;

filename = sprintf('extracellular_pulses_%.0f_Hz',Fpulse);
sz = [7,ndir*1.25];
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz,'Color','w');
print('-dpdf',[filename,'.pdf']);

V = VV;
switch size(V,1)
    case 4
        dV = zeros(4,size(V,2));
        dV(1,:) = V(1,:) - V(2,:);  % GABA
        dV(2,:) = V(2,:) - V(4,:);  % AMPA+NMDA
        dV(3,:) = V(2,:) - V(3,:);  % AMPA
        dV(4,:) = V(3,:) - V(4,:);  % NMDA
    case 3
        dV = zeros(2,size(V,2));
        dV(1,:) = V(1,:) - V(2,:);  % GABA
        dV(2,:) = V(2,:) - V(3,:);  % AMPA+NMDA
    case 2
        dV = nan(2,size(V,2));
        dV(2,:) = V(1,:) - V(2,:);  % AMPA+NMDA
    otherwise
        keyboard
end

% dV = flipud(diff(flipud(V)));
idx = find(t>t0 & t<t0+(Npulses-1)*Tpulse+0.1);
area = [trapz(dV(:,idx),2); trapz(V(end,idx))]/Fs;

if size(dV,1) == 2
    col = [0,0,.8 ; .8,.4,0];
    lgnd = {'GABA','AMPA+NMDA'};
else
    col = [0,0,.8 ; .8,.4,0 ; .8,0,0 ; 0,.8,0];
    lgnd = {'GABA','AMPA+NMDA','AMPA','NMDA'};
end

figure;
axes('Position',[0.05,0.05,0.9,0.9],'NextPlot','Add');
hndl = zeros(size(dV,1),1);
for i=1:size(dV,1)
    hndl(i) = plot(t(1:downsample_factor:end),dV(i,1:downsample_factor:end),...
        'Color',col(i,:),'LineWidth',1);
end
axis tight;
yl = ylim;
axis([xlim,floor(yl(1)),ceil(yl(2))]);
yl = ylim;
m = yl(1);
M = yl(2);
if abs(m) > M
    placeScaleBars(t(end)-0.2,m+0.2,0.2,ceil(abs(m)/2),'200 ms',...
        sprintf('%.0f mV',ceil(abs(m)/2)),'k','LineWidth',2);
else
    placeScaleBars(t(1)+0.05,M-0.2-ceil(M/2),0.2,ceil(M/2),'200 ms',...
        sprintf('%.0f mV',ceil(M/2)),'k','LineWidth',2);
end
h = legend(hndl,lgnd,'Location','NorthEast');
set(h,'Box','Off');
axis off;
for i=1:length(area)-1
    text(0.05,m+i*(abs(m)+M)/20,sprintf('area = %.3f',area(end-i)),'FontSize',11,...
        'Color',col(end-i+1,:));
end
sz = [6,4];
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz,'Color','w');
print('-dpdf',[filename,'_individuals.pdf']);

dt = 1/Fs;
save([filename,'.mat'],'V','dV','dt','t0','T','Tpulse','Npulses','Fpulse',...
    'area','lgnd');

