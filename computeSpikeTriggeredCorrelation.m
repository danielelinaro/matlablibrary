function [stc_V,stc_I] = computeSpikeTriggeredCorrelation(t,V,I,features,t0,ttran,dur,window,do_plot)
% [stc_V,stc_I] = computeSpikeTriggeredCorrelation(t,V,I,features,t0,ttran,dur,window,DEBUG)

if ~ exist('do_plot','var')
    do_plot = 0;
end

dt = diff(t(1:2));
T = -window(1):dt:window(2);
stc_V = zeros(2,length(T));
stc_I = zeros(2,length(T));
for ref = [1,2]
    other = 1 + mod(ref,2);
    X = cell(2,1);
    Y = cell(2,1);
    events = cell(2,1);
    for i=1:length(features(ref).tp)
        idx = find(features(ref).tp{i} > t0+ttran+window(1) & ...
            features(ref).tp{i} < t0+dur-window(2));
        tp = features(ref).tp{i}(idx);
        tth = features(ref).tth{i}(idx);
        tend = features(ref).tend{i}(idx);
        if isempty(tp)
            continue;
        elseif isscalar(tp)
            events{ref} = tp;
        elseif tth(2)-tp(1) > window(2)
            events{ref} = tp(1);
        else
            events{ref} = [];
        end
        for j=2:length(tp)-1
            if tp(j)-tend(j-1) > window(1) && tth(j+1)-tp(j) > window(2)
                events{ref} = [events{ref} , tp(j)];
            end
        end
        idx = find(features(other).tp{i} > t0+ttran & ...
            features(other).tp{i} < t0+dur);
        tp = features(other).tp{i}(idx);
        tth = features(other).tth{i}(idx);
        tend = features(other).tend{i}(idx);
        idx = [];
        for j=1:length(events{ref})
            before = find(tp <= events{ref}(j), 1, 'last');
            after = find(tp >= events{ref}(j), 1, 'first');
            if (isempty(before) || isempty(after) || before ~= after) && ...
                    (isempty(before) || events{ref}(j)-tend(before) > window(1)) && ...
                    (isempty(after) || tth(after)-events{ref}(j) > window(2))
                idx = [idx ; j];
            end
        end
        if ~ isempty(idx)
            for j=1:2
                [~,x] = ETSA(t,V{j}(i,:),events{ref}(idx),window);
                X{j} = [X{j} ; x];
                [~,y] = ETSA(t,I{j}(i,:),events{ref}(idx),window);
                Y{j} = [Y{j} ; y];
            end
        end
    end

    stc_V(ref,:) = diag(corr(X{1},X{2}))';
    stc_I(ref,:) = diag(corr(Y{1},Y{2}))';

    if do_plot
        figure;
        axes('Position',[0.1,0.75,0.8,0.2],'NextPlot','Add');
        plot(T*1e3,mean(X{ref}),'k','LineWidth',1);
        plot(T*1e3,mean(X{other}),'Color',[.5,.5,.5],'LineWidth',2);
        hndl = legend('Reference','Other','Location','NorthEast');
        set(hndl,'Box','Off');
        set(gca,'TickDir','Out','XTick',[],'YTick',-100:10:60);
        axis tight;
        yl = ylim;
        axis([xlim,yl(1),-40]);
        ylabel('Voltage (mV)');
        axes('Position',[0.1,0.5,0.8,0.2],'NextPlot','Add');
        plot(T*1e3,mean(Y{ref}),'r');
        plot(T*1e3,mean(Y{other}),'Color',[1,.5,.5],'LineWidth',2);
        ylabel('Current (pA)');
        hndl = legend('Reference','Other','Location','NorthEast');
        set(hndl,'Box','Off');
        set(gca,'TickDir','Out','XTick',[],'YTick',-3000:100:2000);
        axis tight;
        yl = ylim;
        axis([xlim,min(mean(Y{other}))-200,yl(2)]);
        axes('Position',[0.1,0.1,0.8,0.35],'NextPlot','Add');
        plot(T*1e3,stc_V(ref,:),'k');
        plot(T*1e3,stc_I(ref,:),'r');
        hndl = legend('Voltage','Current','Location','SouthEast');
        set(hndl,'Box','Off');
        axis tight;
        axis([xlim,-0.05,0.6]);
        set(gca,'TickDir','Out','XTick',[-window(1)*1e3,0,20:20:window(2)*1e3],'YTick',0:.25:.5);
        xlabel('Time (ms)');
        ylabel('Correlation');
        set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,8],'PaperSize',[8,8]);
    end

end
