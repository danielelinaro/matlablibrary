function [phi_smooth,dphi_smooth,dphi_mean,dphi_std,h] = smoothPRC(phi,dphi,nbin,sigma,ax)
% [phi_smooth,dphi_smooth,dphi_mean,dphi_std,h] = smoothPRC(phi,dphi,nbin,sigma,ax)

edges = linspace(0,1,nbin+1);
phi_smooth = edges(1:end-1) + diff(edges(1:2))/2;
[~,bin] = histc(phi,edges);
dphi_mean = zeros(length(edges)-1,1);
dphi_std = zeros(length(edges)-1,1);
dphi_n = zeros(length(edges)-1,1);
for i=1:length(edges)-1
    dphi_mean(i) = mean(dphi(bin == i));
    dphi_std(i) = std(dphi(bin == i));
    dphi_n(i) = sum(bin == i);
end
dphi_smooth = gaussMovingAverage(phi_smooth,dphi_mean,sigma);

h = 0;

if ~ exist('ax','var')
    ax = gca;
end

if ax > 0
    orange = [1,.5,0];
    magenta = [1,0,.5];
    grey = [.6,.6,.6];
    set(ax,'NextPlot','Add');
    errorbar(phi_smooth,dphi_mean,dphi_std,'o','Color',orange,...
        'MarkerFaceColor',orange,'LineWidth',2);
    plot(phi,dphi,'.','Color',grey,'MarkerSize',3);
    h = plot(phi_smooth,dphi_smooth,'Color',magenta, 'LineWidth', 4);
    axis([0,1,min(dphi_mean-3*dphi_std),max(dphi_mean+3*dphi_std)]);
end


