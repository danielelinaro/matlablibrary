function out = FDM(A, U, F, D, events)
%
% Frequency Dependend Modulation of synaptic "efficacy"
%
% w = FDM(A, U, F, D, events)
%
% This function returns the output observable (e.g. amplitude, or slope)
% upon the activation of a synapse with short-term plasticity..
%
% Parameters: A --> amplitude [a.u.]
%             U --> probability of release (within the range [0 ; 1])
%             F --> recovery from facilitation [s]
%             D --> recovery from depression [s]
%
%       'events' --> arrays containing the activation times [s]
%
% March 2009 - Michele Giugliano, michele.giugliano@ua.ac.be
%

nev = length(events);
out = zeros(1,nev);
out(1) = A * U;
events = sort(events);  % All events [s],  must be increasing in time..

R = 1.;                 % Initialization..
u = U;                  % Initialization..

for k=2:nev                 % Starting from the second event..
    delta = events(k) - events(k-1);       % Let's take care of the STP..
    R = (R * (1. - u) - 1.) * exp(-delta/D) + 1; % STP..
    u =  u * (1. - U) * exp(-delta/F) + U;       % STP..
    out(k) = A * R * u;  % Steep increase and exp decay...
end
