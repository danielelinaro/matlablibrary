function extract_unitary_epsp(fname_pre, fname_post)
%
% extract_unitary_epsp(fname_pre, fname_post)
%
% fname_pre: name of the ASCII  file, containing the PREsynaptic membrane
%         potential trace corresponding to the 30 Hz regular stim. protocol
%
% fname_post: name of the ASCII  file, containing the POSTsynaptic membrane
%         potential trace corresponding to the 30 Hz regular stim. protocol
%
%
% e.g. extract_unitary_epsp('30Hz_pre.txt','30Hz_post.txt')
%
% The routine assumes that the sampling frequency is 10 kHz.
%
%
% The routine produces as an output a *.mat file (i.e. 'unitaryEPSP.mat'), 
% containing the following variables:
%
%
% time : time axis [s]
% vpre : presynaptic membrane potential trace [mV]
% vpost: postsynaptic membrane potential trace [mV], upon removal of the
%        offset (i.e. the resting membrane potential)
% Epost: the resting membrane potential [mV]
%
% peaks: a series of indexes, corresponding to the spike times
% events: the elements returned by 'time(peaks)'
% 
% x:    time axis of the unitary EPSP extracted
% y:    waveform of the unitary EPSP extracted
% modelEPSP: model best-fitting the unitary EPSP extracted
% pars: best fit parameters for the double exponential that fits the 
%
%

global x y;

srate  = 10000.;                            % Sampling frequency [Hz] 
dt     = 1./srate;                          % Sampling interval [s]

vpre   = load(fname_pre);                   % presynaptic AP-train voltage
peaks  = extract_peaks(vpre, 0.);           % indexes of "peaks"
time   = dt * (0:(length(vpre)-1));         % time-axis, [s]
events = time(peaks);                       % actual peak-times, in s

vpost  = load(fname_post);                  % postsynaptic trace voltage
Epost  = mean(vpost(1:peaks(1)));           % resting Vm [mV]
vpost  = vpost - Epost;                     % resting Vm removed [mV]

% Let's now isolate the waveform corresponding to the 'recovery' EPSP
% i.e. it is the one corresponding to the last elicited APs

last_peak = peaks(end);           % last presynaptic activation (index)
last_tsp  = events(end);          % last presynaptic activation (time [s])
endpoint  = peaks(end) + fix(0.2 * srate);  % 200msec after the start (indx)

indexrange = last_peak:endpoint;
y      = vpost(indexrange) - mean(vpost(last_peak-100:last_peak));    % unitary EPSP waveform,
EPSPamplitude = max(y);
y      = y ./ EPSPamplitude;          % ...normalized to its peak amplitude
x      = time(indexrange) - time(last_peak); % corresponding time coordinates [s]

% Let's now fit a double exponential to such a 'recovery' EPSP

pars  = [0.001 0.01 0.001];     % initial conditions for the rise time, the
                                % decay time, and the axonal propagation
                                % delay.
                                
tmp = optimset('TolX',1e-12, 'TolFun', 1e-12, 'Display','off');
                                
for k=1:5,                      % Let's repeat it few times 
 pars = fminsearch(@cost, pars .* (1 + 0.1*rand(size(pars))), tmp);
%  plot(x, y, x, doubleexp(pars(1), pars(2), pars(3), x));
%  drawnow;
end


modelEPSP = doubleexp(pars(1), pars(2), pars(3), x);

% Let's now save all the information on disk


outfname = 'unitaryEPSP.mat';
save(outfname, 'time', 'vpre', 'vpost', 'Epost', 'peaks', 'events', 'x', 'y', 'modelEPSP', 'pars');



% Let's now visualize the result of the fit procedure, restoring amplitudes
% and offsets for the sake of illustration only.

figure(1); set(gca, 'Color', [1 1 1]);
P         = plot(x*1000., (EPSPamplitude*y + Epost)*1000., 'k', x*1000, (EPSPamplitude*modelEPSP + Epost)*1000., 'r');
hold on;
YLIM = get(gca, 'YLim');
L = line([0 0] + pars(3) * 1000., YLIM);
set(L, 'Color', [0 0 0], 'LineStyle', '--');

set(P, 'LineWidth', 2);
title('Unitary EPSP');
legend([P(1) P(2) L], 'Measured EPSP', 'Model EPSP', 'AP-2-EPSP delay');
xlabel('time [ms]', 'FontName', 'Arial', 'FontSize', 20); 
ylabel('{V_{post} [mV]}', 'FontName', 'Arial', 'FontSize', 20);
set(gca, 'XLim', [min(x), max(x)]*1000);
set(gca, 'FontName', 'Arial', 'FontSize', 20);

print(gcf, '-dpng', '-loose', 'unitaryEPSP.png');
end



function out = extract_peaks(data, threshold)
% peaks_indexes = extract_peaks(data_waveform, threshold)
%
% note: this is based on (positive slope) threshold crossing detections..
%
% Simplified version - this extracts the +peaks, returning just the indexes
% corresponding to those maxima which are between two successive threshold
% crossings..
%
% Aug 22nd 2007 - Michele Giugliano
%

N    = length(data);        % Number of points in the data waveform..
bool = 0;                   % Useful boolean variable for threshold-cross
out  = [];                  % Vector containing first the indexes of 
                            % threshold crossings and later of the peaks..

for i=1:N,                  % Detection of threshold crossings..
 if ((data(i) > threshold) & (~bool)), bool = 1; out = [out, i]; end % if
 if ((data(i) < threshold) & (bool)),  bool = 0;                 end % if
end % for i

M    = length(out);         % Number of (positive slope) threshold crossing
peak = [];                  % Temporary data structure..

for k=1:M,                  
 istart = out(k);
 if (k == M), istop = N; else istop = out(k+1); end;
 chunk  = data(istart:istop);   % An index interval is set for each event..
 tmq    = find(chunk == max(chunk));
 peak   = [peak, (istart + tmq(1) - 1)];
end

out = peak;            % Indexes corresponding to peaks only are returned.
end



function out = doubleexp(t1, t2, d, t)
%
% out = doubleexp(t1, t2, d, t)
%
% t1 [s] : rising time constant
% t2 [s] : decaying time constant
% d  [s] : delay
% t  [s] : time axis
%
% out --> double exponential, with max amplitude == 1,
% regardless of the combination {t1, t2}..
% 
% Sep 2007 - Michele Giugliano (mgiugliano@gmail.com)
%

% Let's consider the function f(t) = (exp(-t/t1)-exp(-t/t2))/(t1-t2)
% I compute the time 'tt' at which the first derivative of f(t) is zero
tt = (t1*t2/(t2-t1)) * log(t2/t1);          

% I then compute f(tt)
aa = (exp(-tt/t1)-exp(-tt/t2))/(t1-t2);

% I appropriately prepare a normalization prefactor.. (so it has unitary
% peak amplitude).
A  = 1. / (aa * (t1 - t2));

% Then, the actual function as an output
out =  A * ( exp(-(t-d)/t1) - exp(-(t-d)/t2) ) .* (t >= d);
end




function out = cost(pars)
%
%
global x y;

t1 = pars(1);
t2 = pars(2);
d  = pars(3);

model = doubleexp(t1, t2, d, x);
out   = norm(y - model)/length(x);
end

