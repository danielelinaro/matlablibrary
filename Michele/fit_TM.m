function fit_TM(fname_pre, fname_post)
%
% fit_TM(fname_pre, fname_post)
%
% fname_pre: name of the ASCII  file, containing the PREsynaptic membrane
%         potential trace corresponding to the 30 Hz regular stim. protocol
%
% fname_post: name of the ASCII  file, containing the POSTsynaptic membrane
%         potential trace corresponding to the 30 Hz regular stim. protocol
%
%
% e.g. fit_TM('30Hz_pre.txt','30Hz_post.txt')
%
% The routine assumes that the file 'unitaryEPSP.mat' is currently
% available and contains already the best-fit EPSP parameters.
%
close all;
global time p events peaks APEAKS;

srate  = 10000.;                            % Sampling frequency [Hz] 
dt     = 1./srate;                          % Sampling interval [s]

A    = load('unitaryEPSP.mat');
p    = A.pars;
clear A;


extract_presynaptic_spike_times(fname_pre);
tmp    = load('spike_times.mat');
vpre    = tmp.vpre;
peaks  = tmp.peaks;
time   = tmp.time;
events = tmp.events;
clear tmp;

vpost  = load(fname_post);                  % postsynaptic trace voltage
Epost  = mean(vpost(1:peaks(1)));           % resting Vm [mV]
vpost  = vpost - Epost;                     % resting Vm removed [mV]


% Let's extract the amplitudes of the EPSPs, without deconvolving them

ppeaks = [peaks, length(time)];
APEAKS = zeros(size(peaks));
for k=1:length(peaks),
 tmp       = vpost(ppeaks(k):ppeaks(k+1));
 APEAKS(k) = max(tmp);
end

% Let's now best-fit the TM model

pars = [0.3 0.05 0.1];      % Initial conditions for A, U, and tau_rec
% [tmp nnn] = modeltrain(pars);
% plot(time, vpost, time, tmp);

tmp = optimset('TolX',1e-12, 'TolFun', 1e-12, 'Display','off');
for k=1:3,
 pars = fminsearch(@train_cost, pars.* (1 + 0.1*rand(size(pars))), tmp);
end

[modelTRAIN nnn] = modeltrain(pars);
plot(time, vpost, time, modelTRAIN);


figure(1);
set(gca, 'Color', [1 1 1]);
subplot(2,1,1);
Ppre = plot(time*1000., vpre*1000., 'k', time(peaks)*1000., vpre(peaks)*1000., 'ro');
xlabel('time [ms]', 'FontName', 'Arial', 'FontSize', 20); 
ylabel('presynaptic [mV]', 'FontName', 'Arial', 'FontSize', 20);
set(gca, 'FontName', 'Arial', 'FontSize', 20);
set(gca, 'YLim', [-80 150]);

subplot(2,1,2);
Ppost = plot(time*1000., modelTRAIN*1000., 'r', time*1000., vpost*1000., 'k');
YLIM  = get(gca, 'YLim');
YLIM  = [-0.1*YLIM(2) YLIM(2)];
xlabel('time [ms]', 'FontName', 'Arial', 'FontSize', 20); 
ylabel('postsynaptic [mV]', 'FontName', 'Arial', 'FontSize', 20);
%title('TM model best fit');
set(gca, 'FontName', 'Arial', 'FontSize', 20);

subplot(2,1,2);
legend([Ppost(1) Ppost(2)], 'Model best-fit','Postsynaptic response');
set([Ppre(1) Ppost(1) Ppost(2)], 'LineWidth', 2);
set([Ppost(1)], 'LineWidth', 3);
set(gca, 'YLim', YLIM);

hold on;
for k=1:length(events)
 L = line(events(k) * [1 1], YLIM);
 set(L, 'Color', [0 0 0], 'LineStyle', '--');
end
hold off;


print(gcf, '-dpng', '-loose', 'best_fit.png');

end



function out = train_cost(pars)
%
%
global APEAKS;
 [Vmodel MPEAKS] = modeltrain(pars);
 out = norm(MPEAKS - APEAKS);
end



function [Vmodel MPEAKS] = modeltrain(pars)
%
%
global time p events peaks;

 A = abs(pars(1));
 U = abs(pars(2));
 D = abs(pars(3));
 if (U>1), U =1; end

 Vmodel = zeros(size(time));
 amply  = FDM(A, U, 0.0001, D, events);                    % Tsodyks-Markram, 1997
 for k=1:length(events),
   tmq    = [0*time(1:peaks(k)-1), amply(k) * doubleexp(p(1), p(2), p(3), time(peaks(k):end)-time(peaks(k)))];
   Vmodel = Vmodel + tmq; 
 end
 
 ppeaks = [peaks, length(time)];
 MPEAKS = zeros(size(peaks));
 for k=1:length(peaks),
  tmp       = Vmodel(ppeaks(k):ppeaks(k+1));
  MPEAKS(k) = max(tmp);
 end 
end




function out = doubleexp(t1, t2, d, t)
%
% out = doubleexp(t1, t2, d, t)
%
% t1 [s] : rising time constant
% t2 [s] : decaying time constant
% d  [s] : delay
% t  [s] : time axis
%
% out --> double exponential, with max amplitude == 1,
% regardless of the combination {t1, t2}..
% 
% Sep 2007 - Michele Giugliano (mgiugliano@gmail.com)
%

% Let's consider the function f(t) = (exp(-t/t1)-exp(-t/t2))/(t1-t2)
% I compute the time 'tt' at which the first derivative of f(t) is zero
tt = (t1*t2/(t2-t1)) * log(t2/t1);          

% I then compute f(tt)
aa = (exp(-tt/t1)-exp(-tt/t2))/(t1-t2);

% I appropriately prepare a normalization prefactor.. (so it has unitary
% peak amplitude).
A  = 1. / (aa * (t1 - t2));

% Then, the actual function as an output
out =  A * ( exp(-(t-d)/t1) - exp(-(t-d)/t2) ) .* (t >= d);
end
