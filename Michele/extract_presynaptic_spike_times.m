function extract_presynaptic_spike_times(fname_pre)
%
% extract_presynaptic_spike_times(fname_pre)
%
% fname_pre: name of the ASCII  file, containing the PREsynaptic membrane
%         potential trace corresponding to the 30 Hz regular stim. protocol
% 
% The routine produces as an output a *.mat file (i.e. 'spike_times.mat'), 
% containing the following variables:
%
% time : time axis [s]
% vpre : presynaptic membrane potential trace [mV]
%
% peaks: a series of indexes, corresponding to the spike times
% events: the elements returned by 'time(peaks)'
%
% e.g. exctract_presynaptic_spike_times('30Hz_pre.txt')
%
% The routine assumes that the sampling frequency is 10 kHz.
%

srate  = 10000.;                            % Sampling frequency [Hz] 
dt     = 1./srate;                          % Sampling interval [s]

vpre   = load(fname_pre);                   % presynaptic AP-train voltage
peaks  = extract_peaks(vpre, -0.01);           % indexes of "peaks"
time   = dt * (0:(length(vpre)-1));         % time-axis, [s]
events = time(peaks);                       % actual peak-times, in s

outfname = 'spike_times.mat';
save(outfname, 'time', 'vpre', 'peaks', 'events');

end




function out = extract_peaks(data, threshold)
% peaks_indexes = extract_peaks(data_waveform, threshold)
%
% note: this is based on (positive slope) threshold crossing detections..
%
% Simplified version - this extracts the +peaks, returning just the indexes
% corresponding to those maxima which are between two successive threshold
% crossings..
%
% Aug 22nd 2007 - Michele Giugliano
%

N    = length(data);        % Number of points in the data waveform..
bool = 0;                   % Useful boolean variable for threshold-cross
out  = [];                  % Vector containing first the indexes of 
                            % threshold crossings and later of the peaks..

for i=1:N,                  % Detection of threshold crossings..
 if ((data(i) > threshold) & (~bool)), bool = 1; out = [out, i]; end % if
 if ((data(i) < threshold) & (bool)),  bool = 0;                 end % if
end % for i

M    = length(out);         % Number of (positive slope) threshold crossing
peak = [];                  % Temporary data structure..

for k=1:M,                  
 istart = out(k);
 if (k == M), istop = N; else istop = out(k+1); end;
 chunk  = data(istart:istop);   % An index interval is set for each event..
 tmq    = find(chunk == max(chunk));
 peak   = [peak, (istart + tmq(1) - 1)];
end

out = peak;            % Indexes corresponding to peaks only are returned.
end

