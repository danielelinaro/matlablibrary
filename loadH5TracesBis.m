function [t,varargout] = loadH5TracesBis(files, varargin)
% [t, out1, out2, ...] = loadH5TracesBis(files, in1, in2, ...)

if isempty(files)
    t = [];
    varargout = {};
    return;
end

if isstruct(files(1))
    files =  arrayfun(@(x) x.name, files, 'UniformOutput', 0);
end

if ischar(files)
    files = {files};
end

entities_index = [varargin{:}];

nEnt = length(entities_index);   % number of entities to read
if nEnt == 0
    error('You must specify at least one entity to read.');
end

varargout = cell(nEnt);

nFiles = length(files);          % number of files
[entities,info] = loadH5Trace(files{1},entities_index);
t = (0 : length(entities(1).data)-1) * info.dt;
% fprintf(1, '.');
for jj=1:nEnt
    varargout{jj} = nan(nFiles,length(t));
    varargout{jj}(1,:) = entities(jj).data;
end

for ii=2:nFiles
%     fprintf(1, '.');
    entities = loadH5Trace(files{ii},entities_index);
    for jj=1:nEnt
        if length(entities(jj).data) == length(t)
            varargout{jj}(ii,1:length(entities(jj).data)) = ...
                entities(jj).data;
        else
            fprintf('Recordings have different durations.\n');
        end
    end
%     if mod(ii,50) == 0 && ii ~= nFiles
%         fprintf(1, '\n');
%     end
end
% fprintf(1, '\n');
