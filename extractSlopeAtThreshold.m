function [v_atOnset, dVdt_atOnset, time_atOnset, dVdtThres, time_atdVdtThres, ...
    v_atdVdtThres, d2Vdt2_atdVdtThres, spikes, figHandles] = ...
    extractSlopeAtThreshold(dt,voltage, dVdtThres, spikeThreshold, plotFlag, varargin)
% EXTRACTSLOPEATTHRESHOLD extracts the slope of the voltage
% trace at a specific value of its derivative.
%
% [v_atOnset, dVdt_atOnset, time_atOnset, dVdtThres, time_atdVdtThres, 
%    v_atdVdtThres, d2Vdt2_atdVdtThres, spikes, figHandles] = 
%    extractSlopeAtThreshold(dt,voltage, dVdtThres, spikeThreshold, plotFlag)
% 
% Parameters:
%   dt - time step  (1x1)  [ms]
%   voltage - voltage vector or matrix (MxN) - each trace is a row! [mV]
%   dVdtThres - threshold of the derivative of the voltage, at which we
%   are interested in the value of the second derivative of the voltage
%   trace --- LEAVE EMPTY or set to 'aboveOnset' to make it 1.1 timen the
%   average onset.
%
% Returns
%   dVdt_atOnset - a cell array of size (Mx1) containing the slopes of the
%       action potentials at the kink (defied here as the peak of the 3rd
%       derivative of the voltage). [mV/ms]
%   v_atOnset - just as dVdt_atKink, but containing the voltage values at
%       this point
%   time_atOnset - time of the spike onset
%   d2Vdt2_atdVdtThres - a cell array of size (Mx1) containing the values
%       of the second derivative of the voltage, where the first derivative is dVdtThres.
%   v_atdVdtThres - just as d2Vdt2_atdVdtThres, but containing the voltage values at
%       this point
%   time_atdVdtThres - time value where this point is.
%   spikes - a cell array of size (Mx1), containing spike properties for
%       all spikes in the respective traces. See "analizeAP(...)" for a
%       description of the fields. [mV/ms/ms]

% Author: Istvan Biro - 2013.06.18


%%  PREPARE

beforeOnsetTime = 2; % [ms] time to still consider before spike onset is detected by 3rd derivative peak. Necessary for "dVdtThres" values below spike onset point

if ~exist('spikeThreshold') || isempty(spikeThreshold), spikeThreshold = -15; end

loose_pre_cut_enable = 1; % AP detection feature... unimportant  (refers to padding data before a spike to make its detection possible if it's near the edge of the trace)
loose_post_cut_enable = 1; % AP detection feature... unimportant  (refers to padding data after a spike to make its detection possible if it's near the edge of the trace)
prePeakTime = 4/1000;  % [s] time to consider, before each peak, when analizing spike properties.
postPeakTime = 8/1000; % [s] time to consider, after each peak, when analizing spike properties.

if exist('functions'), addpath('functions'); end
[numberOfTraces, lengthOfTrace] = size(voltage);
dVdt_atOnset = cell(numberOfTraces,1);
time_atOnset = cell(numberOfTraces,1);
time_atdVdtThres = cell(numberOfTraces,1);
v_atdVdtThres = cell(numberOfTraces,1);
d2Vdt2_atdVdtThres = cell(numberOfTraces,1);
spikes = cell(numberOfTraces,1);

time = 0:dt:dt*(length(voltage)-1);

kinkIndexes = cell(numberOfTraces,1); % indexes where the kinks are in voltage

if plotFlag==1
    fig_Vt = figure(); hold all;
    fig_dv = figure(); hold all;
else
    fig_Vt = [];
    fig_dv = [];
end


%% EVALUATE VARAGIN
%% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%% go throught traces one by one and get data required
for tr=1:numberOfTraces
    
    v = voltage(tr,:); % makes it easiet to work with...
    
    % get the location of the kink, as the tpeak of the third derivative...
    % and other spike properties...
    [AP, v , dt , myFigure]  = analizeAP(v, dt/1000, 'threshold' , spikeThreshold ,'figHandle', 0, 'loose_pre_cut_enable', loose_pre_cut_enable, 'loose_post_cut_enable',loose_post_cut_enable, 'prePeakTime', prePeakTime, 'postPeakTime', postPeakTime );
    dt = 1000*dt; % [ms]
    %     AP =
    %
    %                   amplitudes: [1x355 double]
    %       steepestUpstrokeSpeeds: [1x355 double]
    %        steepestUpstrokeIndex: [1x355 double]
    %        averageUpstrokeSpeeds: [1x355 double]
    %      steepestDownstrokeIndex: [1x355 double]
    %     steepestDownstrokeSpeeds: [1x355 double]
    %      averageDownstrokeSpeeds: [1x355 double]
    %                   halfwidths: [1x355 double]
    %                 upstrokeTime: [1x355 double]
    %               downstrokeTime: [1x355 double]
    %                   onsetIndex: [1x355 double]
    %                     endIndex: [1x355 double]
    %                    peakIndex: [1x355 double]
    %                        sagVs: [1x355 double]
    %                     sagIndex: [1x355 double]
    %                       traces: [355x181 double]
    %             traces_peakIndex: 61
    
    numberOfSpikes = length(AP.onsetIndex);
    
    % save spike info
    spikes{tr} = AP;
    
    % save indexts where the kinks are
    kinkIndexes{tr} = zeros(1,numberOfSpikes) * NaN;
    kinkIndexes{tr}(1,:) = AP.onsetIndex;
    
    % calculate the derivatives - 1st and second order
    dvdt = (v(3:end)-v(1:end-2)) ./ (2*dt);
    dvdt = [ dvdt(1) dvdt dvdt(end) ]; % offset and length correction by artificial repetition of first and last element
    %     d2vdt2 = (dvdt(3:end)-dvdt(1:end-2)) ./ (2*dt);
    %     d2vdt2 = [ d2vdt2(1) d2vdt2 d2vdt2(end) ]; % offset and length correction by artificial repetition of first and last element
    
    time = [0:dt:dt*(length(v)-1)];
    
    % first derivate value at kink i.e. spike onset
    
    dVdt_atOnset{tr}(1,:) = zeros(1,numberOfSpikes) * NaN;
    dVdt_atOnset{tr}(1,:) = dvdt(AP.onsetIndex);
    
    v_atOnset{tr}(1,:) = zeros(1,numberOfSpikes) * NaN;
    v_atOnset{tr}(1,:) = v(AP.onsetIndex);
    
    time_atOnset{tr}(1,:) = zeros(1,numberOfSpikes) * NaN;
    time_atOnset{tr}(1,:) = time(AP.onsetIndex);
    
    
    
    if ~exist('dVdtThres') || isempty(dVdtThres), dVdtThres = 1.1*mean(dVdt_atOnset{tr});
    elseif ~isnumeric(dVdtThres) && strcmp(dVdtThres,'aboveOnset'),  dVdtThres = 1.1*mean(dVdt_atOnset{tr});
    elseif isnumeric(dVdtThres), dVdtThres = dVdtThres;  % nothing to change
    else error(' NO THRESHOLD SET OR INCORRECT')
    end
    
    
    
    % now we need d2Vdt2_atdVdtThres - a cell array of size (Mx1) containing the values
    % of the second derivative of the voltage, where the first derivative
    % is dVdtThres.
    % The data points we are looking for will either be before or after their
    % respective kink point, AP.onsetIndex(i), and before half way to the spike peak,
    % AP.peakIndex(i).
    % Also, the crossing of this trace, dvdt with dVdtThres is of interest
    % for us, thus we will fit a polynomial on neighboring points to the
    % crossing (intersect) and extract the exact values.
    
    % initialize
    polynomInterpOrder = 2; % order of polynomial to fit
    numberOfPointsToInterpolate = ceil( (polynomInterpOrder+1)/2 );   % number of points that must be involved in the fit - x2 : below and above threshold
    d2Vdt2_atdVdtThres{tr} = zeros(1,numberOfSpikes) * NaN;  % slopes of the derivate (dvdt) at threshold crossing
    index_toInterpolate_atdVdtThres{tr} = zeros(2*numberOfPointsToInterpolate,numberOfSpikes) * NaN;  % indexes of points that must be involved in the fit
    v_atdVdtThres{tr} = zeros(1,numberOfSpikes) * NaN;  % voltage value at dvdt threshold crossing
    time_atdVdtThres{tr} = zeros(1,numberOfSpikes) * NaN;  % time value at dvdt threshold crossing
    
    beforeOnsetIndex = ceil(beforeOnsetTime/dt);
    
    % fit each spike
    for spk = 1:numberOfSpikes
        helper = AP.onsetIndex(spk) - floor( ( AP.peakIndex(spk) - AP.onsetIndex(spk) )*0.25 ) ;
        fromToInd = [ AP.onsetIndex(spk)-beforeOnsetIndex: AP.peakIndex(spk) ]; % index range in which to observe threshold crossing
        fromToInd = fromToInd( find(fromToInd>0) ); % just to make sure...
        ind_cross_high = find( (dvdt(fromToInd)>dVdtThres) , 1); % indexes of first point above the threshold but after the kink onset, relative to the dvdt(fromToInd) begin
        if ~isempty(ind_cross_high)
            ind_cross_high = ind_cross_high + fromToInd(1) -1 ; % to relate to entire dvdt, not just dvdt(fromToInd)
            
            %             if dVdt_atKink{tr}(spk) >= dVdtThres % in this case fits are better if one takes lower points on dvdt(v), that is earlier data points on spike voltage
            %                 ind_toInterpolate = [ind_cross_high-numberOfPointsToInterpolate-1 : ind_cross_high+numberOfPointsToInterpolate-2];  % indexes of points below and above the crossing level on which to interpolate
            %                 % disp('*')
            %             else  % in this case fits are better if one takes higher points on dvdt(v), that is earlier data points on spike voltage
            %                 ind_toInterpolate = [ind_cross_high-numberOfPointsToInterpolate : ind_cross_high+numberOfPointsToInterpolate-1];  % indexes of points below and above the crossing level on which to interpolate
            %                 % disp('n')
            %             end
            
            ind_toInterpolate = [ind_cross_high-numberOfPointsToInterpolate : ind_cross_high+numberOfPointsToInterpolate-1];  % indexes of points below and above the crossing level on which to interpolate
            
            
            % interpolate wot a polynomial of the order of polynomInterpOrder
            % - first, get the v value
            [pv] = polyfit(dvdt(ind_toInterpolate), v(ind_toInterpolate) , polynomInterpOrder); % note : x and y inverted to use polyval
            [v_atTh] = polyval(pv,dVdtThres);
            v_atdVdtThres{tr}(spk) = v_atTh;
            index_toInterpolate_atdVdtThres{tr}(:,spk) = ind_toInterpolate;
            % - than get the slope
            [p] = polyfit(v(ind_toInterpolate) , dvdt(ind_toInterpolate), polynomInterpOrder); % note : x and y normal again
            k = polyder(p);
            [slope] = polyval(k,v_atTh);
            d2Vdt2_atdVdtThres{tr}(spk) = slope;
            % - now we get the time of the dVdtThres crossing... we do so
            % by knowing that voltage values there increase.. thus we look
            % vhere the voltage crosses v_atTh and make an
            % interpolation on 3 neighboring voltage values to get the
            % exact time
            v_local = v(ind_toInterpolate);
            t_local = time(ind_toInterpolate);
            tind = find( v_local > v_atTh ,1);
            if ~isempty(tind) && tind>=2
                v_1_2 = v_local([tind-1 tind]);
                t_1_2 = t_local([tind-1 tind]);
                [pvt] = polyfit(v_1_2, t_1_2 , 1);
                [t_atTh] = polyval(pvt,v_atTh);
                time_atdVdtThres{tr}(spk) = t_atTh;
            else time_atdVdtThres{tr}(spk) = NaN;
            end
            
            % keyboard
            
            % plot a few things
            if plotFlag==1
                % - points that were used to fit
                pp_fitpoints = plot(v(ind_toInterpolate),dvdt(ind_toInterpolate),'bo');
                % - polynomial
                figure(fig_dv); hold all;
                xx = linspace(dvdt(ind_toInterpolate(1)), dvdt(ind_toInterpolate(end)) , 20);
                yy = polyval(pv,xx);
                pp_poly = plot(yy,xx,'b-');
                % - slope
                figure(fig_dv); hold all;
                offs = dVdtThres-slope*v_atTh;
                xx = [v(ind_toInterpolate(1)) v(ind_toInterpolate(end))];
                yy = slope*xx+offs;
                pp_slope = line(xx,yy,'color',[1 0 0]);
            end
            
        end
    end
    
    
    % NOW PLOT STUFF
    
    if plotFlag==1
        % plot voltage traces in time and mark spike onset location - and
        % threshold crossing (where we took the second derivate)
        figure(fig_Vt); hold all;
        p_vt = plot(time, v, 'k-', 'linewidth', 1);
        p_vt_onset = plot(time(kinkIndexes{tr}),v(kinkIndexes{tr}), 'o', 'color', [0 1 0], 'markerSize', 3, 'markerfacecolor', [0 1 0]);
        p_vt_atdVdtThres = plot( time_atdVdtThres{tr} , v_atdVdtThres{tr} , 'o', 'color', [1 0 0], 'markerSize', 3, 'markerfacecolor', [1 0 0]);
        
        
        % plot dVdt(v) and mark the onset location and the threshold intercept
        figure(fig_dv); hold all;
        p_dv = plot(v, dvdt, 'k-', 'linewidth', 1);
        p_dv_onset = plot(v(kinkIndexes{tr}),dvdt(kinkIndexes{tr}), 'o', 'color', [0 1 0], 'markerSize', 3, 'markerfacecolor', [0 1 0]);
        p_dv_thrcross = plot( v_atdVdtThres{tr} , dVdtThres , 'o', 'color', [1 0 0], 'markerSize', 3, 'markerfacecolor', [1 0 0]);
    end
    
    
end

figHandles = [fig_Vt fig_dv];


end