function [model,x_pred,y_pred,conf] = linearRegression(x,y,npoints)
% [model,x_pred,y_pred,conf] = linearRegression(x,y,npoints)

if ~ exist('npoints','var')
    npoints = 50;
end

x = x(:);
y = y(:);

g = fittype('a+b*x','coeff',{'a','b'},'indep','x');
model = fit(x,y,g,'StartPoint',polyfit(x,y,1));
x_pred = linspace(min(x),max(x),npoints)';
y_pred = model(x_pred);
y_hat = model(x);
y_err = y - y_hat;
n = length(x);
dof = n - 2;
t = tinv(1-0.025,dof);
s_err = sum(y_err.^2);
conf = t * sqrt((s_err/(n-2))*(1.0/n + ((x_pred-mean(x)).^2) / ...
    (sum(x_pred.^2) - n*(mean(x).^2))));
