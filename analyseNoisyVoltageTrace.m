function [spks,F,cv,ff,edges,PSTH,psth] = analyseNoisyVoltageTrace(t,V,bin_dur,doPlot)
% [spks,F,cv,ff] = analyseNoisyVoltageTrace(t,V,bin_dur,doPlot)

if ~ exist('doPlot','var')
    doPlot = 1;
end

spks = extractAPPeak(t,V,-20);
F = cellfun(@(x) length(x), spks) / t(end);
isi = cellfun(@(x) diff(x), spks, 'UniformOutput', 0);
cv = cellfun(@(x) std(x)/mean(x), isi);

edges = 0 : bin_dur : t(end);
cnt = cellfun(@(x) histc(x,edges), spks, 'UniformOutput', 0);
psth = cellfun(@(x) x/bin_dur, cnt, 'UniformOutput' , 0);
ff = cellfun(@(x) var(x)/mean(x), cnt);
PSTH = sum(cell2mat(psth))/length(psth);

if doPlot
    figure;
    axes('Position',[0.1,0.3,0.6,0.6]);
    rasterplot(spks,'k');
    set(gca, 'XTick', [], 'XColor', [1,1,1]);
    ylabel('Trial #');
    
    axes('Position',[0.1,0.1,0.6,0.15]);
    hndl = bar(edges, PSTH, 0.8, 'histc');
    set(hndl,'FaceColor',[0.6,0.6,1]);
    axis([0,t(end),0,ceil(max(PSTH)/5)*5]);
    box off;
    xlabel('Time (s)');
    ylabel('Rate (spikes/s)')

    Fm = mean(F);
    Fs = std(F);
    cvm = mean(cv);
    cvs = std(cv);
    ffm = mean(ff);
    ffs = std(ff);

    axes('Position',[0.8,0.525,0.1,0.375],'NextPlot','Add');
    plot(F,ff,'ko','MarkerSize',5);
    errorbar(Fm,ffm,ffs,'ko','MarkerFaceColor','k','MarkerSize',7);
    axis([Fm-2*Fs,Fm+2*Fs,ffm-2*ffs,ffm+2*ffs]);
    ylabel('Fano factor');
    box off;
    set(gca, 'XTick', [], 'XColor', [1,1,1]);
    title(sprintf('Bin size = %.0f ms', bin_dur*1e3));
    axes('Position',[0.8,0.1,0.1,0.375],'NextPlot','Add');
    plot(F,cv,'ko','MarkerSize',5);
    errorbar(Fm,cvm,cvs,'ko','MarkerFaceColor','k','MarkerSize',7);
    axis([Fm-2*Fs,Fm+2*Fs,cvm-2*cvs,cvm+2*cvs]);
    xlabel('Rate (spikes/s)')
    ylabel('CV');
    box off;
    
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,8,6]);
    print('-depsc2','noisy_trials.eps');
    
end
