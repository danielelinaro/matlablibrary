function [taus,ci] = computeTauFromFiles(files, Ke, spike_thresh, window, do_plot)
% computeTauFromFiles computes the membrane time constant using recordings
% in which a brief hyperpolarizing protocol was injected into a cell.
% 
% [taus,ci] = computeTauFromFiles(files, Ke, spike_thresh, window, do_plot)
% 
% Parameters:
%         files - the list of files to read.
%            Ke - the electrode kernel to use for the compensation of the voltage
%                 traces.
%  spike_thresh - the spike threshold to use for discarding traces with
%                 spikes just after the stimulation.
%        window - a single value indicating the duration of the time window to
%                 consider to fit the exponential to the mean membrane voltage.
%       do_plot - 1 to plot the results, 0 otherwise.
% 

% Author: Daniele Linaro - March 2012

narginchk(5,5);

if length(files) == 1 && strcmp(files{1},'recording.mat')
    data = load('recording.mat');
    t = (0:size(data.V,2)-1) * data.dt;
    Vc = data.V;
    t0 = data.tstart+data.dur;
    dur = data.dur;
else
    [t,Vr,I,stim] = loadH5Traces(files);
    if ~ isempty(Ke)
        Vc = AECoffline(Vr,I,Ke);
    else
        Vc = Vr;
    end
    t0 = sum(stim{1}(1:2,1));
    dur = stim{1}(2);
end
Fc = 1000;
Vc = filterTrace(Vc,1/diff(t(1:2)),Fc,2);
Voff = mean(Vc(:,t>5/Fc & t<min(0.1,t0/5)),2);
idx = find(t < 5/Fc);
Vc(:,idx) = repmat(Voff,[1,length(idx)]);
t1 = t0 + window;
[~,Vth] = extractAPThreshold(t,Vc,spike_thresh,[],3);
if ~ isnan(nanmean(cellfun(@(x) nanmean(x), Vth)))
    spike_thresh = nanmean(cellfun(@(x) nanmean(x), Vth)) + 10;
end
Vc = Vc(max(Vc(:,t>=t0-dur & t<t1),[],2) < spike_thresh, :);
Vm = mean(Vc);
idx = find(t >= t0 & t <= t1);
[~,i] = min(Vm(idx));
idx = idx(i:end);
[~,i] = max(Vm(idx));
srate = 1/diff(t(1:2));
singleExp = fitExp(Vm(idx(1:i)), srate, 1);
ciSingle = confint(singleExp.F);
ciSingle = ciSingle(:,2);
doubleExp = fitExp(Vm(idx(1:i)), srate, 2);
ciDouble = confint(doubleExp.F);
ciDouble = ciDouble(:,3:4);
taus = [doubleExp.F.b0,doubleExp.F.b1];
[taus,ndx] = sort(taus);
ciDouble = ciDouble(:,ndx);
if taus(1) < 1e-3 || taus(2)/taus(1) > 5
    disp('Using two exponentials.');
    ci = ciDouble;
else
    disp('Using one exponential.');
    taus = singleExp.F.b0;
    ci = ciSingle;
end

if do_plot
    T = t(idx(1:i)) - t0;
    V = Vm(idx(1:i));
    figure; hold on;
    set(gca, 'FontSize', 8, 'FontName', 'Arial');
    idx = find(t >= t0-dur-10e-3 & t <= t(idx(i))+60e-3);
    plot(t(idx)*1e3, Vc(:,idx), 'Color', [.6,.6,.6]);
    plot(t(idx)*1e3, Vm(idx), 'Color', [.3,.3,.3], 'LineWidth', 4);
    if length(taus) == 1
        plot((T+t0)*1e3, singleExp.F(T)+min(V), 'r', 'LineWidth', 2);
    else
        plot((T+t0)*1e3, doubleExp.F(T)+min(V), 'r', 'LineWidth', 2);
    end
    axis tight;
    axis([xlim, ylim+[-3,3]]);
    xlabel('time (ms)');
    ylabel('voltage (mV)');
    set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,7,4]);
    text(t(idx(end))*1e3-60, min(V)+4, ...
        sprintf('\\tau_0 = %.2f ms', taus(1)*1e3), ...
        'FontName', 'Arial', 'FontSize', 9);
    if length(taus) == 2
        text(t(idx(end))*1e3-60, min(V)+2, ...
            sprintf('\\tau_1 = %.2f ms', taus(2)*1e3), ...
            'FontName', 'Arial', 'FontSize', 9);
    end
    print('-depsc2', 'tau.eps');
end
