function [K,v0] = fullKernel(v,i,ksize)
% FULLKERNEL calculates the raw kernel from the autocorrelation of the input current
% i and correlation between current and recorded potential v
% i.e. finds the best linear filter which transforms i into v
% 
% [K,v0] = fullKernel(v,i,ksize)
% 
% Parameters:
%       v - recorded membrane voltage (in volts).
%       i - injected current (in amperes).
%   ksize - length of the kernel (number of samples).
% 
% Returns:
%       K - the raw kernel (in Ohms).
%      v0 - the resting membrane voltage (in volts).
% 

% Author: Daniele Linaro, December 2011.

vi = zeros(ksize,1);
ii = zeros(ksize,1);
vref = mean(v);

for k=1:ksize
    vi(k) = mean((v(k:end)-vref) .* i(1:end-k+1));
    ii(k) = mean(i(k:end) .* i(1:end-k+1));
end

vi = vi-mean(i)^2;
K = toeplitz(ii)\vi;
v0 = vref - mean(i)*sum(k);
