function [entities,info,comments] = loadH5Trace(filename,entities_index)
% loadH5Traces loads an H5 file saved by the software LCG.
% 
% [entities,info,comments] = loadH5Trace(filename,entities_index)
%
% Argument:
%         filename - the path of an H5 file to load.
%   entities_index - an array containing the indexes of the entities to
%                    load.
% 
% Returns:
%   entities - an array of structures, each corresponding to one
%              recorded entity. Each structure contains the following
%              fields:
%         id - the unique identifier of the entity
%       data - the actual data recorded during the experiment
%   metadata - additional data used by the entity
%      units - the units of the entity (e.g., "mV" or "pA")
%       name - the name of the entity
% parameters - the parameters of the entity
% 
%       info - a structure containing additional information about the
%              stimulation, such as sampling rate and duration.
%   comments - a cell array containing optional comments added to the file.
% 
% For more information about the software suite LCG, see
% 
% Linaro, D., Couto, J., & Giugliano, M. (2014).
% Command-line cellular electrophysiology for conventional and real-time
% closed-loop experiments. Journal of Neuroscience Methods, 230, 5?19.
% doi:10.1016/j.jneumeth.2014.04.003
% 
% See also listH5Files

% Author: Daniele Linaro, sometime in 2012

warning('off','MATLAB:imagesci:deprecatedHDF5:deprecatedAttributeSyntax');

if ~exist('entities_index','var')
    entities_index = [];
end

try
    version = hdf5read(filename, '/Info/version');
catch
    version = 1;
    info = hdf5info(filename);
    ngroups = length(info.GroupHierarchy.Groups);
    for k=1:ngroups
        if strcmp(info.GroupHierarchy.Groups(k).Name, '/Metadata');
            version = 0;
            break;
        end
    end
end

switch version
    case 0
        [entities,info,comments] = loadH5TraceV0(filename);
    case 1
        [entities,info,comments] = loadH5TraceV1(filename);
    case 2
        [entities,info,comments] = loadH5TraceV2(filename,entities_index);
    otherwise
        error('Unknown H5 file version (%.0f).', version);
end

warning('on','MATLAB:imagesci:deprecatedHDF5:deprecatedAttributeSyntax');
