function [Vm,AP] = analyseSpontaneous(t,V,thresh,doPlot,before,after,interval)
% analyseSpontaneous analyses a spontaneous voltage trace.
% 
% [Vm,AP] = analyseSpontaneous(t,V,thresh,doPlot,before,after,interval)
% 
% Parameters:
%        t - time vector.
%        V - voltage vector.
%   thresh - spike detection threshold.
%   doPlot - whether to plot the results, default.
% 
% Returns:
%       Vm - a structure containing the statistics of the subthreshold
%            membrane voltage. Its fields are mean, median and
%            distribution, which in turn is a structure with two fields,
%            edges and count.
%       AP - a structure containing a summary of the features of the action
%            potentials. Its fields are those returned by APanalysis, plus:
%            frequency - the number of spikes divided by the duration of
%                        the recording.
%            T - time vector.
%            Vm - mean action potential.
%            dVm - derivative of the mean action potential.
% 
% See also APanalysis.
% 

% Author: Daniele Linaro - February 2013.

if ~ exist('thresh','var')
    thresh = loadThreshold(0);
end

if ~ exist('doPlot','var')
    doPlot = 1;
end

if ~ exist('before','var')
    before = 4e-3;
end

if ~ exist('after','var')
    after = 6.5e-3;
end

if ~ exist('interval','var')
    interval = 5e-3;
end

Vtrimmed = V;

[tp,Vp] = extractAPPeak(t,V,thresh,interval);
if ~ isempty(tp{1})
    
    dt = diff(t(1:2));
    
    %%% spike shape analysis
    tp = tp{1};
    n = length(tp);
    for k=1:n-1
        if diff(tp(k:k+1)) < after
            after = diff(tp(k:k+1));
        end
    end
    window = round([before,after-0.5e-3] / dt);
    peakpos = round(tp/dt) + 1;
    Vspks = nan(size(V,1),sum(window)+1);
    for k=1:n
        if peakpos(k)-window(1) > 0 && peakpos(k)+window(2) <= length(V)
            Vspks(k,:) = V(peakpos(k)-window(1):peakpos(k)+window(2));
        end
    end
    [r,~] = find(isnan(Vspks));
    r = unique(r);
    idx = setdiff(1:size(Vspks,1),r);
    Vspks = Vspks(idx,:);
    T = (0:size(Vspks,2)-1) * dt;
    AP = APanalysis(T,Vspks,thresh);
    AP.tp = tp;
    AP.Vp = Vp;
    AP.frequency = AP.nspks/t(end);
    AP.T = T;
    AP.Vm = mean(Vspks,1);
    AP.dVm = (AP.Vm(3:end) - AP.Vm(1:end-2)) / (2*dt);

    % remove the spikes from the trace
    for k=1:n
        ndx = peakpos(k)-window(1):peakpos(k)+window(2);
        ndx = ndx(ndx > 0 & ndx <= length(Vtrimmed));
        Vtrimmed(ndx) = nan;
    end
    
else
    
    AP = [];

end

%%% membrane potential distribution analysis
binWidth = 1;
Vm.distribution.edges = floor(min(Vtrimmed)) : binWidth : ceil(max(Vtrimmed));
Vm.distribution.count = histc(Vtrimmed,Vm.distribution.edges);
Vm.mean = nanmean(Vtrimmed);
Vm.median = nanmedian(Vtrimmed);

if doPlot
    figure;

    green = [0.25,0.75,0];
    blue = [0,0.1,1];
    orange = [1,0.5,0];
    
    axes('Position',[0.05,0.1,0.6,0.7],'NextPlot','add');
    plot(t,V,'k','LineWidth',1);
    plot(t([1,end])+[-1,1],-65+[0,0],'--','Color',green,'LineWidth',2);
    plot(t([1,end])+[-1,1],-0+[0,0],'--','Color',green,'LineWidth',2);
    text(-1,-65,'-65 mV','HorizontalAlignment','Right');
    text(-1,0,'0 mV','HorizontalAlignment','Right');
    if isfield(AP,'frequency')
        f = AP.frequency;
    else
        f = 0;
    end
    text(t(end),max(max(V)+5,0),sprintf('Firing frequency = %.1f Hz',f),...
        'HorizontalAlignment','Right');
    text(t(end),max(max(V)+5,0)+6,sprintf('Resting Vm = %.1f mV',Vm.mean),...
        'HorizontalAlignment','Right');
    yl = ylim;
    axis off;
    placeScaleBars(-5,-40,[],20,'','20 mV','k','LineWidth',2);
    placeScaleBars(0,min(V),5,[],'5 sec','','k','LineWidth',2);

    axes('Position',[0.75,0.1,0.15,0.7],'NextPlot','add','TickDir','Out','LineWidth',0.8);
    pos = get(gca,'Position');
    set(gca,'XLim',[yl(1),Vm.distribution.edges(end)]);
    set(gca,'Position',[pos(1:3), pos(4)*diff([yl(1),Vm.distribution.edges(end)])/diff(yl)]);
    hndl = bar(Vm.distribution.edges,Vm.distribution.count/sum(Vm.distribution.count),...
        0.75,'histc');
    set(hndl,'FaceColor',[0.6,0.6,1],'LineWidth',1,'EdgeColor','k');
    set(gca,'View',[-90,90],'XAxisLocation','Top',...
        'XTick',round(yl(1)/10)*10 : 10 : Vm.distribution.edges(end))
    xlabel('Membrane voltage (mV)');
    ylabel('Fraction');
    box off;

    if exist('Vspks','var')
        pos = get(gca,'Position');
        axes('Position',[0.75,sum(pos([2,4]))+0.05,0.15,0.95-sum(pos([2,4]))-0.05],...
            'NextPlot','Add');
        plot(T,Vspks,'Color',[.6,.6,.6]);
        plot(T,AP.Vm,'Color',orange,'LineWidth',2);
        plot(T(2:end-1),AP.Vm(1)+AP.dVm/3000,'Color',blue,'LineWidth',1);
        plot(T([1,end]),-65+[0,0],'--','Color',green,'LineWidth',2);
        plot(T([1,end]),-0+[0,0],'--','Color',green,'LineWidth',2);
%         placeScaleBars(T(1),-30,[],20,'',{'60 V/sec',[0,0.1,1];'20 mV',[1,0.5,0]},...
%             'k','LineWidth',2);
        placeScaleBars(T(1),-30,[],20,'','20 mV','k','LineWidth',2);
        placeScaleBars(T(end)-0.005,min(min(Vspks)),0.005,[],'5 msec','','k','LineWidth',2);
        axis tight
        axis off;
    end
end
