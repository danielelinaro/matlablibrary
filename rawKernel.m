function K = rawKernel(vi,ii)
% K=rawKernel(VI,II)
%
% Calculates the raw kernel from the autocorrelation of the input current
% II and correlation between current and recorded potential VI
% i.e. finds the best linear filter which transforms I into V
%
% R. Brette, Sep 2005

K = toeplitz(ii)\vi;
