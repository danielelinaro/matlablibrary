function covT_theory = computeCov_theory5(cell1,cell2,var_in,c,T,nu1,nu2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% compute linear response prediction for spike count covariance in long windows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DEBUG = 0;

%%% fit line to f-I curve
% data_in = load(strcat(cell1,'/fI_curves/fI_curves.mat'));

% I = data_in.Iu{2};
% I = [I(1)-(I(2)-I(1)); I];
% f = data_in.fm{2};
% f = [0; f];

% fIfit1 = fit(I,f,'poly1');
% Itest = 0:.1:floor(max(data_in.Iu{2}));
% ftest = fIfit1.p1*Itest + fIfit1.p2;
% ind = find(abs(ftest - nu1) == min(abs(ftest - nu1)));
% fIgain1 = fIfit1.p1;

% clear data_in;
% data_in = load(strcat(cell2,'/fI_curves/fI_curves.mat'));


% I = data_in.Iu{2};
% I = [I(1)-(I(2)-I(1)); I];
% f = data_in.fm{2};
% f = [0; f];

% fIfit2 = fit(I,f,'poly1');
% Itest = 0:.1:floor(max(data_in.Iu{2}));
% ftest = fIfit2.p1*Itest + fIfit2.p2;
% ind = find(abs(ftest - nu2) == min(abs(ftest - nu2)));
% fIgain2 = fIfit2.p1;

%%% or fit the f-I curve with a power-law
data_in = load(strcat(cell1,'/fI_curves/fI_curves.mat'));
start = find(data_in.fm{2} > 0, 1, 'first');
if start > 1
    start = start-1;
end
I = data_in.Iu{2}(start:end);
f = data_in.fm{2}(start:end);
% I = data_in.Iu{2};
% f = data_in.fm{2};
I = [data_in.rheobase(2,1); I];
f = [0; f];
[I,idx] = sort(I);
f = f(idx);
fIfit1 = fit(I,f,'power2');
Itest = 0:.1:floor(max(data_in.Iu{2}));
ftest = fIfit1.a*Itest.^fIfit1.b + fIfit1.c;
ind = find(abs(ftest - nu1) == min(abs(ftest - nu1)));
fIgain1 = fIfit1.a*fIfit1.b*Itest(ind)^(fIfit1.b-1);

if DEBUG
    clf;
    hold on;
    plot(I,f,'ko');
    plot(Itest,ftest,'k');
end

data_in = load(strcat(cell2,'/fI_curves/fI_curves.mat'));
start = find(data_in.fm{2} > 0, 1, 'first');
if start > 1
    start = start-1;
end
I = data_in.Iu{2}(start:end);
f = data_in.fm{2}(start:end);
% I = data_in.Iu{2};
% f = data_in.fm{2};
I = [data_in.rheobase(2,1); I];
f = [0; f];
[I,idx] = sort(I);
f = f(idx);

fIfit2 = fit(I,f,'power2');
Itest = 0:.1:floor(max(data_in.Iu{2}));
ftest = fIfit2.a*Itest.^fIfit2.b + fIfit2.c;
ind = find(abs(ftest - nu2) == min(abs(ftest - nu2)));
fIgain2 = fIfit2.a*fIfit2.b*Itest(ind)^(fIfit2.b-1);

covT_theory = fIgain1*fIgain2;

if DEBUG
    plot(I,f,'rs');
    plot(Itest,ftest,'r');
    pause;
end

% %%% estimate the spike count covariance
% %%% assume the transfer function is constant
% freq = -100:.001:100; % Hz
% % freq = .001:.001:50; % Hz
% % freq = -1/T:.001:1/T;
% k0 = find(freq==0);
% freq = freq([1:k0-1 k0+1:end]);
% kT = (sin(pi.*freq.*T).^2)./(pi^2.*freq.^2);
% covT_theory = zeros(size(var_in));

% %%% input power spectrum
% % Pss_AMPA = .005./(1+(2*pi*.005*freq).^2); %OU noise with timescale 5 ms
% % Pss_GABA = .01./(1+(2*pi*.01*freq).^2); %OU noise with timescale 10 ms
% Pss_AMPA = .005./(1+(2*pi*.005*freq).^2); %OU noise with timescale 5 ms
% Pss_GABA = .01./(1+(2*pi*.01*freq).^2); %OU noise with timescale 10 ms
% Pss_NMDA = .1./(1+(2*pi*.1*freq).^2);
% % Pss0 = Pss_AMPA + Pss_GABA;
% Pss0 = Pss_NMDA;

% for i=1:length(var_in)

% 	%%% normalize total input power to what it should be
% 	Pss = Pss0./(trapz(freq,Pss0)/var_in(i));

% 	% covT_theory(i) = c*fIgain1*fIgain2; %*trapz(freq,kT.*Pss);
% 	% Pss = ones(size(freq));
% 	% Pss = Pss./(trapz(freq, Pss)/var_in(i));

% 	% covT_theory(i) = c*fIgain1*fIgain2*trapz(freq,kT.*Pss);
% 	% covT_theory(i) = c*fIgain1*fIgain2*var_in(i);

% 	% Pss = var_in(i);
% 	% covT_theory(i) = c*fIgain1*fIgain2*Pss;
% end

% % Pss: current^2 * time 
% % window: 1/freq^2 = time^2
% % fIgain: sp/s / current

