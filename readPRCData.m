function data = readPRCData(filename)
% data = readPRCData(filename)

data = struct;

if ~ exist(filename, 'file')
    disp([filename, ': no such file.']);
    return;
end

info = hdf5info(filename);

details = hdf5read(filename, '/Details');
for k=1:length(details.MemberNames)
    if isnumeric(details.Data{k})
        data.(details.MemberNames{k}) = details.Data{k};
        if strcmp(details.MemberNames{k},'T') || ...
           strcmp(details.MemberNames{k},'Tdelay') || ...
           strcmp(details.MemberNames{k},'dt') || ...
           strcmp(details.MemberNames{k},'durperturb') 
            data.(details.MemberNames{k}) = data.(details.MemberNames{k}) / 1000;
        end
    else
        data.(details.MemberNames{k}) = details.Data{k}.Data;
    end
end

for k=1:length(info.GroupHierarchy.Groups.Datasets)
    name = info.GroupHierarchy.Groups.Datasets(k).Name;
    [~,key] = strread(name(2:end), '%s%s', 'delimiter', '/');
    data.(key{1}) = hdf5read(filename, name);
    data.(key{1}) = data.(key{1}) / 1000;
end

% Add a missing attribute in the first version of the simulator 
if ~ isfield(data,'durperturb')
    data.durperturb = 0.5e-3;
end
