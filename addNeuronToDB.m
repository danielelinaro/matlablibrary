function result = addNeuronToDB(neuron,overwrite)
% addNeuronToDB adds information about a cell to the database.
% Data is not added to the database if an entry with equal cell id and
% experimenter name already exists in the database.
% 
% result = addNeuronToDB(neuron,overwrite)
% 
% Parameters:
%      neuron - a structure returned by makeBaseNeuronStruct or
%               makeNeuronStruct.
%   overwrite - whether to overwrite ('y') or not ('n'). Default behavior
%               is to ask the user.
% 
% Returns:
%   1 if the data was successfully added, 0 otherwise.
% 
% Example:
% 
% neuron = makeNeuronStruct('Experimenter','Daniele Linaro',...
%         'AnimalType','Wistar','AnimalAge',18,...
%         'NeuronType','Pyramidal','BrainArea','S1',...
%         'CellId','20130226A01','CellDate','20130226',...
%         'ProjectName','sinusoids');
% addNeuronToDB(neuron);
% 
% See also makeBaseNeuronStruct, makeNeuronStruct.
% 

% Author: Daniele Linaro - February 2013.

if ~exist('overwrite','var')
    overwrite = -1; % ask
else
    switch lower(overwrite)
        case 'y',
            overwrite = 1;  % force overwrite
        case 'n',
            overwrite = 0;  % never overwrite
        otherwise,
            error('Accepted options for overwrite are ''y'' or ''n''.');
    end
end

% open the connection to the database
try
    hndl = mysql('open','localhost','daniele','ilenia');
catch err
    error('Unable to connect to the database [%s].', err.identifier);
end

% change database
foo = mysql('use electrophysiology');

% check whether the database already contains a record with the same cell
% id and experimenter's name
query = sprintf(['SELECT n.animal_age FROM neurons n INNER JOIN experimenters e ON e.id = n.experimenter_id ', ...
    'WHERE n.id = ''%s'' AND e.id = ''%d'';'], neuron.id, neuron.experimenter_id);
age = mysql(query);
mysql('close');

if ~ isempty(age)
    % if the neuron is already in the database, ask the user whether they
    % want to update the information
    if overwrite < 0
        ok = input(sprintf(['The database already contains a record with ', ...
            'cell ID = [%s] and experimenter ID = [%d].\nDo you want to update ', ...
            'the information currently in the database? [y/N] '], ...
            neuron.id, neuron.experimenter_id),'s');
    end
    if overwrite==1 || (overwrite==-1 && strcmpi(ok,'y'))
        cell_id = neuron.id;
        experimenter_id = neuron.experimenter_id;
        neuron = rmfield(rmfield(neuron,'id'),'experimenter_id');
        columns = fieldnames(neuron);
        values = struct2cell(neuron);
%         idx = ~ cellfun(@(x) isempty(x) | iff(isscalar(x), isnan(x), false), values);
%         updateInDatabase('electrophysiology', cell_id, columns(idx), values(idx), experimenter_id);
        updateInDatabase('electrophysiology', cell_id, columns, values, experimenter_id);
        result = 1;
    else
        result = 0;
    end
else
    columns = fieldnames(neuron);
    values = struct2cell(neuron);
    idx = ~ cellfun(@(x) isempty(x) | iff(isscalar(x), isnan(x), false), values);
    addToDatabase('electrophysiology','neurons',columns(idx),values(idx));
    result = 1;
end
