function spks = extractSpikes(t,V,thresh,interp)
% EXTRACTSPIKES extracts spikes from a voltage trace.
% 
% spks = extractSpikes(t,V,thresh,interp)
% 
% Arguments:
%       t - the time vector. It must be an array of size (1xN), where N is the
%       number of time instants in the recording(s).
%       V - the voltage vector. It must be a matrix of size MxN, where M is
%       the number of recordings.
%  thresh - the threshold for spike detection.
%  interp - a boolean value that says whether the time of threshold
%  crossing should be interpolated, to get a better fit of the time of
%  threshold crossing (default: no).
% 
% Returns:
%    spks - a cell array [of size (Nx1)] with the spike times for each voltage trace.
% 

% Author: Daniele Linaro - some time in 2012.

if ~ exist('interp','var')
    interp = 0;
end

[m,n] = size(V);
spks = cell(m,1);
dt = diff(t(1:2));

for i=1:m
    VV = [V(i,1:end-1); V(i,2:end)];
    idx = find(VV(1,:) < thresh(i) & VV(2,:) >= thresh(i));
    spks{i} = t(idx);
    if interp
        for j=1:length(idx)
            spks{i}(j) = polyval(polyfit(VV(:,idx(j)), t(idx(j))+[0;dt], 1), thresh(i));
        end
    end
end

