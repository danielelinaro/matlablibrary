function [tpeak,vpeak] = extractAPPeak(T,V,threshold,min_distance)
% EXTRACTAPPEAK finds the spike peaks.
%
% [tpeak,vpeak] = extractAPPeak(T,V,threshold,min_distance)
%
% Parameters:
%             T - time vector, with size (1xN) where N is the number of samples.
%             V - voltage trace(s), with size (MxN), where M is the number of 
%                 traces and N is the number of samples.
%     threshold - threshold for spike detection: it can either be an (Mx1) array or a scalar.
%  min_distance - minimal distance between two subsequent spikes (default 0.005 s).
% Return values:
%     tpeak - a cell array [of size (Mx1)] with the spike peak times for each voltage trace.
%     vpeak - a cell array [of size (Mx1)] with the peak voltage values.
%

% Daniele Linaro - May 2012

if ~ exist('threshold','var')
    threshold = [];
end
threshold = adjustThreshold(V,threshold);

if ~ exist('min_distance','var') || isempty(min_distance)
    min_distance = 5e-3;
end
mpd = round(min_distance/diff(T(1:2)));
nexp = size(V,1);
tpeak = cell(nexp,1);
vpeak = cell(nexp,1);

for k=1:nexp
    if max(V(k,:)) < threshold(k)
        tpeak{k} = [];
        vpeak{k} = [];
    else
        [pks,locs] = findpeaks(V(k,:), 'MinPeakHeight', threshold(k), 'MinPeakDistance', mpd);
        dV = diff([V(k,locs-1) ; V(k,locs)]);
        idx = find(dV > 30);
        if ~ isempty(idx)
            for i=1:length(idx)
                fprintf(1, ['Removing spike #%d in trial #%d because it has a prominence ', ...
                    'of more than 30 mV.\n'], idx(i), k);
            end
        end
        idx = find(dV <= 30);
        pks = pks(idx);
        locs = locs(idx);
        tpeak{k} = T(locs(pks<100));
        vpeak{k} = pks(pks<100);
    end
end
