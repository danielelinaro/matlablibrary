function str = buildCommandString(channel, frequency, pulseWidth, pulseDuration)
% This script builds the string that can be used to program the Stimulus Generator
% manufactured by MultiChannel Systems. The stimulation pulse is biphasic.
% 
% Parameters:
%        channel - the number of the channel to program.
%      frequency - the frequency of the stimulation.
%     pulseWidth - the amplitude of the (biphasic) pulse (in mV, default = 800).
%  pulseDuration - the duration of the pulse (in seconds, default = 200 us).
% 
% Returns:
%            str - the program string.

% Author: Daniele Linaro - March 2012

if channel < 1 || channel > 8
    error('Valid channels are in the range [1,8].');
end

if frequency <= 0
    error('The stimulation frequency must be positive.');
end

if ~ exist('pulseWidth','var')
    pulseWidth = 800;
end

if ~ exist('pulseDuration','var')
    pulseWidth = 200e-6;
end

dt = 20e-6;        % time step
waitDur = [100e-6, 10e-3, 1];   % waiting times of the stimulator
t = 1/frequency;

npulses = pulseDuration / dt;
npulses = npulses * 2;      % biphasic stimulation
npulses = npulses + 1;      % will be set to 0 and will be held

waitTime = t - npulses * dt;
nWait = zeros(3,1);

for k=3:-1:1
    nWait(k) = floor(waitTime / waitDur(k));
    waitTime = mod(waitTime, waitDur(k));
end

npulses = int32(npulses + waitTime / dt);
nranges = 1 + length(find(nWait~=0));
str = sprintf('prog\tk%d;ranges=%d;pulses=%d;', channel, nranges, npulses);

n = int32(pulseDuration/dt);
for k=1:n
    str = [str, sprintf('%.0f;', -pulseWidth)];
end
for k=1:n
    str = [str, sprintf('%.0f;', pulseWidth)];
end
for k=2*n+1:npulses
    str = [str, '0;'];
end

for k=1:3
    if nWait(k) ~= 0
        str = [str, sprintf('wait%d=%d;', k, nWait(k))];
    end
end

dur = double(npulses)*dt + waitDur*nWait;
if abs(t-dur) > 1e-9
    fprintf(2, 'Unable to build the command string.\n');
    str = '';
else
    fprintf(1, 'The length of the command string is %d characters.\n', length(str));
end

