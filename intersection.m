function z = intersection(x,y)

if isempty(x) || isempty(y)
    z = [];
    return;
end

x = sort(x);
y = sort(y);

try
if all(x < y(1)) || all(x > y(2))
    z = [];
else
    z = [max(x(1),y(1)) , min(x(2),y(2))];
end
catch
    keyboard
end