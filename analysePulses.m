function analysePulses(t,Vpre,Vpost,I)
% analysePulses(t,Vpre,Vpost,I)

n = size(Vpre,1);
dt = diff(t(1:2));
idx = find(I(1,:));
stim = find(diff(idx) > 1);
stim = [1, stim+1, length(idx)+1];
nstim = length(stim)-1;
t0 = t(idx(1)-1);
tend = t(idx(end)+1);

dt_scale_bar = 0.1;       % [s]
t_scale_bar = t(idx(stim(end-2))) + (diff(t(idx(stim(end-2:end-1)))) - dt_scale_bar)/2;

ndx = find(t > 0.9*t0 & t < 1.1*tend);
preamble = round(0.9*t0/dt);

VPRE = meanWithOffset(Vpre,preamble);
VPOST = meanWithOffset(Vpost,preamble);

lightRed = [1,.6,.6];

figure;
axes('Position',[0.1,0.55,0.8,0.4],'NextPlot','Add');
for k=1:nstim
    patch([t(idx(stim(k))),t(idx(stim(k+1)-1)),t(idx(stim(k+1)-1)),t(idx(stim(k)))],...
        [-20,-20,100,100],lightRed,'EdgeColor',lightRed);
end
plot(t(ndx),VPRE(ndx),'k');
placeScaleBars(t_scale_bar,40,dt_scale_bar,40,...
    sprintf('%d ms',dt_scale_bar*1e3),'40 mV','k','LineWidth',2);
title(sprintf('Number of repetitions: %d', n));
axis tight;
axis off;

axes('Position',[0.1,0.1,0.8,0.45],'NextPlot','Add');
for k=1:nstim
    patch([t(idx(stim(k))),t(idx(stim(k+1)-1)),t(idx(stim(k+1)-1)),t(idx(stim(k)))],...
        [-2,-2,10,10],lightRed,'EdgeColor',lightRed);
end
plot(t(ndx),VPOST(ndx),'k');
placeScaleBars(t_scale_bar,4,[],4,'','4 mV','k','LineWidth',2);
axis tight;
axis off;

set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,8,4]);
print('-depsc2','pulses.eps');
