function hndl = plotFractions(x,dx,groups,cmap)
% hndl = plotFractions(x,dx,groups,cmap)

n = size(groups,2);
if ~ exist('cmap','var') || isempty(cmap)
    cmap = lines(n);
end
y = cumsum(sum(groups)/size(groups,1));
y = [0,y];
x = [x-dx/2,x-dx/2,x+dx/2,x+dx/2];
hndl = zeros(length(y)-1,1);
for i=length(y):-1:2
    hndl(i-1) = plot(x,y([i-1,i,i,i-1]),'Color',cmap(i-1,:),'LineWidth',2);
    if y(i-1) == y(i)
        set(hndl(i-1),'Visible','Off');
    else
        text(x(end)-dx/2,sum(y([i-1,i]))/2,sprintf('n=%d',sum(groups(:,i-1))),...
            'Rotation',90,'Color',cmap(i-1,:),'HorizontalAlignment','Center',...
            'VerticalAlignment','Middle','FontSize',12);
    end
end
