function out = stein(T, A, tau, freq, dt)
% STEIN simulates a Stein process.
% 
% out = stein(T, A, tau, freq, dt)
% 
% Parameters:
%      T - duration of the trace [s]
%      A - amplitude of the jump
%    tau - time constant of the exponential decay between successive events [s]
%   freq - mean frequency of the (exponentially-distributed) events [Hz]
%     dt - integration step [s]
%
% x = (1-dt/tau) x + A  1
%

%  Author: Michele Giugliano - mgiugliano@gmail.com

if freq > 0
    N    = 2*freq*T;
    temp = random('exp', 1./freq, 1, N);
else
    freq = -freq;
    N    = freq*T;
%     N =5
    temp = 1./freq * ones(N,1);
end

% Note:
% By definition 
%
% [nn,xx] = hist(temp, 100);
% dx      = (xx(2)-xx(1));
% nn      = nn / (length(temp) * dt);
%
% converges to [xx, (1./freq)*exp(-xx*(1./freq))]
%

M    = floor(T/dt);
u    = zeros(M,1);
t    = 0;
i    = 1;
k    = 0;
while k <= M
    t    = t + temp(i);
    k    = floor(t/dt);
    if (k>M), break; end;
    u(k) = 1;
    i    = i + 1;
    if (i>N), break; end;
end

AA  = [1, (dt/tau-1)]; 
BB  = [0, A];
out = filter(BB, AA, u, 0);

end