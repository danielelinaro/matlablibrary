function R = computeInputResistance(t,V,stim)
% R = computeInputResistance(t,V,stim)

n = size(V,1);
dV = zeros(n,1);

t0 = cellfun(@(x) sum(x(1:3,1)), stim);
stimdelay = cellfun(@(x) x(3,1), stim);
stimdur = cellfun(@(x) x(4,1), stim);
dI = cellfun(@(x) diff(x(4:-1:3,3)), stim) * 1e-12;

for k=1:n
    a = find(t > t0(k)-0.25*stimdelay(k) & t < t0(k));
    b = find(t > t0(k)+0.75*stimdur(k) & t < t0(k)+stimdur(k));
    dV(k) = (mean(V(k,a)) - mean(V(k,b))) * 1e-3;
end

R = mean(dV ./ dI);

