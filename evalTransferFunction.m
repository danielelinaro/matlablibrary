function [w,mag,phi] = evalTransferFunction(A,Z,P,Dt,w1,w2,n)
% evalTransferFunction evaluates a rational transfer function over
% a specified interval
%
% [w,mag,phi] = evalTransferFunction(A,Z,P,Dt,w1,w2,n)
% 
% Parameters:
%     A - the constant term in the expression of the transfer function.
%     Z - the zeros of the transfer function (in radians/sec).
%     P - the poles of the transfer function (in radians/sec).
%    Dt - the delay that accounts for the best fit of the phase of
%               the transfer function (in seconds).
% w1,w2 - lower and upper bounds of the frequency interval (in
%         radians/sec).
%     n - number of points.
% 
% Returns:
%     w - the frequency values at which the magnitude was evaluated (in radians/sec).
%   mag - the magnitude of the response of the system.
%   phi - the phase of the response of the system.
% 
% See also fitTransferFunction

% Author: Daniele Linaro - sometime in 2013

if ~ exist('n','var')
    n = 100;
end

w = logspace(log10(w1),log10(w2),n);

%%% first version
M = length(Z);
N = length(P);
mag = repmat(A,size(w));
mag = mag * prod(P) / prod(Z);
phi = zeros(size(w));
for k=1:M
    mag = mag .* sqrt(w.^2 + Z(k)^2);
    phi = phi + atan(w/Z(k));
end
for k=1:N
    mag = mag ./ sqrt(w.^2 + P(k)^2);
    phi = phi - atan(w/P(k));
end
% mag = abs(mag);
phi = phi-w*Dt;

%%% equivalent version
% sys = zpk(Z,P,A*prod(P)/prod(Z));
% [mag,phi] = bode(sys,w);
% mag = squeeze(mag);
% phi = squeeze(phi)' - w*Dt;
