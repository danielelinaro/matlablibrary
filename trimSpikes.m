function Vout = trimSpikes(t,Vin,tth,tend,ref_points,window,mode)
% Vout = trimSpikes(t,Vin,tth,tend,ref_points,window,mode)

narginchk(4,7);

if length(ref_points) ~= 2
    error('You must provide two reference points.');
end
use_end = nan(2,1);
for i=1:2
    if strcmpi(ref_points{i},'threshold')
        use_end(i) = 0;
    elseif strcmpi(ref_points{i},'end')
        use_end(i) = 1;
    else
        error('reference points must be either ''threshold'' or ''end''.');
    end
end

if all(use_end==0) && all(window==0)
    Vout = Vin;
    return;
end

if ~ exist('mode','var')
    mode = 'nan';
end

if ~strcmp(mode,'line') && ~strcmp(mode,'nan')
    error('mode must be either ''line'' or ''nan''.');
end

dt = diff(t(1:2));
Vout = Vin;
ntrials = size(Vin,1);
for i=1:ntrials
    nspks = length(tth{i});
    for j=1:nspks
        if use_end(2) && tend{i}(j)-tth{i}(j) > 15e-3
            keyboard
        end
        if use_end(1)
            tstart = tend{i}(j);
        else
            tstart = tth{i}(j);
        end
        if use_end(2)
            tstop = tend{i}(j);
        else
            tstop = tth{i}(j);
        end
        tstart = tstart - window(1);
        tstop = tstop + window(2);
        try
            Vstart = Vin(i,round(tstart/dt)-1);
        catch
            tstart = t(1);
            Vstart = Vin(i,1);
        end
        try
            Vstop = Vin(i,round(tstop/dt)+1);
        catch
            tstop = t(end);
            Vstop = Vin(i,end);
        end
        ndx = find(t>=tstart & t<=tstop);
        if ~ isempty(ndx)
            switch mode
                case 'line',
                    Vout(i,ndx) = Vstart + ...
                    (Vstop-Vstart)/(tstop-tstart) * (t(ndx) - tstart);
                case 'nan',
                    Vout(i,ndx) = nan;
            end
        end
    end
end
