function features = APanalysis(t,V,thresh,deriv,hndl,window)
% APANALYSIS extracts descriptive features from an action potential.
% 
% features = APanalysis(t,V,thresh,hndl,window)
% 
% Parameters:
%          t - (1xN) time vector, where N is the number of samples.
%          V - (MxN) voltage vector, where M is the number of trials.
%     thresh - spike threshold.
%      deriv - which derivative to use.
%       hndl - handle of the axes where to plot the results. Use 0 if
%              you don't want to plot.
%     window - plot interval.
% 
% Returns:
%   features - a struct with the summary of features of the AP.
% 

% Author: Daniele Linaro - May 2012

% default values
if ~ exist('thresh','var') || isempty(thresh)
    thresh = -20;
end
if ~ exist('deriv','var')
    deriv = [];
end
if ~ exist('hndl','var')
    hndl = [];
end
if exist('window','var')
    if length(window) ~= 2 
        error('window must be a 2-element vector.');
    end
    t0 = window(1);
    t1 = window(2);
else
    t0 = [];
    t1 = [];
end

features.nspks = size(V,1);
coeff = sqrt(features.nspks);

pars = defaultParameters(loadCellType());
max_ahp_dur = pars.max_ahp_dur;
max_adp_dur = pars.max_adp_dur;
min_dV_thresh = pars.min_dV_thresh;
[tpeak,Vpeak] = extractAPPeak(t,V,thresh,pars.min_isi);
[tth,Vth] = extractAPThreshold(t,V,thresh,tpeak,deriv);
[tahp,Vahp] = extractAPAHP(t,V,max_ahp_dur,thresh,tpeak,tth);
[tend,Vend] = extractAPEnd(t,V,thresh,pars.min_dV_thresh_coeff,tpeak,Vth,tahp);
[tadp,Vadp] = extractAPADP(t,V,max_adp_dur,thresh,tth,tahp);
[Vhalf,width,interval] = extractAPHalfWidth(t,V,thresh,tpeak,Vpeak,tth,Vth,tend);
accel = extractAPAcceleration(diff(t(1:2)),V,pars.dVdtThresh);
rapidness = extractAPRapidness(t,V,tpeak,tth,Vth,min_dV_thresh,pars.rapidness_window,1,1);

%%% spike peak
features.peak.tm = mean(cell2mat(tpeak));
features.peak.tstd = std(cell2mat(tpeak));
features.peak.tsem = std(cell2mat(tpeak)) / coeff;
features.peak.Vm = mean(cell2mat(Vpeak));
features.peak.Vstd = std(cell2mat(Vpeak));
features.peak.Vsem = std(cell2mat(Vpeak)) / coeff;

%%% spike threshold
features.threshold.tm = mean(cell2mat(tth));
features.threshold.tstd = std(cell2mat(tth));
features.threshold.tsem = std(cell2mat(tth)) / coeff;
features.threshold.Vm = mean(cell2mat(Vth));
features.threshold.Vstd = std(cell2mat(Vth));
features.threshold.Vsem = std(cell2mat(Vth)) / coeff;

%%% spike end
features.end.tm = mean(cell2mat(tend));
features.end.tstd = std(cell2mat(tend));
features.end.tsem = std(cell2mat(tend)) / coeff;
features.end.Vm = mean(cell2mat(Vend));
features.end.Vstd = std(cell2mat(Vend));
features.end.Vsem = std(cell2mat(Vend)) / coeff;

%%% spike AHP
features.ahp.tm = mean(cell2mat(tahp));
features.ahp.tstd = std(cell2mat(tahp));
features.ahp.tsem = std(cell2mat(tahp)) / coeff;
features.ahp.Vm = mean(cell2mat(Vahp));
features.ahp.Vstd = std(cell2mat(Vahp));
features.ahp.Vsem = std(cell2mat(Vahp)) / coeff;
features.max_ahp_dur = max_ahp_dur;

%%% spike ADP
features.adp.tm = mean(cell2mat(tadp));
features.adp.tstd = std(cell2mat(tadp));
features.adp.tsem = std(cell2mat(tadp)) / coeff;
features.adp.Vm = mean(cell2mat(Vadp));
features.adp.Vstd = std(cell2mat(Vadp));
features.adp.Vsem = std(cell2mat(Vadp)) / coeff;
features.max_adp_dur = max_adp_dur;

%%% spike half width
features.halfwidth.Vm = mean(cell2mat(Vhalf));
features.halfwidth.Vstd = std(cell2mat(Vhalf));
features.halfwidth.Vsem = std(cell2mat(Vhalf)) / coeff;
features.halfwidth.wm = mean(cell2mat(width)*1e3);
features.halfwidth.wstd = std(cell2mat(width)*1e3);
features.halfwidth.wsem = std(cell2mat(width)*1e3) / coeff;

%%% rise time
rise_time = (cell2mat(tpeak) - cell2mat(tth)) * 1e3;
features.rise_time.tm = mean(rise_time);
features.rise_time.tstd = std(rise_time);
features.rise_time.tsem = std(rise_time) / coeff;

%%% amplitude
amp = cell2mat(Vpeak) - cell2mat(Vth);
features.amplitude.Vm = mean(amp);
features.amplitude.Vstd = std(amp);
features.amplitude.Vsem = std(amp) / coeff;

%%% rise rate
features.rise_rate.mean = mean(amp./rise_time);
features.rise_rate.std = std(amp./rise_time);
features.rise_rate.sem = std(amp./rise_time) / coeff;

%%% acceleration
idx = cellfun(@(x) length(x)==1 & all(x)>0, accel);
features.acceleration.mean = mean(cell2mat(accel(idx)));
features.acceleration.std = std(cell2mat(accel(idx)));
features.acceleration.sem = std(cell2mat(accel(idx))) / coeff;

%%% rapidness
tmp = flattenCellArray(rapidness,'full');
features.rapidness.mean = nanmean(tmp);
features.rapidness.std = nanstd(tmp);
features.rapidness.sem = nanstd(tmp) / sqrt(sum(~isnan(tmp)));

if ~isempty(hndl)
    lightRed = [1,.6,.6];
    grey = [.6,.6,.6];
    try
        axes(hndl);
    catch
        hndl = figure;
    end
    hold on;
    if ~ isempty(t0)
        patch([t0,t0,t1,t1],[-100,50,50,-100],lightRed,'EdgeColor',lightRed);
        axis([t0-3e-3,t1+5e-3,-80,50]);
        plot([t0-3e-3,t0-2e-3],-40+[0,0],'k','LineWidth',2);
        plot(t0-3e-3+[0,0],[-35,-25],'k','LineWidth',2);
    end
    plot(t,V,'Color',[.6,.6,.6]);
    axis tight;
    axis([xlim,-80,50]);
    if size(V,1) > 1
        plot(t,mean(V),'k','LineWidth',2);
    end
    plot(xlim,-65+[0,0],'--','Color',grey);
    plot(xlim,[0,0],'--','Color',grey);
    plot(features.threshold.tm,features.threshold.Vm,'ko','MarkerSize',5,...
        'MarkerFaceColor','c','LineWidth',1);
    plot(tpeak{1},Vpeak{1},'ko','MarkerSize',5,'MarkerFaceColor','m','LineWidth',1);
    plot(features.end.tm,features.end.Vm,'ko','MarkerSize',5,...
        'MarkerFaceColor','r','LineWidth',1);
    plot(tahp{1},Vahp{1},'ko','MarkerSize',5,'MarkerFaceColor','g','LineWidth',1);
    plot(tadp{1},Vadp{1},'ko','MarkerSize',5,'MarkerFaceColor','b','LineWidth',1);
    plot(interval{1}+[-0.5e-3;0.5e-3],Vhalf{1}+[0;0],'g','LineWidth',2);
    if ~ isempty(t0)
        text(t0-2.5e-3,-41,'1 ms','HorizontalAlignment','Center','VerticalAlignment','Top');
        text(t0-3.1e-3,-30,'10 mV','HorizontalAlignment','Right');
        text(t0-3.1e-3,-65,'-65 mV','HorizontalAlignment','Right');
        text(t0-3.1e-3,0,'0 mV','HorizontalAlignment','Right');
        text(t1+2.5e-3,50,sprintf('V_{th} = %.1f mV', Vth{1}(1)),...
            'HorizontalAlignment','Left','VerticalAlignment','Top');
        text(t1+2.5e-3,43,sprintf('V_{peak} = %.1f mV', Vpeak{1}),...
            'HorizontalAlignment','Left','VerticalAlignment','Top');
        text(t1+2.5e-3,36,sprintf('V_{half} = %.1f mV', Vhalf{1}),...
            'HorizontalAlignment','Left','VerticalAlignment','Top');
        text(t1+2.5e-3,29,sprintf('t_{half} = %.2f ms', features.halfwidth.wm),...
            'HorizontalAlignment','Left','VerticalAlignment','Top');
    end
    axis off;
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,6,4],'PaperSize',[6,4]);
    print('-dpdf','ap.pdf');
end

