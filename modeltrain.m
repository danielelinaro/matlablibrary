function [modelPeakAmplitudes,Vmodel] = modeltrain(TMpars,EPSPpars,T,eventTimes,peakTimes)
% [modelPeakAmplitudes,Vmodel] = modeltrain(TMpars,EPSPpars,T,eventTimes,peakTimes)

dt = diff(T(1:2));
peaks = round(peakTimes / dt);
events = round(eventTimes / dt);

A = TMpars(1);
U = TMpars(2);
D = TMpars(3);
F = TMpars(4);
Vmodel = zeros(size(T));
amplitudes  = FDM(A, U, F, D, eventTimes);                    % Tsodyks-Markram, 1997
for k=1:length(events)
    tmq = [0*T(1:events(k)-1), amplitudes(k) * ...
        doubleexp(EPSPpars(1), EPSPpars(2), EPSPpars(3), T(events(k):end)-T(events(k)))];
    Vmodel = Vmodel + tmq;
end
if length(TMpars) > 4
    A = TMpars(5);
    U = TMpars(6);
    D = TMpars(7);
    F = 1e-3;
    amplitudes  = FDM(A, U, F, D, eventTimes);
    for k=1:length(events)
        tmq = [0*T(1:events(k)-1), amplitudes(k) * ...
            doubleexp(EPSPpars(1), EPSPpars(2), EPSPpars(3), T(events(k):end)-T(events(k)))];
        Vmodel = Vmodel + tmq;
    end
end

ppeaks = [peaks, length(T)];
modelPeakAmplitudes = zeros(size(peaks));
for k=1:length(peaks),
    tmp = Vmodel(ppeaks(k):ppeaks(k+1));
    modelPeakAmplitudes(k) = max(tmp);
end
