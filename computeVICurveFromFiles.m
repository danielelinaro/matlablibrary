function [I,V,R,V0,sag,sag_ratio,rebound,tau] = computeVICurveFromFiles(files,Ke,doPlot,Irange,Vrange)
% [I,V,R,V0,sag,sag_ratio,rebound] = computeVICurveFromFiles(files,Ke,doPlot)
%
% Arguments:
%   files - the files to read.
%      Ke - the kernel electrode to use. If Ke = [], then the traces are
%           assumed to be already compensated.
%  doPlot - whether to make a plot of the results. The default is no.
% 
% Returns:
%        I - the values of injected current.
%        V - the corresponding mean voltage at steady state.
%        R - input resistance (in MOhm), as a linear fit to the (I,V) points.
%       V0 - resting membrane potential, the point (0,V0) on the (I,V) curve.
%      sag - the overshoot of membrane potential when applying a current.
%  rebound - number of rebound spikes present after the end of the stimulus.
%      tau - the time constant of the cell.
% 
% See also computeVICurve.
% 

% Author: Daniele Linaro

if ~ exist('Vrange','var') || isempty(Vrange)
    Vrange = [-1000,1000];
end

if ~ exist('Irange','var') || isempty(Irange)
    Irange = [-10000,10000];
end

if ~ exist('doPlot','var') || isempty(doPlot)
    doPlot = 0;
end

if length(files) == 1 && strcmp(files{1},'recording.mat')
    data = load('recording.mat');
    t = (0:size(data.V,2)-1) * data.dt;
    Vc = data.V;
    Iinj = data.I;
    stim = cell(size(Vc,1),1);
    j = find(t > data.tstart,1);
    for i=1:length(stim)
        stim{i} = zeros(3,12);
        stim{i}(:,2) = 1;
        stim{i}(1,1) = data.tstart;
        stim{i}(2,1) = data.dur;
        stim{i}(3,1) = t(end) - data.tstart - data.dur + data.dt;
        stim{i}(2,3) = data.I(i,j);
    end
else
    [t,Vr,Iinj,stim] = loadH5Traces(files);
    Iinj = Iinj(:,1:size(Vr,2));  % this should not be necessary...
    if ~ isempty(Ke)
        Vc = AECoffline(Vr,Iinj,Ke);
    else
        Vc = Vr;
    end
end
Fc = 1000;
Vc = filterTrace(Vc,1/diff(t(1:2)),Fc,2);
Voff = mean(Vc(:,t>5/Fc & t<stim{1}(1)),2);
idx = find(t < 5/Fc);
Vc(:,idx) = repmat(Voff,[1,length(idx)]);
[I,V,R,V0,sag,sag_ratio,rebound,tau] = computeVICurve(t,Vc,stim,Irange,Vrange);

if doPlot
    idx = find(t > sum(stim{1}(1:end-2,1))-0.5);
    figure;

    axes('Position',[0.5,0.1,0.4,0.3],'NextPlot','Add');
    plot(I,V,'bo','MarkerFaceColor','r','MarkerSize',3);
    plot(I([1,end]),polyval([R.value*1e6,V0.value*1e-3],I([1,end])*1e-12)*1e3,...
        'k--','LineWidth',1);
    axis tight;
    xlabel('I (pA)');
    ylabel('V (mV)')
    
    Istep = max(diff(sort(cellfun(@(x) x(end-1,3), stim))));
    
    set(gca,'Position',[0.15,0.1,0.4,0.25],'FontName','Arial','FontSize',9);
    
    axes('Position', [0.7,0.25,0.1,0.1],'FontName','Arial','FontSize',9);
    text(0,0.5,sprintf('R = %.2f M\\Omega', R.value));
    text(0,0,sprintf('\\tau = %.0f ms', tau*1e3));
    axis off;
    
    axes('Position',[0.1,0.5,0.45,0.4],'NextPlot','Add',...
         'FontName','Arial','FontSize',9);
    plot(t(idx),Vc(:,idx),'k','LineWidth',1);
    axis tight;
    axis([xlim,ylim+[-5,5]]);
    xl = xlim;
    xl(1) = xl(2)-1;
    yl = ylim;
    yl(2) = yl(1)+5;
    plot(xl,yl(1)+[0,0],'k','LineWidth',2);
    plot(xl(1)+[0,0],yl,'k','LineWidth',2);
    axis off;
    text(xl(1),sum(yl)/2,'5 mV',...
        'HorizontalAlignment','Right',...
        'VerticalAlignment','Middle');
    text(sum(xl)/2,yl(1),'1 s',...
        'HorizontalAlignment','Center',...
        'VerticalAlignment','Top');

    axes('Position',[0.65,0.55,0.3,0.24],'NextPlot','Add',...
         'FontName','Arial','FontSize',9);
    plot(t(idx),Iinj(:,idx),'k','LineWidth',1);
    axis tight;
    axis([xlim,ylim-[2*Istep,0]])
    xl = xlim;
    xl(1) = xl(2)-1;
    yl = ylim;
    yl(2) = yl(1)+Istep;
    plot(xl,yl(1)+[0,0],'k','LineWidth',2);
    plot(xl(1)+[0,0],yl,'k','LineWidth',2);
    axis off;
    text(xl(1),sum(yl)/2,sprintf('%g pA',Istep),...
        'HorizontalAlignment','Right',...
        'VerticalAlignment','Middle');
    text(sum(xl)/2,yl(1),'1 s',...
        'HorizontalAlignment','Center',...
        'VerticalAlignment','Top');

    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,9,6]);
    print('-depsc2','VI_curve.eps');
end
