/***********************************************************************************************

 Antwerp, 16/11/2009 
 Antwerp, 11/7/2009
 Bern, 30/4/2004 - Michele Giugliano, PhD & Maura Arsiero, PhD

***********************************************************************************************/

#include "lib/generate_trial.h"
#include "lib/generate_trial.c"
#include "lib/error_msgs.c"
#include "lib/file_parsing.c"
#include "lib/rando.c"
#include "lib/waveforms.c"

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define OUTFILENAME "stimulus.dat"	// Default output file name..


int main(int argc, char *argv[]) {

double *output;                   // Utility variable.
double **allchans;                // Output waveform(s).
INT index;                        // Index addressing the output waveform.
double srate, dt;                 // User-specified sampling rate [Hz] and period [s].
int output_on_file, verbose;      // Flags to turn on the write_on_file behavior and verbosity.
INT i, Nchan;	                  // Requested number of channels to generate..
INT chan_size;                    // Number of samples per channels (all of them must share it)..
int ss;		                      // Bool containing the existence of all the files specified..
FILE *fp, *fopen();

srand49((long) time ((time_t *) 0)); // This uniquely initializes the seed of the random number generator by the current time

// Argument parsing..
if (argc < 5) { printf(USAGE, argv[0]); return 0; }

verbose        = atoi(argv[1]); if (verbose != 1) verbose = 0;
output_on_file = atoi(argv[2]); 
srate          = atof(argv[3]);
if (srate <= 0) { error("Zero or negative sampling rate !", verbose);  return -1; }
dt = 1./srate;  

	
	
// Memory is allocated..
Nchan    = argc - 4;		// ..because the first four argv[] are: name of exe, 'verbose', 'outonfile' and 'srate'.
allchans = (double **) calloc(Nchan, sizeof(double *));		// Memory for the output data structure is allocated here.
if (allchans == NULL) { error("Unable to allocate the output channel(s) !", verbose);  return -1; }

	
	
// Actual waveforms are generated..
chan_size      = 0;			// This will contain the number of samples of the first wave (all the others must match it!)
for (i=0; i < Nchan; i++) {
 ss = generate_trial(argv[4+i], verbose, 0, OUTFILENAME, &output, &index, srate, dt);
 if (ss == -1) return -1;
 if (i==0) chan_size = index;
 else     if (index != chan_size) { error("Output channel(s) do not share same length !", verbose);  return -1; } 
 allchans[i] = (double *) output;
}


// Output binary file is written on disk..		
if (output_on_file == 1) {
 fp = fopen(OUTFILENAME, "w");
 fwrite(&srate, sizeof(double), 1, fp);
 fwrite(&Nchan, sizeof(unsigned long int), 1, fp);
 fwrite(&chan_size, sizeof(unsigned long int), 1, fp);
 for (i=0; i<Nchan; i++)	fwrite(allchans[i], sizeof(double), chan_size, fp);
 fclose(fp);
} // end if


// Memory is finally released..
for (i=0; i<Nchan; i++) free(allchans[i]);	// Memory is released here..
free(allchans);					// Memory is released here..
return 0;
} // end main()
