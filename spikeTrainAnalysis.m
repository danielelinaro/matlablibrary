function features = spikeTrainAnalysis(T,V)
% features = spikeTrainAnalysis(T,V)

[r,c] = size(V);
if r > c
    V = V';
    n = c;
else
    n = r;
end

V = filterTrace(V,1/diff(T(1:2)),5000,2);
features = repmat(struct([]), [n,1]);
[tth,Vth] = extractAPThreshold(T,V);
[tp,Vp] = extractAPPeak(T,V);

for k=1:n
    if isempty(tth{k}) % no spikes
        continue;
    end
    if length(tp{k}) ~= length(tth{k})
        error('The number of spike thresholds is different from the number of spike peaks!');
    end

    nspikes = length(Vth{k});
    
    % isi
    %features(k).isi = diff(tp{k});
    features(k).f = nspikes / diff(T([1,end]));
    
    % spike threshold
    features(k).threshold.Vm = mean(Vth{k});
    features(k).threshold.Vs = std(Vth{k}) / sqrt(nspikes);

    % spike peak
    features(k).peak.Vm = mean(Vp{k});
    features(k).peak.Vs = std(Vp{k}) / sqrt(nspikes);
    
    % AP drop
    if nspikes > 1
        features(k).ap_drop.ap12 = (Vp{k}(1)-Vth{k}(1)) - (Vp{k}(2)-Vth{k}(2));
        features(k).ap_drop.ap1n = (Vp{k}(1)-Vth{k}(1)) - (Vp{k}(end)-Vth{k}(end));
        features(k).ap_drop.ap2n = (Vp{k}(2)-Vth{k}(2)) - (Vp{k}(end)-Vth{k}(end));
        features(k).ap_drop.max = features(k).ap_drop.ap12;
        for ii=2:nspikes-1
            drop = (Vp{k}(ii)-Vth{k}(ii)) - (Vp{k}(ii+1)-Vth{k}(ii+1));
            if drop > features(k).ap_drop.max
                features(k).ap_drop.max = drop;
            end
        end
    end
        
    % accomodation index
    if nspikes >= 11
        features(k).accom_index = diff(tp{k}(1:2)) / diff(tp{k}(10:11));
    end
    
    % delay in first spike
    features(k).delay_first = (tp{k}(1) - T(1)) * 1e3;
    
end
