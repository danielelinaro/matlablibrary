function R = computeReliabilityCorrelation(V,sigma)
% R = computeReliabilityCorrelation(V,sigma)

[r,c] = size(V);
if r > c
    V = V';
end

[N,ns] = size(V);
nk = 1000;
% x = linspace(-5*sigma, 5*sigma, nk);
x = linspace(-60e-3, 60e-3, nk);
K = 1/sqrt(2*pi*sigma^2) * exp(-x.^2/(2*sigma^2));
K = K/sum(K);
s = zeros(N,ns+nk-1);
nrm = zeros(N,1);

for k=1:N
    s(k,:) = conv(V(k,:),K);
    nrm(k) = norm(s(k,:));
end

R = 0;
for ii=1:N
    for jj=ii+1:N
        R = R + s(ii,:)*s(jj,:)'/(nrm(ii)*nrm(jj));
    end
end

R = 2/(N*(N-1)) * R;
        