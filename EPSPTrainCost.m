function c = EPSPTrainCost(TMpars,EPSPpars,T,eventTimes,peakTimes,peakAmplitudes)
% c = EPSPTrainCost(TMpars,EPSPpars,T,eventTimes,peakTimes,peakAmplitudes)
modelPeakAmplitudes = modeltrain(TMpars,EPSPpars,T,eventTimes,peakTimes);
c = norm(peakAmplitudes - modelPeakAmplitudes);

