function [Vout,idx] = removeSpikes(t,Vin,threshold,window)
% REMOVESPIKES substitutes spikes with NaN values.
% 
% [Vout,idx] = removeSpikes(t,Vin,threshold,window)
%
% Parameters:
%         t - time trace.
%       Vin - voltage trace(s). Multiple trials should be arranged in a MxN
%             matrix, where M is the number of trials and N is the number of
%             samples in each trial.
% threshold - the threshold for spike detection.
%    window - a 2-elements vector that contains the interval before and after
%             the spike that should be removed. 
% 
% Returns:
%      Vout - a matrix with the same size a Vin, where spikes are
%             substituted with NaNs.
%       idx - indices such that Vout(k,idx{k}) = NaN, with k = [1,M].
% 

% Author: Daniele Linaro - June 2012

[r,c] = size(Vin);
if r > c
    Vin = Vin';
end

if ~ isempty(threshold)
    spks = extractSpikes(t,Vin,threshold,1);
else
    spks = extractAPThreshold(t,Vin);
end
Vout = Vin;
ntrials = size(Vin,1);
idx = cell(ntrials,1);
for ii=1:ntrials
    nspks = length(spks{ii});
    for jj=1:nspks
        idx{ii} = [idx{ii}, find(t>spks{ii}(jj)-window(1) & t<spks{ii}(jj)+window(2))];
    end
    Vout(ii,idx{ii}) = NaN;
end
