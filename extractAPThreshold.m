function [tthresh,vthresh] = extractAPThreshold(T,V,thresh,tp,deriv)
% EXTRACTAPTHRESHOLD finds the spike threshold using the first maximum
% of the third derivative of the voltage.
%
% [tthresh,vthresh] = extractAPThreshold(T,V,thresh,tp,deriv)
%
% Parameters:
%        T - time vector, with size (1xN) where N is the number of samples.
%        V - voltage trace(s), with size (MxN), where M is the number of 
%            traces and N is the number of samples.
%   thresh - threshold for _preliminary_ spike detection: it can either be an (Mx1) array or a scalar.
%       tp - peak of the action potentials, as returned by extractAPPeak.
%   deriv - which deriv to use for the detection (1 = first derivative,
%            3 = third derivative (default)).
%
% Return values:
%  tthresh - a cell array [of size (Mx1)] with the spike times for each voltage trace.
%  vthresh - a cell array [of size (Mx1)] with the threshold voltage values.
%
% See also extractAPPeak.

% Daniele Linaro - February 2012

[default_thresh,default_deriv] = loadThreshold;

if ~ exist('deriv','var') || isempty(deriv)
    deriv = default_deriv;
elseif deriv ~= 1 && deriv ~= 3
    fprintf(1, 'deriv = %d\n', deriv);
    error('Unknown derivative: it must be either 1 or 3.');
end
suffix = {'st','nd','rd'};
fprintf(1, 'extractAPThreshold>> using the %d%s derivative.\n',...
    deriv, suffix{deriv});

if ~ exist('thresh','var')
    thresh = default_thresh;
end

thresh = adjustThreshold(V,thresh);

if ~ exist('tp','var') || isempty(tp)
    tp = extractAPPeak(T,V,thresh);
end
dt = T(2)-T(1);
nexp = size(V,1);
tthresh = cell(nexp,1);
vthresh = cell(nexp,1);
nspks = cellfun(@(x)length(x), tp);
% n = round(1e-3/dt);

if deriv == 1
    dVdt = (V(1,3:end)-V(1,1:end-2)) / (2*dt);
    if isempty(tp{1})
        idx = find(T>T(1)+2e-3 & T<T(end)-2e-3);
    else
        idx = find(T>T(1)+2e-3 & T<tp{1}(1)-2e-3);
        if isempty(idx)
            idx = find(T>tp{1}(1)+5e-3 & T<tp{1}(2)-5e-3);
        end
    end
    m = mean(dVdt(idx));
    s = std(dVdt(idx));
end

for ii=1:nexp

    if nspks(ii) == 0
        continue;
    end
    
    tthresh{ii} = nan(1,nspks(ii));
    vthresh{ii} = nan(1,nspks(ii));
    
    for jj=1:nspks(ii)
        if jj > 1
            interval = min(2e-3,diff(tp{ii}(jj-1:jj))-1e-3);
        else
            interval = 2e-3;
        end
        idx = find(T >= tp{ii}(jj)-interval & T <= tp{ii}(jj));
        t = T(idx);
        v = V(ii,idx);
        dvdt = (v(3:end)-v(1:end-2)) ./ (2*dt);
        if deriv == 1
            kk = find(dvdt > m+8*s, 1, 'first');
            if isempty(kk)
                [~,kk] = max(dvdt);
            end
            tthresh{ii}(jj) = t(kk+1);
            vthresh{ii}(jj) = v(kk+1);
        else
%             baseline = mean(dvdt(1:n)) + 10*std(dvdt(1:n));
%             baseline = 0.2 * max(dvdt);
            d2vdt2 = (dvdt(3:end)-dvdt(1:end-2)) ./ (2*dt);
            d3vdt3 = (d2vdt2(3:end)-d2vdt2(1:end-2)) ./ (2*dt);
            [~,locs] = findpeaks(d3vdt3,'MinPeakDistance',2);
            [peaks,idx] = sort(d3vdt3(locs),'descend');
            if idx(1) > idx(2) && peaks(2)/peaks(1) > 0.4
                kk = locs(idx(2));
            else
                kk = locs(idx(1));
            end
%             if d3vdt3(locs(end-1)) / d3vdt3(locs(end)) > 0.4 || ...
%                     (v(locs(end-1)+3) <= -30 && v(locs(end)+3) >= -20)
%                 kk = locs(end-1);
%             else
%                 kk = locs(end);
%             end
%             if jj == 31
%                 figure;
%                 hold on;
%                 plot(T,V,'k','LineWidth',1);
%                 plot(t,v,'r','LineWidth',1);
%                 plot(t(4:end-3),d3vdt3*1e-12,'g','LineWidth',1)
%                 plot(t(locs+3),d3vdt3(locs)*1e-12,'mo','MarkerFaceColor','w',...
%                     'LineWidth',1,'MarkerSize',3);
%                 plot(t(kk+3),d3vdt3(kk)*1e-12,'bs','MarkerFaceColor','w',...
%                     'LineWidth',1,'MarkerSize',4);
%                 keyboard
%             end
            tthresh{ii}(jj) = t(kk+3);
            vthresh{ii}(jj) = v(kk+3);
%             [~,locs] = findpeaks(fliplr(d3vdt3),'NPeaks',30);
%             while v(end-locs(1)-2) > -20 && length(locs) > 1
%                 locs = locs(2:end);
%             end
%             try
%             if d3vdt3(end-locs(1)+1) > d3vdt3(end-locs(2)+1)
%                 kk = locs(1);
%             else
%                 kk = locs(2);
%             end
%             catch
%                 [~,kk] = max(fliplr(dvdt));
%                 kk = kk-2;
%             end
%             tthresh{ii}(jj) = t(end-kk-2);
%             vthresh{ii}(jj) = v(end-kk-2);
        end
        if vthresh{ii}(jj) > thresh
            fprintf(1, 'extractAPThreshold>>> lowering one threshold value because detected one too high.\n');
            idx = find(v >= thresh(ii),1) - 1;
            try
            tthresh{ii}(jj) = t(idx);
            vthresh{ii}(jj) = v(idx);
            catch
                keyboard
            end
        end

%         keyboard        
%         figure;
%         hold on;
%         plot(t,v,'k','LineWidth',1);
%         plot(t(2:end-1),dvdt*1e-4,'r','LineWidth',1);
%         plot(t(3:end-2),d2vdt2*1e-8,'g','LineWidth',1);
%         plot(t(4:end-3),d3vdt3*1e-12,'b','LineWidth',1);
%         plot(t(end-locs(1)-2),d3vdt3(end-locs(1)+1)*1e-12,'cs','LineWidth',1,'MarkerFaceColor','w');
%         plot(t(end-locs(1)-2),v(end-locs(1)-2),'cs','LineWidth',1,'MarkerFaceColor','w');


%         for k=fliplr(locs)
%             if v(k+3) <= thresh(ii) && dvdt(k+2) <= baseline
%                 break
%             end
%         end
% %         if jj == 4
% %             keyboard
% %         end
% %         [~,k] = max(d3vdt3);        % NOTE that the k index refers to the d3vdt3 array,
% %                                     % which is displaced by 3 samples with
% %                                     % respect to the original v array.
% % 
% %         % This is a rather non-orthodox way of finding the right index of
% %         % the threshold, but it is due to the fact that the curve
% %         % of the third derivative (see Fig. 1 in Henze and Buzsaki,
% %         % Neuroscience, 2001) has two clear maxima (of which the first one
% %         % corresponds to the threshold) and many smaller maxima, due to the noise
% %         % introduced by successive derivations: as a consequence, it is not a wise
% %         % idea to use findpeaks(dv3dt3,1). Also the usage of max is slightly
% %         % problematic, because quite often the first peak is not the
% %         % absolute maximum of the trace, so we have to be smart and
% %         % find the first maximum with the following trick.
% %         while v(k+3) > thresh(ii)
% %             [~,k] = max(d3vdt3(1:k-1));
% %         end
%         k = k+3;
%         if isempty(t(k)) || isempty(v(k))
%             k = find(v > thresh(ii), 1);
%         end
%         tthresh{ii}(jj) = t(k);
%         vthresh{ii}(jj) = v(k);
    end
end
