function removeNeuronFromDB(cellID, experimenter)
% removeNeuronFromDB removes a neuron and its associated information from
% the database.
% 
% removeNeuronFromDB(cellID, experimenter)
% 
% Parameters:
%         cellID - the ID of the cell to remove.
%   experimenter - the name of the person who performed the recording.
% 
% NOTE that the script will NOT ask for confirmation before deleting the
% data from the DB.
% 

% Author: Daniele Linaro - September 2013.

% open the connection to the database
try
    hndl = mysql('open','localhost','daniele','ilenia');
catch err
    error('Unable to connect to the database [%s].', err.identifier);
end

% change database
foo = mysql('use electrophysiology');

% check whether the cell is present in the database
query = sprintf(['SELECT n.age FROM neurons n INNER JOIN experimenters e ON e.id = n.experimenter_id ', ...
    'WHERE n.id = ''%s'' AND e.name = ''%s'';'], cellID, experimenter);
age = mysql(query);
if isempty(age)
    fprintf(1, '%s: no such cell.\n', cellID);
else
    query = sprintf(['DELETE n.* FROM neurons n INNER JOIN experimenters e ON e.id = n.experimenter_id ', ...
    'WHERE n.id = ''%s'' AND e.name = ''%s'';'], cellID, experimenter);
    mysql(query);
end

mysql('close');
