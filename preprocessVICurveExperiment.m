function preprocessVICurveExperiment(Irange,Vrange)
% preprocessVICurveExperiment(Irange = [-1000,0], Vrange = [-100,0])

if ~ exist('Irange','var') || isempty(Irange)
    Irange = [-1000,0];
end

if ~ exist('Vrange','var') || isempty(Vrange)
    Vrange = [-100,0];
end

if ~ exist('recording.mat','file')
    kernels = dir('*_kernel.dat');
    if ~ isempty(kernels)
        Ke = load(kernels(1).name);
    else
        kernels = dir('../../ap/01/*_kernel.dat');
        if ~ isempty(kernels)
            Ke = load(['../../ap/01/',kernels(1).name]);
        else
            Ke = [];
        end
    end
    files = listH5Files;
else
    files = {'recording.mat'};
    Ke = [];
end
[I,V,R,V0,sag,sag_ratio,rebound,tau] = computeVICurveFromFiles(files,Ke,1,Irange,Vrange);
save('VI_curve.mat','I','V','R','V0','sag','sag_ratio','rebound','tau','Ke');
