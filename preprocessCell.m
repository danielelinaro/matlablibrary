function preprocessCell(varargin)
% preprocessCell	Perform a pre-processing of the data contained in the
% current folder and relative to a single cell recording.
% 
% preprocessCell(...)
% 
% Possible options are:
%                    CellType - the type of cell, used to set some default
%                               values. Default is 'PYR', other
%                               possibilities are 'PV' and 'SOM'.
%              SpikeThreshold - the threshold value to use for the
%                               detection of action potentials (default: -20 mV).
%               ForceAnalysis - whether to perform analysis even if it has
%                               already been performed (default false).
%              OverwriteFiles - whether to overwrite the scripts already
%                               present in the subfolders (default false).
%                               Implies ForceAnalysis.
%                 TemplateDir - where the template scripts are located
%                               (default $MATLAB_TEMPLATES_FOLDER).
%   DesiredAPFeaturesProtocol - which protocol to use to extract AP
%                               features, 'steps' or 'ap' (default 'steps').
%              RheobasesTimes - how many rheobase times should be
%                               considered to compute AP properties when
%                               DesiredAPFeaturesProtocol is 'steps'
%                               (default 3).
%             ForceZeroOffset - whether to consider the absence of an
%                               'offset' file as indication that the offset
%                               was effectively equal to 0 pA (default false).
%                    TargetDV - the value of Vm deflection to consider for
%                               the computation of the sag (default -20 mV).
%       APThresholdDerivative - which derivative to use for the extraction
%                               of the threshold: accepted values are 1 and
%                               3, with 3 being the default.
% 

% Author: Daniele Linaro - danielelinaro@gmail.com

[thresh,deriv] = loadThreshold(0);

p = inputParser;

p.addParameter('CellType', '', @ischar);
p.addParameter('SpikeThreshold', thresh, @isfloat);
p.addParameter('ForceAnalysis', false, @islogical);
p.addParameter('OverwriteFiles', false, @islogical);
p.addParameter('RheobaseTimes', 3, @(x) x>0);
p.addParameter('TemplateDir', getenv('MATLAB_TEMPLATES_FOLDER'), @ischar);
p.addParameter('DesiredAPFeaturesProtocol', 'steps', @ischar);
p.addParameter('ForceZeroOffset', false, @islogical);
p.addParameter('TargetDV', -20, @(x) x<0);
p.addParameter('APThresholdDerivative', 3);

parse(p, varargin{:});

if isempty(p.Results.CellType)
    cell_type = 'PYR';
else
    cell_type = p.Results.CellType;
end
spike_threshold = p.Results.SpikeThreshold;
force_analysis = p.Results.ForceAnalysis;
overwrite_files = p.Results.OverwriteFiles;
rheobase_times = p.Results.RheobaseTimes;
template_dir = p.Results.TemplateDir;
desired_AP_features_protocol = p.Results.DesiredAPFeaturesProtocol;
force_zero_offset = p.Results.ForceZeroOffset;
target_dV = p.Results.TargetDV;
ap_thresh_der = p.Results.APThresholdDerivative;

if isempty(ap_thresh_der)
    if exist('SPIKE_THRESHOLD','file')
        ap_thresh_der = deriv;
    else
        if strcmpi(cell_type,'cult')
            ap_thresh_der = 1;
        else
            ap_thresh_der = 3;
        end
    end
end

if overwrite_files
    force_analysis = true;
end

if isempty(template_dir)
    template_dir = [getenv('HOME'),'/MatlabLibrary/Templates'];
end

if ~ exist('SPIKE_THRESHOLD','file') || overwrite_files
    fid = fopen('SPIKE_THRESHOLD','w');
    fprintf(fid, '%f\n%d\n', spike_threshold, ap_thresh_der);
    fclose(fid);
end

if ~ exist('CELL_TYPE','file')
    fid = fopen('CELL_TYPE','w');
    fprintf(fid, '%s', cell_type);
    fclose(fid);
elseif ~ isempty(p.Results.CellType)
    fid = fopen('CELL_TYPE','r');
    old_cell_type = fscanf(fid, '%s');
    fclose(fid);
    if ~ strcmp(cell_type,old_cell_type)
        reply = input(sprintf('Overwrite old cell type [%s] with new cell type [%s]? [y/N] ', ...
            old_cell_type, cell_type), 's');
        if ~isempty(reply) && (reply == 'y' || reply == 'Y')
            fid = fopen('CELL_TYPE','w');
            fprintf(fid, '%s', cell_type);
            fclose(fid);
        end
    end
end

cwd = pwd;
idx = strfind(cwd,'/');
file = [cwd(idx(end)+1:end),'.dat'];
if exist(file,'file')
    outfile = [file(1:end-3),'kcl'];
    if ~ exist(outfile,'file')
        if isempty(which('ImportHEKA'))
            addpath ~/Postdoc/Research/MatlabLibrary/sigTOOL/program
            sigTOOL;
            addpath ~/Postdoc/Research/MatlabLibrary/HEKA
        end
        outfile = ImportHEKA(file);
    end
    data = load(outfile,'-mat');
    nchan = length(find(cellfun(@(x) strcmp(x(1:4),'chan'), fieldnames(data))));
    if ~ exist('vi','dir')
        folder = 'vi/01';
    else
        folder = '';
        for i=1:length(dir('vi/0*'))
            if length(dir(sprintf('vi/%02d',i))) == 2 || ...
                exist(sprintf('vi/%02d/recording.mat',i),'file')
                % folder is empty or the file already exists
                folder = sprintf('vi/%02d',i);
                break;
            end
        end
        if isempty(folder)
            folder = sprintf('vi/%02d',i+1);
        end
    end
    if ~ exist([folder,'/recording.mat'],'file') || force_analysis
        testIV(data,[nchan-1,nchan],folder,-80);
%         testIV(data,[nchan-1,nchan],folder,-70);
    end
end

RMP = nan;
RMP_protocol = '';
spontaneous_firing_rate = nan;
spontaneous_CV = nan;
time_constant = nan;
Rin_CC = nan;
Rin_VC = nan;
Ra = nan;
C = nan;
INa_max = nan;
Vh_INa_max = nan;
rheobase = nan;
fI_gain = nan;
evoked_CV = nan;
max_time_to_first_spike = nan;
no_spikes_at_max_time_to_first = nan;
AP_threshold = nan;
AP_peak = nan;
AP_duration = nan;
AP_halfwidth = nan;
AP_ahp = nan;
AP_ahp_time = nan;
AP_ahp_max_time = nan;
AP_adp = nan;
AP_adp_time = nan;
AP_adp_max_time = nan;
AP_amplitude = nan;
AP_rise_rate = nan;
AP_rise_time = nan;
AP_rapidness = nan;
accommodation_index = nan;
max_no_spikes = nan;
max_firing_rate = nan;
normalized_spike_times_at_max_rate = nan;
normalized_spike_amplitudes_at_max_rate = nan;
normalized_spike_times = nan;
normalized_spike_amplitudes = nan;
EPSP_tau_rise = nan;
EPSP_tau_decay = nan;
EPSP_amplitude = nan;
EPSP_frequency = nan;
EPSC_tau_rise = nan;
EPSC_tau_decay = nan;
EPSC_amplitude = nan;
EPSC_frequency = nan;
sag = nan;
sag_ratio = nan;
sag_dV = nan;
got_ramp_protocol = 0;
AP_features_protocol = '';
time_constant_protocol = '';
have = struct([]);
VI_folder = '';

for d = dir('.')'
    if d.name(1) ~= '.' && d.isdir
        for sd = dir([d.name,'/0*'])'
            cwd = pwd;
            cd([d.name,'/',sd.name]);
            if exist('DO_NOT_USE','file')
                fprintf(1, 'Skipping directory %s/%s because of DO_NOT_USE file.\n', d.name, sd.name);
                cd(cwd);
                continue;
            end
            scripts = {};
            switch d.name
                case 'kernel'                    
                case 'steps'
                    if exist('stimuli','dir') && ~ isempty(dir('*kernel.dat'))
                        files = listH5Files;
                        entities = loadH5Trace(files{1});
                        waveform_entity = nan;
                        for i=1:length(entities)
                            if strcmp(entities(i).name,'Waveform') && ...
                                    strcmp(entities(i).units,'pA')
                                waveform_entity = i;
                                if size(entities(i).metadata,1) == 3
                                    stim_idx = 2;
                                else
                                    [m,j] = max(entities(i).metadata(:,1));
                                    idx = setdiff(1:size(entities(i).metadata,2),j);
                                    if all(entities(i).metadata(idx) < m)
                                        stim_idx = j;
                                    else
                                        keyboard
                                    end
                                end
                            end
                        end
                        nfiles = length(files);
                        Istim = zeros(nfiles,1);
                        for i=1:nfiles
                            entities = loadH5Trace(files{i});
                            Istim(i) = entities(waveform_entity).metadata(stim_idx,3);
                        end
                        Istim = unique(Istim);
                        fprintf(1, '>>> [%s/%s] stimulation amplitudes: [', d.name, sd.name);
                        for i=1:length(Istim)-1
                            fprintf(1, '%.0f,',Istim(i));
                        end
                        fprintf(1, '%.0f] pA.\n', Istim(end));
                        % depending on what the stimuli look like, we use
                        % different scripts:
                        scripts = {};
                        if any(Istim) > 0
                            scripts = {'spike_features'};
                        end
                        if sum(Istim <= 0) >= 2
                            % we need at least two points to compute a V-I
                            % curve
                            scripts = [scripts, 'VI_curve'];
                        end
                        if sum(Istim >= 0) >= 3
                            % we need at least three points to compute a
                            % f-I curve
                            scripts = [scripts, 'fI_curve'];
                        end
                    elseif exist('recording.mat','file')
                        data = load('recording.mat');
                        switch data.protocol_name
                            case 'CCstep'
                                scripts = {'VI_curve','fI_curve','spike_features'};
                            case 'lightStepsVC'
                                scripts = {'ChR2'};
                            case 'lightSteps'
                                scripts = {};
                            otherwise
                                keyboard
                        end
                    else
                        files = listH5Files;
                        entities = loadH5Trace(files{1});
                        if (strcmp(entities(1).units,'pA') || strcmp(entities(1).units,'mV')) && ...
                                strcmp(entities(2).units,'V') && length(dir('stimuli/*.stim')) > 2
                            scripts = {'ChR2'};
                        end
                    end
                case 'fi'
                    if exist('fI_PID.xml','file')
                        scripts = {'fI_pid'};
                    elseif exist('stimuli','dir') || ~ isempty(dir('*.stim'))
                        scripts = {'fI_curve','spike_features'};
                    end
                case 'ap'
                    scripts = {'ap'};
                case 'tau'
                    if ~ exist('tau.mat','file') || force_analysis
                        preprocessTimeConstantExperiment(spike_threshold);
                    end
                case 'ramp'
                    scripts = {'ramp'};
                case 'pulses'
                    if ~ exist('pulses.mat','file') || force_analysis
                        preprocessPulsesExperiment(spike_threshold);
                    end
                case 'impedance'
                    if ~ exist('impedance.mat','file') || force_analysis
                        [~,list] = system('find ../.. -name VI_curve.mat');
                        if ~ isempty(list)
                            idx = strfind(list,char(10));
                            fname = list(1:idx(1)-1);
                            data = load(fname,'R');
                            preprocessImpedanceExperiment([],[],data.R.value);
                        else
                            preprocessImpedanceExperiment;
                        end
                    end
                case 'spontaneous'
                    if (~ exist('spontaneous.mat','file') && ...
                            ~ exist('spontaneous_VC.mat','file')) %|| force_analysis
                        preprocessSpontaneousRecording(spike_threshold,6);
                    end
                case 'vi'
                    if exist('recording.mat','file')
                        if ~ exist('Vsteps_VC.mat','file') || force_analysis
                            preprocessVoltageStepsProtocol;
                        end
                    else
                        files = listH5Files;
                        entities = loadH5Trace(files{1});
                        for i=1:length(entities)
                            if (strcmp(entities(i).units,'mV') && isempty(entities(i).metadata)) || ...
                                (strcmp(entities(i).units,'pA') && ~isempty(entities(i).metadata))
                                scripts = {'VI_curve'};
                                if isempty(VI_folder)
                                    VI_folder = [d.name,'/',sd.name];
                                end
                                [t,Vr,~,info] = loadH5Traces(files);
                                t0 = sum(info{1}(1:end-2,1));
                                tend = sum(info{1}(1:end-1,1));
                                if max(max(Vr(:,t>t0 & t<tend))) > 0
                                    fprintf(1, '>>>>> Adding spike_features.m to the list of scripts in %s/%s.\n',...
                                        d.name,sd.name);
                                    scripts = {'VI_curve','spike_features'};
                                end
                                break;
                            end
                        end
                        if isempty(scripts) && (~exist('Vsteps_VC.mat','file') || force_analysis)
                            preprocessVoltageStepsProtocol;
                        end
                    end
                case 'ltp_ltd'
                    curdir = pwd;
                    if (~ exist('../ltp_ltd.m','file') || force_analysis) ...
                            && strcmp(curdir(end-1:end),'01')
                        cd('..')
                        eval(sprintf('!cp %s/ltp_ltd.m .',template_dir));
                        ltp_ltd;
                        cd(sd.name)
                    end
                case {'Rin','rin'}
                    scripts = {'Rin'};
                case 'zap'
                    if ~ exist('impedance.mat','file') || force_analysis
                        preprocessZapExperiment;
                    end
                case 'test_pulses'
                    if ~ exist('test_pulses.mat','file') || force_analysis
                        preprocessTestPulses;
                    end
                case 'extracellular'
                    if ~ exist('extracellular.mat','file') || ~ exist('extracellular.pdf','file') || force_analysis
                        preprocessExtracellularTestPulses(true);
                    end
                otherwise
                    fprintf(1, '>>> Unknown protocol [%s].\n', d.name);
            end
            for s = scripts
                if ~ exist(['./',s{1},'.m'],'file') || (force_analysis && overwrite_files)
                    eval(sprintf('!cp %s/%s.m .',template_dir,s{1}));
                    eval(s{1})
                elseif force_analysis
                    eval(s{1})
                end
            end
            try
                offset = load('offset');
            catch
                if force_zero_offset
                    offset = 0;
                else
                    offset = nan;
                end
            end
            for f = dir('*.mat')'
                switch f.name
                    case 'ap.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        if isnan(AP_threshold) || strcmpi(desired_AP_features_protocol,'ap')
                            data = load(f.name);
                            AP_halfwidth = data.features.halfwidth.wm*1e-3;
                            AP_peak = data.features.peak.Vm;
                            AP_threshold = data.features.threshold.Vm;
                            AP_duration = data.features.end.tm - data.features.threshold.tm;
                            AP_ahp = data.features.ahp.Vm;
                            AP_ahp_time = data.features.ahp.tm - data.features.peak.tm;
                            AP_ahp_max_time = data.features.max_ahp_dur;
                            AP_adp = data.features.adp.Vm;
                            AP_adp_time = data.features.adp.tm - data.features.peak.tm;
                            AP_adp_max_time = data.features.max_adp_dur;
                            AP_amplitude = data.features.amplitude.Vm;
                            AP_rise_rate = data.features.rise_rate.mean*1e3;
                            AP_rise_time = data.features.rise_time.tm*1e-3;
                            AP_rapidness = data.features.rapidness.mean;
                            AP_features_protocol = 'ap';
                        end
                        if isnan(RMP) && offset == 0
                            data = load(f.name);
                            RMP = data.V0;
                            RMP_protocol = 'AP';
                            try
                            fprintf(1, 'Extracting RMP from AP protocol recording in %s/%s -> %.1f mV.\n', ...
                                d.name, sd.name, RMP);
                            catch
                                keyboard
                            end
                        end
                        have(1).(f.name(1:end-4)) = 1;
                    case 'spontaneous.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        if offset == 0
                            RMP = data.Vm.mean;
                            RMP_protocol = 'spontaneous';
                            fprintf(1, 'Extracting RMP from spontaneous recording in %s/%s -> %.1f mV.\n', ...
                                d.name, sd.name, RMP);
                        end
                        if isempty(data.AP)
                            spontaneous_firing_rate = 0;
                            spontaneous_CV = 0;
                        else
                            spontaneous_firing_rate = data.AP.frequency;
                            isi = diff(data.AP.tp);
                            spontaneous_CV = std(isi)/mean(isi);
                            if isnan(AP_threshold) || strcmpi(desired_AP_features_protocol,'spontaneous') || ...
                                (strcmp(AP_features_protocol,'ap') && ~ strcmpi(desired_AP_features_protocol,'ap'))
                                tp = extractAPPeak(data.AP.T,data.AP.Vm,spike_threshold);
                                [tth,Vth] = extractAPThreshold(data.AP.T,data.AP.Vm,spike_threshold,tp);
                                [tahp,Vahp] = extractAPAHP(data.AP.T,data.AP.Vm,30e-3,spike_threshold,tp,tth);
                                tend = extractAPEnd(data.AP.T,data.AP.Vm,spike_threshold,[],tp,Vth,tahp);
                                AP_threshold = data.AP.threshold.Vm;
                                AP_peak = data.AP.peak.Vm;
                                AP_duration = tend{1}-tth{1};
                                AP_halfwidth = data.AP.halfwidth.wm*1e-3;
                                AP_ahp = Vahp{1};
                                AP_amplitude = data.AP.amplitude.Vm;
                                AP_rise_rate = data.AP.rise_rate.mean*1e3; % [mV/s]
                                AP_rise_time = data.AP.rise_time.tm*1e-3; % [s]
                                AP_features_protocol = 'spontaneous';
                            end
                        end
                        have(1).(f.name(1:end-4)) = 1;
                    case 'spontaneous_VC.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        if isnan(EPSC_tau_decay)
                            EPSC_tau_decay = mean(data.tau_decay);
                            EPSC_amplitude = mean(data.amplitudes);
                            EPSC_frequency = length(data.event_times) / data.recording_duration;
                        end
                        have(1).(f.name(1:end-4)) = 1;
                    case 'tau.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        time_constant = pickTau(data.taus);
                        time_constant_protocol = 'tau';
                        have(1).(f.name(1:end-4)) = 1;
                    case 'VI_curve.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        Rin_CC = data.R.value;
                        if isnan(RMP) || ~strcmp(RMP_protocol,'spontaneous')
                            RMP = data.V0.value - data.R.value*offset*1e-3;
                            RMP_protocol = 'VI';
                            fprintf(1, 'Extracting RMP from V-I curve in %s/%s -> %.1f mV.\n', ...
                                d.name, sd.name, RMP);
                        end
                        if isnan(time_constant)
                            time_constant = pickTau(data.tau);
                            time_constant_protocol = 'steps';
                        end
                        dV = data.V - data.V0.value;
                        idx = find(data.I < 0);
                        [~,i] = min(abs(dV(idx)-target_dV));
                        sag_dV = mean(data.V(data.I == data.I(idx(i))) - data.V0.value);
                        sag = mean(data.sag(data.I == data.I(idx(i))));
                        sag_ratio = mean(data.sag_ratio(data.I == data.I(idx(i))));
                        if sag_ratio < 0
                            keyboard
                        end
                        
                        if ~ exist('Vsteps','var') || size(Vsteps,1) < 3
                            if exist('recording.mat','file')
                                data = load('recording.mat');
                                V = data.V;
                                I = data.I;
                                t = (0:size(V,2)-1) * data.dt;
                                t0 = data.tstart;
                                dur = data.dur;
                                idx = find(t>t0+dur/2,1);
                                Idc = I(:,idx);
                            else
                                files = listH5Files;
                                kernels = dir('*_kernel.dat');
                                [t,Vr,I,info] = loadH5Traces(files);
                                try
                                    Ke = load(kernels(1).name);
                                    V = AECoffline(Vr,I,Ke);
                                catch
                                    V = Vr;
                                end
                                Idc = cellfun(@(x) x(end-1,3), info);
                                t0 = sum(info{1}(1:end-2,1));
                                dur = info{1}(end-1,1);
                            end
                            [Idc,idx] = sort(Idc);
                            V = V(idx,:);
                            I = I(idx,:);
                            Fs = 1/diff(t(1:2));
                            Fc = 1000;
                            N = Fs/Fc*2;
                            padding = repmat(V(:,1),[1,N]);
                            V = filterTrace(V,Fs,Fc,2);
                            V(:,1:N) = padding;
                            V0 = mean(V(:,t>t0-0.1 & t<t0),2);
                            dV = mean(V(:,t>t0+dur-0.1 & t<t0+dur),2) - V0;
                            [~,i] = min(abs(dV(Idc<0)-target_dV));
                            if ~ isempty(i)
                                idx = find(t>=t0-0.1 & t<=t0+dur+0.3);
                                if ~exist('Vsteps','var')
                                    Vsteps = V(i,idx);
                                    Isteps = I(i,idx);
                                    col = [0,0,0];
                                    stepsdur = dur;
                                else
                                    if size(Vsteps,2) < length(idx)
                                        idx = idx(1:size(Vsteps,2));
                                    elseif size(Vsteps,2) > length(idx)
                                        Vsteps = Vsteps(:,1:length(idx));
                                        Isteps = Isteps(:,1:length(idx));
                                    end
                                    Vsteps = [V(i,idx) ; Vsteps];
                                    Isteps = [I(i,idx) ; Isteps];
                                    col = [0,0,0 ; col];
                                end
                                tsteps = t(idx) - t0;
                            end
                        end

                        have(1).(f.name(1:end-4)) = 1;
                    case 'fI_curve.mat'
                        if (~ exist('tsteps','var') || tsteps(end)>2 || size(Vsteps,1) == 1) && ...
                                ~ exist('fI_PID.xml','file')
                            if exist('recording.mat','file')
                                data = load('recording.mat');
                                V = data.V;
                                I = data.I;
                                t = (0:size(V,2)-1) * data.dt;
                                t0 = data.tstart;
                                dur = data.dur;
                                idx = find(t>t0+dur/2,1);
                                Idc = I(:,idx);
                            else
                                files = listH5Files;
                                kernels = dir('*_kernel.dat');
                                [t,Vr,I,info] = loadH5Traces(files);
                                try
                                    Ke = load(kernels(1).name);
                                    V = AECoffline(Vr,I,Ke);
                                catch
                                    V = Vr;
                                end
                                Idc = cellfun(@(x) x(end-1,3), info);
                                t0 = sum(info{1}(1:end-2,1));
                                dur = info{1}(end-1,1);
                            end
                            [Idc,idx] = sort(Idc);
                            V = V(idx,:);
                            I = I(idx,:);
                            Fs = 1/diff(t(1:2));
                            Fc = 1000;
                            N = Fs/Fc*2;
                            padding = repmat(V(:,1),[1,N]);
                            V = filterTrace(V,Fs,Fc,2);
                            V(:,1:N) = padding;
                            tp = extractAPPeak(t,V,spike_threshold,10e-3);
                            V0 = mean(V(:,t>t0-0.1 & t<t0),2);
                            dV = mean(V(:,t>t0+dur-0.1 & t<t0+dur),2) - V0;
                            idx = find(Idc<0);
                            [~,i] = min(abs(dV(idx)-target_dV));
                            if ~ isempty(i)
                                jdx = idx(i);
                                col = [0,0,0];
                                clear tsteps
                            else
                                jdx = [];
                                if ~ exist('col','var')
                                    col = [];
                                end
                            end
                            n = cellfun(@(x) length(x(x>t0 & x<t0+dur)), tp);
                            i = find(n>0 & Idc>0,1);
                            jdx = [jdx,i];
                            col = [col;.8,.4,.1];
                            if ~ isempty(i)
                                [~,i] = min(abs(Idc-rheobase_times*Idc(i)));
                            else
                                [~,i] = min(abs(Idc-mean(Idc(Idc>0))));
                            end
                            if Idc(i) ~= Idc(jdx(end))
                                jdx = [jdx,i];
                                col = [col;0,0,0];
                            else
                                col(end,:) = [0,0,0];
                            end
                            [~,j] = max(n);
                            if all(n(jdx) ~= n(j)) && all(Idc(jdx) ~= Idc(j))
                                jdx = [jdx,j];
                                col = [col;1,0,0];
                            end
                            idx = find(t>=t0-0.1 & t<=t0+dur+0.3);
                            if ~ exist('tsteps','var')
                                tsteps = t(idx) - t0;
                                Vsteps = V(jdx,idx);
                                Isteps = I(jdx,idx);
                            else
                                Vsteps = [Vsteps ; V(jdx,idx)];
                                Isteps = [Isteps ; I(jdx,idx)];
                            end
                            stepsdur = dur;
                        end
                        
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        if exist('fI_PID.xml','file')
                            Iu = data.I{1}(data.f{1} < 100);
                            fu = data.f{1}(data.f{1} < 100);
                            rheobase_guess = min(Iu);
                            [a,b,c,II,ff,rb] = fitFICurve(Iu,fu,[rheobase_guess,rheobase_guess*0.9]);
                            if isreal(rb)
                                data.rheobase = rb;
                            else
                                data.rheobase = (-c/a)^(1/b);
                            end
                            data.gain = a*b*mean(II)^(b-1);
                            if isnan(data.gain)
                                data.gain = 0;
                            end
                            data.I = II;
                            data.f = ff;
                        end
                        if length(unique(data.I)) > 2 && (isnan(max_firing_rate) || ...
                                max(data.f) > max_firing_rate)
                            max_firing_rate = max(data.f);
                            fI_gain = data.gain;
                            if isnan(rheobase) || ~ got_ramp_protocol
                                rheobase = data.rheobase;
                            end
                            try
                                evoked_CV = data.evoked_CV;
                            catch
                            end
                            have(1).(f.name(1:end-4)) = 1;
                        end
                    case 'ramp.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        rheobase = mean(data.Ithresh);
                        got_ramp_protocol = 1;
                        have(1).(f.name(1:end-4)) = 1;
                    case 'spike_features.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        if ~ strcmp(d.name,'steps') && ~ strcmp(d.name,'fi') && ~ strcmp(d.name,'vi')
                            continue;
                        end
                        data = load(f.name);
                        % the trials in which the cell spiked
                        with_spikes = find(cellfun(@(x) length(x(x>data.tstart(1) & ...
                            x<data.tstart(1)+data.stepdur(1))), data.tp));
                        if ~ isempty(with_spikes)
                            idx = arrayfun(@(i) find(data.tp{i}>data.tstart(i) & ...
                                data.tp{i}<data.tstart(i)+data.stepdur(i)), with_spikes, 'UniformOutput', 0);
                            tp = cellfun(@(x,y) x(y), data.tp(with_spikes), idx, 'UniformOutput', 0);
                            Vp = cellfun(@(x,y) x(y), data.Vp(with_spikes), idx, 'UniformOutput', 0);
                            Vth = cellfun(@(x,y) x(y), data.Vth(with_spikes), idx, 'UniformOutput', 0);
                            [max_time_to_first_spike,i] = max(cellfun(@(x) x(1), tp) - data.tstart(with_spikes));
                            no_spikes_at_max_time_to_first = length(tp{i});
                            [max_no_spikes,i] = max(cellfun(@(x) length(x), tp));
                            if isscalar(data.stepdur)
                                data.stepdur = data.stepdur + zeros(size(data.stepamp));
                            end
                            normalized_spike_times_at_max_rate = (tp{i}-data.tstart(i))/data.stepdur(i);
                            normalized_spike_amplitudes_at_max_rate = Vp{i} - Vth{i};
                            normalized_spike_amplitudes_at_max_rate = normalized_spike_amplitudes_at_max_rate / ...
                                normalized_spike_amplitudes_at_max_rate(1);
                            Imin = min(data.stepamp(with_spikes));
                            [~,i] = min(abs(data.stepamp-rheobase_times*Imin));
                            jdx = find(data.stepamp(with_spikes) == data.stepamp(i));
                            try
                                normalized_spike_times = (tp{jdx(1)}-data.tstart(with_spikes(jdx(1)))) / ...
                                    data.stepdur(with_spikes(jdx(1)));
                                normalized_spike_amplitudes = Vp{jdx(1)} - Vth{jdx(1)};
                                normalized_spike_amplitudes = normalized_spike_amplitudes / normalized_spike_amplitudes(1);
                                if isnan(AP_threshold) || strcmpi(desired_AP_features_protocol,'steps')
                                    AP_threshold = mean(cellfun(@(x) x(1), data.Vth(with_spikes(jdx(1)))));
                                    AP_ahp = mean(cellfun(@(x) x(1), data.Vahp(with_spikes(jdx(1)))));
                                    tmp = cellfun(@(x,y) y-x, data.tp, data.tahp, 'UniformOutput', 0);
                                    AP_ahp_time = mean(cellfun(@(x) x(1), tmp(with_spikes(jdx(1)))));
                                    AP_ahp_max_time = data.max_ahp_dur;
                                    AP_adp = mean(cellfun(@(x) x(1), data.Vadp(with_spikes(jdx(1)))));
                                    tmp = cellfun(@(x,y) y-x, data.tp, data.tadp, 'UniformOutput', 0);
                                    AP_adp_time = mean(cellfun(@(x) x(1), tmp(with_spikes(jdx(1)))));
                                    AP_adp_max_time = data.max_adp_dur;
                                    AP_peak = mean(cellfun(@(x) x(1), data.Vp(with_spikes(jdx(1)))));
                                    AP_duration = mean(cellfun(@(x,y) y(1)-x(1), data.tth(with_spikes(jdx(1))), ...
                                        data.tend(with_spikes(jdx(1)))));
                                    AP_halfwidth = mean(data.AP_half_width(with_spikes(jdx(1)),1));
                                    AP_amplitude = mean(data.AP_amplitude(with_spikes(jdx(1)),1));
                                    AP_rise_rate = mean(data.AP_rise_rate(with_spikes(jdx(1)),1));
                                    AP_rise_time = mean(data.AP_rise_time(with_spikes(jdx(1)),1));
                                    AP_rapidness = nanmean(flattenCellArray(data.rapidness,'full'));
                                    AP_features_protocol = 'steps';
                                end
                                k = max(2,min(4,round(mean(cellfun(@(x) length(x), tp(jdx)))/5)));
                                accommodation_index = mean(computeAccommodationIndex(tp(jdx),k));
                                if accommodation_index < 0
                                    tmp = mean(computeAccommodationIndex(tp(jdx),k+1));
                                    if tmp > 0
                                        k = k+1;
                                        accommodation_index = tmp;
                                    end
                                end
                                fprintf(1, 'Discarded %d ISIs for the computation of the accommodation index.\n', k);
                            catch
                                keyboard
                            end
                        end
                        have(1).(f.name(1:end-4)) = 1;
                    case 'Vsteps_VC.mat'
                        try
                            if have(1).(f.name(1:end-4))
                                fprintf(1, '>>> Ignoring %s in %s/%s.\n', f.name, d.name, sd.name);
                                have(1).(f.name(1:end-4)) = have(1).(f.name(1:end-4)) + 1;
                                continue;
                            end
                        catch
                        end
                        data = load(f.name);
                        if ~ isfield(data,'Ra')
                            preprocessVoltageStepsProtocol;
                            data = load(f.name);
                        end
                        Ra = data.Ra;
                        INa_m = mean(reshape(data.INa,[2,length(data.INa)/2]));
                        [m,i] = min(INa_m);
                        this_dir = pwd;
                        idx = strfind(this_dir,'/');
                        this_dir = this_dir(idx(end-2)+1:end);
                        if isnan(INa_max)
                            C = data.C;
                            Rin_VC = data.R;
                            Vh_INa_max = data.Vh(i*2);
                            INa_max = INa_m(i);
                            Vsteps_folder = this_dir;
                        elseif data.C/C < 0.7 || data.C/C > 1.3
                            fprintf(1, '===>>> C = %.0f pF in %s and %.0f pF in %s.\n',...
                                C, Vsteps_folder, data.C, this_dir);
                        elseif data.R/Rin_VC < 0.7 || data.R/Rin_VC > 1.3
                            fprintf(1, '===>>> Rin = %.0f MOhm in %s and %.0f MOhm in %s.\n',...
                                Rin_VC, Vsteps_folder, data.R, this_dir);
                        elseif m < INa_max
                            C = data.C;
                            Rin_VC = data.R;
                            Vh_INa_max = data.Vh(i*2);
                            INa_max = INa_m(i);
                            Vsteps_folder = pwd;
                        end
                        have(1).(f.name(1:end-4)) = 1;
                end
            end
            cd(cwd)
        end
    end
end

if ~ isnan(Rin_CC) && Rin_CC > 0
    Rin = Rin_CC;
elseif Rin_VC > 0
    Rin = Rin_VC;
else
    Rin = nan;
end

if isnan(C)
    C = time_constant/Rin_CC*1e6;
elseif ~isnan(Rin_VC) && strcmp(time_constant_protocol,'steps') && Rin_VC*C > 0
    time_constant = Rin_VC*C*1e-6;
end

tau = time_constant;

for fid=[1,fopen('cell.txt','w')]
    fprintf(fid, 'Access resistance: %.0f MOhm.\n', Ra);
    fprintf(fid, 'Resting membrane potential: %.1f mV.\n', RMP);
    fprintf(fid, 'Membrane time constant: %.2f ms (VC: %.0f ms).\n', tau*1e3, Rin_VC*C*1e-3);
    fprintf(fid, 'Input resistance: %.0f MOhm (CC: %.0f MOhm, VC: %.0f MOhm).\n', Rin, Rin_CC, Rin_VC);
    fprintf(fid, 'Membrane capacitance: %.0f pF (CC: %.0f pF).\n', C, tau/Rin_CC*1e6);
    fprintf(fid, 'Sag: %.1f mV @ %.1f mV deflection.\n', sag, sag_dV);
    fprintf(fid, 'Sag ratio: %.3f @ %.1f mV deflection.\n', sag_ratio, sag_dV);
    fprintf(fid, 'Maximal sodium current: %.0f pA @ %.0f mV.\n', INa_max, Vh_INa_max);
    fprintf(fid, 'Spontaneous firing rate: %.3f Hz.\n', spontaneous_firing_rate);
    fprintf(fid, 'Spontaneous CV: %.3f.\n', spontaneous_CV);
    fprintf(fid, 'Rheobase: %.0f pA.\n', rheobase);
    fprintf(fid, 'f-I curve gain: %.0f Hz/nA.\n', fI_gain*1e3);
    fprintf(fid, 'Evoked CV: %.3f.\n', evoked_CV);
    fprintf(fid, 'Maximum time to first spike: %.0f ms.\n', max_time_to_first_spike*1e3);
    fprintf(fid, 'No. of spikes @ maximum time to first spike: %d.\n', no_spikes_at_max_time_to_first);
    fprintf(fid, 'AP threshold: %.1f mV.\n', AP_threshold);
    fprintf(fid, 'AP peak: %.1f mV.\n', AP_peak);
    fprintf(fid, 'AP amplitude: %.1f mV.\n', AP_amplitude);
    fprintf(fid, 'AP duration: %.1f ms.\n', AP_duration*1e3);
    fprintf(fid, 'AP halfwidth: %.1f ms.\n', AP_halfwidth*1e3);
    fprintf(fid, 'AP AHP: %.1f mV.\n', AP_ahp);
    fprintf(fid, 'AP AHP time: %.1f ms.\n', AP_ahp_time*1e3);
    fprintf(fid, 'AP AHP max time: %.1f ms.\n', AP_ahp_max_time*1e3);
    fprintf(fid, 'AP ADP: %.1f mV.\n', AP_adp);
    fprintf(fid, 'AP ADP time: %.1f ms.\n', AP_adp_time*1e3);
    fprintf(fid, 'AP ADP max time: %.1f ms.\n', AP_adp_max_time*1e3);
    fprintf(fid, 'AP rise time: %.2f ms.\n', AP_rise_time*1e3);
    fprintf(fid, 'AP rise rate: %.2f mV/ms.\n', AP_rise_rate*1e-3);
    fprintf(fid, 'AP rapidness: %.2f ms^-1.\n', AP_rapidness);
    fprintf(fid, 'Accommodation index: %.4f.\n', accommodation_index);
    fprintf(fid, 'Maximum firing rate: %.1f spikes/s.\n', max_firing_rate);
    fprintf(fid, 'Normalized spike times: [ ');
    if length(normalized_spike_times) <= 6
        fprintf(fid, '%.2f ', normalized_spike_times);
    else
        fprintf(fid, '%.2f ', normalized_spike_times(1:4));
        fprintf(fid, '... ');
        fprintf(fid, '%.2f ', normalized_spike_times(end-1:end));
    end
    fprintf(fid, '].\n');
    fprintf(fid, 'Normalized spike amplitudes: [ ');
    if length(normalized_spike_amplitudes) <= 6
        fprintf(fid, '%.2f ', normalized_spike_amplitudes);
    else
        fprintf(fid, '%.2f ', normalized_spike_amplitudes(1:4));
        fprintf(fid, '... ');
        fprintf(fid, '%.2f ', normalized_spike_amplitudes(end-1:end));
    end
    fprintf(fid, '].\n');
    fprintf(fid, 'Normalized spike times at maximum firing rate: [ ');
    if length(normalized_spike_times_at_max_rate) <= 6
        fprintf(fid, '%.2f ', normalized_spike_times_at_max_rate);
    else
        fprintf(fid, '%.2f ', normalized_spike_times_at_max_rate(1:4));
        fprintf(fid, '... ');
        fprintf(fid, '%.2f ', normalized_spike_times_at_max_rate(end-1:end));
    end
    fprintf(fid, '].\n');
    fprintf(fid, 'Normalized spike amplitudes at maximum firing rate: [ ');
    if length(normalized_spike_amplitudes_at_max_rate) <= 6
        fprintf(fid, '%.2f ', normalized_spike_amplitudes_at_max_rate);
    else
        fprintf(fid, '%.2f ', normalized_spike_amplitudes_at_max_rate(1:4));
        fprintf(fid, '... ');
        fprintf(fid, '%.2f ', normalized_spike_amplitudes_at_max_rate(end-1:end));
    end
    fprintf(fid, '].\n');
    fprintf(fid, 'Maximum no. of elicited spikes: %d.\n', max_no_spikes);
    fprintf(fid, 'EPSC (taud,amp,freq): (%.2f ms, %.2f pA, %.4f Hz).\n', ...
        EPSC_tau_decay*1e3, EPSC_amplitude, EPSC_frequency);
end
fclose(fid);

save('cell.mat','RMP','tau','C','Rin','Rin_CC','Rin_VC','evoked_CV',...
    'spontaneous_firing_rate','spontaneous_CV','rheobase','sag','sag_dV',...
    'fI_gain','AP_peak','AP_duration','AP_halfwidth','AP_threshold',...
    'AP_ahp','AP_ahp_time','AP_ahp_max_time','AP_adp','AP_adp_time','AP_adp_max_time',...
    'accommodation_index','max_no_spikes','INa_max','Vh_INa_max',...
    'EPSC_tau_decay','EPSC_amplitude','EPSC_frequency','max_time_to_first_spike',...
    'no_spikes_at_max_time_to_first','max_firing_rate','rheobase_times',...
    'sag_ratio','AP_features_protocol','Ra','normalized_spike_times_at_max_rate',...
    'normalized_spike_amplitudes_at_max_rate','AP_amplitude','AP_rise_time','AP_rise_rate',...
    'AP_rapidness','normalized_spike_times','normalized_spike_amplitudes');

if exist('tsteps','var')
    
    figure;
    subplot(5,1,1:4);
    hold on;
    for i=1:size(Vsteps,1)
        plot(tsteps,Vsteps(i,:),'Color',col(i,:),'LineWidth',1);
    end
    ylabel('Vm (mV)','FontSize',11);
    axis([tsteps([1,end]),ylim]);
    box off;
    set(gca,'TickDir','Out','LineWidth',1,'XTick',[],'YTick',-120:30:60,...
        'Color','w','XColor','k','YColor','k','FontSize',11);
    
    subplot(5,1,5);
    hold on;
    for i=1:size(Isteps,1)
        plot(tsteps,Isteps(i,:),'Color',col(i,:),'LineWidth',1);
    end
    xlabel('Time (s)','FontSize',11);
    ylabel('Current (pA)','FontSize',11);
    axis([tsteps([1,end]),ylim]);
    box off;
    idx = find(tsteps>stepsdur/2,1);
    set(gca,'TickDir','Out','LineWidth',1,'YTick',unique(sort([0;Isteps(:,idx)])),...
        'Color','w','XColor','k','YColor','k','FontSize',11);

    sz = [8,5];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    cwd = pwd;
    idx = strfind(cwd,'/');
    print('-dpdf',[cwd(idx(end)+1:end),'.pdf']);
    
%     VV = Vsteps([1,2],:);
%     II = Isteps([1,2],:);
%     VV = VV - repmat(mean(VV(:,1:1000),2),[1,size(VV,2)]);
%     figure;
%     subplot(5,1,1:4);
%     hold on;
%     plot(tsteps,VV,'k','LineWidth',1);
%     placeScaleBars(tsteps(end)-0.1,40,0.1,40,'','','k','LineWidth',1);
%     axis([tsteps([1,end]),-30,120]);
%     axis off;
%     subplot(5,1,5);
%     hold on;
%     plot(tsteps,II,'k','LineWidth',1);
%     placeScaleBars(tsteps(end)-0.1,10,[],10,'','','k','LineWidth',1);
%     axis([tsteps([1,end]),-30,50]);
%     axis off;
%     set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
%     cwd = pwd;
%     print('-dpdf',[cwd(idx(end)+1:end),'_2.pdf']);
end

end
