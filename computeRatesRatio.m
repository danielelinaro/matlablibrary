function R_excOverR_inh = computeRatesRatio(mode, Vm, Rm, tau_exc, tau_inh, E_exc, E_inh)
% R_excOverR_inh = computeRatesRatio(mode, Vm, Rm, tau_exc, tau_inh, E_exc, E_inh)
% 
% Parameters:
%     mode - either 'bio' or 'relative'
%       Vm - membrane potential (mV)
%       Rm - membrane resistance (MOhm)
%  tau_exc - time constant of excitatory inputs (msec), default 5.
%  tau_inh - time constant of inhibitory inputs (msec), default 10.
%    E_exc - reversal potential of excitatory inputs (mV), default 0.
%    E_inh - reversal potential of inhibitory inputs (mV), default -80.
% 

if ~ exist('tau_exc','var')
    tau_exc = 5;
end
if ~ exist('tau_inh','var')
    tau_inh = 10;
end
if ~ exist('E_exc','var')
    E_exc = 0;
end
if ~ exist('E_inh','var')
    E_inh = -80;
end

Vm = Vm * 1e-3;         % (V)
tau_exc = tau_exc*1e-3; % (s)
tau_inh = tau_inh*1e-3; % (s)
E_exc = E_exc * 1e-3;   % (V)
E_inh = E_inh * 1e-3;   % (V)

if strcmp(mode, 'relative')
    Rm = Rm * 1e6;          % (Ohm)
    g_exc = 0.02 / Rm;      % (S)
    g_inh = 0.06 / Rm;      % (S)
elseif strcmp(mode, 'bio')
    g_exc = 50e-12;           % (S)
    g_inh = 190e-12;          % (S)
else
    error('Unknown mode.');
end

R_excOverR_inh = (g_inh * tau_inh * (E_inh - Vm)) / (g_exc * tau_exc * (Vm - E_exc));

