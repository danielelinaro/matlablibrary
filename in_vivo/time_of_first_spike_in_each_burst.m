function [t_first_spike_in_each_burst,t_nonburst_spikes] = time_of_first_spike_in_each_burst(t_spikes,isi_frequency_threshold)    
    % Extracts burst times and the times of spikes that occur outside of
    % bursts.  The burst time is defined to be the time of the first spike
    % in a burst.  A burst is defined as a continguous sequences of spikes
    % with all ISIs between then less than or equal to
    % 1/isi_frequency_threshold, and the pre- and post- ISIs greater than
    % 1/isi_frequency_threshold.  The ISI before the first spike, and the
    % ISI after the last, are both assumed to be longer than
    % 1/isi_frequency_threshold.
    
    % t_spikes should be a row vector, isi_frequency_threshold a scalar.
    % t_spikes is typically in seconds, isi_frequency_threshold in Hz.
    
    if isempty(t_spikes) ,
        t_first_spike_in_each_burst=zeros(1,0);  % row vector, zero length
        t_nonburst_spikes=zeros(1,0);  % row vector, zero length
    elseif isscalar(t_spikes) ,
        t_first_spike_in_each_burst=zeros(1,0);  % row vector, zero length
        t_nonburst_spikes = t_spikes;
    else
        % Extract the burst times
        isi_threshold = 1/isi_frequency_threshold;  % s
        isis = diff(t_spikes);  % length N-1, where N is the length of t_spikes
        is_isi_within_burst=(isis<=isi_threshold);  % logical, one element per ISI
        is_preceeding_isi_outside_burst = [true ~is_isi_within_burst(1:end-1)];  % logical, one element per ISI
        is_isi_first_in_burst = is_isi_within_burst & is_preceeding_isi_outside_burst;  % logical, one element per ISI
        is_spike_first_in_burst = [is_isi_first_in_burst false];  % logical, length N
        t_first_spike_in_each_burst=t_spikes(is_spike_first_in_burst);  % get the time of each spike that is first in burst

        % Extract the times of non-burst spikes
        does_spike_preceed_burst_isi = [is_isi_within_burst false];  % logical, length N
        does_spike_follow_burst_isi = [false is_isi_within_burst];  % logical, length N
        is_spike_within_burst = does_spike_preceed_burst_isi | does_spike_follow_burst_isi ;  % logical, length N
        is_spike_outside_burst = ~is_spike_within_burst ;  % logical, length N
        t_nonburst_spikes = t_spikes(is_spike_outside_burst);  % get the time of each spike that is not in any burst
    end
end
