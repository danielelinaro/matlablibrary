function y = flattenCellArray(x,mode)
% y = flattenCellArray(x,'matrix')
% y = flattenCellArray(x,'full')

if ~ iscell(x)
    error('x must be a cell array');
end

if ~ exist('mode','var')
    mode = 'matrix';
end

y = [];
n = length(x);

for k=1:n
    if strcmp(mode,'matrix')
        y = [y ; x{k}];
    elseif strcmp(mode,'full')
        y = [y ; x{k}(:)];
    end
end
