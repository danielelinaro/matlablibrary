function [tburst,Vburst,tburst_full,Vburst_full] = ...
    locateBursts(tsp,Vsp,rate_threshold,min_no_spikes,tol,force_decrease)
% locateBursts    Find bursts in a spike train. A burst is defined as a
% series of spikes emitted at a minimum rate and with decreasing amplitude
% of subsequent spikes in a burst.
% 
% [tburst,Vburst,tburst_full,Vburst_full] = locateBursts(tsp,Vsp,rate_threshold,min_no_spikes,tol,force_decrease)
% 
% Parameters:
%              tsp - an Mx1 cell array containing the spike times.
%              Vsp - an Mx1 cell array containing the corresponding values
%                    of voltage.
%   rate_threshold - the minimum firing rate of spikes within a burst.
%    min_no_spikes - minimum number of spikes in a burst.
%              tol - tolerance for the detection of a decreasing peak
%                    (default 0.02, 0 means that all peaks must be strictly
%                    decreasing).
%   force_decrease - whether all spikes in a burst must have decreasing
%                    amplitude (default 0).
% 
% Returns:
%           tburst - an Mx1 cell array containing the times of the first
%                    spike in each burst.
%           Vburst - the corresponding voltage values.
%      tburst_full - an Mx1 cell array. Each of the M cells contains a
%                    cell array, whose elements contain the spike times of
%                    all spikes in each bursts. For example,
%                    tburst_full{3}{12} will contain the times of the
%                    spikes in the 12th burst of the 3rd trial.
%      Vburst_full - the corresponding voltage values.
% 
% See also: findJuxtacellularSpikes, findExtracellularSpikes
% 

% Author: Daniele Linaro - May 2015

n = length(tsp);
isi = cellfun(@(x) diff(x), tsp, 'UniformOutput', 0);
has_spikes = cellfun(@(x) ~isempty(x), isi);
tburst = cell(n,1);
Vburst = cell(n,1);
tburst_full = cell(n,1);
Vburst_full = cell(n,1);

if ~ exist('force_decrease','var') || isempty(force_decrease)
    force_decrease = 0;
end

if ~ exist('tol','var') || isempty(tol)
    tol = 0.02;
end

if isscalar(rate_threshold)
    rate_threshold = rate_threshold + zeros(1,50);
else
    rate_threshold = [rate_threshold(:)', rate_threshold(end)+zeros(1,50-length(rate_threshold))];
end

for i=1:n
    tburst_full{i} = {};
    Vburst_full{i} = {};
    if has_spikes(i)
        f = 1./isi{i};
        j = 1;
        while j <= length(f)
            cnt = 0;
            while f(j) >= rate_threshold(cnt+1)
                cnt = cnt+1;
                j = j+1;
                if j > length(f)
                    break;
                end
            end
            if cnt >= min_no_spikes-1
                idx = j-cnt:j;
                Vpeaks = abs(Vsp{i}(idx));
                dV = diff(Vpeaks);
                if dV(1) > tol*Vpeaks(1)
                    j = j-cnt;
                else
                    if force_decrease
                        % consider as part of a burst only the spikes with
                        % monotonically decreasing amplitudes
                        stop = find(dV > tol*Vpeaks(1), 1, 'first');
                    else
                        % only the first 3 spikes must have decreasing
                        % amplitudes
                        jdx = 1:min(2,length(dV));
                        stop = find(dV(jdx) > tol*Vpeaks(1), 1, 'first');
                    end
                    if isempty(stop)
                        stop = length(idx);
                    end
                    if stop >= min_no_spikes
                        tburst{i} = [tburst{i}, tsp{i}(idx(1))];
                        Vburst{i} = [Vburst{i}, Vsp{i}(idx(1))];
                        tburst_full{i} = [tburst_full{i}, tsp{i}(idx(1:stop))];
                        Vburst_full{i} = [Vburst_full{i}, Vsp{i}(idx(1:stop))];
                    end
                    j = idx(stop);
                end
            end
            j = j+1;
        end
    end
end
