function [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,V,juxta,extra,varargin)
%
% [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,V,juxta,extra)
% [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,V,juxta,extra,threshold_factor)
% [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,V,juxta,extra,threshold_factor,window)
% [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,V,juxta,extra,threshold_factor,window,Vraw,VAP)
% 
% juxta and extra are structures containing the following fields:
%       tsingle - single spike times
%        tburst - first spike in burst times
%   tburst_full - all spikes in burst times
% 

p = inputParser;
p.addOptional('threshold_factor',5,@(x) isnumeric(x) && isscalar(x) && (x>0));
p.addOptional('window',[0.25,0.25],@(x) isnumeric(x) && length(x)==2 && all(x)>0);
p.addOptional('Vraw',[],@isnumeric);
p.addOptional('VAP',[],@isnumeric);
p.addOptional('data',struct([]),@isstruct);
p.parse(varargin{:});

threshold_factor = p.Results.threshold_factor;
window = p.Results.window;
Vraw = p.Results.Vraw;
VAP = p.Results.VAP;
data = p.Results.data;
if isempty(data)
    data = struct([]);
end

clear p

if all(cellfun(@(x) numel(x), {Vraw,VAP}) > 0)
    if all(size(V) == size(Vraw)) && all(size(V) == size(VAP))
        interactive = 1;
    else
        error('Vraw and VAP must have the same size as V');
    end
elseif any(cellfun(@(x) numel(x), {Vraw,VAP}) > 0)
    error('Both Vraw and VAP must be specified');
else
    interactive = 0;
end

dt = diff(t(1:2));

% extract number of trials and samples per trial
[nsweeps,nsamples] = size(V);

% Quiroga et al., 2004
noise_amplitude = median(abs(V(:))/0.6745);
threshold = threshold_factor * noise_amplitude;

% time vector
t_events = -window(1) : dt : window(2);
% will contain the voltage dynamics of all events (i.e., deflections that
% cross the threshold)
V_events = cell(nsweeps,1);
% time and voltage peaks of all SWs
t_event_peaks = cell(nsweeps,1);
V_event_peaks = cell(nsweeps,1);
% number of SWs in each trial
n_events = zeros(nsweeps,1);

before = round(window(1) / dt);
after = round(window(2) / dt);

%%% extract all deflections that cross the threshold: these
%%% constitute the initial set of ''events''
warning('off','signal:findpeaks:largeMinPeakHeight');
for i=1:nsweeps
    [~,crossings] = findpeaks(V(i,:),'minpeakheight',...
        threshold,'minpeakdistance',round(70e-3/dt));
    crossings = crossings(t(crossings) > window(1) & ...
        t(crossings) < t(end)-window(2));
    t_event_peaks{i} = t(crossings);
    V_event_peaks{i} = V(i,crossings);
    n_events(i) = length(crossings);
    V_events{i} = zeros(n_events(i), -before+after+1);
    for j=1:n_events(i)
        idx = crossings(j)-before : crossings(j)+after;
        jdx = find(idx > 0 & idx < nsamples);
        V_events{i}(j,jdx) = V(i,idx(jdx));
    end
end
warning('on','signal:findpeaks:largeMinPeakHeight');

%%% extract the duration of all events
middle = find(t_events < diff(t_events(1:2))/2, 1, 'last');
durations = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
t_event_start = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
t_event_end = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
for i=1:nsweeps
    for j=1:n_events(i)
        idx = find(V_events{i}(j,:) < noise_amplitude);
        start = idx(find(idx < middle, 1, 'last'));
        stop = idx(find(idx > middle, 1, 'first'));
%         if isempty(start)
%             idx = middle-round(100e-3/dt):middle;
%             [~,jdx] = findpeaks(V_events(i,idx),'MinPeakHeight',-V_events(i,middle)/5);
%             start = idx(jdx(end));
%         elseif isempty(stop)
%             idx = middle:middle+round(100e-3/dt);
%             [~,jdx] = findpeaks(-V_events(i,idx),'MinPeakHeight',-V_events(i,middle)/5);
%             stop = idx(jdx(end));
%         end
        if j > 1 && t_event_peaks{i}(j) + t_events(start) < t_event_peaks{i}(j-1)
            [~,k] = min(V_events{i}(j,t_events>-diff(t_event_peaks{i}(j-1:j)) & t_events<0));
            start = middle - k;
        elseif j < n_events(i) && t_event_peaks{i}(j) + t_events(stop) > t_event_peaks{i}(j+1)
            [~,k] = min(V_events{i}(j,t_events>0 & t_events<diff(t_event_peaks{i}(j:j+1))));
            stop = middle+k;
        end
        durations{i}(j) = t_events(stop) - t_events(start);
        t_event_start{i}(j) = t_event_peaks{i}(j) + t_events(start);
        t_event_end{i}(j) = t_event_peaks{i}(j) + t_events(stop);
    end
end

%%% classify the events in the following groups
%   0 - unknown
%   1 - SW with spiking activity
%   2 - SW with no spiking activity
%   3 - dentate spike
%   4 - artifact
types = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
if exist('juxta','var') && exist('extra','var') && ~ isempty(juxta) && ~ isempty(extra)
    firing_rate_before_event = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
    firing_rate_during_event = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
    firing_rate_after_event = arrayfun(@(n) zeros(1,n), n_events, 'UniformOutput', 0);
    before = 100e-3;
    tolerance = 20e-3;
    after = 100e-3;
    % when the classification is interactive, this window will be used to detect
    % bursts around events and prompt the user for the type of event
    large_burst_window = [1,1];
    for i=1:nsweeps
        for j=1:n_events(i)
            DEBUG = 0;
            
            % look for bursts in a window around the current event
            juxta_bursts = find(juxta.tburst{i} > t_event_start{i}(j)-large_burst_window(1) & ...
                juxta.tburst{i} < t_event_end{i}(j)+large_burst_window(2),1);
            extra_bursts = find(extra.tburst{i} > t_event_start{i}(j)-large_burst_window(1) & ...
                extra.tburst{i} < t_event_end{i}(j)+large_burst_window(2),1);
            
            % look for spikes in a window around the peak of the current event
            juxta_spikes = find(juxta.tsingle{i} > t_event_peaks{i}(j)-5e-3 & ...
                juxta.tsingle{i} < t_event_peaks{i}(j)+5e-3,1);
            
            found = 0;
            if ~isempty(data)
                for fld = {'SW','SW_no_activity','DS','artifacts','unknown'}
                    if isfield(data,fld{1})
                        if any(t_event_peaks{i}(j) == data.(fld{1}).tp{i})
                            found = 1;
                            % fprintf(1, 'Found SPW at t = %.2f s in %s.\n', t_event_peaks{i}(j), fld{1});
                            break
                        end
                    end
                end
            end
            
            if found
                switch fld{1}
                    case 'unknown',
                        types{i}(j) = 0;
                    case 'SW',
                        types{i}(j) = 1;
                    case 'SW_no_activity'
                        types{i}(j) = 2;
                    case 'DS',
                        types{i}(j) = 3;
                    case 'artifacts',
                        types{i}(j) = 4;
                end
            else
                if interactive && (~ isempty(juxta_bursts) || ~ isempty(juxta_spikes)) % || ~ isempty(extra_bursts))
                    ax = plot_sharp_wave(t,Vraw(i,:),VAP(i,:),V(i,:),t_event_peaks{i}(j),...
                        t_event_start{i}(j),t_event_end{i}(j),large_burst_window,extra,...
                        juxta,i,threshold);
                    if ~ isempty(juxta_spikes)
                        title(ax(3), sprintf('Sweep %d/%d - single spike present', i, nsweeps));
                    else
                        title(ax(3), sprintf('Sweep %d/%d - burst present', i, nsweeps));
                    end
                    % ask the user whether or not the current event is an artifact
                    reply = input(['Enter event type: (0 - unknown, 1 - SW, 2 - SW with no activity, ',...
                        '3 - DS, 4 - artifact) '],'s');
                    while isempty(reply) || isnan(str2double(reply)) || ~sum(str2double(reply) == 0:4)
                        reply = input('Please enter event type: ','s');
                    end
                    types{i}(j) = str2double(reply);
                else
                    
                    %%% firing rate before event
                    a = find(extra.tsingle{i} > t_event_start{i}(j)-before-tolerance & ...
                        extra.tsingle{i} < t_event_start{i}(j)-tolerance);
                    b = find(juxta.tsingle{i} > t_event_start{i}(j)-before-tolerance & ...
                        juxta.tsingle{i} < t_event_start{i}(j)-tolerance);
                    c = find(extra.tburst{i} > t_event_start{i}(j)-before-tolerance & ...
                        extra.tburst{i} < t_event_start{i}(j)-tolerance);
                    d = find(juxta.tburst{i} > t_event_start{i}(j)-before-tolerance & ...
                        juxta.tburst{i} < t_event_start{i}(j)-tolerance);
                    firing_rate_before_event{i}(j) = length(a) + length(b);
                    for k = c(:)'
                        firing_rate_before_event{i}(j) = firing_rate_before_event{i}(j) + length(extra.tburst_full{i}{k});
                    end
                    for k = d(:)'
                        firing_rate_before_event{i}(j) = firing_rate_before_event{i}(j) + length(juxta.tburst_full{i}{k});
                    end
                    firing_rate_before_event{i}(j) = firing_rate_before_event{i}(j) / before;
                    
                    %%% firing rate during event
                    a = find(extra.tsingle{i} > t_event_start{i}(j)-tolerance & ...
                        extra.tsingle{i} < t_event_end{i}(j));
                    b = find(juxta.tsingle{i} > t_event_start{i}(j)-tolerance & ...
                        juxta.tsingle{i} < t_event_end{i}(j));
                    c = find(extra.tburst{i} > t_event_start{i}(j)-tolerance & ...
                        extra.tburst{i} < t_event_end{i}(j));
                    d = find(juxta.tburst{i} > t_event_start{i}(j)-tolerance & ...
                        juxta.tburst{i} < t_event_end{i}(j));
                    firing_rate_during_event{i}(j) = length(a) + length(b);
                    for k = c(:)'
                        firing_rate_during_event{i}(j) = firing_rate_during_event{i}(j) + length(extra.tburst_full{i}{k});
                    end
                    for k = d(:)'
                        firing_rate_during_event{i}(j) = firing_rate_during_event{i}(j) + length(juxta.tburst_full{i}{k});
                    end
                    firing_rate_during_event{i}(j) = firing_rate_during_event{i}(j) / (durations{i}(j)+tolerance);
                    
                    %%% firing rate after event
                    a = find(extra.tsingle{i} > t_event_end{i}(j) & ...
                        extra.tsingle{i} < t_event_end{i}(j)+after);
                    b = find(juxta.tsingle{i} > t_event_end{i}(j) & ...
                        juxta.tsingle{i} < t_event_end{i}(j)+after);
                    c = find(extra.tburst{i} > t_event_end{i}(j) & ...
                        extra.tburst{i} < t_event_end{i}(j)+after);
                    d = find(juxta.tburst{i} > t_event_end{i}(j) & ...
                        juxta.tburst{i} < t_event_end{i}(j)+after);
                    firing_rate_after_event{i}(j) = length(a) + length(b);
                    for k = c(:)'
                        firing_rate_after_event{i}(j) = firing_rate_after_event{i}(j) + length(extra.tburst_full{i}{k});
                    end
                    for k = d(:)'
                        firing_rate_after_event{i}(j) = firing_rate_after_event{i}(j) + length(juxta.tburst_full{i}{k});
                    end
                    firing_rate_after_event{i}(j) = firing_rate_after_event{i}(j) / after;
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    a = find(extra.tsingle{i} > t_event_start{i}(j)-tolerance & ...
                        extra.tsingle{i} < t_event_end{i}(j));
                    b = find(juxta.tsingle{i} > t_event_start{i}(j)-tolerance & ...
                        juxta.tsingle{i} < t_event_end{i}(j));
                    c = find(extra.tburst{i} > t_event_start{i}(j)-tolerance & ...
                        extra.tburst{i} < t_event_end{i}(j));
                    d = find(juxta.tburst{i} > t_event_start{i}(j)-tolerance & ...
                        juxta.tburst{i} < t_event_end{i}(j));
                    
                    if length(d) == 1
                        % if we have one juxtacellular burst and its duration is
                        % greater than half the duration of the event, we consider the
                        % event an artifact.
                        if durations{i}(j) < 2*diff(juxta.tburst_full{i}{d}([1,end]))
                            types{i}(j) = 4;
                        end
                    elseif length(b) == 1 && isempty([a,c,d])
                        % only one juxtacellular spike during the event: if the
                        % distance between the peak of the spike and the peak of
                        % the event is less than 2 ms and the duration of the event
                        % is less than 50 ms, we consider the event an artifact.
                        % NOTE: reduce to 20-30 ms?
                        if any(abs(juxta.tsingle{i}(b) - t_event_peaks{i}(j)) < 2e-3) && durations{i}(j) < 50e-3
                            types{i}(j) = 4;
                        end
                    end
                    
                    % still unknown
                    if types{i}(j) == 0
                        if firing_rate_after_event{i}(j) <= firing_rate_before_event{i}(j)
                            % the firing rate after either a local sharp wave or a
                            % dentate spike must decrease
                            if firing_rate_during_event{i}(j) > 0
                                types{i}(j) = 1;
                            else
                                if V_event_peaks{i}(j) > 2
                                    % large deflections with no activity are dentate
                                    % spikes
                                    types{i}(j) = 3;
                                else
                                    types{i}(j) = 2;
                                end
                            end
                        elseif firing_rate_during_event{i}(j) == 0
                            types{i}(j) = 2;
                        end
                    end
                end
            end
            
            if DEBUG
                ax = plot_sharp_wave(t,Vraw(i,:),VAP(i,:),V(i,:),t_event_peaks{i}(j),...
                    t_event_start{i}(j),t_event_end{i}(j),window,extra,...
                    juxta,i,threshold);
                switch(types{i}(j))
                    case 0,
                        title(ax(3),'Unknown');
                    case 1,
                        title(ax(3),'SW with activity');
                    case 2,
                        title(ax(3),'SW with no activity');
                    case 3,
                        title(ax(3),'Dentate spike');
                    case 4,
                        title(ax(3),'Artifact');
                end
                keyboard
            end
        end
    end
end

SW.tp = cell(nsweeps,1);
SW.Vp = cell(nsweeps,1);
SW.durations = cell(nsweeps,1);
SW.t_event_start = cell(nsweeps,1);
SW.t_event_end = cell(nsweeps,1);
SW_no_activity.tp = cell(nsweeps,1);
SW_no_activity.Vp = cell(nsweeps,1);
SW_no_activity.durations = cell(nsweeps,1);
SW_no_activity.t_event_start = cell(nsweeps,1);
SW_no_activity.t_event_end = cell(nsweeps,1);
DS.tp = cell(nsweeps,1);
DS.Vp = cell(nsweeps,1);
DS.durations = cell(nsweeps,1);
DS.t_event_start = cell(nsweeps,1);
DS.t_event_end = cell(nsweeps,1);
artifacts.tp = cell(nsweeps,1);
artifacts.Vp = cell(nsweeps,1);
artifacts.durations = cell(nsweeps,1);
artifacts.t_event_start = cell(nsweeps,1);
artifacts.t_event_end = cell(nsweeps,1);
unknown.tp = cell(nsweeps,1);
unknown.Vp = cell(nsweeps,1);
unknown.durations = cell(nsweeps,1);
unknown.t_event_start = cell(nsweeps,1);
unknown.t_event_end = cell(nsweeps,1);

for i=1:nsweeps
    for j=1:n_events(i)
        switch(types{i}(j))
            case 0,
                unknown.tp{i} = [unknown.tp{i}, t_event_peaks{i}(j)];
                unknown.Vp{i} = [unknown.Vp{i}, V_event_peaks{i}(j)];
                unknown.durations{i} = [unknown.durations{i}, durations{i}(j)];
                unknown.t_event_start{i} = [unknown.t_event_start{i},  t_event_start{i}(j)];
                unknown.t_event_end{i} = [unknown.t_event_end{i},  t_event_end{i}(j)];
            case 1,
                SW.tp{i} = [SW.tp{i}, t_event_peaks{i}(j)];
                SW.Vp{i} = [SW.Vp{i}, V_event_peaks{i}(j)];
                SW.durations{i} = [SW.durations{i}, durations{i}(j)];
                SW.t_event_start{i} = [SW.t_event_start{i},  t_event_start{i}(j)];
                SW.t_event_end{i} = [SW.t_event_end{i},  t_event_end{i}(j)];
            case 2,
                SW_no_activity.tp{i} = [SW_no_activity.tp{i}, t_event_peaks{i}(j)];
                SW_no_activity.Vp{i} = [SW_no_activity.Vp{i}, V_event_peaks{i}(j)];
                SW_no_activity.durations{i} = [SW_no_activity.durations{i}, durations{i}(j)];
                SW_no_activity.t_event_start{i} = [SW_no_activity.t_event_start{i},  t_event_start{i}(j)];
                SW_no_activity.t_event_end{i} = [SW_no_activity.t_event_end{i},  t_event_end{i}(j)];
            case 3,
                DS.tp{i} = [DS.tp{i}, t_event_peaks{i}(j)];
                DS.Vp{i} = [DS.Vp{i}, V_event_peaks{i}(j)];
                DS.durations{i} = [DS.durations{i}, durations{i}(j)];
                DS.t_event_start{i} = [DS.t_event_start{i},  t_event_start{i}(j)];
                DS.t_event_end{i} = [DS.t_event_end{i},  t_event_end{i}(j)];
            case 4,
                artifacts.tp{i} = [artifacts.tp{i}, t_event_peaks{i}(j)];
                artifacts.Vp{i} = [artifacts.Vp{i}, V_event_peaks{i}(j)];
                artifacts.durations{i} = [artifacts.durations{i}, durations{i}(j)];
                artifacts.t_event_start{i} = [artifacts.t_event_start{i},  t_event_start{i}(j)];
                artifacts.t_event_end{i} = [artifacts.t_event_end{i},  t_event_end{i}(j)];
        end
    end
end

SW.t = t_events;
SW_no_activity.t = t_events;
DS.t = t_events;
artifacts.t = t_events;
unknown.t = t_events;

types = flattenCellArray(types,'full');
V_events = flattenCellArray(V_events,'matrix');
SW.V = V_events(types==1,:);
SW_no_activity.V = V_events(types==2,:);
DS.V = V_events(types==3,:);
artifacts.V = V_events(types==4,:);
unknown.V = V_events(types==0,:);

end

function ax = plot_sharp_wave(t,Vraw,VAP,VLFP,tp,tstart,tend,window,extra,juxta,i,thresh)
dt = diff(t(1:2));
t_lim = tp + window.*[-1,1];
a = find(extra.tsingle{i} > t_lim(1) & extra.tsingle{i} < t_lim(2));
b = find(juxta.tsingle{i} > t_lim(1) & juxta.tsingle{i} < t_lim(2));
c = find(extra.tburst{i} > t_lim(1) & extra.tburst{i} < t_lim(2));
d = find(juxta.tburst{i} > t_lim(1) & juxta.tburst{i} < t_lim(2));
idx = find(t > t_lim(1) & t < t_lim(2));
clf;
ax = [...
    axes('Position',[.1,.1,.85,.225],'NextPlot','Add','XLim',t_lim,...
    'TickDir','Out')
    axes('Position',[.1,.375,.85,.225],'NextPlot','Add','XLim',t_lim,...
    'TickDir','Out','XTickLabel',{})
    axes('Position',[.1,.65,.85,.3],'NextPlot','Add','XLim',t_lim,...
    'TickDir','Out','XTickLabel',{}) ...
    ];
linkaxes(ax,'x');
xlabel(ax(1),'Time (s)');
plot(ax(1),t(idx),VAP(idx),'Color',[.1,.6,.1]);
jdx = round(extra.tsingle{i}(a)/dt);
plot(ax(1),t(jdx),VAP(jdx),'kv','MarkerFaceColor','k');
jdx = round(juxta.tsingle{i}(b)/dt);
plot(ax(1),t(jdx),VAP(jdx),'k^','MarkerFaceColor','k');
for k=c(:)'
    jdx = round(extra.tburst_full{i}{k}/dt);
    plot(ax(1),t(jdx),VAP(jdx),'cv','MarkerFaceColor','c');
end
for k=d(:)'
    jdx = round(juxta.tburst_full{i}{k}/dt);
    plot(ax(1),t(jdx),VAP(jdx),'m^','MarkerFaceColor','m');
end

plot(ax(2),t(idx),VLFP(idx),'k');
plot(ax(2),tp+[0,0],ylim(ax(2)),'r--');
plot(ax(2),tstart+[0,0],ylim(ax(2)),'r--');
plot(ax(2),tend+[0,0],ylim(ax(2)),'r--');
plot(ax(2),xlim(ax(2)),thresh+[0,0],'Color',[.1,.1,.6]);

plot(ax(3),t(idx),Vraw(idx),'Color',[.6,.1,.1]);
end
