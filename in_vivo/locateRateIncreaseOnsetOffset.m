function locateRateIncreaseOnsetOffset(suffix, burst_index_interval, n_SW, n_reps, T, baseline_start)
% locateRateIncreaseOnsetoffset(suffix, burst_index_interval, n_SW, n_reps, T, baseline_start)
% 
% See also detectFiringRateChange

load('SW_triggered.mat',['SW_triggered_t',suffix],'burst_index_big');

if ~ exist('n_reps','var') || isempty(n_reps)
    n_reps = 500;
end

if ~ exist('burst_index_interval','var') || isempty(burst_index_interval)
    burst_index_interval = [0,1];
end
idx = find(burst_index_big >= burst_index_interval(1) & ...
    burst_index_big <= burst_index_interval(2));

if ~ exist('n_SW','var') || isempty(n_SW)
    n_SW = min(5000, round(length(idx)/5));
end

if ~ exist('suffix','var') || isempty(suffix)
    suffix = 'spike_extra';
end

if ~ exist('baseline_start','var') || isempty(baseline_start)
    baseline_start = -0.5;
end

if ~ exist('T','var') || isempty(T)
    T = 20e-3;
end

%%% compute the probability of detecting an increase in the firing rate
start = -0.3;
stop = 0.05;
step = 2e-3;
time = [start:5e-3:-0.15, -0.15:step:-0.04, -0.035:5e-3:0, 1e-3:1e-3:30e-3, 35e-3:5e-3:stop];
p = zeros(size(time));

for i=1:length(time)
    fprintf(1, '[%03d/%03d]\n', i, length(time));
    eval(['p(i) = detectFiringRateChange(SW_triggered_t',suffix,...
        '(idx),[],baseline_start,time(i),T,n_SW,n_reps,''increase'');']);
end

filename = sprintf('firing_rate_onset_offset_%s_BI=%.1f_%.1f_N=%d_reps=%d_baseline=%.0f_ms.mat',...
    suffix,burst_index_interval(1),burst_index_interval(2),n_SW,n_reps,baseline_start*1e3);
save(filename,'n_SW','n_reps','start','stop','T','step','time','p',...
    'suffix','burst_index_interval','baseline_start',['SW_triggered_t',suffix]);

%%%
% load(filename,'time','p','suffix','burst_index_interval');
if max(burst_index_interval) < 1
    load('optimal_PSTH_binwidths.mat',['opt_binwidth_t',suffix(1:strfind(suffix,'_')-1),'_low_bf'],'interval');
    eval(['binwidth = opt_binwidth_t',suffix(1:strfind(suffix,'_')-1),'_low_bf;']);
elseif min(burst_index_interval) > 0
    load('optimal_PSTH_binwidths.mat',['opt_binwidth_t',suffix(1:strfind(suffix,'_')-1),'_high_bf'],'interval');
    eval(['binwidth = opt_binwidth_t',suffix(1:strfind(suffix,'_')-1),'_high_bf;']);
else
    load('optimal_PSTH_binwidths.mat',['opt_binwidth_t',suffix],'interval');
    eval(['binwidth = opt_binwidth_t',suffix,';']);
end
load('SW_triggered.mat',['SW_triggered_t',suffix],'burst_index_big');
eval(['spikes = SW_triggered_t',suffix,';']);

idx = find(burst_index_big >= burst_index_interval(1) & ...
    burst_index_big <= burst_index_interval(2));
[nu,edges] = psth(spikes(idx),binwidth,interval);

target_prob = 0.9;
stop_time = -10e-3;
g = fittype('1./(1+exp(-k*(t-t0)))','indep','t','coeff',{'k','t0'});
model_onset = fit(time(time<stop_time)',p(time<stop_time)',g,'StartPoint',[10,-0.15]);
model_offset = fit(time(time>=stop_time)',p(time>=stop_time)',g,'StartPoint',[-100,0]);
onset = (model_onset.t0 - 1/model_onset.k * log((1-target_prob)/target_prob));
offset = (model_offset.t0 - 1/model_offset.k * log((1-target_prob)/target_prob));
figure;
ax = betterSubplot(2,1,'Offset',[0.15,0.1;0.05,0.05]);
axes(ax(1));
plot(onset*1e3+[0,0],[0,1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(model_onset.t0*1e3+[0,0],[0,1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(offset*1e3+[0,0],[0,1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(model_offset.t0*1e3+[0,0],[0,1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(time(time<stop_time)*1e3,model_onset(time(time<stop_time)),'r','LineWidth',2);
plot(time(time>=stop_time)*1e3,model_offset(time(time>=stop_time)),'r','LineWidth',2);
plot(time*1e3,p,'ko','MarkerFaceColor','w','MarkerSize',4,...
    'LineWidth',0.8);
xlabel('Time since SW (ms)');
ylabel('Detection probability');

dnu = (nu(3:end) - nu(1:end-2)) / (2*binwidth);
axes(ax(2));
nu(end) = nu(end-1);
hndl = zeros(2,1);
hndl(2) = plot(edges(2:end-1)*1e3,dnu/100,'r','LineWidth',1);
plot(interval*1e3,mean(nu(edges<model_onset.t0))+[0,0],'m','LineWidth',1);
plot(interval*1e3,[0,0],'Color',[.4,.4,.4],'LineWidth',1);
hndl(1) = plot(edges*1e3,nu,'k','LineWidth',1);
plot(onset*1e3+[0,0],[0,max(nu)*1.1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(model_onset.t0*1e3+[0,0],[0,max(nu)*1.1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(offset*1e3+[0,0],[0,max(nu)*1.1],'-','Color',[.6,.6,.6],'LineWidth',1);
plot(model_offset.t0*1e3+[0,0],[0,max(nu)*1.1],'-','Color',[.6,.6,.6],'LineWidth',1);
axis tight;
axis([interval*1e3,-10,max(nu)*1.1]);
xlabel('Time since SW (ms)');
ylabel('Firing rate (spikes/s)');
h = legend(hndl,'\nu','d\nu/dt (/100)','Location','NorthWest');
set(h,'Box','Off');
set(gca,'XTick',(interval(1):diff(interval)/2:interval(2))*1e3);
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,7],'PaperSize',[6,7]);
print('-dpdf',[filename(1:end-4),'.pdf']);
