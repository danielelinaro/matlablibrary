function out_spikes = pickSpikes(in_spikes, index)
% out_spikes = pickSpikes(in_spikes, index)
n_trials = length(in_spikes);
n_spikes = [0 ; cumsum(cellfun(@(x) length(x), in_spikes))];
out_spikes = cell(size(in_spikes));
for i=1:n_trials
    idx = n_spikes(i)+1:n_spikes(i+1);
    jdx = intersect(index,idx) - n_spikes(i);
    out_spikes{i} = in_spikes{i}(jdx);
end
