function [halfwidth,Vh,tp,Vp,asymmetry,duration,Vd] = extractExtracellularAPFeatures(t,V,tspks,Vspks,do_plot)
% [halfwidth,Vh,tp,Vp,asymmetry,duration,Vd] = extractExtracellularAPFeatures(t,V,tspks,Vspks,do_plot)

if ~ exist('do_plot','var')
    do_plot = 0;
end

n = size(V,1);
halfwidth = zeros(n,1);
Vh = zeros(n,1);
tp = zeros(n,2);
Vp = zeros(n,2);
asymmetry = zeros(n,1);
duration = zeros(n,1);
Vd = zeros(n,1);

warning('off','signal:findpeaks:largeMinPeakHeight');
for i=1:n
    [halfwidth(i),Vh(i)] = extract_half_width(t,V(i,:),tspks(i),Vspks(i));
    [tp(i,:),Vp(i,:)] = extract_peaks(t,V(i,:),tspks(i),Vspks(i));
    asymmetry(i) = extract_asymmetry(t,V(i,:),tspks(i),tp(i,:),Vp(i,:));
    [duration(i),Vd(i)] = extract_duration(t,V(i,:),tspks(i),Vspks(i),0.25);
end
warning('on','signal:findpeaks:largeMinPeakHeight');

if do_plot
    features = [Vspks, halfwidth, Vp, asymmetry];
    feature_names = {'V peak (mV)', 'Halfwidth (ms)', 'Overshoot before (mV)', ...
        'Overshoot after (mV)', 'Asymmetry'};
    coeff = [1,1e3,1,1,1];
    ticks = {-2:0.25:2 ; 0:0.5:3 ; -0.6:0.2:2 ; -0.6:0.2:2 ; 0:7};
    rows = 2;
    cols = 5;
    n = size(features,2);
    cnt = 0;
    figure;
    ax = betterSubplot(rows,cols,'Delta',[0.05,0.1],'Offset',[0.05,0.1; 0.025,0.025]);
    for i=1:n
        for j=i+1:n
            cnt = cnt+1;
            a = ceil(cnt/cols);
            b = mod(cnt-1,cols)+1;
            x = features(:,i)*coeff(i);
            y = features(:,j)*coeff(j);
            plot(ax(a,b), x, y, 'k.','MarkerSize',3);
            xlabel(ax(a,b), feature_names{i});
            ylabel(ax(a,b), feature_names{j});
            xl = [min(x),max(x)];
            if xl(1) < ticks{i}(1) || isnan(xl(1))
                xl(1) = ticks{i}(1);
            end
            if xl(2) > ticks{i}(end) || isnan(xl(2))
                xl(2) = ticks{i}(end);
            end
            yl = [min(y),max(y)];
            if yl(1) < ticks{j}(1) || isnan(yl(1))
                yl(1) = ticks{j}(1);
            end
            if yl(2) > ticks{j}(end) || isnan(yl(2))
                yl(2) = ticks{j}(end);
            end
            xl = [ticks{i}(find(ticks{i}<=xl(1),1,'last')), ticks{i}(find(ticks{i}>=xl(2),1,'first'))];
            yl = [ticks{j}(find(ticks{j}<=yl(1),1,'last')), ticks{j}(find(ticks{j}>=yl(2),1,'first'))];
            axis(ax(a,b), [xl,yl]);
            set(ax(a,b),'XTick',ticks{i},'YTick',ticks{j});
        end
    end
    sz = [cols*3,rows*3];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','extracellular_spikes_features.pdf');
end

end

function [HW,Vh] = extract_half_width(t,V,tspk,Vspk)
[HW,Vh] = extract_duration(t,V,tspk,Vspk,0.5);
end

function [dur,Vd] = extract_duration(t,V,tspk,Vspk,fraction)
s = warning('off','MATLAB:polyfit:PolyNotUnique');
window = 3e-3;
Vd = Vspk*fraction;
idx = find(t>tspk-window & t<tspk);
start = find(V(idx) > Vd, 1, 'last');
tstart = polyval(polyfit(V(idx(start):idx(start+1)),t(idx(start):idx(start+1)),1),Vd);
idx = find(t>tspk & t<tspk+window);
stop = find(V(idx) > Vd, 1, 'first');
tstop = polyval(polyfit(V(idx(stop-1):idx(stop)),t(idx(stop-1):idx(stop)),1),Vd);
dur = tstop-tstart;
warning(s)
% clf;
% hold on;
% plot(t,V,'k*-');
% plot(tspk,Vspk,'ro');
% plot(xlim,Vd+[0,0],'g--');
% plot(tstart,Vd,'ms');
% plot([tstart,tstop],Vd+[0,0],'ro-','MarkerFaceColor','w');
end

function [tp,Vp] = extract_peaks(t,V,tspk,Vsp)
interval = [-1e-3, 0 ; 0, 2e-3];
tp = zeros(1,2);
Vp = zeros(1,2);
% Vth = Vsp/3;
Vth = 0;
flag = 0;
for i=1:2
    idx = find(t>=tspk+interval(i,1) & t<=tspk+interval(i,2));
%     [~,locs] = findpeaks(V(idx));
    [~,locs] = findpeaks(V(idx), 'MinPeakHeight', Vth);
    if isempty(locs)
    [~,locs] = findpeaks(V(idx));
        flag = 1;
    end
    if isempty(locs)
        [Vp(i),j] = max(V(idx));
        tp(i) = t(idx(j));
        flag = 1;
    else
        if i == 1
            tp(i) = t(idx(locs(end)));
            Vp(i) = V(idx(locs(end)));
        else
            tp(i) = t(idx(locs(1)));
            Vp(i) = V(idx(locs(1)));
        end
    end
end
% if flag
%     clf;
%     hold on;
%     plot(t,V,'k*-');
%     plot(tp(1),Vp(1),'ro');
%     plot(tp(2),Vp(2),'ro');
%     pause
% end
end

function asymmetry = extract_asymmetry(t,V,tspk,tp,Vp)
if any(isnan(tp))
    asymmetry = nan;
    return;
end
if Vp(1) < 0
    tstart = tp(1);
    Vstart = Vp(1);
else
    Vstart = 0;
    idx = find(t>=tp(1)-0.1e-3 & t<tspk);
    start = find(V(idx) > Vstart, 1, 'last');
    tstart = polyval(polyfit(V(idx(start):idx(start+1)),...
        t(idx(start):idx(start+1)),1),Vstart);
end
if Vp(2) < 0
    tstop = tp(2);
    Vstop = Vp(2);
else
    Vstop = 0;
    idx = find(t>tspk & t<=tp(2)+0.1e-3);
    stop = find(V(idx) > Vstop, 1, 'first');
    tstop = polyval(polyfit(V(idx(stop-1):idx(stop)),t(idx(stop-1):idx(stop)),1),Vstop);
end
asymmetry = -tstart/tstop;
% if any(Vp < 0)
% clf;
% hold on;
% plot(t,V,'k');
% plot(tp(1),Vp(1),'ro');
% plot(tp(2),Vp(2),'ro');
% plot([tstart,tspk],Vstart+[0,0],'bv-','MarkerFaceColor','w');
% plot([tspk,tstop],Vstop+[0,0],'m^-','MarkerFaceColor','w');
% pause
% end
end
