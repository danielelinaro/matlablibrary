clear all;
close all;
clc;
addpath /Users/daniele/Postdoc/Spike_sorting/Quiroga/wave_clus_2.0wb/Wave_clus/Batch_files
addpath /Users/daniele/Postdoc/Spike_sorting/Quiroga/wave_clus_2.0wb/Wave_clus/SPC

%%
sweeps_to_remove = 1;
analyzeInVivoExperiment('remove',sweeps_to_remove);
