function [Vhw,hw,hw_interval,tbhp,Vbhp,tahp,Vahp,tadp,Vadp] = extractJuxtacellularAPFeatures(t,V,tp,Vp,do_plot,AP_type)
% [Vhw,hw,hw_interval,tbhp,Vbhp,tahp,Vahp,tadp,Vadp] = extractJuxtacellularAPFeatures(t,V,tp,Vp,do_plot,AP_type)

if ~ exist('do_plot','var')
    do_plot = 1;
end

n = size(V,1);

dt = diff(t(1:2));
before = round(3e-3/dt);
after = round(3e-3/dt);
peak = round((tp-t(1))/dt);

idx = peak-before:peak;
[Vbhp,pos] = min(V(:,idx),[],2);
tbhp = t(idx(pos))';

idx = peak:peak+after;
[Vahp,pos] = min(V(:,idx),[],2);
tahp = t(idx(pos))';

tadp = zeros(size(tahp));
Vadp = zeros(size(tahp));

for i=1:n
    jdx = idx(pos(i)):idx(pos(i))+after;
    [Vadp(i),j] = max(V(i,jdx));
    tadp(i) = t(jdx(j));
end

Vhw = Vp/2;
hw_interval = zeros(n,2);
for i=1:n
    idx = 1:peak(i);
    below = find(V(i,idx) <= Vhw(i), 1, 'last');
    hw_interval(i,1) = polyval(polyfit(V(i,idx(below):idx(below)+1),t(idx(below):idx(below)+1),1),Vhw(i));
    idx = peak(i):peak(i)+after;
    below = find(V(i,idx) <= Vhw(i), 1, 'first');
    hw_interval(i,2) = polyval(polyfit(V(i,idx(below)-1:idx(below)),t(idx(below)-1:idx(below)),1),Vhw(i));
end
hw = diff(hw_interval,1,2);

if do_plot

%     nclusters = 2;
%     idx = find(AP_type <= 1);
%     jdx = kmeans([Vp(idx),hw(idx)],nclusters);
%     cluster_idx = zeros(size(Vp));
%     for j=1:nclusters
%         cluster_idx(idx(jdx==j)) = j;
%     end
%     for i=1:length(cluster_idx)
%         if cluster_idx(i) ~= 0
%             grp = cluster_idx(i);
%         elseif AP_type(i) > 1
%             cluster_idx(i) = grp;
%         elseif AP_type(i) == 0
%             cluster_idx(i) = 0;
%         end
%     end
%     marker = '.x';
    
    nclusters = 1;
    cluster_idx = ones(size(Vp));
    marker = '.';
    
    features = [Vp, hw, Vbhp, Vahp, Vadp];
    feature_names = {'V peak (mV)', 'Halfwidth (ms)', 'BHP (mV)', ...
        'AHP (mV)', 'ADP (mV)'};
    coeff = [1,1e3,1,1,1];
    ticks = {0:10 ; 0:0.5:5 ; -5:2 ; -5:2 ; -2:5};
    rows = 2;
    cols = 5;
    n = size(features,2);
    cmap = [0,0,0 ; 1,0,0 ; 0,1,0 ; 0,0,1 ; 1,.5,0 ; 0,1,.5 ; .5,0,1 ; .5,0,0 ; 0,.5,0 ; 0,0,.5];
    if max(AP_type) >= size(cmap,1)
        cmap = [0,0,0 ; lines(max(AP_type))];
    end
    figure;
    ax = betterSubplot(rows,cols,'Delta',[0.05,0.1],'Offset',[0.05,0.1; 0.025,0.025]);
    for k=1:nclusters
        for type = 0:max(AP_type)
            cnt = 0;
            for i=1:n
                for j=i+1:n
                    cnt = cnt+1;
                    a = ceil(cnt/cols);
                    b = mod(cnt-1,cols)+1;
                    x = features(AP_type==type & cluster_idx==k,i)*coeff(i);
                    y = features(AP_type==type & cluster_idx==k,j)*coeff(j);
                    plot(ax(a,b),x,y,marker(k),'Color',cmap(type+1,:),'MarkerSize',3);
                    xlabel(ax(a,b), feature_names{i});
                    ylabel(ax(a,b), feature_names{j});
                    xl = [min(x),max(x)];
                    if xl(1) < ticks{i}(1)
                        xl(1) = ticks{i}(1);
                    end
                    if xl(2) > ticks{i}(end)
                        xl(2) = ticks{i}(end);
                    end
                    yl = [min(y),max(y)];
                    if yl(1) < ticks{j}(1)
                        yl(1) = ticks{j}(1);
                    end
                    if yl(2) > ticks{j}(end)
                        yl(2) = ticks{j}(end);
                    end
                    xl = [ticks{i}(find(ticks{i}<=xl(1),1,'last')), ticks{i}(find(ticks{i}>=xl(2),1,'first'))];
                    yl = [ticks{j}(find(ticks{j}<=yl(1),1,'last')), ticks{j}(find(ticks{j}>=yl(2),1,'first'))];
                    if type > 0
                        curr_xl = xlim(ax(a,b));
                        curr_yl = ylim(ax(a,b));
                        xl = [min(xl(1),curr_xl(1)), max(xl(2),curr_xl(2))];
                        yl = [min(yl(1),curr_yl(1)), max(yl(2),curr_yl(2))];
                    end
                    axis(ax(a,b), [xl,yl]);
                    set(ax(a,b),'XTick',ticks{i},'YTick',ticks{j});
                end
            end
        end
    end
    sz = [cols*3,rows*3];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','juxtacellular_spikes_features.pdf');

    figure;
    hold on;
    for type = 0:max(AP_type)
        plot(t*1e3,V(AP_type==type,:),'Color',cmap(type+1,:),'LineWidth',1);
        plot(tbhp(AP_type==type)*1e3,Vbhp(AP_type==type),'v','Color',cmap(type+1,:),...
            'MarkerFaceColor','w','LineWidth',1,'MarkerSize',4);
        plot(tahp(AP_type==type)*1e3,Vahp(AP_type==type),'s','Color',cmap(type+1,:),...
            'MarkerFaceColor','w','LineWidth',1,'MarkerSize',4);
        plot(tadp(AP_type==type)*1e3,Vadp(AP_type==type),'^','Color',cmap(type+1,:),...
            'MarkerFaceColor','w','LineWidth',1,'MarkerSize',4);
        interval = hw_interval(AP_type==type,:);
        vhw = Vhw(AP_type==type,:);
        for i=1:length(vhw)
            plot(interval(i,:)*1e3,vhw(i)+[0,0],'o-','Color',cmap(type+1,:),...
                'MarkerFaceColor','w','LineWidth',1,'MarkerSize',4);
        end
    end
    xlabel('Time (ms)');
    ylabel('Voltage (mV)');
    set(gca,'TickDir','Out','LineWidth',0.8);
    axes('Position',[0.65,0.65,0.3,0.3],'NextPlot','Add');
    lw = ones(max(AP_type)+1,1);
    lw(1) = 2;
    for type = 0:max(AP_type)
        plot(t*1e3,mean(V(AP_type==type,:)),'Color',cmap(type+1,:),'LineWidth',lw(type+1));
    end
    axis tight;
    axis off;
    xl = xlim;
    yl = ylim;
    placeScaleBars(xl(2)-1.5,yl(2)-2,1,2,'1 ms','2 mV','k','LineWidth',1);
    sz = [6,4];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','all_juxtacellular_spikes.pdf');
end
