function L = histSWCMorpho(data,edges,method)
% L = histSWCMorpho(data,edges,method)

if ~ exist('method','var')
    method = 1;
end
if all(method ~= [1,2])
    error('method must be either 1 or 2');
end

% idx = data(:,2) ~= 2;
% coords = data(idx,3:5);
% id = data(idx,1);
% parent_id = data(idx,end);
data = data(data(:,2)~=2,:);
coords = data(:,3:5);
id = data(:,1);
parent_id = data(:,end);

if method == 1
    [n,bin] = histc(coords(:,2), edges);
    f = @(x,y,new_y) (y(1)-x(1))/(y(2)-x(2)) * (new_y - x(2)) + x(1);
else
    dst = sqrt(sum(data(:,3:5).^2,2));
    n = zeros(size(edges));
    bin = zeros(size(dst));
    for i=1:length(edges)-1
        idx = find(dst >= edges(i) & dst < edges(i+1));
        n(i) = length(idx);
        bin(idx) = i;
    end
    idx = find(dst == edges(end));
    n(end) = length(idx);
    bin(idx) = length(edges);
end

L = zeros(size(n));
cnt = zeros(3,1);
for i=2:size(coords,1)
    parent_index = find(id == parent_id(i));
    if bin(parent_index) == bin(i)
        % both the current point and its parent fall within this bin
        L(bin(i)) = L(bin(i)) + norm(coords(i,:) - coords(parent_index,:));
        cnt(1) = cnt(1) + 1;
    elseif abs(bin(parent_index)-bin(i)) == 1
        % current point and parent are not within this bin
        if method == 1
            y = edges(bin(parent_index));
            if ~ ((y > coords(i,2) && y < coords(parent_index,2)) || ...
                    (y > coords(parent_index,2) && y < coords(i,2)))
                y = edges(bin(i));
            end
            x = f(coords(i,1:2),coords(parent_index,1:2),y);
            z = f(coords(i,3:-1:2),coords(parent_index,3:-1:2),y);
            point = [x,y,z];
            dst_to_point = norm(coords(i,:)-point);
            dst_parent_point = norm(coords(parent_index,:)-point);
            if dst_to_point+dst_parent_point - norm(coords(i,:)-coords(parent_index,:)) < 1e-6
                cnt(1) = cnt(1)+1;
                L(bin(i)) = L(bin(i)) + dst_to_point;
                L(bin(parent_index)) = L(bin(parent_index)) + dst_parent_point;
            else
                clf;
                hold on;
                plot3(coords([i,parent_index],1),coords([i,parent_index],2),coords([i,parent_index],3),'ko-');
                plot3(x,y,z,'rs');
                box on;
                view(3);
                pause
            end
        else
            L(bin(i)) = L(bin(i)) + dst(i) - edges(bin(i));
            L(bin(parent_index)) = L(bin(parent_index)) + edges(bin(i)) - dst(parent_index);
            cnt(2) = cnt(2)+1;
        end
    else
        cnt(3) = cnt(3)+1;
    end
end

% L = L';
