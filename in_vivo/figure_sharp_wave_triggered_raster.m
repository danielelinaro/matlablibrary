function fig_h = figure_sharp_wave_triggered_raster(swt_event_times_per_sharp_wave, title_string)
% swt_event_times_per_sweep is a columnar cell array, each element a double
% row vector of event times in seconds.
% title_string is a string, to be used as a title.
    
    %n_events_per_sharp_wave = cellfun(@(x) length(x), swt_event_times_per_sharp_wave);  % double col vector
    %[~,idx] = sort(n_events_per_sharp_wave);
    %swt_event_times_per_sharp_wave = swt_event_times_per_sharp_wave(idx);  % sort by number of events in sweep
    t_min = -3;
    t_max = +3;
    fig_h = figure('Color','w');
    subplot(3,1,1:2);
    title(title_string);
    rasterplot(swt_event_times_per_sharp_wave,'k','LineWidth',1);
    axis tight;
    axis([t_min,t_max,0,length(swt_event_times_per_sharp_wave)]);
    set(gca,'TickDir','out','XTick',t_min:t_max,'YTick',0:50:length(swt_event_times_per_sharp_wave));
    box off;
    ylabel('Sharp-wave #');
    binwidth = 20e-3;  % s
    [mean_event_rate_in_bin,mean_burst_rate_edge_times] = psth(swt_event_times_per_sharp_wave,binwidth,[t_min t_max]);
    subplot(3,1,3);
    hold on;
    % hndl = bar(edges,nu,'histc');
    % set(hndl, 'FaceColor','k');
    % plot(edges([1,end]),mean(nu(edges>2))+[0,0],'Color',[1,.7,.7],'LineWidth',2);
    grand_mean_event_rate = mean(mean_event_rate_in_bin);
    patch([t_min t_max t_max t_min t_min] , ...
          [0 0 grand_mean_event_rate grand_mean_event_rate 0], ...
          [1 .7 .7], ...
          'EdgeColor','none');
    plot(mean_burst_rate_edge_times,mean_event_rate_in_bin,'k','LineWidth',1);
    text(2,0.7*max(mean_event_rate_in_bin),sprintf('Bin width = %.0f ms', binwidth*1e3));
    axis([t_min,t_max,0,1.1*max(mean_event_rate_in_bin)]);
    set(gca,'Layer','top');
    xlabel('Time since SW (s)');
    ylabel('Event rate (Hz)');
    set(gca,'TickDir','out','XTick',t_min:t_max);
    box off;

end
