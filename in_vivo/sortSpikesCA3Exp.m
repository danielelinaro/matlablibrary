function sortSpikesCA3Exp(nclusters,features_idx)
% sortSpikesCA3Exp(nclusters,features_idx)

%%% prepare the data we are going to use
% load some preprocessed data
extra = load('CA3_experiment_extracellular_data.mat','Vpeaks_extra',...
    'tspike_extra','Vspike_extra','args');
tspike = extra.tspike_extra;
Vspike = extra.Vspike_extra;
% find the bursts (only to save them later)
[tburst,Vburst,tburst_full,Vburst_full] = locateBursts(extra.tspike_extra,...
    extra.Vspike_extra,50,3,[],1);
% find the spike types
types = spiketype(tspike,tburst_full);
types_flat = flattenCellArray(types,'full');

%%% load the data
if ~ isempty(extra.args.ignoreSweeps)
    [t,~,VAP] = loadIBWFiles('.', 'Ignore', extra.args.ignoreSweeps, 'Bands', ...
        extra.args.APFilterBand);
else
    [t,~,VAP] = loadIBWFiles('.', 'Sweeps', extra.args.sweeps, 'Bands', ...
        extra.args.APFilterBand);
end
[~,V] = ETSA(t,VAP,tspike,extra.args.APWindow);
V = V(types_flat==0 | types_flat==1,:);
T = (0:size(V,2)-1) * diff(t(1:2)) - extra.args.APWindow(1);
Vpeaks = flattenCellArray(Vspike,'full');
Vpeaks = Vpeaks(types_flat==0 | types_flat==1);
tpeaks = zeros(size(Vpeaks));

%%% compute spike features
fprintf(1, 'Extracting spike features... ');
[hw,~,~,Vp,asymmetry] = extractExtracellularAPFeatures(T,V,tpeaks,Vpeaks,1);
fprintf(1, 'done.\n');
feature_names = {'V peak (mV)', 'Halfwidth (ms)', 'Overshoot before (mV)', ...
    'Overshoot after (mV)', 'Asymmetry'};
features = [Vpeaks,hw*1e3,Vp,asymmetry];

if ~ exist('features_idx','var') || isempty(features_idx)
    features_idx = input('Which features do you want to use? ');
end
if ~ exist('nclusters','var') || isempty(nclusters)
    nclusters = input('Maximum number of clusters? ');
end

%%% select only those spikes that have all features
if any(any(isnan(features),2))
    keyboard
end

% use_pca = 1;
% if use_pca
%     [~,score] = pca(features);
%     feature_names = {'PC 1','PC 2','PC 3'};
%     features = score(:,1:3);
% end

%%%%%%%%%%%%%%%%%%%%%%%%
%%% do the spike sorting
%%%%%%%%%%%%%%%%%%%%%%%%
%%% spike sorting
clu = sortSpikes2(features,feature_names,T,V,...
    cellfun(@(x,y) x(y==0 | y==1), tspike, types, 'UniformOutput', 0),...
    features_idx,nclusters);
nclusters = length(unique(clu));
fprintf(1, 'There are %d clusters.\n', nclusters);
good_clusters = input('Enter the numbers of the clusters to keep (starting from 0): ');

%%% assign each spike in a burst to the correct cluster
clusters = nan(size(types_flat));
clusters(types_flat == 0 | types_flat == 1) = clu;
for i=1:length(clusters)
    if isnan(clusters(i))
        clusters(i) = clusters(i-1);
    end
end

%%% save the data
save('CA3_experiment_extracellular_spikes_sorted.mat','tspike','Vspike',...
    'tburst','Vburst','tburst_full','Vburst_full','types','types_flat',...
    'features','feature_names','features_idx','nclusters','clusters',...
    'good_clusters');

!touch SPIKE_SORTED_KK

