function [clusters,nclusters,features_idx] = sortSpikes2(features,feature_names,...
    t,V,spike_times,features_idx,nclusters,method)
% function [clusters,nclusters,features_idx] = sortSpikes2(features,feature_names,...
%     t,V,spike_times,features_idx,nclusters)

if ~ exist('method','var') || isempty(method)
    method = 'kk';
end

switch method
    case 'kmeans'
        fclus = @use_kmeans;
    case 'kk'
        fclus = @use_klustakwik;
end

if ~ exist('features_idx','var') || isempty(features_idx)
    %%% ask the user which features to use
    nfeatures = size(features,2);
    
    f = figure;
    ax = betterSubplot(nfeatures,nfeatures,'Offset',[0,0;0,0],'Delta',[0,0]);
    for i=1:nfeatures
        for j=i+1:nfeatures
            plot(ax(i,j), features(:,i), features(:,j),'k.','MarkerSize',3);
        end
    end
    set(ax,'XTick',[],'YTick',[]);
    
    features_idx = input('Which features do you want to use for clustering? ');
    close(f);
end

if ~ exist('nclusters','var') || isempty(nclusters)
    %%% find the optimal number of clusters
    wss = zeros(1,10);
    warning('off','stats:kmeans:MissingDataRemoved');
    for k=1:10
        [~,~,sumd] = fclus(features(:,features_idx),k);
        wss(k) = sum(sumd);
    end
    warning('on','stats:kmeans:MissingDataRemoved');
    f = figure;
    plot(wss,'ko-','MarkerFaceColor','w','LineWidth',1);
    xlabel('Number of clusters');
    ylabel('Within clusters sum of squares');
    set(gca,'TickDir','Out','LineWidth',0.8);
    box off;
    
    nclusters = input('How many clusters shall I use? ');
    close(f);
end

%%% perform the actual clustering
clusters = fclus(features(:,features_idx),nclusters);
ask_undo = 0;
undone = 0;

while 1
    clusters_index = unique(clusters);
    nclusters = length(clusters_index);
    
    close all;
    
    %%% plot the features, clustered
    ticks = {-4:0.25:2 ; 0:0.5:3 ; -0.5:0.1:2.5 ; -0.5:0.1:2.5 ; 0:12};
    rows = 2;
    cols = 5;
    n = size(features,2);
    cnt = 0;
    cmap = [0,0,0 ; 1,0,0 ; 0,1,0 ; 0,0,1 ; 1,1,0 ; 0,1,1 ; 1,0,1 ; 1,0.5,0 ; 0,1,0.5 ; 0.5,0,1];
    if nclusters > size(cmap,1)
        cmap = [cmap ; lines(nclusters-size(cmap,1))];
    end
    figure;
    ax = betterSubplot(rows,cols,'Delta',[0.05,0.1],'Offset',[0.05,0.1; 0.025,0.025]);
    for i=1:n
        for j=i+1:n
            cnt = cnt+1;
            a = ceil(cnt/cols);
            b = mod(cnt-1,cols)+1;
            x = features(:,i);
            y = features(:,j);
            for k=1:nclusters
                plot(ax(a,b), x(clusters==clusters_index(k)), y(clusters==clusters_index(k)), '.', 'Color', ...
                    cmap(k,:), 'MarkerSize', 3);
            end
            xlabel(ax(a,b), feature_names{i});
            ylabel(ax(a,b), feature_names{j});
            xl = [min(x),max(x)];
            yl = [min(y),max(y)];
            xl = [ticks{i}(find(ticks{i}<xl(1),1,'last')), ticks{i}(find(ticks{i}>xl(2),1,'first'))];
            yl = [ticks{j}(find(ticks{j}<yl(1),1,'last')), ticks{j}(find(ticks{j}>yl(2),1,'first'))];
            try
                axis(ax(a,b), [xl,yl]);
            catch
                keyboard
            end
            set(ax(a,b),'XTick',ticks{i},'YTick',ticks{j});
        end
    end
    sz = [cols*3,rows*3];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','extracellular_spikes_features_clustered.pdf');
    
    %%% plot a raster and the spike shapes, clustered
    figure;
    if ~ isempty(spike_times)
        axes('Position',[0.1,0.4,0.45,0.5],'NextPlot','Add','LineWidth',0.8,...
            'TickDir','Out');
        spks = cell(nclusters,1);
        for i=1:nclusters
            spks{i} = pickSpikes(spike_times,find(clusters==clusters_index(i)));
            rasterplot(spks{i},'Color',cmap(i,:),'LineWidth',0.8);
        end
        axis([0,20,0,length(spks{i})]);
        xlabel('Time (s)');
        ylabel('Trial #');
        set(gca,'YTick',[1,length(spks{i})],'XTick',0:5:20);
    end
    
    axes('Position',[0.1,0.1,0.45,0.2],'NextPlot','Add','LineWidth',0.8,...
        'TickDir','Out');
    for i=1:nclusters
        f = cellfun(@(x) length(x)/20, spks{i});
        plot(f,'Color',cmap(i,:),'LineWidth',1);
    end
    axis([0,length(spks{1}),ylim]);
    xlabel('Trial #');
    ylabel('Firing rate (spikes/s)');
    set(gca,'XTick',0:10:length(spks{1}));
    
    axes('Position',[0.7,0.1,0.25,0.8],'NextPlot','Add','LineWidth',0.8,...
        'TickDir','Out');
    % for i=1:nclusters
    %     plot(t*1e3,V(clusters==i,:),'Color',min(cmap(i,:)+[.3,.3,.3],[1,1,1]));
    % end
    for i=1:nclusters
        m = nanmean(V(clusters==clusters_index(i),:));
        s = nanstd(V(clusters==clusters_index(i),:));
        plot(t*1e3,m,'Color',cmap(i,:),'LineWidth',2);
        for j=1:5:length(m)
            plot(t(j)*1e3+[0,0],m(j)+s(j)*[-1,1],'LineWidth',1,'Color',cmap(i,:));
        end
    end
    set(gca,'YTick',-0.5:0.25:0.5,'XTick',t(1)*1e3:t(end)*1e3);
    axis([t([1,end])*1e3,-0.5,0.5]);
    xlabel('Time (ms)');
    ylabel('V (mV)');
    sz = [7,4];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','extracellular_spikes_clustered.pdf');
    
    %%% plot the auto- and cross-correlograms
    window = [0.2,0.2];
    binwidth = 1e-3;
    figure;
    ax = betterSubplot(nclusters,nclusters,'Delta',[0.025,0.05],'Offset',[0.05,0.05; 0.025,0.025]);
    set(ax,'XLim',window.*[-0.1,0.1]*1e3,'XTick',(-window(1):0.01:window(2))*1e3);
    for i=1:nclusters
        for j=1:nclusters
            if j < i
                set(ax(i,j),'Visible','Off');
            else
                if i == j
                    [nu,edges] = computeCorrelogram(spks{i}, [], window, binwidth);
                else
                    [nu,edges] = computeCorrelogram(spks{i}, spks{j}, window, binwidth);
                end
                b = bar(ax(i,j),edges*1e3,nu,'histc');
                if i == j
                    set(b,'FaceColor',cmap(i,:),'EdgeColor',cmap(i,:));
                else
                    set(b,'FaceColor','k','EdgeColor','k');
                end
                plot(ax(i,j),-3+[0,0],[0,max(1,max(nu))],'k','LineWidth',2);
                plot(ax(i,j),+3+[0,0],[0,max(1,max(nu))],'k','LineWidth',2);
                set(ax(i,j), 'YLim', [0,max(1,max(nu))]);
                if i == j
                    xlabel(ax(i,j), 'Time (ms)');
                    ylabel(ax(i,j), 'Firing rate (spikes/s)');
                    title(ax(i,j), sprintf('#%d',i-1));
                    idx = find(edges>0);
                    jdx = find(nu(idx)>0,1);
                    if edges(idx(jdx)) > 20e-3
                        set(ax(i,j),'XLim',window.*[-1,1]*1e3,'XTick',(-window(1):0.1:window(2))*1e3);
                    end
                end
            end
        end
    end
    sz = [nclusters*2,nclusters*2];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','extracellular_spikes_correlograms.pdf');
    
    if nclusters == 1
        break;
    end
    
    if ask_undo
        reply = '';
        while isempty(reply) || (reply ~= 'y' && reply ~= 'n')
            reply = lower(input('Keep merged clusters? Y/N: ','s'));
        end
        if reply == 'n'
            clusters = clusters_old;
            undone = 1;
        else
            for i=1:nclusters
                clusters(clusters == clusters_index(i)) = i-1;
            end
            clusters_index = unique(clusters);
            undone = 0;
        end
        ask_undo = 0;
    else
        undone = 0;
    end
    
    if ~ undone
        reply = lower(input('Merge clusters (to remove a cluster, merge it with cluster #0)? Y/N [N]: ','s'));
        if ~ isempty(reply) && reply == 'y'
            got_clusters = 0;
            while ~ got_clusters
                idx = input('Enter the indexes of the two clusters you want to merge (e.g., [1,2]): ');
                idx = sort(idx(:))';
                if length(idx) ~= 2
                    fprintf(1, 'You must enter exactly two cluster indexes.\n');
                    got_clusters = 0;
                else
                    got_clusters = 1;
                    for i = idx
                        if ~ ismember(i,clusters_index)
                            fprintf(1, '%d: no such cluster.\n', i);
                            got_clusters = 0;
                            break;
                        end
                    end
                end
            end
            clusters_old = clusters;
            clusters(clusters == idx(2)) = idx(1);
            ask_undo = 1;
        else
            break;
        end
    end
end

end

%%%%%
%%%%%
function clusters = use_kmeans(features,nclusters)
warning('off','stats:kmeans:MissingDataRemoved');
clusters = kmeans(features,nclusters);
warning('on','stats:kmeans:MissingDataRemoved');
end

%%%%%
%%%%%
function clusters = use_klustakwik(features,nclusters)
klustakwik = '/Users/daniele/Postdoc/Spike_sorting/klustakwik/KlustaKwik';
filebase = 'recording';
elec_no = 1;
min_clus = 2;
max_clus = nclusters+1;
max_possible_clus = nclusters+1;
[r,c] = size(features);
fid = fopen(sprintf('%s.fet.%d',filebase,elec_no),'w');
fprintf(fid, '%d\n', c);
for i=1:r
    fprintf(fid, '%e ', features(i,:));
    fprintf(fid, '\n');
end
fclose(fid);
command = sprintf(['%s %s %d -DropLastNFeatures 0 -UseDistributional 0 ', ...
    '-MinClusters %d -MaxClusters %d -MaxPossibleClusters %d'], ...
    klustakwik, filebase, elec_no, min_clus, max_clus, max_possible_clus);
fprintf(1, 'Running klustakwik... ');
[status,result] = system(command);
fprintf(1, 'done.\n');
clusters = load(sprintf('%s.clu.%d',filebase,elec_no));
clusters = clusters(2:end)-1;
end
