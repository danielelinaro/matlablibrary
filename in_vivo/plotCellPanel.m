function plotCellPanel(cell_id, SPW_no, window, bursts_binwidth, singles_binwidth)
% plotCellPanel generates a summary plot of a cell
% 
% Usage:
%   plotCellPanel(cell_id, SPW_no, window, bursts_binwidth, singles_binwidth)
% 
% Arguments:
%            cell_id - the ID of the cell, i.e., the path of the folder that
%                      contains preprocessed and raw data
%             SPW_no - the number of the sharp wave that will be plotted
%                      (default: 1)
%             window - a 2-element vector containing the window around the
%                      sharp wave peak (default: [1,1])
%   singles_binwidth - the binwidth (in seconds) to use for the computation
%                      of single spikes PSTH (default: 0.02 s)
%    bursts_binwidth - the binwidth (in seconds) to use for the computation
%                      of bursts PSTH (default: 0.02 s)
%
% Examples:
% 
%   % highest SPW participation index (for bursts)
%   plotCellPanel('20160303B', 26, [1,1], 20e-3, 20e-3);
% 
%   % highest SPW participation index (for spikes)
%   plotCellPanel('20150902A', 18, [1,1], 20e-3, 20e-3);
% 
%   % highest firing rate
%   plotCellPanel('20150805A', 46, [1,1], 20e-3, 20e-3);
% 

% Author: Daniele Linaro - June 2016

if ~ exist(cell_id,'file')
    error('%s: no such directory', cell_id);
end

if ~ exist('SPW_no','var') || isempty(SPW_no)
    SPW_no = 1;
end

if ~ exist('window','var') || isempty(window)
    window = [1,1];
end

if window(1) < 0
    error('The first element of window must be positive');
end

if ~ exist('bursts_binwidth','var') || isempty(bursts_binwidth)
    bursts_binwidth = 20e-3;
end

if ~ exist('singles_binwidth','var') || isempty(singles_binwidth)
    singles_binwidth = 20e-3;
end

load([cell_id,'/CA3_experiment_juxtacellular_data.mat'],...
    'tspike_juxta','tsingle_juxta','tburst_juxta','tburst_juxta_full');
load([cell_id,'/CA3_experiment_LFP_data.mat'],'SW','args');

if isempty(args.sweeps)
    [t,~,VAP,VLFP] = loadIBWFiles(cell_id, 'Ignore', args.ignoreSweeps, ...
        'Bands', [args.APFilterBand ; args.LFPFilterBand]);
else
    [t,~,VAP,VLFP] = loadIBWFiles(cell_id, args.sweeps, ...
        'Bands', [args.APFilterBand ; args.LFPFilterBand]);
end

[~,VAP] = ETSA(t,VAP,SW.tp,window);
VSPW = ETSA(t,VLFP,SW.tp,window);
T = (0:size(VAP,2)-1) * diff(t(1:2)) - window(1);

[~,~,~,~,unknown] = extractSharpWaves(T+window(1),VSPW);

triggered_spikes = ETE(tspike_juxta, SW.tp, window);
triggered_singles = ETE(tsingle_juxta, SW.tp, window);
triggered_bursts = ETE(tburst_juxta, SW.tp, window);
[nu_singles,edges_singles] = psth(triggered_singles,singles_binwidth,window.*[-1,1]);
[nu_bursts,edges_bursts] = psth(triggered_bursts,bursts_binwidth,window.*[-1,1]);

xticks = -1:.5:1;

bursts_col = [.8,0,0];
singles_col = [0,0,0];
spikes_col = [.6,.6,.6];
lfp_col = [0,0,.8];
patch_col = [1,.8,.4];

figure;

%%%%
parent = axes('Position',[0.1,0.72,0.8,0.25],'NextPlot','Add','LineWidth',0.8,...
    'TickDir','Out');
plot(T,VAP(SPW_no,:),'k','LineWidth',1);
axis tight;
axis off;
xl = xlim(parent);
placeScaleBars(xl(end)-0.01*diff(xl),0.7,[],1,'','','k','LineWidth',1);
if ~ isempty(triggered_bursts{SPW_no})
    jdx = find(T>triggered_bursts{SPW_no}(1)-10e-3 & ...
        T<triggered_bursts{SPW_no}(1)+50e-3);
    ax = make_inset(parent,triggered_bursts{SPW_no}(1)+100e-3,...
        0.3,diff(T(jdx([1,end]))),3);
    plot(ax,T(jdx),VAP(SPW_no,jdx),'k','LineWidth',1);
    placeScaleBars(T(jdx(end))-5e-3,0.5,5e-3,[],'','','k','LineWidth',1);
    placeScaleBars(T(jdx(end)),0.5,[],1,'','','k','LineWidth',1);
    axis tight;
    axis off;
end
if ~ isempty(triggered_singles{SPW_no})
    jdx = find(T>triggered_singles{SPW_no}(1)-10e-3 & ...
        T<triggered_singles{SPW_no}(1)+20e-3);
    ax = make_inset(parent,triggered_singles{SPW_no}(1)+50e-3,...
        0.3,diff(T(jdx([1,end]))),3);
    plot(ax,T(jdx),VAP(SPW_no,jdx),'k','LineWidth',1);
    placeScaleBars(T(jdx(end))-5e-3,0.5,5e-3,[],'','','k','LineWidth',1);
    placeScaleBars(T(jdx(end)),0.5,[],1,'','','k','LineWidth',1);
    axis tight;
    axis off;
end

%%%%
axes('Position',[0.1,0.35,0.8,0.35],'NextPlot','Add','LineWidth',0.8,...
    'TickDir','Out');
patch([-window(1)*0.995,window(2),window(2),-window(1)*0.995],...
    SPW_no+[-0.5,-0.5,0.5,0.5],patch_col,'EdgeColor',patch_col);
% plot([0,0],[0.5,length(triggered_spikes)+0.5],'Color',[0,.6,0],'LineWidth',1);
rasterplot(triggered_spikes,'Color',spikes_col,'LineWidth',1);
rasterplot(triggered_bursts,'Color',bursts_col,'LineWidth',2);
rasterplot(triggered_singles,'Color',singles_col,'LineWidth',1);
axis([window.*[-1,1],0.5,length(triggered_spikes)+0.5]);
set(gca,'XTick',xticks,'XTickLabel',[],'YTick',[1,length(triggered_spikes)]);
ylabel('SPW number');

%%%%
axes('Position',[0.1,0.1,0.8,0.2],'NextPlot','Add','LineWidth',0.8,...
    'TickDir','Out');
[hAx,h1,h2] = plotyy(edges_bursts,nu_bursts,T,VSPW,'plot','plot');
plot(hAx(1),edges_singles,nu_singles,'Color',singles_col,'LineWidth',1);
col = [0,0,0 ; lfp_col];
ylimits = [0,max([nu_bursts,nu_singles])*1.1 ;[min(VSPW)*1.2,ceil(max(VSPW)/0.5)*0.5]]; 
step = [ceil(ylimits(1,2)/40)*10,0.5];
for i=1:length(hAx)
    set(hAx(i), 'LineWidth', 0.8, 'FontSize', 10, 'TickDir', 'Out',...
        'XLim',window.*[-1,1],'XTick',xticks,'YColor',col(i,:),'Box','Off',...
        'YLim',ylimits(i,:),'YTick',0:step(i):ylimits(i,2));
end
set(h1,'LineWidth',1,'Color',bursts_col);
set(h2,'LineWidth',1,'Color',lfp_col);
xlabel('Time since SPW (s)');
ylabel(hAx(1),'Event rate (1/s)');
ylabel(hAx(2),'Voltage (mV)');

axes('Position',[0.1,0.1,0.8,0.87],'NextPlot','Add');
axis([window.*[-1,1],0,1]);
plot(unknown.t_event_start{1}-window(1)+[0,0],ylim,'Color',[.8,0,.4],'LineWidth',1);
plot(unknown.t_event_end{1}-window(1)+[0,0],ylim,'Color',[.8,0,.4],'LineWidth',1);
axis off;

sz = [7,6];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
print('-dpdf',[cell_id,'.pdf']);

%%% save the data to a mat file compatible with origin
interval = window.*[-1,1];
t = T;
V_AP = VAP(SPW_no,:);
V_SPW = VSPW;
SPW_onset = unknown.t_event_start{1}-window(1);
SPW_offset = unknown.t_event_end{1}-window(1);
[spikes_raster,edges_raster] = binarizeSpikeTrain(triggered_spikes,1e-3,interval,10e-3);
singles_raster = binarizeSpikeTrain(triggered_singles,1e-3,interval,10e-3);
bursts_raster = binarizeSpikeTrain(triggered_bursts,1e-3,interval,10e-3);
spikes_raster = full(spikes_raster);
singles_raster = full(singles_raster);
bursts_raster = full(bursts_raster);
save([cell_id,'.mat'],'t','V_*','SPW_o*set','SPW_no','edges_*','nu_*','*_raster','window');

end

function child = make_inset(parent,x,y,duration,magnification)
xl = xlim(parent);
yl = ylim(parent);
parent_pos = get(parent,'Position');
pos = zeros(1,4);
pos(1) = parent_pos(1) + (x-xl(1))/diff(xl)*parent_pos(3);
pos(2) = parent_pos(2) + (y-yl(1))/diff(yl)*parent_pos(4);
pos(3) = duration/diff(xl)*magnification*parent_pos(3);
pos(4) = sum(parent_pos([2,4])) - pos(2);
child = axes('Position',pos,'NextPlot','Add');
end
