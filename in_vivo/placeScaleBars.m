function placeScaleBars(x,y,dx,dy,xlabel,ylabel,varargin)
% placeScaleBars(x,y,dx,dy,xlabel,ylabel)

if ischar(xlabel)
    xlabel = {xlabel, 'k'};
end
if ischar(ylabel)
    ylabel = {ylabel, 'k'};
end

fntsize = 8;

if ~ isempty(dx)
    plot([x,x+dx],[y,y],varargin{:});
    if ~ isempty(xlabel{1})
        [nlbl,c] = size(xlabel);
        dst = 1 / (nlbl+1);
        if c == 1
            xlabel = [xlabel, cell(nlbl,1)];
            for k=1:nlbl
                xlabel{k,2} = 'k';
            end
        end
        for k=1:size(xlabel,1)
            if xlabel{k,1}(1) == '-'
                lbl = xlabel{k,1}(2:end);
                align = 'Bottom';
                ypos = y+diff(xlim)/100;
            elseif xlabel{k,1}(1) == '+'
                lbl = xlabel{k,1}(2:end);
                align = 'Top';
                ypos = y-diff(ylim)/100;
            else
                lbl = xlabel{k,1};
                align = 'Top';
                ypos = y-diff(ylim)/100;
            end
            text(x+k*dst*dx,ypos,lbl,'Color',xlabel{k,2}, ...
                'HorizontalAlignment','Center','VerticalAlignment',align,...
                'FontName','Arial','FontSize',fntsize);
        end
    end
end
if ~ isempty(dy)
    plot([x,x],[y,y+dy],varargin{:});
    if ~ isempty(ylabel{1})
        [nlbl,c] = size(ylabel);
        dst = 1. / (nlbl+1);
        if c == 1
            ylabel = [ylabel, cell(nlbl,1)];
            for k=1:nlbl
                ylabel{k,2} = 'k';
            end
        end
        for k=1:size(ylabel,1)
            if ylabel{k,1}(1) == '-'
                lbl = ylabel{k,1}(2:end);
                align = 'Left';
                xpos = x+diff(xlim)/75;
            elseif ylabel{k,1}(1) == '+'
                lbl = ylabel{k,1}(2:end);
                align = 'Right';
                xpos = x-diff(xlim)/75;
            else
                lbl = ylabel{k,1};
                align = 'Right';
                xpos = x-diff(xlim)/75;
            end
    %         text(xpos, y+k*dst*dy, lbl, 'Color', ylabel{k,2}, ...
    %             'HorizontalAlignment',align,'VerticalAlignment','Middle',...
    %             'FontName','Arial','FontSize',9);
            text(xpos, y+k*dst*dy, lbl, 'Color', ylabel{k,2}, ...
                'HorizontalAlignment','Center','VerticalAlignment','Bottom',...
                'FontName','Arial','FontSize',fntsize,'Rotation',90);
        end
    end
end
