function [tsingle,ispikes] = spikediff(tspikes,tbursts_full)
% [tsingle,ispikes] = spikediff(tspikes,tbursts_full)
n = length(tspikes);
tsingle = cell(n,1);
ispikes = cell(n,1);
for i=1:n
    [tsingle{i},ispikes{i}] = ...
        setdiff(tspikes{i},flattenCellArray(tbursts_full{i},'full'));
end
