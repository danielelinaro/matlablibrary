function preprocessSWCA3Exp(window,binwidth)

if ~ exist('binwidth','var')
    binwidth = 0.2;
end

if ~ exist('window','var') || isempty(window)
    window = [2,2];
end

spikes = load('spikes_CA3_exp.mat');
SW = load('sharp-waves_CA3_exp.mat');
SW_triggered_tspike_juxta = ETE(spikes.tspike_juxta,SW.tSW,window);
SW_triggered_tburst_juxta = ETE(spikes.tburst_juxta,SW.tSW,window);
SW_triggered_tsingle_juxta = ETE(spikes.tsingle_juxta,SW.tSW,window);
SW_triggered_tspike_extra = ETE(spikes.tspike_extra,SW.tSW,window);
SW_triggered_tburst_extra = ETE(spikes.tburst_extra,SW.tSW,window);
SW_triggered_tsingle_extra = ETE(spikes.tsingle_extra,SW.tSW,window);
% tburst_juxta_triggered_SW = ETE(SW.tSW,spikes.tburst_juxta,window);
% tsingle_juxta_triggered_SW = ETE(SW.tSW,spikes.tsingle_juxta,window);

middle = find(SW.time_SW == 0);
nSW = size(SW.voltage_SW,1);
SW_duration = zeros(nSW,1);
for i=1:nSW
    idx = find(SW.voltage_SW(i,:) < 0);
    start = idx(find(idx < middle, 1, 'last'));
    stop = idx(find(idx > middle, 1, 'first'));
%     dV = (SW.voltage_SW(i,3:end) - SW.voltage_SW(i,1:end-2)) / (2*SW.dt);
%     dV = [dV(1), dV, dV(end)];
%     if min(dV(start:middle-1)) < -3
%         jdx = start:middle-1;
%         [~,j] = min(dV(jdx));
%         if SW.voltage_SW(i,jdx(j)) < SW.voltage_SW(i,middle) / 10
%             [~,k] = min(SW.voltage_SW(i,jdx(j):jdx(j)+round(10e-3/SW.dt)));
%             start = jdx(j) + k - 1;
%         end
%     end
    if isempty(start)
        idx = middle-round(100e-3/SW.dt):middle;
        [~,jdx] = findpeaks(-SW.voltage_SW(i,idx),'MinPeakHeight',-SW.voltage_SW(i,middle)/5);
        start = idx(jdx(end));
    elseif isempty(stop)
        idx = middle:middle+round(100e-3/SW.dt);
        [~,jdx] = findpeaks(-SW.voltage_SW(i,idx),'MinPeakHeight',-SW.voltage_SW(i,middle)/5);
        stop = idx(jdx(end));
    end
    SW_duration(i) = SW.time_SW(stop) - SW.time_SW(start);
end

save('sharp-wave-triggered_spikes_CA3_exp.mat','window',...
    'SW_triggered_tspike_juxta','SW_triggered_tburst_juxta','SW_triggered_tsingle_juxta',...
    'SW_triggered_tspike_extra','SW_triggered_tburst_extra','SW_triggered_tsingle_extra');

%%% Do some plotting

figure;
axes('Position',[0.05,0.6,0.25,0.35],'NextPlot','Add','FontSize',11);
plot(SW.time_SW,SW.voltage_SW,'k','LineWidth',0.5);
plot(SW.time_SW,nanmean(SW.voltage_SW),'r','LineWidth',2);
axis tight;
yl = ylim;
text(SW.time_SW(1),yl(2),sprintf('N = %d', size(SW.voltage_SW,1)),...
    'VerticalAlignment','Top','HorizontalAlignment','Left','FontSize',11);
placeScaleBars(SW.time_SW(end)-0.1,yl(2)-0.05,0.1,[],'100 ms','','k',...
    'LineWidth',1);
axis off;

axes('Position',[0.35,0.6,0.1,0.35],'TickDir','Out','NextPlot','Add',...
    'XAxisLocation','Top','View',[-90,90],'LineWidth',0.8,'FontSize',11);
axis([xlim,yl]);
edges = yl(1):0.1:yl(2);
n = histc(max(SW.voltage_SW,[],2),edges);
n = n/sum(n);
b = bar(edges,n,0.75,'histc');
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
xlabel('Peak voltage (mV)');
ylabel('Fraction');
box off;
axis tight;

edges = 0:5:ceil(max(SW_duration*1e3)/50)*50;
n = histc(SW_duration*1e3,edges);
n = n/sum(n);
ax = axes('Position',[0.1,0.5,0.2,0.075],'TickDir','Out','NextPlot','Add',...
    'LineWidth',0.8,'FontSize',11);
b = bar(edges,n,0.75,'histc');
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
axis([edges([1,end]),ylim]);
yl = ylim;
set(ax, 'XTick',edges(1):50:edges(end), 'YTick', [0,yl(2)]);
xlabel('SW duration (ms)');
ylabel('Fraction');

ISWI = flattenCellArray(cellfun(@(x) diff(x), SW.tSW, ...
    'UniformOutput', 0), 'full');
edges = logspace(-2,2,20);
n = histc(ISWI,edges);
n = n/sum(n);
ax = axes('Position',[0.1,0.1,0.4,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',0.8,'FontSize',11);
b = bar(edges,n,0.75,'histc');
delete(findobj('marker','*'));
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
set(gca,'XScale','Log','XTick',10.^(-2:2),'XTickLabel',10.^(-2:2));
axis([0.01,100,0,max(n)*1.1]);
xlabel('Interval (s)');
ylabel('Fraction');

IBI = flattenCellArray(cellfun(@(x) diff(x), spikes.tburst_juxta, ...
    'UniformOutput', 0), 'full');
edges = logspace(-2,2,20);
n = histc(IBI,edges);
n = n/sum(n);
yl = ylim;
b = bar(ax,edges,n,0.3,'histc');
set(b,'FaceColor',[1,.5,0],'LineWidth',1);
delete(findobj('marker','*'));
if yl(2) < max(n)*1.1
    axis([0.01,100,0,max(n)*1.1]);
end
h = legend('Sharp-waves','Bursts','Location','NorthWest');
set(h,'Box','Off');

n = 3;
above = 0.07;
below = 0.05;
offset = 0.05;
h = (1-offset-(above+below)*n)/n;
ax = arrayfun(@(i) axes('Position',[0.625,offset+below*i+(i-1)*(above+h),0.3,h],...
    'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',11), 1:n);

[nu,edges] = psth(SW_triggered_tburst_juxta,binwidth,window.*[-1,1]);
b = bar(ax(end),edges,nu,0.75,'histc');
set(b,'FaceColor',[1,.5,.0],'LineWidth',1);
if all(nu == 0)
    axis(ax(end),[edges([1,end]),0,1]);
else
    axis(ax(end),[edges([1,end]),0,1.1*max(nu)]);
end
% xlabel(ax(end),'Time since SW (s)');
ylabel(ax(end),'Burst rate');
title(ax(end),'SW-triggered burst rate');

[nu,edges] = psth(SW_triggered_tsingle_juxta,binwidth,window.*[-1,1]);
b = bar(ax(end-1),edges,nu,0.75,'histc');
set(b,'FaceColor',[.8,0,.3],'LineWidth',1);
axis(ax(end-1),[edges([1,end]),0,1.1*max(nu)]);
% xlabel(ax(end-1),'Time since SW (s)');
ylabel(ax(end-1),'Spike rate');
title(ax(end-1),'SW-triggered single spike rate');

[nu,edges] = psth(SW_triggered_tspike_extra,binwidth,window.*[-1,1]);
b = bar(ax(end-2),edges,nu,0.75,'histc');
set(b,'FaceColor',[0,.3,.8],'LineWidth',1);
axis(ax(end-2),[edges([1,end]),0,1.1*max(nu)]);
xlabel(ax(end-2),'Time since SW (s)');
ylabel(ax(end-2),'Spike rate');
title(ax(end-2),'SW-triggered MUA spike rate');

% [nu,edges] = psth(tburst_juxta_triggered_SW,binwidth,window.*[-1,1]);
% b = bar(ax(end-2),edges,nu,0.75,'histc');
% set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
% axis(ax(end-2),[edges([1,end]),0,1.1*max(nu)]);
% % xlabel(ax(end-2),'Time since burst (s)');
% ylabel(ax(end-2),'SW rate');
% title(ax(end-2),'Burst-triggered SW rate');
% 
% [nu,edges] = psth(tsingle_juxta_triggered_SW,binwidth,window.*[-1,1]);
% b = bar(ax(end-3),edges,nu,0.75,'histc');
% set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
% axis(ax(end-3),[edges([1,end]),0,1.1*max(nu)]);
% % xlabel(ax(end-3),'Time since spike (s)');
% xlabel(ax(end-3),'Time since event (s)');
% ylabel(ax(end-3),'SW rate');
% title(ax(end-3),'Spike-triggered SW rate');

sz = [8,4.5];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
print('-dpdf','sharp-waves_CA3_exp.pdf');
