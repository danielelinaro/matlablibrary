function [nu,edges,count] = computeCorrelogram(events_A, events_B, window, binwidth)
% [nu,edges] = computeCorrelogram(events_A, events_B, window, binwidth)

if isempty(events_B)
    ev = ETE(events_A, events_A, window);
    auto = 1;
else
    ev = ETE(events_A, events_B, window);
    auto = 0;
end

[nu,edges,count] = psth(ev, binwidth, window.*[-1,1]);
if auto
    nu(nu == max(nu)) = 0;
end

