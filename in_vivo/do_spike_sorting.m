function [classes,features] = do_spike_sorting(dt,V)

%%% Parameters
% Detection parameters
par.sr = 1/dt;                     % sampling rate (in Hz).
par.w_pre = 20;                    % number of pre-event data points stored (default 20)
par.w_post = 40;                   % number of post-event data points stored (default 44))
par.ref = floor(3 * par.sr/1000);  % detector dead time (in ms, converted to datapoints)
par.stdmin = 8;                    % minimum threshold for detection
par.stdmax = 20;                   % maximum threshold for detection
par.detect_fmin = 300;             % high pass filter for detection
par.detect_fmax = 1000;            % low pass filter for detection (default 1000)
par.sort_fmin = 300;               % high pass filter for sorting 
par.sort_fmax = 1000;              % low pass filter for sorting (default 3000)
par.detection = 'neg';             % type of threshold: pos, neg, both
par.segments = 1;                  % number of segments the data is subdivided in

% Interpolation parameters
par.int_factor = 2;                % interpolation factor
par.interpolation = 'y';           % interpolation with cubic splines (y or n)

% Features parameters
par.inputs = 10;                   % number of inputs to the clustering
par.scales = 4;                    % number of scales for the wavelet decomposition
par.features = 'wav';              % type of feature (wav or pca)
if strcmp(par.features,'pca')
    par.inputs = 3;
else
    par.inputs = 10;
end

% Template matching parameters
par.match = 'n';                   % for template matching (y or n)
par.max_spk = 50000;               % max. # of spikes before starting templ. match.
par.permut = 'n';                  % for selection of random 'par.max_spk' spikes before starting templ. match. (y or n)

% SPC parameters
par.mintemp = 0.00;                % minimum temperature for SPC
par.maxtemp = 0.201;               % maximum temperature for SPC
par.tempstep = 0.01;               % temperature steps
par.SWCycles = 100;                % SPC iterations for each temperature
par.KNearNeighb=11;                % number of nearest neighbors for SPC
par.num_temp = floor((par.maxtemp ...
    -par.mintemp)/par.tempstep);   % total number of temperatures 
par.min_clus = 20;                 % minimun size of a cluster
par.min_clus_abs = 20;
par.min_clus_rel = 0.005;
par.max_clus = 33;                 % maximum number of clusters allowed
par.randomseed = 0;                % if 0, random seed is taken as the clock value (default)
par.temp_plot = 'log';             % temperature plot scale (log or lin)
par.fname = 'data';                % filename for interaction with SPC
par.fname_in = 'data_in';

[classes,features] = sortSpikes(V,par);

end


