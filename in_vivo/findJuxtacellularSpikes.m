function [tsp,Vsp] = findJuxtacellularSpikes(t,V,burst_threshold,...
    threshold_factor,threshold,min_threshold_factor,num_spikes_to_reach_min_threshold)
% findJuxtacellularSpikes    Extract juxtacellular spikes from a voltage trace.
% 
% [tsp,Vsp] = findJuxtacellularSpikes(t,V,burst_threshold,threshold_factor)
% 
% Arguments:
%                     t - time: a 1xN array, where N is the number of
%                         samples.
%                     V - voltage: a MxN matrix, where M is the number of
%                         trials.
%       burst_threshold - the frequency threshold that will be used to
%                         identify bursts. This is used to determine the
%                         window after a spike in which the algorithm looks
%                         for missed spikes by gradually reducing the
%                         threshold for spike detection. If not specified or
%                         empty, a default value of 20 ms is used.
%      threshold_factor - an integer specifying the level of the initial threshold
%                         relative to the noise. Default: 20.
%             threshold - a hard threshold for detecting spikes. Default: NaN
%                         and threshold_factor is used. If specified,
%                         threshold_factor is ignored.
%  min_threshold_factor - an integer specifying the level of the minimal
%                         threshold relative to the noise. Default: 6.
%  num_spikes_to_reach_min_threshold - the number of spikes in a burst
%                         after which the moving threshold reaches the
%                         minimum value. Default: 4.
% 
% Returns:
%                tsp - an Mx1 cell array containing the spike times.
%                Vsp - an Mx1 cell array containing the corresponding
%                      values of voltage.
% 
% See also: locateBursts, computeThreshold, findExtracellularSpikes
% 

% Author: Daniele Linaro, May 2015

if ~ exist('threshold','var')
    threshold = nan;
end

if ~ exist('threshold_factor','var')
    threshold_factor = 20;
end

dt = diff(t(1:2));
% minimal distance between peaks
mpd = ceil(5e-3/dt);
n = size(V,1);
tsp = cell(n,1);
Vsp = cell(n,1);
% window after a spike in which the algorithm looks for a possibly missed
% spike (in a burst)
if ~ exist('burst_threshold','var') || isempty(burst_threshold)
    orig_window = ceil(20e-3/dt);
else
    orig_window = ceil(1./(burst_threshold*dt));
end

if isscalar(orig_window)
    orig_window = orig_window + zeros(1,20);
else
    orig_window = [orig_window(:)', orig_window(end)+zeros(1,20-length(orig_window))];
end

if ~ exist('min_threshold_factor','var') || isempty(min_threshold_factor)
    min_threshold_factor = 6;
end

if ~ exist('no_spikes_to_reach_min_th','var') || isempty(num_spikes_to_reach_min_threshold)
    num_spikes_to_reach_min_threshold = 4;
end

for i=1:n
    if isnan(threshold)
        s = computeThreshold(V(i,:));
        orig_th = threshold_factor*s;
        % amount by which the moving threshold is reduced every time a spike is
        % encountered (it hits the minimum value for the threshold after
        % no_spikes_to_reach_min_th spikes in a burst
        dth = (threshold_factor-min_threshold_factor)/(num_spikes_to_reach_min_threshold-1)*s;
        % minimal threshold for the detection of juxtacellular spikes
        min_th = min_threshold_factor*s;
    else
        orig_th = threshold;
        % minimal threshold for the detection of juxtacellular spikes
        %min_th = threshold/5;
        min_th = min_threshold_factor*computeThreshold(V(i,:));
        % amount by which the moving threshold is reduced every time a spike is
        % encountered
        dth = (threshold-min_th)/(num_spikes_to_reach_min_threshold-1);
    end
%     plot(xlim,min_th+[0,0],'k');
%     for j=1:no_spikes_to_reach_min_th
%         plot(xlim,orig_th-(j-1)*dth+[0,0],'r--');
%     end
    % initial value of the moving threshold
    th = orig_th;
    % find a second approximation of the spike times
    if max(V(i,:)) < th
        % fprintf(1, '\n');
        continue;
    end
    [~,locs] = findpeaks(V(i,:),'minpeakheight',th,'minpeakdistance',mpd);
    th = orig_th-dth;
    j = 1;
    w = 1;
    % the first spike coincides with the first peak
    tsp{i} = t(locs(1));
    while 1
        % fprintf(1, '.');
        % the index of the last spike that was added to the list
        pos = round(tsp{i}(end)/dt);
        if j < length(locs)
            window = min(orig_window(w),round((t(locs(j+1)) - tsp{i}(end))/dt)+mpd);
        else
            window = orig_window(w);
        end
        window = min(window,length(V(i,:))-pos);
        % find the maximum in a window after the current spike
        [m,k] = max(V(i,pos+mpd:pos+window));
        if j < length(locs) && t(pos+mpd+k-1) == t(locs(j+1))
            % the max in the window after the spike coincides with
            % the following spike
            tsp{i} = [tsp{i}, t(locs(j+1))];
            j = j+1;
            th = max(th - dth, min_th);
            w = w+1;
        elseif j < length(locs) && m < th
            % the max in the window after the spike is below the
            % current threshold
            tsp{i} = [tsp{i}, t(locs(j+1))];
            j = j+1;
            th = orig_th-dth;
            w = 1;
        elseif j < length(locs) && k == window-mpd+1
            % the max in the window after the spike corresponds
            % with the last sample of the window
            tsp{i} = [tsp{i}, t(locs(j+1))];
            j = j+1;
            th = orig_th-dth;
            w = w+1;
        elseif m > th
            % the max in the window after the spike is above the
            % current threshold: add it to the list
            tsp{i} = [tsp{i}, t(pos+mpd+k-1)];
            th = max(th-dth, min_th);
            w = w+1;
        else
            break;
        end
    end
    % fprintf(1, '\n');
    Vsp{i} = V(i,round(tsp{i}/dt)+1);
end
