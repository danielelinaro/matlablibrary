function ax = betterSubplot(varargin)
% 
% ax = betterSubplot(rows,columns,...)
% ax = betterSubplot(rows,columns,'Offset',[xa,ya;xb,yb])
% ax = betterSubplot(rows,columns,'Delta',[dx,dy])
% 
p = inputParser;
p.addRequired('rows', @(x) isnumeric(x) && isscalar(x) && x>0);
p.addRequired('columns', @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('offset', [0.1,0.1 ; 0.05,0.05], ...
    @(x) isnumeric(x) && all(size(x) == [2,2]) && all(all(x>=0)));
p.addParameter('delta', [0.1,0.1], ...
    @(x) isnumeric(x) && numel(x)==2 && all(x>=0));
p.parse(varargin{:});

rows = p.Results.rows;
columns = p.Results.columns;
offset = p.Results.offset;
delta = p.Results.delta;

width = [(1-(columns-1)*delta(1)-sum(offset(:,1)))/columns, ...
    (1-(rows-1)*delta(2)-sum(offset(:,2)))/rows];

try
    ax = arrayfun(@(i) arrayfun(@(j) axes('Position', ...
        [offset(1,1)+j*(width(1)+delta(1)),offset(1,2)+i*(width(2)+delta(2)),width], ...
        'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',10), ...
        0:columns-1, 'UniformOutput', 0), 0:rows-1, 'UniformOutput', 0);
catch
    offset = zeros(2);
    delta = [0,0];
    width = [(1-(columns-1)*delta(1)-sum(offset(:,1)))/columns, ...
    (1-(rows-1)*delta(2)-sum(offset(:,2)))/rows];
    ax = arrayfun(@(i) arrayfun(@(j) axes('Position', ...
        [offset(1,1)+j*(width(1)+delta(1)),offset(1,2)+i*(width(2)+delta(2)),width], ...
        'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',10), ...
        0:columns-1, 'UniformOutput', 0), 0:rows-1, 'UniformOutput', 0);
end

ax = flipud(flattenCellArray(ax));

if iscell(ax(1))
    tmp = repmat(ax{1},size(ax));
    for i=1:size(ax,1)
        for j=1:size(ax,2)
            tmp(i,j) = ax{i,j};
        end
    end
    ax = tmp;
end
