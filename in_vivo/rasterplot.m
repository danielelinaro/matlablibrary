function hndl = rasterplot(spktimes,varargin)
% RASTERPLOT plots a raster plot of spike times.
% 
% hndl = rasterplot(spktimes,...)
% 
% Arguments:
%   spktimes - the times at which spikes were fired. It must be an array
%   of cells, where each cell contains the spike times of a single neuron
%   or trial.
% 
% Additional (optional) arguments will be passed unchanged to the function 
% plot.
% 

% 
% Author: Daniele Linaro - September 2009.
% 

if strcmpi(varargin{1},'offset')
    offset = varargin{2};
    varargin = varargin(3:end);
else
    offset = 0;
end

hold on;
for ii=1:length(spktimes)
    for jj=1:length(spktimes{ii})
        plot([spktimes{ii}(jj),spktimes{ii}(jj)],offset+[ii-0.4,ii+0.4],varargin{:});
    end
end
hndl = 0;