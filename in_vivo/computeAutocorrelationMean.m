function ac_mean = computeAutocorrelationMean(spikes,window,binwidth)
% ac_mean = computeAutocorrelationMean(spikes,window,binwidth)

DEBUG = 0;

if ~ exist('binwidth','var') || isempty(binwidth)
    binwidth = 1e-3;
end
if ~ exist('window','var') || isempty(window)
    window = [0,40e-3];
end

[nu,edges,count] = computeCorrelogram(spikes, [], window, binwidth);
count = sum(count);
count(count == max(count)) = 0;
prob = count/sum(count);
ac_mean = (edges(1:end-1)+binwidth/2)*prob(1:end-1)';

if DEBUG
    clf;
    hold on;
    plot((edges(1:end-1)+binwidth/2)*1e3,nu(1:end-1),'ko-',...
        'MarkerFaceColor','w','LineWidth',1);
    plot(ac_mean*1e3+[0,0],ylim,'r','LineWidth',2);
    xlabel('Time (ms)');
    ylabel('Firing rate (spikes/s)');
    set(gca,'TickDir','Out','LineWidth',0.8,'XTick',(0:0.01:window(2))*1e3,...
        'YTick',0:4:20);
    pause;
end
