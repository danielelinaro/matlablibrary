function [event_rate_per_bin,bin_left_edge,bin_centers] = psth_mod(spks, binwidth, interval)
% PSTH   Peri-stimulus time histogram.
% 
% [nu,edges,count] = PSTH(spks, binwidth)
% [nu,edges,count] = PSTH(spks, binwidth, interval)
% 
% Computes the peri-stimulus time histogram of spike times.
% 
% Arguments:
%      spks - the spike times, as an array of cells.
%  binwidth - the width of the histogram bin (in seconds).
%  interval - a 2-elements vector containing the onset and end times of the
%             stimulus. If this argument is not given, the first and the
%             last spike times are used.
%
% Returns:
%        nu - the firing rate.
%     edges - the edges used for the binning.
%     count - the actual spike count for each bin.
% 

% Author: Daniele Linaro - September 2009.


if ~ exist('interval','var')
    interval = [min(cellfun(@(x) x(1), spks)), max(cellfun(@(x) x(end), spks))];
end

edges = interval(1) : binwidth : interval(2);
ntrials = length(spks);
count_raw = zeros(ntrials,length(edges));
for k=1:ntrials
    try
        count_raw(k,:) = histc(spks{k},edges);
    catch, end
end
count = count_raw(:,1:end-1);  % Last bin has zero bin width b/c histc() is peculiar, so drop it
event_rate_per_bin = sum(count,1) / (ntrials*binwidth);  % events/sec, averaged across trials
  % note that event_rate_per_bin is one shorter than edges, b/c there's one
  % less bin than edges.
bin_centers=(edges(1:end-1)+edges(2:end))/2;
bin_left_edge = edges(1:end-1);