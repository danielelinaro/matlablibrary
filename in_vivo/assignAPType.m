function AP_type = assignAPType(tspike,tburst_full)
% AP_type = assignAPType(tspike,tburst_full)

n = length(tspike);
AP_type = cellfun(@(x) zeros(size(x)), tspike, 'UniformOutput', 0);

for i=1:n
    for j=1:length(tburst_full{i})
        start = find(tspike{i} == tburst_full{i}{j}(1), 1, 'first');
        types = 1:length(tburst_full{i}{j});
        AP_type{i}(start:start+length(types)-1) = types;
    end
end
