function [t,V_return,SW,MUA] = extractSharpWavesAndSpikes(filename,SWband,MUAband,do_plot)
% [t,V,SW,MUA] = extractSharpWavesAndSpikes(filename,SWband,MUAband)

narginchk(1,4);

warning('off','signal:findpeaks:largeMinPeakHeight')

if ~ exist('SWband','var') || isempty(SWband)
    SWband = [3,20];
end

if ~ exist('MUAband','var') || isempty(MUAband)
    MUAband = [300,1000];
end

if ~ exist('do_plot','var')
    do_plot = 0;
end

data = IBWread(filename);
dt = data.dx * 1e-3;
t = (0:length(data.y)-1) * dt;
% filter properties
N = 2;
Rp = 0.1;
Rs = 40;

%%% filter in the mua band
wndw = [1e-3,2e-3];
[b,a] = ellip(N,Rp,Rs,MUAband*2*dt);
V = filtfilt(b,a,data.y);

%%% extract the juxtacellular spikes
tjuxta = findJuxtacellularSpikes(t,V);

%%% indicate whether this sweep has juxtacellular spikes or not
if isempty(tjuxta)
    MUA.has_juxta = 0;
else
    MUA.has_juxta = 1;
end

%%% extract the other extracellular spikes
textra = findExtracellularSpikes(t,V,[],tjuxta,[],wndw);

% Do spike sorting on all spikes, extra- and juxta-
[MUA.unsorted_spike_times,idx] = sort([textra,tjuxta]);
idx_extra = find(idx<=length(textra));
idx_juxta = find(idx>length(textra));
nspks = length(MUA.unsorted_spike_times);
peaks = round(MUA.unsorted_spike_times/dt)+1;
before = round(wndw(1)/dt);
after = round(wndw(2)/dt);
MUA.T = -wndw(1) : dt : wndw(2);
MUA.V = zeros(nspks,length(MUA.T));
for i=1:nspks
    MUA.V(i,:) = V(peaks(i)-before : peaks(i)+after);
end
if ~ isempty(MUA.unsorted_spike_times)
    % sort only the extracellular spikes, i.e. those that were
    % detected by a downward threshold crossing
    if length(idx_extra) > 1
        try
            MUA.classes = do_spike_sorting(dt,MUA.V(idx_extra,:));
        catch
            fprintf(1, 'Not performing spike sorting.\n');
            MUA.classes = {1:length(idx_extra)};
        end
    elseif ~isempty(idx_extra)
        MUA.classes = {1};
    else
        MUA.classes = {};
    end
    for i=1:length(MUA.classes)
        MUA.classes{i} = idx_extra(MUA.classes{i})';
    end
    if MUA.has_juxta
        MUA.classes = {idx_juxta', MUA.classes{:}}; %#ok<CCAT>
    end
end

% break out just the non-juxta classes
if MUA.has_juxta ,
    if isempty(MUA.classes) ,
        MUA.extra_classes = cell(1,0);
    elseif isscalar(MUA.classes) ,
        MUA.extra_classes = cell(1,0);
    else
        MUA.extra_classes = MUA.classes(2:end);
    end
else
    MUA.extra_classes = MUA.classes;
end

% Sort the spike times by class
spike_times_per_class = cell(length(MUA.classes),1);
for i=1:length(MUA.classes) ,
    indices_of_spikes_in_this_class = sort(MUA.classes{i});
    spike_times_per_class{i} = MUA.unsorted_spike_times(indices_of_spikes_in_this_class);
end

% Sort out just the juxta spikes
if MUA.has_juxta ,
    MUA.juxta_spike_times = spike_times_per_class{1};
else
    MUA.juxta_spike_times = zeros(1,0);
end

% Sort out the non-juxta spikes
if MUA.has_juxta ,
    if length(spike_times_per_class)==1,
        MUA.extra_spike_times = zeros(1,0);
        MUA.extra_spike_times_per_class = cell(0,1);
    else
        MUA.extra_spike_times = flattenCellArray(spike_times_per_class(2:end),'full')';
        MUA.extra_spike_times_per_class = spike_times_per_class(2:end);
    end
else
    MUA.extra_spike_times = flattenCellArray(spike_times_per_class,'full')';
    MUA.extra_spike_times_per_class = spike_times_per_class;
end

% For each class of spikes determined by spike sorting, look for bursts of
% spikes.
burst_threshold = 70;   % [Hz]
%freq_isi = cellfun(@(x) 1./diff(MUA.unsorted_spike_times(sort(x))), MUA.classes, 'UniformOutput', 0);
%spike_times_per_class = cellfun(@(is_in_class) MUA.unsorted_spike_times(sort(is_in_class)), MUA.classes, 'UniformOutput', 0);
burst_times_per_class = cell(length(MUA.classes),1);  % one event train per spike class
nonburst_spike_times_per_class = cell(length(MUA.classes),1);  % one event train per spike class
for i=1:length(MUA.classes) ,
    indices_in_this_class =  MUA.classes{i};    
    spike_times_this_class = MUA.unsorted_spike_times(sort(indices_in_this_class));
    [burst_times_this,nonburst_spike_times_this] = time_of_first_spike_in_each_burst(spike_times_this_class,burst_threshold);
    burst_times_per_class{i} = burst_times_this;
    nonburst_spike_times_per_class{i} = nonburst_spike_times_this;
    
%     % compute the old way, to check
%     if isempty(spike_times_this_class) ,
%         old_burst_times_this = zeros(1,0);
%         old_nonburst_spike_times_this = zeros(1,0);
%     elseif isscalar(spike_times_this_class) ,
%         old_burst_times_this = zeros(1,0);
%         old_nonburst_spike_times_this = spike_times_this_class;
%     else        
%         % At least two spikes
%         isis_this_class = diff(spike_times_this_class);
%         freq_isi_this_class = 1./isis_this_class;  % has at least one element
%         inst_freq_of_previous_isi = [0 freq_isi_this_class] ;  % One element per spike, assume ISI before 1st spike has zero inst freq
%         inst_freq_of_next_isi = [freq_isi_this_class 0] ;  % One element per spike, assume ISI after last spike has zero inst freq      
%         start_indices = find(inst_freq_of_next_isi > burst_threshold & inst_freq_of_previous_isi < burst_threshold);  % these are spike indices
%         stop_indices = find(inst_freq_of_previous_isi > burst_threshold & inst_freq_of_next_isi < burst_threshold);  % these are spike indices
%         if isempty(start_indices) ,
%             % If no bursts, all spikes are isolated
%             old_burst_times_this = zeros(1,0);
%             old_nonburst_spike_times_this = spike_times_this_class;   
%         else
%             spike_times_this_class = MUA.unsorted_spike_times(sort(MUA.classes{i}));
%             old_burst_times_this = spike_times_this_class(start_indices);
%             if length(stop_indices) ~= length(start_indices) ,
%                 error('Internal error: Different numbers of burst starts and stops.');
%             end
%             idx = flattenCellArray(arrayfun(@(x,y) x:y, start_indices, stop_indices, 'UniformOutput', 0), 'full');
%             old_nonburst_spike_times_this = spike_times_this_class(setdiff(1:length(spike_times_this_class),idx));
%         end        
%     end
%     
%     % check
%     if length(old_burst_times_this)==length(burst_times_this) && all(old_burst_times_this==burst_times_this) ,            
%         % do nothing
%     else
%         warning('The two vars that capture per-class burst times do not agree.');
%     end    
%     
%     if length(old_nonburst_spike_times_this)==length(nonburst_spike_times_this) && all(old_nonburst_spike_times_this==nonburst_spike_times_this) ,            
%         % do nothing
%     else
%         warning('The two vars that capture per-class single-spike times do not agree.');
%     end    
    
end

% break out just the non-juxta classes
if MUA.has_juxta ,
    if isempty(MUA.classes) ,
        nonburst_spike_times_per_extra_class = cell(1,0);
    elseif isscalar(MUA.classes) ,
        nonburst_spike_times_per_extra_class = cell(1,0);
    else
        nonburst_spike_times_per_extra_class = nonburst_spike_times_per_class(2:end);
    end
else
    nonburst_spike_times_per_extra_class = nonburst_spike_times_per_class;
end

% Identify bursts in the juxta spikes, and the extracellular spikes
[MUA.juxta_burst_times,MUA.juxta_nonburst_spike_times]=time_of_first_spike_in_each_burst(MUA.juxta_spike_times,burst_threshold);
%[MUA.extra_burst_times,MUA.extra_nonburst_spike_times]=time_of_first_spike_in_each_burst(MUA.extra_spike_times,burst_threshold);
% Shouldn't burstify non-sorted extracellular spikes

% Compare the juxta burst times
if MUA.has_juxta ,
    mua_burst_times=burst_times_per_class{1};
    if isempty(mua_burst_times) ,
        mua_burst_times = zeros(1,0);
    end       
    if length(mua_burst_times)==length(MUA.juxta_burst_times) && all(mua_burst_times==MUA.juxta_burst_times) ,
        % do nothing
    else
        error('The two fields that capture juxtacellular burst times do not agree.');
    end
else
    if isempty(MUA.juxta_burst_times) ,
        % do nothing
    else
        error('The two fields that capture juxtacellular burst times do not agree.');
    end
end



%
% Now extract the sharp wave events
%

%%% filter in the sharp wave band
[b,a] = ellip(N,Rp,Rs,SWband*2*dt);
V_sw_raw = filtfilt(b,a,data.y);

% Filter with a gaussian for smoothing
sigma_t = 0.030;  % s
kernel_radius=4*round(sigma_t/dt);
t_radius=dt*(1:kernel_radius)';
t_kernel=[-flipud(t_radius) ; ...
          0 ; ...
          t_radius];
kernel_proto=exp(-(t_kernel/sigma_t).^2);
kernel=kernel_proto/sum(kernel_proto);
V_sw=conv1(V_sw_raw,kernel,'same');
%V_sw=V_sw_raw;

% figure;
% plot(t,V_unsmoothed,'r', ...
%      t,V_sw,'b');
% legend('original','smoothed');
% xlabel('Time (s)');
% ylabel('Sharp-wave signal (V)');

% Extract times of sharp-wave events
swth = 3*std(V_sw); % changed from 5 on 2/6/2015
[~,crossings] = findpeaks(V_sw,'minpeakheight',swth,'minpeakdistance',round(0.1/dt));
% dV = (V_sw(3:end)-V_sw(1:end-2))/(2*dt);
% [~,locs] = findpeaks(dV,'minpeakheight',15,'minpeakdistance',round(0.1/dt));
% locs = locs+1;
SW.sharp_wave_times = t(crossings);

% Extract peri-event voltage traces
nsws = length(SW.sharp_wave_times);
wndw = [1,1];
SW.T = -wndw(1) : dt : wndw(2);
SW.V = nan(nsws,length(SW.T));
before = round(wndw(1)/dt);
after = round(wndw(2)/dt);
for i=1:nsws
    idx = crossings(i)-before : crossings(i)+after;
    jdx = find(idx > 0 & idx < length(V_sw));
    SW.V(i,jdx) = V_sw(idx(jdx));
end

%%% SW-triggered firing rate
wndw = [-3,3];  % seconds
%%%% sharp-wave-triggered spike and burst times
nSharpWaves = length(SW.sharp_wave_times);
MUA.swt_unsorted_spike_times = cell(nSharpWaves,1);
MUA.swt_unsorted_nonburst_spike_times = cell(nSharpWaves,1);
MUA.swt_nonburst_extra_spike_times = cell(nSharpWaves,1);
swt_spike_times_per_class_per_sw = cell(nSharpWaves,1);
swt_burst_times_per_class_per_sw = cell(nSharpWaves,1);
swt_nonburst_spike_times_per_class_per_sw = cell(nSharpWaves,1);
SW.closest_preceding_burst = nan(nSharpWaves,length(MUA.classes));
SW.closest_following_burst = nan(nSharpWaves,length(MUA.classes));
MUA.swt_juxta_spike_times = cell(nSharpWaves,1);
MUA.swt_extra_spike_times = cell(nSharpWaves,1);
MUA.swt_juxta_burst_times = cell(nSharpWaves,1);
%MUA.swt_extra_burst_times = cell(nSharpWaves,1);
MUA.swt_juxta_nonburst_spike_times = cell(nSharpWaves,1);
%MUA.swt_extra_nonburst_spike_times = cell(nSharpWaves,1);
for i=1:nSharpWaves ,
    % unsorted spike times
    MUA.swt_unsorted_spike_times{i} = ...
        MUA.unsorted_spike_times(MUA.unsorted_spike_times > SW.sharp_wave_times(i)+wndw(1) & ...
                                 MUA.unsorted_spike_times < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);
    
    % juxta spike times
    MUA.swt_juxta_spike_times{i} = ...
        MUA.juxta_spike_times(MUA.juxta_spike_times > SW.sharp_wave_times(i)+wndw(1) & ...
                              MUA.juxta_spike_times < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);

    % extra spike times
    MUA.swt_extra_spike_times{i} = ...
        MUA.extra_spike_times(MUA.extra_spike_times > SW.sharp_wave_times(i)+wndw(1) & ...
                              MUA.extra_spike_times < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);
    
    % juxta burst times
    MUA.swt_juxta_burst_times{i} = ...
        MUA.juxta_burst_times(MUA.juxta_burst_times > SW.sharp_wave_times(i)+wndw(1) & ...
                              MUA.juxta_burst_times < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);

%     % extra burst times
%     MUA.swt_extra_burst_times{i} = ...
%         MUA.extra_burst_times(MUA.extra_burst_times > SW.sharp_wave_times(i)+wndw(1) & ...
%                               MUA.extra_burst_times < SW.sharp_wave_times(i)+wndw(2)) ...
%         - SW.sharp_wave_times(i);
    
    % juxta nonburst spike times
    MUA.swt_juxta_nonburst_spike_times{i} = ...
        MUA.juxta_nonburst_spike_times(MUA.juxta_nonburst_spike_times > SW.sharp_wave_times(i)+wndw(1) & ...
                                     MUA.juxta_nonburst_spike_times < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);

%     % extra nonburst spike times
%     MUA.swt_extra_nonburst_spike_times{i} = ...
%         MUA.extra_nonburst_spike_times(MUA.extra_nonburst_spike_times > SW.sharp_wave_times(i)+wndw(1) & ...
%                                      MUA.extra_nonburst_spike_times < SW.sharp_wave_times(i)+wndw(2)) ...
%         - SW.sharp_wave_times(i);
    
    % unsorted single spike times
    nonburst_spikes = sort(flattenCellArray(nonburst_spike_times_per_class,'full'))';  % spikes are sorted, burstified, then non-burst spikes are lumped together
    MUA.swt_unsorted_nonburst_spike_times{i} = ...
        nonburst_spikes(nonburst_spikes > SW.sharp_wave_times(i)+wndw(1) & ...
                        nonburst_spikes < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);

    % unsorted single spike times
    nonburst_extra_spikes = sort(flattenCellArray(nonburst_spike_times_per_extra_class,'full'))';  % spikes are sorted, burstified, then non-burst spikes are lumped together
    MUA.swt_nonburst_extra_spike_times{i} = ...
        nonburst_extra_spikes(nonburst_extra_spikes > SW.sharp_wave_times(i)+wndw(1) & ...
                              nonburst_extra_spikes < SW.sharp_wave_times(i)+wndw(2)) ...
        - SW.sharp_wave_times(i);
    
    for j=1:length(MUA.classes)
        % sorted spike times
        swt_spike_times_per_class_per_sw{i}{j} = ...
            spike_times_per_class{j}(spike_times_per_class{j} > SW.sharp_wave_times(i)+wndw(1) & ...
                                         spike_times_per_class{j} < SW.sharp_wave_times(i)+wndw(2)) ...
            - SW.sharp_wave_times(i);        
        % sorted burst times
        swt_burst_times_per_class_per_sw{i}{j} = ...
            burst_times_per_class{j}(burst_times_per_class{j} > SW.sharp_wave_times(i)+wndw(1) & ...
                               burst_times_per_class{j} < SW.sharp_wave_times(i)+wndw(2)) ...
            - SW.sharp_wave_times(i);
        try
            m = min(SW.sharp_wave_times(i) - burst_times_per_class{j}(burst_times_per_class{j} ...
                < SW.sharp_wave_times(i)));
            SW.closest_preceding_burst(i,j) = -m;
        catch
        end
        try
            m = min(burst_times_per_class{j}(burst_times_per_class{j} > SW.sharp_wave_times(i)) ...
                - SW.sharp_wave_times(i));
            SW.closest_following_burst(i,j) = m;
        catch
        end
        % sorted non-burst spike times
        swt_nonburst_spike_times_per_class_per_sw{i}{j} = ...
            nonburst_spike_times_per_class{j}(nonburst_spike_times_per_class{j} > SW.sharp_wave_times(i)+wndw(1) & ...
                                      nonburst_spike_times_per_class{j} < SW.sharp_wave_times(i)+wndw(2)) ...
            - SW.sharp_wave_times(i);        
        

    end
end
if ~ isempty(SW.sharp_wave_times)
    [nu,edges] = psth(MUA.swt_unsorted_spike_times,0.2,wndw);
end

if MUA.has_juxta , 
    mua_swt_juxta_burst_times = cell(nSharpWaves,1);
    for iSharpWave=1:nSharpWaves ,
        mua_swt_juxta_burst_times{iSharpWave} = swt_burst_times_per_class_per_sw{iSharpWave}{1};
    end
    
    if length(mua_swt_juxta_burst_times)==length(MUA.swt_juxta_burst_times) ,
        for iSharpWaves=1:nSharpWaves ,
            mua_swt_juxta_burst_times_this = mua_swt_juxta_burst_times{iSharpWave};
            if isempty(mua_swt_juxta_burst_times_this) ,
                mua_swt_juxta_burst_times_this = zeros(1,0);
            end
            check=MUA.swt_juxta_burst_times{iSharpWave};
            if isempty(check) ,
                check = zeros(1,0);
            end
            if length(mua_swt_juxta_burst_times_this)==length(check) && all(mua_swt_juxta_burst_times_this==check) ,
                % do nothing
            else
                error('The two fields that capture SW-triggered juxtacellular burst times do not agree.');
            end
        end
    else
        error('The two fields that capture SW-triggered juxtacellular burst times do not agree.');
    end
end    

if do_plot
    %[b,a] = ellip(N,Rp,Rs,MUAband*2*dt);
    %V = filtfilt(b,a,data.y);
    ax = zeros(4,1);
    figure;
    ax(1) = axes('Position',[0.1,0.5,0.55,0.45],'NextPlot','Add');
    plot(t,V,'k');
    plot(MUA.unsorted_spike_times,V(round(MUA.unsorted_spike_times/dt)),'go','MarkerFaceColor',[.6,.6,.6],'MarkerSize',4);
    plot(t([1,end]),-muath+[0,0],'--','Color',[.4,.4,.4]);
    if MUA.has_juxta
        plot(t([1,end]),jcth+[0,0],'--','Color',[.4,.4,.4]);
    end
    axis([t([1,end]),1.5*min(V),1.5*max(V)]);
    set(gca,'TickDir','Out','XTick',t(1):5:t(end));
    ylabel('Voltage (mV)');
    
    if ~ isempty(MUA.unsorted_spike_times)
        cmap = cool(length(MUA.classes));
        ax(3) = axes('Position',[0.7,0.5,0.25,0.45],'NextPlot','Add');
        for i=1:length(MUA.classes)
%             if length(MUA.classes{i}) > 0
                plot(MUA.T,MUA.V(MUA.classes{i},:),'Color',cmap(i,:));
%             end
        end
        axis tight;
        xl = xlim;
        yl = ylim;
        if MUA.has_juxta
            placeScaleBars(xl(2)-0.5e-3,yl(2)-0.5,0.5e-3,0.4,'0.5 ms','0.4 mV','k');
        else
            placeScaleBars(xl(2)-0.5e-3,yl(1)+0.1,0.5e-3,0.2,'0.5 ms','0.2 mV','k');
        end
        axis off;
    end

    %[b,a] = ellip(N,Rp,Rs,SWband*2*dt);
    %V = filtfilt(b,a,data.y);
%     dV = (V(3:end) - V(1:end-2)) / (2*dt);
    ax(2) = axes('Position',[0.1,0.1,0.55,0.3],'NextPlot','Add');
    plot(t,V_sw,'k');
%     plot(t(2:end-1),0.8*(dV-min(dV))/(max(dV)-min(dV)),'b');
    plot(t([1,end]),swth+[0,0],'--','Color',[.4,.4,.4]);
    for i=1:length(SW.sharp_wave_times)
        plot(SW.sharp_wave_times(i)+[0,0],[-0.5,1],'r--');
    end
    axis([t([1,end]),-0.5,1]);
%     set(gca,'TickDir','Out','XTick',0:5:20,'YTick',-0.5:.5:1);
    xlabel('Time (s)');
    ylabel('Voltage (mV)');
    for i=1:nspks
        plot(MUA.unsorted_spike_times(i)+[0,0],[0.9,1],'k');
    end
    for i=1:length(burst_times_per_class)
        for j=1:length(burst_times_per_class{i})
            plot(burst_times_per_class{i}(j)+[0,0],[0.9,1],'Color',cmap(i,:));
        end
    end
    
    if ~ isempty(SW.sharp_wave_times)
        ax(4) = axes('Position',[0.75,0.1,0.2,0.3],'NextPlot','Add');
        axis([wndw,0,max(nu)+5]);
        set(gca,'TickDir','Out','XTick',wndw(1):wndw(2),'YTick',0:10:100);
        bar(edges,nu,'histc');
        xlabel('Time since SW (s)');
        ylabel('Firing rate (Hz)');
    end
    linkaxes(ax(1:2),'x');
    set(gcf,'Color','w');
end

V_return = data.y;

warning('on','signal:findpeaks:largeMinPeakHeight');

end

function classes = do_spike_sorting(dt,V)

%%% Parameters
% Detection parameters
par.sr = 1/dt;                     % sampling rate (in Hz).
par.w_pre = 20;                    % number of pre-event data points stored (default 20)
par.w_post = 40;                   % number of post-event data points stored (default 44))
par.ref = floor(3 * par.sr/1000);  % detector dead time (in ms, converted to datapoints)
par.stdmin = 8;                    % minimum threshold for detection
par.stdmax = 20;                   % maximum threshold for detection
par.detect_fmin = 300;             % high pass filter for detection
par.detect_fmax = 1000;            % low pass filter for detection (default 1000)
par.sort_fmin = 300;               % high pass filter for sorting 
par.sort_fmax = 1000;              % low pass filter for sorting (default 3000)
par.detection = 'neg';             % type of threshold: pos, neg, both
par.segments = 1;                  % number of segments the data is subdivided in

% Interpolation parameters
par.int_factor = 2;                % interpolation factor
par.interpolation = 'y';           % interpolation with cubic splines (y or n)

% Features parameters
par.inputs = 10;                   % number of inputs to the clustering
par.scales = 4;                    % number of scales for the wavelet decomposition
par.features = 'pca';              % type of feature (wav or pca)
if strcmp(par.features,'pca'), par.inputs=3; end

% Template matching parameters
par.match = 'y';                   % for template matching (y or n)
par.max_spk = 20000;               % max. # of spikes before starting templ. match.
par.permut = 'y';                  % for selection of random 'par.max_spk' spikes before starting templ. match. (y or n)

% SPC parameters
par.mintemp = 0.00;                % minimum temperature for SPC
par.maxtemp = 0.201;               % maximum temperature for SPC
par.tempstep = 0.01;               % temperature steps
par.SWCycles = 100;                % SPC iterations for each temperature
par.KNearNeighb=11;                % number of nearest neighbors for SPC
par.num_temp = floor((par.maxtemp ...
    -par.mintemp)/par.tempstep);   % total number of temperatures 
par.min_clus = 20;                 % minimun size of a cluster
par.max_clus = 33;                 % maximum number of clusters allowed
par.randomseed = 0;                % if 0, random seed is taken as the clock value (default)
par.temp_plot = 'log';             % temperature plot scale (log or lin)
par.fname = 'data';                % filename for interaction with SPC
par.fname_in = 'data_in';

classes = sortSpikes(V,par);

end


