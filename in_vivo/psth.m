function [nu,edges,count] = psth(spks, binwidth, interval)
% PSTH   Peri-stimulus time histogram.
% 
% [nu,edges,count] = PSTH(spks, binwidth)
% [nu,edges,count] = PSTH(spks, binwidth, interval)
% 
% Computes the peri-stimulus time histogram of spike times.
% 
% Arguments:
%      spks - the spike times, as an array of cells.
%  binwidth - the width of the histogram bin (in seconds).
%  interval - a 2-elements vector containing the onset and end times of the
%             stimulus. If this argument is not given, the first and the
%             last spike times are used.
%
% Returns:
%        nu - the firing rate.
%     edges - the edges used for the binning.
%     count - the actual spike count for each bin.
% 

% Author: Daniele Linaro - September 2009.


if ~ exist('interval','var')
    interval = [min(cellfun(@(x) x(1), spks)), max(cellfun(@(x) x(end), spks))];
end

edges = interval(1) : binwidth : interval(2);
ntrials = length(spks);
count = zeros(ntrials,length(edges));
for k=1:ntrials
    try
        count(k,:) = histc(spks{k},edges);
    catch, end
end
nu = sum(count,1) / (ntrials*binwidth);  % events/sec, averaged across trials
