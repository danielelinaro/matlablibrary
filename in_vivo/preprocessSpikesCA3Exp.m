function preprocessSpikesCA3Exp(varargin)
% preprocessSpikesCA3Exp    Extract spike and burst information from a
% juxtacellular recording in CA3.
% 
% preprocessSpikesCA3Exp(...)
% 
% Possible options are:
%                Sweeps - which sweeps to load (default all those in the
%                         current folder).
%          IgnoreSweeps - which sweeps to ignore (default none).
%            FilterBand - frequency band used for filtering the raw data
%                         (default [300,1000] Hz).
%        BurstThreshold - the threshold for identifying bursts (default 30 Hz).
%       ThresholdFactor - threshold factor (default 20).
%    MinNoSpikesInBurst - specify the minimum number of spikes that make up
%                         a burst (default 3).
%           SpikeWindow - extracted window before and after (extracellular)
%                         spike peak (default [1e-3,2e-3] sec).
%            SortSpikes - whether to sort extracellular spikes (default 0).
%            OutputFile - name of the file where the preprocessed data will
%                         be saved (default spikes_CA3_exp.mat).
%              DeadTime - the time after a juxtacellular spike during which
%                         extracellular spikes are discarded (default 4e-3
%                         sec).
%     

% Author: Daniele Linaro - July 2015

%%%% Default values
% which sweeps to load
sweeps_to_load = [];
% which sweeps to ignore
sweeps_to_ignore = [];
% band for the filter
filter_band = [300,1000];
% window to extract around the peak of the (extracellular) spikes
spike_window = [1e-3,2e-3];
% minimum rate of spikes in a burst
burst_threshold = 50; % [Hz]
% threshold factor
threshold_factor = 20;
% minimum number of spikes in a burst
min_no_spikes_in_burst = 3;
% whether or not spikes should be sorted
sort_spikes = 0;
% output file name
output_filename = 'spikes_CA3_exp.mat';
% whether to use previously saved file
load_file = [];
% deadtime for detection of extracellular spikes
deadtime = 4e-3;   % [s]

%%% Parse the arguments
for i=1:2:nargin
    switch lower(varargin{i})
        case 'sweeps',
            sweeps_to_load = varargin{i+1};
        case 'ignoresweeps',
            sweeps_to_ignore = varargin{i+1};
        case 'sortspikes',
            if ischar(varargin{i+1})
                if strcmpi(varargin{i+1},'yes') || varargin{i+1} == 'y'
                    sort_spikes = 1;
                else
                    sort_spikes = 0;
                end
            else
                sort_spikes = varargin{i+1};
            end
        case 'filterband',
            filter_band = varargin{i+1};
            if length(filter_band) ~= 2 || diff(filter_band) < 0
                error('Filter band must be a 2-element vector of increasing values.')
            end
        case 'spikewindow',
            spike_window = varargin{i+1};
            if length(spike_window) ~= 2 || any(spike_window) < 0
                error('Spike window must be a 2-element vector of non-negative values.')
            end
        case 'burstthreshold',
            burst_threshold = varargin{i+1};
            if burst_threshold < 30
                answer = input(sprintf(['The minimum recommended value of burst rate threshold is 30 Hz. ', ...
                    'Are you sure you want to use %g Hz (y/[n])? '], burst_threshold), 's');
                if isempty(answer) || answer == 'n'
                    return;
                end
            end
        case 'thresholdfactor',
            threshold_factor = varargin{i+1};
            if threshold_factor < 5
                error('Threshold factor must be greater than 4.');
            end
        case 'minnospikesinburst',
            min_no_spikes_in_burst = varargin{i+1};
            if min_no_spikes_in_burst <= 1
                error('The minimum number of spikes in a burst must be greater than 1.');
            end
            if min_no_spikes_in_burst < 3 || min_no_spikes_in_burst > 5
                answer = input(sprintf(['The recommended value of spikes in a burst is 3. ', ...
                    'Are you sure you want to use %d (y/[n])? '], min_no_spikes_in_burst), 's');
                if isempty(answer) || answer == 'n'
                    return;
                end
            end
        case 'outputfile',
            output_filename = varargin{i+1};
        case 'loadfile',
            load_file = varargin{i+1};
        case 'deadtime',
            deadtime = varargin{i+1};
        otherwise,
            error('Unknown option %s.', varargin{i});
    end
end

clear varargin

if isempty(load_file)
    % general filter properties
    N = 2;
    Rp = 0.1;
    Rs = 40;
    % load and filter the data
    if isempty(sweeps_to_ignore)
        [t,~,V] = loadIBWFiles(sweeps_to_load, filter_band, N, Rp, Rs);
    else
        [t,~,V] = loadIBWFiles('ignore', sweeps_to_ignore, filter_band, N, Rp, Rs);
    end

    %%% Extract juxtacellular spikes and bursts
    fprintf(1, 'Extracting juxtacellular spikes... ');
    [tspike_juxta,Vspike_juxta] = findJuxtacellularSpikes(t,V,burst_threshold,threshold_factor);
    fprintf(1, 'done.\n');
    fprintf(1, 'Locating the bursts... ');
    [tburst_juxta,Vburst_juxta,tburst_juxta_full,Vburst_juxta_full] = ...
        locateBursts(tspike_juxta,Vspike_juxta,burst_threshold,min_no_spikes_in_burst);
    fprintf(1, 'done.\n');
    [tsingle_juxta,idx] = spikediff(tspike_juxta,tburst_juxta_full);
    Vsingle_juxta = cellfun(@(x,y) x(y), Vspike_juxta, idx, 'UniformOutput', 0);
    n_juxta = cellfun(@(x) iff(isempty(x),0,length(x)), tspike_juxta);
    n_total_juxta = sum(n_juxta);
    n_bursts_juxta = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tburst_juxta));
    n_single_spikes_juxta = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tsingle_juxta));
    n_spikes_in_bursts_juxta = sum(cellfun(@(x) sum(cellfun(@(y) length(y), x)), tburst_juxta_full));
    firing_rate_juxta = cellfun(@(x) iff(isempty(x), 0, length(x)), tspike_juxta)/t(end);

    %%% Extract extracellular spikes and bursts
    fprintf(1, 'Extracting extracellular spikes... ');
    [tspike_extra,Vspike_extra] = findExtracellularSpikes(t,V,[],tspike_juxta,...
        tburst_juxta_full,deadtime,spike_window);
    fprintf(1, 'done.\n');
    fprintf(1, 'Locating the bursts... ');
    [tburst_extra,Vburst_extra,tburst_extra_full,Vburst_extra_full] = ...
        locateBursts(tspike_extra,Vspike_extra,burst_threshold,min_no_spikes_in_burst);
    fprintf(1, 'done.\n');
    [tsingle_extra,idx] = spikediff(tspike_extra,tburst_extra_full);
    Vsingle_extra = cellfun(@(x,y) x(y), Vspike_extra, idx, 'UniformOutput', 0);
    n_extra = cellfun(@(x) iff(isempty(x),0,length(x)), tspike_extra);
    n_total_extra = sum(n_extra);
    n_bursts_extra = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tburst_extra));
    n_single_spikes_extra = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tsingle_extra));
    n_spikes_in_bursts_extra = sum(cellfun(@(x) sum(cellfun(@(y) length(y), x)), tburst_extra_full));
    firing_rate_extra = cellfun(@(x) iff(isempty(x), 0, length(x)), tspike_extra)/t(end);

    if sort_spikes
        %%% Extract and sort extracellular spikes
        [~,Vextra,T] = ETSA(t,V,tspike_extra,spike_window);
        classes = do_spike_sorting(dt,Vextra);
    end
else
    if ~ exist(load_file,'file')
        error('%s: no such file.',load_file);
    end
    tmp = load_file;
    load(load_file);
    load_file = tmp;
    clear tmp
    if isempty(sweeps_to_ignore)
        [t,~,V] = loadIBWFiles(sweeps_to_load, filter_band, N, Rp, Rs);
    else
        [t,~,V] = loadIBWFiles('ignore', sweeps_to_ignore, filter_band, N, Rp, Rs);
    end
end

%%%%%% PLOTTING

%%% Juxtacellular data
make_figure(t,V,tspike_juxta,tsingle_juxta,tburst_juxta,tburst_juxta_full,...
    Vburst_juxta_full,n_total_juxta, n_spikes_in_bursts_juxta,firing_rate_juxta);
print('-dpdf','juxtacellular_spikes_CA3_exp.pdf');
%%% Extracellular data
make_figure(t,V,tspike_extra,tsingle_extra,tburst_extra,tburst_extra_full,...
    Vburst_extra_full,n_total_extra, n_spikes_in_bursts_extra,firing_rate_extra);
print('-dpdf','extracellular_spikes_CA3_exp.pdf');

clear t V i idx

if isempty(load_file)
    fprintf(1, 'Saving the data... ');
    save(output_filename);
    fprintf(1, 'done.\n');
end

end

function make_figure(t,V,tspike,tsingle,tburst,tburst_full,Vburst_full,n_total,n_spikes_in_bursts,firing_rate)
n_trials = length(tspike);
spike_window = [2e-3,3e-3];
[~,Vspikes,T] = ETSA(t,V,tsingle,spike_window);

figure;
axes('Position',[0.02,0.5,0.2,0.44],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
title('Individual spikes');
plot(T,Vspikes,'k','LineWidth',0.7);
axis tight;
yl = ylim;
if yl(2) < 0.75
    yl(2) = 0.75;
    axis([xlim,yl]);
end
placeScaleBars(spike_window(2)-1e-3,yl(2)/2,1e-3,0.5,'1 ms','500 \muV','k','LineWidth',1);
axis off;

if ~ all(cellfun(@(x) isempty(x), tburst))
    burst_window = [20e-3,100e-3];
    [~,Vbursts,T] = ETSA(t,V,tburst,burst_window);

    ax = zeros(2,1);
    ax(1) = axes('Position',[0.32,0.54,0.33,0.4],'NextPlot','Add','TickDir','Out',...
        'LineWidth',1,'FontSize',11);
    title('Bursts');
    plot(T*1e3,Vbursts,'k','LineWidth',0.8);
    axis([burst_window.*[-1,1]*1e3,min(min(Vbursts))*1.1,max(max(Vbursts))*1.1]);
    set(gca,'XTick',0:50:burst_window(2)*1e3,'YTick',-3:5);
    xlabel('Time since burst (ms)');
    ylabel('Voltage (mV)');
    
    ax(2) = axes('Position',[0.75,0.54,0.2,0.4],'NextPlot','Add','TickDir','Out',...
        'LineWidth',1,'FontSize',11);
    for i=1:n_trials
        for j=1:length(tburst_full{i})
            plot(ax(1), (tburst_full{i}{j}-tburst_full{i}{j}(1))*1e3, ...
                Vburst_full{i}{j}, 's', 'Color', [1,.5,0], ...
                'MarkerFaceColor', 'w', 'MarkerSize', 3, 'LineWidth', 0.8);
            plot(ax(2), Vburst_full{i}{j}/Vburst_full{i}{j}(1),'ko-',...
                'LineWidth',1,'MarkerFaceColor','w', 'MarkerSize', 5);
        end
    end
    n = max(flattenCellArray(cellfun(@(x) max(cellfun(@(y) length(y), x)), ...
        Vburst_full, 'UniformOutput', 0), 'full'));
    axis([0.8,n+0.1,0,1.1]);
    set(gca,'XTick',1:n,'YTick',0:.25:1);
    xlabel('Spike #');
    ylabel('Normalized spike height');
end

axes('Position',[0.08,0.1,0.31,0.28],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
rasterplot(tsingle,'k','LineWidth',1);
rasterplot(tburst,'r','LineWidth',3);
axis([t([1,end]),0,n_trials+1]);
xlabel('Time (s)');
ylabel('Trial #');
set(gca,'XTick',0:5:t(end),'YTick',[1,n_trials]);
title(sprintf('Fraction of spikes in bursts: %.3f', n_spikes_in_bursts/n_total));

axes('Position',[0.47,0.1,0.2,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
plot(firing_rate, 'ko-','MarkerFaceColor','w','MarkerSize',4,'LineWidth',0.75);
axis([0,n_trials+1,0,ceil(max(firing_rate))]);
if max(firing_rate) <= 5
    ticks = 0:ceil(max(firing_rate));
else
    ticks = 0:3:ceil(max(firing_rate));
end
set(gca,'XTick',[1,n_trials],'YTick',ticks);
xlabel('Trial #');
ylabel('Firing rate (spikes/s)');

axes('Position',[0.75,0.1,0.2,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
binwidth = 3e-3;
edges = 0:binwidth:100*binwidth;
isi = flattenCellArray(cellfun(@(x) diff(x), tspike, 'UniformOutput', ...
    0),'full');
n = histc(isi,edges);
bar(edges*1e3,n,'FaceColor','k','EdgeColor','k','LineWidth',0.8);
axis([edges([1,end])*1e3,0,max(n)*1.1]);
yl = ylim;
set(gca,'XTick',0:100:binwidth*100*1e3);
if yl(2) > 10
    set(gca,'YTick',round(linspace(0,floor(yl(2)/10)*10,5)));
else
    set(gca,'YTick',0:2:10);
end
xlabel('ISI (ms)');
ylabel(sprintf('ISIs in %.0f-ms bins',binwidth*1e3));

sz = [8,4.5];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);

end

