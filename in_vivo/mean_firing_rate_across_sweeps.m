function [event_rate_per_bin,bin_center_times] = mean_firing_rate_across_sweeps(event_times, binwidth, interval)
% PSTH   Peri-stimulus time histogram.
% 
% [nu,edges,count] = PSTH(spks, binwidth)
% [nu,edges,count] = PSTH(spks, binwidth, interval)
% 
% Computes the peri-stimulus time histogram of spike times.
% 
% Arguments:
%      spks - the spike times, as an array of cells.
%  binwidth - the width of the histogram bin (in seconds).
%  interval - a 2-elements vector containing the onset and end times of the
%             stimulus. If this argument is not given, the first and the
%             last spike times are used.
%
% Returns:
%        nu - the firing rate.
%     edges - the edges used for the binning.
%     count - the actual spike count for each bin.
% 

% Author: Daniele Linaro - September 2009.


if ~exist('interval','var') ,
    interval = [min(cellfun(@(x) x(1), event_times)), max(cellfun(@(x) x(end), event_times))];
end

edges = interval(1) : binwidth : interval(2);
ntrials = length(event_times);
count_raw = zeros(ntrials,length(edges));
for k=1:ntrials
    try
        count_raw(k,:) = histc(event_times{k},edges);
    catch, end
end
count = count_raw(:,1:end-1);  % Last bin has zero bin width b/c histc() is peculiar, so drop it
bin_center_times = (edges(1:end-1)+edges(2:end))/2;
% if ntrials > 1
%     nu = sum(count) / (ntrials*binwidth);
% else
%     nu = count / (ntrials*binwidth);
% end
event_rate_per_bin = sum(count,1) / (ntrials*binwidth);  % events/sec, averaged across trials
