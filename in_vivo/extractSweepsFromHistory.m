function sweeps = extractSweepsFromHistory(folder, filename)
% sweeps = extractSweepsFromHistory(folder, filename)

if ~ exist('filename','var')
    filename = 'history.txt';
end

if ~ exist('folder','var')
    folder = '.';
end

if folder(end) == '/'
    folder = folder(1:end-1);
end

filename = [folder,'/',filename];

% first of all, read the history file
if ~ exist(filename,'file')
    sweeps = sort(arrayfun(@(x) str2double(x.name(5:end-4)), dir([folder,'/ad0_*.ibw'])));
else
    info = tdfread(filename);
    % default sweeps to load
    if isfield(info,'Sweep_Index')
        sweeps = unique(info.Sweep_Index);
    else
        for fld = fieldnames(info)'
            data = info.(fld{1});
            if strcmp(data(2,:),'Sweep Index')
                break
            end
        end
        if ~ strcmp(data(2,:),'Sweep Index')
            error('No Sweep Index found')
        end
        sweeps = zeros(size(data,1)-2,1);
        for i=1:length(sweeps)
            sweeps(i) = str2double(data(i+2,:));
        end
        sweeps = unique(sweeps);
    end
end