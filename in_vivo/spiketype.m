function types = spiketype(tspikes,tbursts_full)
% types = spiketype(tspikes,tbursts_full)
n = length(tspikes);
types = cellfun(@(x) zeros(size(x)), tspikes, 'UniformOutput', 0);
for i=1:n
    for j=1:length(tbursts_full{i})
        start = find(tspikes{i} == tbursts_full{i}{j}(1));
        m = length(tbursts_full{i}{j});
        stop = start+m-1;
        types{i}(start:stop) = 1:m;
    end
end
