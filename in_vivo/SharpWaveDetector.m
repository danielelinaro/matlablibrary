function varargout = SharpWaveDetector(varargin)
% SHARPWAVEDETECTOR MATLAB code for SharpWaveDetector.fig
%      SHARPWAVEDETECTOR, by itself, creates a new SHARPWAVEDETECTOR or raises the existing
%      singleton*.
%
%      H = SHARPWAVEDETECTOR returns the handle to a new SHARPWAVEDETECTOR or the handle to
%      the existing singleton*.
%
%      SHARPWAVEDETECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHARPWAVEDETECTOR.M with the given input arguments.
%
%      SHARPWAVEDETECTOR('Property','Value',...) creates a new SHARPWAVEDETECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SharpWaveDetector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SharpWaveDetector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SharpWaveDetector

% Last Modified by GUIDE v2.5 28-Aug-2015 17:20:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SharpWaveDetector_OpeningFcn, ...
                   'gui_OutputFcn',  @SharpWaveDetector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% MY FUNCTIONS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%
%%%%
function Vfiltered = filter_data(dt, Vraw, band)
% filter properties
N = 2;
Rp = 0.1;
Rs = 40;
[b,a] = ellip(N,Rp,Rs,band*2*dt);
Vfiltered = filtfilt(b,a,Vraw')';

%%%%
%%%%
function display_SW(sweep, index, hObject, handles)
cla(handles.SW_zoom_axes)
set(handles.SW_zoom_axes,'NextPlot','Add','Box','Off',...
    'XLim',handles.SW_window.*[-1,1],'YLim',[1.1*min(handles.VSW{sweep}(index,:)),...
    1.1*max(handles.SW_threshold,max(handles.VSW{sweep}(index,:)))]);
plot(handles.SW_zoom_axes, handles.tSW, handles.VSW{sweep}(index,:),'k');
plot(handles.SW_zoom_axes, handles.tSW([1,end]), handles.SW_threshold+[0,0], ...
    'Color', [.1,.1,.6]);
plot(handles.SW_zoom_axes,[0,0],ylim(handles.SW_zoom_axes),'--','Color',[.9,.4,.1]);
handles.displayed_sweep = sweep;
handles.displayed_SW = index;
set(handles.SW_number_label, 'String', sprintf('Sharp-wave %d/%d', ...
    sum(handles.nSW(1:sweep-1))+index, handles.total_nSW));
set(handles.SW_slider,'Value',sum(handles.nSW(1:sweep-1))+index);
if handles.SW_accepted{sweep}(index)
    set(handles.SW_status_label,'String','Accepted','ForegroundColor',[.1,.6,.1]);
else
    set(handles.SW_status_label,'String','Rejected','ForegroundColor',[.6,.1,.1]);
end
if get(handles.zoom_axes_checkbox,'Value')
    for ax = [handles.raw_axes,handles.AP_axes,handles.SW_axes]
        set(ax, 'XLim', handles.tSWpeaks{sweep}(index) + handles.SW_window_large .* [-1,1]);
    end
end
yl = ylim(handles.SW_zoom_axes);
tspike = handles.preprocessed_data.tspike_extra{sweep};
tspike = tspike(tspike >= handles.tSWpeaks{sweep}(index)-handles.SW_window(1) & ...
    tspike <= handles.tSWpeaks{sweep}(index)+handles.SW_window(2)) - ...
    handles.tSWpeaks{sweep}(index);
for i=1:length(tspike)
    plot(handles.SW_zoom_axes,tspike(i)+[0,0],[yl(1)+diff(yl)/50,yl(1)+diff(yl)/15],'k','LineWidth',1);
end
tspike = handles.preprocessed_data.tspike_juxta{sweep};
tspike = tspike(tspike >= handles.tSWpeaks{sweep}(index)-handles.SW_window(1) & ...
    tspike <= handles.tSWpeaks{sweep}(index)+handles.SW_window(2)) - ...
    handles.tSWpeaks{sweep}(index);
for i=1:length(tspike)
    plot(handles.SW_zoom_axes,tspike(i)+[0,0],[yl(1)+diff(yl)/50,yl(1)+diff(yl)/15],'r','LineWidth',1);
end
guidata(hObject,handles);

%%%%
%%%%
function display_sweep(index, hObject, handles)
for ax = [handles.raw_axes,handles.AP_axes,handles.SW_axes]
    cla(ax);
end
ax = [handles.raw_axes,handles.AP_axes,handles.SW_axes];
if isfield(handles,'displayed_SW') && get(handles.zoom_axes_checkbox,'Value')
    lim = handles.tSWpeaks{index}(handles.displayed_SW) + handles.SW_window_large .* [-1,1];
else
    lim = handles.t([1,end]);
end
set(ax, 'TickDir', 'Out', 'NextPlot', 'Add', 'Box', 'Off', 'XLim', lim);
idx = find(handles.t >= lim(1) & handles.t <= lim(2));

% raw voltage
plot(handles.raw_axes, handles.t, handles.Vraw(index,:), 'Color', [.6,.1,.1]);
lim = 1.1*[min(handles.Vraw(index,idx)),max(handles.Vraw(index,idx))];
set(handles.raw_axes, 'YLim', lim);

% AP trace
plot(handles.AP_axes, handles.t, handles.VAP(index,:), 'Color', [.1,.6,.1]);
lim = 1.1*[min(handles.VAP(index,idx)),max(handles.VAP(index,idx))];
set(handles.AP_axes, 'YLim', lim);
if isfield(handles,'preprocessed_data') && ...
        all(handles.preprocessed_data.sweeps_to_ignore == handles.removed_sweeps)
    plot(handles.AP_axes, handles.preprocessed_data.tspike_juxta{index}, ...
        handles.preprocessed_data.Vspike_juxta{index}, 'k^', ...
        'MarkerFaceColor', 'k', 'MarkerSize', 4);
    for i=1:length(handles.preprocessed_data.tburst_juxta_full{index})
        plot(handles.AP_axes, handles.preprocessed_data.tburst_juxta_full{index}{i}, ...
            handles.preprocessed_data.Vburst_juxta_full{index}{i}, '^', 'Color', [1,.5,0], ...
            'MarkerFaceColor', [1,.5,0], 'MarkerSize', 5);
    end
    plot(handles.AP_axes, handles.preprocessed_data.tspike_extra{index}, ...
        handles.preprocessed_data.Vspike_extra{index}, 'kv', ...
        'MarkerFaceColor', 'k', 'MarkerSize', 4);
    for i=1:length(handles.preprocessed_data.tburst_extra_full{index})
        plot(handles.AP_axes, handles.preprocessed_data.tburst_extra_full{index}{i}, ...
            handles.preprocessed_data.Vburst_extra_full{index}{i}, 'v', 'Color', [0,.5,1], ...
            'MarkerFaceColor', [0,.5,1], 'MarkerSize', 5);
    end
end
% SW trace
plot(handles.SW_axes, handles.t, handles.VSWband(index,:), 'k');
plot(handles.SW_axes, handles.t([1,end]), handles.SW_threshold+[0,0], ...
    'Color', [.1,.1,.6]);
handles.displayed_sweep = index;
lim = 1.1*[min(handles.VSWband(index,idx)),...
    max(handles.SW_threshold,max(handles.VSWband(index,idx)))];
if isfield(handles,'displayed_SW')
    try
        delete(handles.SW_time_hndl);
    catch
    end
    handles.SW_time_hndl = plot(handles.SW_axes, handles.tSWpeaks{index}(handles.displayed_SW), ...
    handles.VSWpeaks{index}(handles.displayed_SW), 'mo', 'MarkerFaceColor', 'm', 'MarkerSize', 5);
end
set(handles.SW_axes, 'YLim', lim);

linkaxes(ax,'x');
set(handles.sweep_number_label, 'String', sprintf('Sweep %d/%d', index, handles.nsweeps));
set(handles.sweep_slider,'Value',index);
guidata(hObject, handles);

%%%%
%%%%
function display_next_SW(hObject, handles)
try
    if handles.displayed_SW == handles.nSW(handles.displayed_sweep)
        if handles.displayed_sweep == handles.nsweeps
            % last sharp-wave of all
            return;
        end
        sweep = handles.displayed_sweep + find(handles.nSW(handles.displayed_sweep+1:end),1,'first');
        index = 1;
    else
        sweep = handles.displayed_sweep;
        index = handles.displayed_SW + 1;
    end
    display_SW(sweep, index, hObject, handles);
    handles = guidata(hObject);
    display_sweep(sweep, hObject, handles);
catch
end

function display_previous_SW(hObject, handles)
try
    if handles.displayed_SW == 1
        if handles.displayed_sweep == 1
            % first sharp-wave of all
            return;
        end
        sweep = find(handles.nSW(1:handles.displayed_sweep-1),1,'last');
        index = handles.nSW(sweep);
    else
        sweep = handles.displayed_sweep;
        index = handles.displayed_SW - 1;
    end
    display_SW(sweep, index, hObject, handles);
    handles = guidata(hObject);
    display_sweep(sweep, hObject, handles);
catch
end

%%%%
%%%%
function out = is_same(handles, field, val)
out = 0;
try
    if all(handles.(field) == val)
        out = 1;
    end
catch
end

%%%%
%%%%
function extract_sharp_waves(hObject, handles)
warning('off','signal:findpeaks:largeMinPeakHeight');
handles.tSW = -handles.SW_window(1) : handles.dt : handles.SW_window(2);
handles.VSW = cell(handles.nsweeps,1);
handles.tSWpeaks = cell(handles.nsweeps,1);
handles.VSWpeaks = cell(handles.nsweeps,1);
handles.nSW = zeros(handles.nsweeps,1);
before = round(handles.SW_window(1) / handles.dt);
after = round(handles.SW_window(2) / handles.dt);
for i=1:handles.nsweeps
    [~,crossings] = findpeaks(handles.VSWband(i,:),'minpeakheight',...
        handles.SW_threshold,'minpeakdistance',round(50e-3/handles.dt));
    crossings = crossings(handles.t(crossings) > handles.SW_window(1) & ...
        handles.t(crossings) < handles.t(end)-handles.SW_window(2));
    handles.tSWpeaks{i} = handles.t(crossings);
    handles.VSWpeaks{i} = handles.VSWband(i,crossings);
    handles.nSW(i) = length(crossings);
    handles.VSW{i} = zeros(handles.nSW(i), -before+after+1);
    for j=1:handles.nSW(i)
        idx = crossings(j)-before : crossings(j)+after;
        jdx = find(idx > 0 & idx < size(handles.VSWband,2));
        handles.VSW{i}(j,jdx) = handles.VSWband(i,idx(jdx));
    end
end
handles.total_nSW = sum(handles.nSW);
guidata(hObject, handles);
warning('on','signal:findpeaks:largeMinPeakHeight');

%%%%
%%%%
function save_data(filename, hObject, handles)
sweeps_to_load = [];
sweeps_to_ignore = handles.removed_sweeps;
dt = handles.dt;
SW_filter_band = handles.SW_filter_band;
AP_filter_band = handles.AP_filter_band;
threshold_factor = handles.threshold_factor;
tSW = cell(handles.nsweeps,1);
VSW = cell(handles.nsweeps,1);
n_SW = cellfun(@(x) sum(x), handles.SW_accepted);
for i=1:handles.nsweeps
    idx = logical(handles.SW_accepted{i});
    tSW{i} = handles.tSWpeaks{i}(idx);
    VSW{i} = handles.VSWpeaks{i}(idx);
end
[~,voltage_SW,time_SW] = ETSA(handles.t,handles.VSWband,tSW,handles.SW_window);
save(filename, 'sweeps_to_load','sweeps_to_ignore','dt','SW_filter_band',...
    'AP_filter_band','threshold_factor','tSW','VSW','n_SW','voltage_SW','time_SW');
handles.data_file = filename;
guidata(hObject, handles);

%%%%
%%%%
function load_preprocessed_file(filename, hObject, handles)
handles.preprocessed_data = load(filename);
set(handles.AP_filter_band_box,'String',sprintf('[%d,%d]',...
    handles.preprocessed_data.filter_band(1),...
    handles.preprocessed_data.filter_band(2)));
x = handles.preprocessed_data.sweeps_to_ignore;
breaks = [0,find(diff(x) > 1),length(x)];
nbreaks = length(breaks);
y = cell(nbreaks-2,1);
for i=2:nbreaks
    y{i-1} = [x(breaks(i-1)+1),x(breaks(i))];
end
if length(y) > 1
    str = '[';
else
    str = '';
end
for i=1:length(y)
    if diff(y{i})
        str = [str, sprintf('%d:%d', y{i}(1), y{i}(2))];
    else
        str = [str, sprintf('%d', y{i}(1))];
    end
    if i < length(y)
        str = [str, ','];
    end
end
if length(y) > 1
    str = [str, ']'];
end
set(handles.ignore_sweeps_box,'String',str);
handles.preprocessed_filename = filename;
guidata(hObject, handles);

function KeyPress(hObject, eventdata)
switch eventdata.Key
    case 'k',
        % keep the current sharp-wave
        handles = guidata(hObject);
        accept_button_Callback(handles.accept_button, [], handles)
    case 'd',
        % discard the current sharp-wave
        handles = guidata(hObject);
        discard_button_Callback(handles.discard_button, [], handles);
    case 'n',
        % display the next sharp-wave
        display_next_SW(hObject, guidata(hObject));
    case 'p',
        % display the previous sharp-wave
        display_previous_SW(hObject, guidata(hObject));
    otherwise,
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% AUTO-GENERATED FUNCTIONS %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes just before SharpWaveDetector is made visible.
function SharpWaveDetector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SharpWaveDetector (see VARARGIN)

set(hObject, 'WindowKeyPressFcn', @KeyPress)

handles.default_data_file = 'sharp-waves_CA3_exp.mat';
% handles.default_preprocessed_file = 'spikes_CA3_exp.mat';
handles.default_state_file = 'sharp_wave_detector.mat';

for ax = [handles.raw_axes,handles.AP_axes,handles.SW_axes]
    if ax ~= handles.AP_axes
        set(ax, 'XTickLabel', {});
    else
        xlabel(ax, 'Time (s)');
    end
    ylabel(ax, 'Voltage (mV)');
end

handles.SW_window = [0.25,0.25];
handles.SW_window_large = [1,2];

set(handles.zoom_axes_checkbox, 'Value', 1);
set(handles.SW_zoom_axes, 'XLim', handles.SW_window.*[-1,1], 'XTick', ...
    -handles.SW_window(1):0.1:handles.SW_window(2), ...
    'YLim', [-0.6,0.6], 'YTick', -1.5:0.15:1.5);
xlabel(handles.SW_zoom_axes, 'Time (s)');
ylabel(handles.SW_zoom_axes, 'Voltage (mV)');

% Choose default command line output for SharpWaveDetector
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SharpWaveDetector wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SharpWaveDetector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadbutton.
function loadbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    remove_sweeps = eval(get(handles.ignore_sweeps_box,'String'));
catch
    remove_sweeps = [];
end
if is_same(handles, 'removed_sweeps', remove_sweeps)
    disp('Data already loaded.');
    handles.to_filter = 0;
else
    handles.removed_sweeps = remove_sweeps;
    [handles.t,handles.Vraw] = loadIBWFiles('ignore',handles.removed_sweeps);
    handles.dt = diff(handles.t(1:2));
    handles.to_filter = 1;
    handles.nsweeps = size(handles.Vraw,1);
    handles.displayed_sweep = 1;
    set(handles.sweep_slider,'Value',handles.displayed_sweep,'Min',1,...
        'Max',handles.nsweeps,'SliderStep',[1/(handles.nsweeps-1),1]);
end
guidata(hObject, handles);
filterbutton_Callback(hObject, eventdata, handles)

function ignore_sweeps_box_Callback(hObject, eventdata, handles)
% hObject    handle to ignore_sweeps_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ignore_sweeps_box as text
%        str2double(get(hObject,'String')) returns contents of ignore_sweeps_box as a double


% --- Executes during object creation, after setting all properties.
function ignore_sweeps_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ignore_sweeps_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on loadbutton and none of its controls.
function loadbutton_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to loadbutton (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

function SW_filter_band_box_Callback(hObject, eventdata, handles)
% hObject    handle to SW_filter_band_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SW_filter_band_box as text
%        str2double(get(hObject,'String')) returns contents of SW_filter_band_box as a double


% --- Executes during object creation, after setting all properties.
function SW_filter_band_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SW_filter_band_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in filterbutton.
function filterbutton_Callback(hObject, eventdata, handles)
% hObject    handle to filterbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'Vraw')
    disp('No data loaded.');
    return;
end
try
    SW_filter_band = eval(get(handles.SW_filter_band_box,'String'));
catch
    SW_filter_band = [3,20];
end
try
    AP_filter_band = eval(get(handles.AP_filter_band_box,'String'));
catch
    AP_filter_band = [300,1000];
end
if ~handles.to_filter && is_same(handles, 'SW_filter_band', SW_filter_band)
    disp('Data already filtered in SW band.');
else
    handles.SW_filter_band = SW_filter_band;
    fprintf(1, 'Filtering data in SW band... ');
    handles.VSWband = filter_data(handles.dt,handles.Vraw,handles.SW_filter_band);
    fprintf(1, 'done.\n');
end
if ~handles.to_filter && is_same(handles, 'AP_filter_band', AP_filter_band)
    disp('Data already filtered in AP band.');
else
    handles.AP_filter_band = AP_filter_band;
    fprintf(1, 'Filtering data in AP band... ');
    handles.VAP = filter_data(handles.dt,handles.Vraw,handles.AP_filter_band);
    fprintf(1, 'done.\n');
end
handles.to_filter = 0;
guidata(hObject,handles);
threshold_button_Callback(hObject, eventdata, handles);
% handles = guidata(hObject);
% displaySweep(hObject, handles, handles.displayed_sweep);

function AP_filter_band_box_Callback(hObject, eventdata, handles)
% hObject    handle to AP_filter_band_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AP_filter_band_box as text
%        str2double(get(hObject,'String')) returns contents of AP_filter_band_box as a double


% --- Executes during object creation, after setting all properties.
function AP_filter_band_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AP_filter_band_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sweep_slider_Callback(hObject, eventdata, handles)
% hObject    handle to sweep_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
if ~ isfield(handles,'Vraw')
    disp('No data loaded.');
    set(hObject,'Value',get(hObject,'Min'));
    return;
end
% the sweep to be displayed
sweep = round(get(hObject,'Value'));
try
    displayed_SW = handles.displayed_SW;
    % the sweep number the displayed SW belongs to
    SW_sweep = find(cumsum(handles.nSW) >= displayed_SW, 1, 'first');
    if sweep ~= SW_sweep
        handles = rmfield(handles,'displayed_SW');
        guidata(hObject, handles);
    end
catch,end
display_sweep(sweep, hObject, handles);
try
    if sweep ~= SW_sweep
        handles.displayed_SW = displayed_SW;
        guidata(hObject, handles);
    end
catch,end
set(hObject,'Value',sweep);

% --- Executes during object creation, after setting all properties.
function sweep_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sweep_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function threshold_box_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of threshold_box as text
%        str2double(get(hObject,'String')) returns contents of threshold_box as a double


% --- Executes during object creation, after setting all properties.
function threshold_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshold_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in threshold_button.
function threshold_button_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'VSWband')
    return;
end
handles.threshold_factor = str2double(get(handles.threshold_box,'String'));
% handles.SW_threshold = handles.threshold_factor * mean(std(handles.VSWband,[],2));
V = handles.VSWband';
% Quiroga et al., 2004
handles.SW_threshold = handles.threshold_factor * median(abs(V(:))/0.6745);
guidata(hObject,handles);
if ~ isfield(handles,'nSW')
    display_sweep(handles.displayed_sweep, hObject, handles);
else
    start_button_Callback(hObject, eventdata, handles);
end

% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'VSWband')
    return;
end
fprintf(1, 'Extracting sharp-waves... ');
extract_sharp_waves(hObject, handles);
fprintf(1, 'done.\n');
handles = guidata(hObject);
handles.SW_accepted = arrayfun(@(n) zeros(n,1), handles.nSW, ...
    'UniformOutput', 0);
set(handles.sweep_slider,'Value',1);
set(handles.SW_slider,'Value',1,'Min',1,'Max',handles.total_nSW,...
    'SliderStep',[1/(handles.total_nSW-1),1]);
% the first sweep with at least one sharp wave
sweep = find(handles.nSW>0,1,'first');
display_SW(sweep, 1, hObject, handles);
handles = guidata(hObject);
display_sweep(sweep, hObject, handles);
for h = [handles.save,handles.save_as,handles.save_state]
    set(h,'Enable','On');
end

% --- Executes on button press in accept_button.
function accept_button_Callback(hObject, eventdata, handles)
% hObject    handle to accept_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'SW_accepted')
    return;
end
handles.SW_accepted{handles.displayed_sweep}(handles.displayed_SW) = 1;
set(handles.SW_status_label,'String','Accepted','ForegroundColor',[.1,.6,.1]);
guidata(hObject,handles);
display_next_SW(hObject, handles);

% --- Executes on button press in discard_button.
function discard_button_Callback(hObject, eventdata, handles)
% hObject    handle to discard_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'SW_accepted')
    return;
end
handles.SW_accepted{handles.displayed_sweep}(handles.displayed_SW) = 0;
set(handles.SW_status_label,'String','Rejected','ForegroundColor',[.6,.1,.1]);
guidata(hObject,handles);
display_next_SW(hObject, handles);

% --- Executes on slider movement.
function SW_slider_Callback(hObject, eventdata, handles)
% hObject    handle to SW_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'VSW')
    set(hObject,'Value',get(hObject,'Min'));
    return;
end
SW = round(get(hObject,'Value'));
sweep = find(cumsum(handles.nSW) >= SW, 1, 'first');
display_SW(sweep, SW-sum(handles.nSW(1:sweep-1)), hObject, handles);
handles = guidata(hObject);
display_sweep(sweep, hObject, handles);

% --- Executes during object creation, after setting all properties.
function SW_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SW_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'SW_accepted')
    return;
end
filename = get(handles.output_filename_box,'String');
if isempty(filename)
    return;
end
save_data(filename);

% --- Executes during object creation, after setting all properties.
function SW_zoom_axes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SW_zoom_axes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate SW_zoom_axes


% --- Executes on button press in zoom_axes_checkbox.
function zoom_axes_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to zoom_axes_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
display_sweep(handles.displayed_sweep, hObject, handles);



function output_filename_box_Callback(hObject, eventdata, handles)
% hObject    handle to output_filename_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_filename_box as text
%        str2double(get(hObject,'String')) returns contents of output_filename_box as a double


% --- Executes during object creation, after setting all properties.
function output_filename_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_filename_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'data_file')
    % ask the user for a file name
    filename = uiputfile('*.mat','Choose filename',handles.default_data_file);
    if ~ ischar(filename)
        return;
    end
else
    filename = handles.data_file;
end
save_data(filename, hObject, handles);

% --------------------------------------------------------------------
function save_as_Callback(hObject, eventdata, handles)
% hObject    handle to save_as (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% ask the user for a file name
filename = uiputfile('*.mat','Choose filename',handles.default_data_file);
if ~ ischar(filename)
    return;
end
save_data(filename, hObject, handles);

% --------------------------------------------------------------------
function save_state_Callback(hObject, eventdata, handles)
% hObject    handle to save_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
removed_sweeps = handles.removed_sweeps;
AP_filter_band = handles.AP_filter_band;
displayed_sweep = handles.displayed_sweep;
threshold_factor = handles.threshold_factor;
SW_threshold = handles.SW_threshold;
SW_filter_band = handles.SW_filter_band;
SW_accepted = handles.SW_accepted;
displayed_SW = handles.displayed_SW;
save(handles.default_state_file,'removed_sweeps','AP_filter_band',...
    'displayed_sweep','threshold_factor','SW_threshold',...
    'SW_filter_band','SW_accepted','displayed_SW');
if isfield(handles, 'preprocessed_filename')
    preprocessed_filename = handles.preprocessed_filename;
    save(handles.default_state_file,'-append','preprocessed_filename');
end

% --------------------------------------------------------------------
function restore_state_Callback(hObject, eventdata, handles)
% hObject    handle to restore_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = load(handles.default_state_file);
handles.SW_filter_band = data.SW_filter_band;
set(handles.SW_filter_band_box,'String',sprintf('[%d,%d]',...
    handles.SW_filter_band(1),handles.SW_filter_band(2)));
if isfield(data,'preprocessed_filename')
    load_preprocessed_file(data.preprocessed_filename, hObject, handles);
    handles = guidata(hObject);
else
    handles.removed_sweeps = data.removed_sweeps;
    handles.AP_filter_band = data.AP_filter_band;
    set(handles.AP_filter_band_box,'String',sprintf('[%d,%d]',...
        handles.AP_filter_band(1),handles.AP_filter_band(2)));
end
loadbutton_Callback(hObject, eventdata, handles);
handles = guidata(hObject);
handles.threshold_factor = data.threshold_factor;
set(handles.threshold_box,'String',num2str(handles.threshold_factor))
handles.SW_threshold = data.SW_threshold;
start_button_Callback(hObject, eventdata, handles);
handles = guidata(hObject);
handles.SW_accepted = data.SW_accepted;
guidata(hObject, handles);
display_SW(data.displayed_sweep, data.displayed_SW, hObject, handles);
handles = guidata(hObject);
display_sweep(handles.displayed_sweep, hObject, handles);

% --------------------------------------------------------------------
function load_spikes_Callback(hObject, eventdata, handles)
% hObject    handle to load_spikes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = uigetfile('*.mat','Choose preprocessed data file');
if ischar(filename)
    load_preprocessed_file(filename, hObject, handles);
end
