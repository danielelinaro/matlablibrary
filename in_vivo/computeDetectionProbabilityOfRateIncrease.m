function [time,prob] = computeDetectionProbabilityOfRateIncrease(varargin)
% 
% prob = computeDetectionProbabilityOfRateIncrease(spikes, baselineRate, window, ...)
% 

p = inputParser;
p.addRequired('spikes');
p.addRequired('baselineRate', @(x) isnumeric(x) && isscalar(x) && x>0);
p.addRequired('window', @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('interval', [-0.5,0], @(x) isnumeric(x) && numel(x)==2 && diff(x)>0);
p.addParamValue('nSynthetic', 50000, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('nRepetitions', 1000, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('step', 1e-3, @(x) isnumeric(x) && isscalar(x) && x>0);

p.parse(varargin{:});
spikes = p.Results.spikes;
baseline_rate = p.Results.baselineRate;
% window size
window = p.Results.window;
% step with which the window is slid forward
step = p.Results.step;
% interval over which the PSTH is computed
interval = p.Results.interval;
% number of bootstrap repetitions
n_reps = p.Results.nRepetitions;
% number of synthetic trials
n_synthetic = p.Results.nSynthetic;
% fraction of trials used for computing the probability of increase
fraction = 0.5;
% duration of each trial
trial_dur = diff(interval);
% number of synthetic spikes per trial
n_spikes = max(ceil(trial_dur*baseline_rate), 1);
% just so the firing rate is flat
n_spikes = n_spikes*2;
fprintf(1, 'Generating synthetic spike times... ');
% generate Poisson-distributed ISI
isi = -log(rand(n_synthetic,n_spikes))/baseline_rate;
% compute the actual spike times
synthetic_spikes = mat2cell(cumsum(isi,2),ones(n_synthetic,1),n_spikes);
synthetic_spikes = cellfun(@(x) x(x<=trial_dur)+interval(1), synthetic_spikes, ...
    'UniformOutput', 0);
fprintf(1, 'done.\n');
% number of trials used for computing the probability of increase
n_trials = round(length(spikes) * fraction);

time = 0.595 : step : 0.605;
% time = interval(1):step:interval(2)-step;
prob = zeros(size(time));

fprintf(1, 'Computing probabilities...\n');
for i=1:length(time)
    fprintf(1, '[%03d/%03d]\n', i, length(time));
    prob(i) = detectFiringRateChange(spikes,synthetic_spikes,[],time(i),...
        window,n_trials,n_reps,'increase');
    if mod(i,50) == 0
        fprintf(1, '\n');
    end
end
if mod(i,50)
    fprintf(1, '\n');
end
fprintf(1, 'Done!');
