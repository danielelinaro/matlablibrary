D = IBWread('10-2-14 Folder/ad0_2.ibw');
y=D.y;  % mV
dt=D.dx;
t0=D.x0;
n=D.Nsam;  
t=t0+dt*(0:(n-1))';  % ms
plot(t,y)

[Hd,b] = generate_0_3_lowpass();
y_low=conv1(y,b','samenan');

figure;
plot(t,y,t,y_low)

[Hd,b] = generate_4_20_bandpass();
y_theta=conv1(y,b1','samenan');

figure;
plot(t,y,t,y_theta)

[Hd,b2] = generate_300_3000_bandpass();
y_mua=conv1(y,b2','samenan');

figure;
plot(t,y,t,y_mua)
