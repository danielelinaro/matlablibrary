function [triggered_event_times,out_of_window_events] = ETE(events,triggering_events,window)
% [triggered_event_times,out_of_window_events] = ETE(events,triggering_events,window)

if length(events) ~= length(triggering_events)
    error('events and triggering_events must have the same size.');
end
ntrials = length(events);
n_triggering_events = sum(cellfun(@(x) length(x), triggering_events));
triggered_event_times = cell(n_triggering_events,1);
out_of_window_events = cell(ntrials,1);
cnt = 1;
for i=1:ntrials
    ev = events{i};
    tr_ev = triggering_events{i};
    idx = [];
    for j=1:length(triggering_events{i})
        jdx = find(ev >= tr_ev(j) - window(1) & ev <= tr_ev(j) + window(2));
        idx = [idx ; jdx(:)];
        triggered_event_times{cnt} = ev(jdx)-tr_ev(j);
        cnt = cnt+1;
    end
    out_of_window_events{i} = ev(setdiff((1:length(ev))',unique(idx)));
end
