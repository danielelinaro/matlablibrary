function th = computeThreshold(x)
% computeThreshold    Extract the noise threshold from a time series.
% 
% th = computeThreshold(x)
% 
% Parameters:
%    x - the time series.
% 
% Returns:
%   th - the threshold value(s).
% 
% computeThreshold uses Quiroga's formula: th = median(abs(x)/0.6745).
% 

% Author: Daniele Linaro - July 2015

th = median(abs(x)/0.6745);
