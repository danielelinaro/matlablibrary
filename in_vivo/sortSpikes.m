function [classes,features] = sortSpikes(spikes,parameters)

handles = struct('par',parameters);
handles.datatype = 'ASCII';        % type of data. ASCII = mat file

% Extract the spike features
fprintf(1, 'Extracting the spikes'' features... \n');
inspk = wave_features(spikes,handles);
fprintf(1, 'done.\n');

% Clustering
fprintf(1, 'Clustering...\n');
if handles.par.permut == 'y'
    ipermut = randperm(size(inspk,1));
else
    ipermut = 1:size(inspk,1);
end
inspk_aux = inspk(ipermut(1:min(handles.par.max_spk,size(spikes,1))),:);

% Interaction with SPC
save(handles.par.fname_in,'inspk_aux','-ascii');
[clu, tree] = run_cluster(handles);
temp = find_temp(tree,handles);
clusters = unique(clu(temp,3:end));
nclusters = length(clusters);
classes = cell(nclusters,1);
for i=1:nclusters
    classes{i} = ipermut(clu(temp,3:end) == clusters(i));
end

features = inspk_aux;

%
% 
% % IF TEMPLATE MATCHING WAS DONE, THEN FORCE
% if (size(spikes,1)> handles.par.max_spk | ...
%         (handles.par.force_auto == 'y'));
%     classes = zeros(size(spikes,1),1);
%     if length(class1)>=handles.par.min_clus; classes(class1) = 1; end
%     if length(class2)>=handles.par.min_clus; classes(class2) = 2; end
%     if length(class3)>=handles.par.min_clus; classes(class3) = 3; end
%     if length(class4)>=handles.par.min_clus; classes(class4) = 4; end
%     if length(class5)>=handles.par.min_clus; classes(class5) = 5; end
%     f_in  = spikes(classes~=0,:);
%     f_out = spikes(classes==0,:);
%     class_in = classes(find(classes~=0),:);
%     class_out = force_membership_wc(f_in, class_in, f_out, handles);
%     classes(classes==0) = class_out;
%     class0=find(classes==0);
%     class1=find(classes==1);
%     class2=find(classes==2);
%     class3=find(classes==3);
%     class4=find(classes==4);
%     class5=find(classes==5);
% end
