function [tsp,Vsp] = findExtracellularSpikes(t,V,threshold_factor,tspikes_juxta,tbursts_juxta,deadtime,window)
% findExtracellularSpikes    Extract extracellular spikes from a voltage
% trace, using, if available, information about juxtacellularly recorded
% spikes and bursts.
% 
% [tsp,Vsp] = findExtracellularSpikes(t,V,threshold_factor,tspikes_juxta,tbursts_juxta,window)
% 
% Arguments:
%                  t - time: a 1xN array, where N is the number of
%                      samples.
%                  V - voltage: a MxN matrix, where M is the number of
%                      trials.
%   threshold_factor - an integer specifying the level of the threshold
%                      relative to the noise. Default: 5.
%      tspikes_juxta - a Mx1 cell array containing the juxtacellularly
%                      recorded spike times. If not specified, this
%                      information will not be used.
%      tbursts_juxta - a Mx1 cell array containing the times of the
%                      juxtacellular bursts. If not specified, this
%                      information will not be used.
%           deadtime - the time after a juxtacellular spike during which
%                      extracellular spikes are discarded (default 4e-3 sec).
%             window - a 2-element vector containing the window that will
%                      be extracted around the spike times. If specified,
%                      spikes in the first window(1) and last window(2)
%                      seconds in each trace will be discarded.
% 
% Returns:
%                tsp - an Mx1 cell array containing the spike times.
%                Vsp - an Mx1 cell array containing the corresponding
%                      values of voltage.
% 
% See also: findJuxtacellularSpikes, computeThreshold
% 

% Author: Daniele Linaro, May 2015

if ~ exist('window','var')
    window = [0,0];
end
if ~ exist('deadtime','var')
    deadtime = 4e-3;
end
if ~ exist('tbursts_juxta','var')
    tbursts_juxta = [];
end
if ~ exist('tspikes_juxta','var')
    tspikes_juxta = [];
end
if ~ exist('threshold_factor','var') || isempty(threshold_factor)
    threshold_factor = 5;
end

dt = diff(t(1:2));
mpd = round(1e-3/dt);
n = size(V,1);
tsp = cell(n,1);
Vsp = cell(n,1);
warning('off','signal:findpeaks:largeMinPeakHeight');
for i=1:n
    threshold = threshold_factor*computeThreshold(V(i,:));
    [~,locs] = findpeaks(-V(i,:),'minpeakheight',threshold,'minpeakdistance',mpd);
    locs = locs(window(1) < t(locs) & t(locs) < t(end)-window(2));
    tsp{i} = t(locs);
    Vsp{i} = V(i,locs);
    if ~ isempty(tspikes_juxta) && ~ isempty(tspikes_juxta{i})
        for tj = tspikes_juxta{i}(:)'
            idx = find(tsp{i} > tj-deadtime & tsp{i} < tj+deadtime);
            if ~ isempty(idx)
                tsp{i} = tsp{i}(setdiff(1:length(tsp{i}),idx));
            end
        end
        if ~ isempty(tbursts_juxta)
            for tb = tbursts_juxta{i}
                idx = find(tsp{i} > tb{1}(1) & tsp{i} < tb{1}(end));
                if ~ isempty(idx)
%                     for j=idx
%                         fprintf(1, 'Removing spike #%d from sweep #%d: %g in (%g,%g).\n', ...
%                             j, i, tsp{i}(j), tb{1}(1), tb{1}(end));
%                     end
                    tsp{i} = tsp{i}(setdiff(1:length(tsp{i}),idx));
                end
            end
        end
        Vsp{i} = V(i,round(tsp{i}/dt)+1);
    end
end
warning('on','signal:findpeaks:largeMinPeakHeight');