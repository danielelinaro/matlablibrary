function [L,N,bin,stats] = hist3SWCMorpho(data,edges)

% type = 'thorny';
% files = dir([type,'/*_DL.swc']);
% data = load([type,'/',files(1).name]);
% data = data(data(:,2) ~= 2, :);
% binwidth = [10,10];
% edges = {...
%     -300+binwidth(1)/2:binwidth(1):300+binwidth(1)/2, ...
%     -300+binwidth(2)/2:binwidth(2):600+binwidth(2)/2 ...
%     };
IDs = data(:,1);
parent_IDs = data(:,end);
coords = data(:,3:5);
nx = length(edges{1});
ny = length(edges{2});
N = zeros(ny,nx);
L = zeros(ny,nx);
bin = zeros(size(coords,1),2);
for i=1:ny-1
    for j=1:nx-1
        idx = find(coords(:,1)>=edges{1}(j) & coords(:,1)<edges{1}(j+1) & ...
            coords(:,2)>=edges{2}(i) & coords(:,2)<edges{2}(i+1));
        N(i,j) = length(idx);
        bin(idx,1) = j;
        bin(idx,2) = i;
    end
end

n_points = size(coords,1);
total_length = 0;
for node_index=2:n_points
    parent_index = find(IDs == parent_IDs(node_index));
    total_length = total_length + norm(coords(parent_index,:)-coords(node_index,:));
    if all(bin(node_index,:) == bin(parent_index,:))
        try
        L(bin(node_index,2),bin(node_index,1)) = L(bin(node_index,2),bin(node_index,1)) + ...
            norm(coords(node_index,:)-coords(parent_index,:));
        catch
            keyboard
        end
    elseif any(bin(node_index,:) == bin(parent_index,:))
        if bin(node_index,1) == bin(parent_index,1)
            idx = (edges{2} > coords(node_index,2) & edges{2} < coords(parent_index,2)) | ...
                (edges{2} < coords(node_index,2) & edges{2} > coords(parent_index,2));
            y = edges{2}(idx);
            y = y(:);
            pt = point_on_line(coords(node_index,:),coords(parent_index,:),...
                [nan+zeros(size(y)),y,nan+zeros(size(y))]);
        else
            idx = (edges{1} > coords(node_index,1) & edges{1} < coords(parent_index,1)) | ...
                (edges{1} < coords(node_index,1) & edges{1} > coords(parent_index,1));
            x = edges{1}(idx);
            x = x(:);
            pt = point_on_line(coords(node_index,:),coords(parent_index,:),...
                [x,nan+zeros(size(x)),nan+zeros(size(x))]);
        end
        if size(pt,1) == 1
            L(bin(node_index,2),bin(node_index,1)) = L(bin(node_index,2),bin(node_index,1)) + ...
                norm(coords(node_index,:)-pt);
            L(bin(parent_index,2),bin(parent_index,1)) = L(bin(parent_index,2),bin(parent_index,1)) + ...
                norm(coords(parent_index,:)-pt);
        end
    end
end
stats.total_length = total_length;
stats.discarded_length = total_length - sum(sum(L));
stats.discarded_length_fraction = stats.discarded_length/total_length;
