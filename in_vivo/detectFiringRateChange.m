function p = detectFiringRateChange(spikes, ref_spikes, baseline_start, interval_start, T, N, n_reps, mode)
% DETECTFIRINGRATECHANGE computes the probability to detect an increase in
% the output spike rate of a population of cells.
% 
% p = computeDetectionProbability(spikes, ref_spikes, baseline_start, interval_start, T, N, n_reps, mode)
% 
% Parameters:
%            spikes - a cell array containing the spike times.
%        ref_spikes - a cell array containing the reference spike times.
%    baseline_start - the start time of the baseline period, used to
%                     compute the baseline firing rate.
%    interval_start - the start time of the period to consider to compute
%                     the probability of detection of firing rate change.
%                 T - the interval over which the probability should be computed.
%                 N - the number of neurons (or trials) to consider.
%            n_reps - the number of ''bootstrap'' repetitions.
%              mode - either 'increase' or 'decrease', for positive and negative
%                     changes, respectively.
% 
% Returns:
%                 p - the probability of detection of an increase in the
%                     firing rate
% 
% The algorithm is taken from the following paper:
% 
% Tchumatchenko, T., Malyshev, A., Wolf, F., & Volgushev, M. (2011).
% Ultrafast Population Encoding by Cortical Neurons.
% The Journal of neuroscience, 31(34), 12171?12179.
% 

% Author: Daniele Linaro - July 2013

DEBUG = 0;

if ~strcmpi(mode,'increase') && ~strcmpi(mode,'decrease')
    error('mode must be either ''increase'' or ''decrease''.');
end
if isempty(ref_spikes)
    ref_spikes = spikes;
end
if isempty(baseline_start)
    with_baseline = 0;
else
    with_baseline = 1;
end
n_trials = length(spikes);
ref_n_trials = length(ref_spikes);
count = zeros(n_reps,1);
ref_count = zeros(n_reps,1);
for k=1:n_reps
    idx = randperm(ref_n_trials,N);
    if with_baseline
        ref_count(k) = sum(cellfun(@(x) length(find(x>baseline_start & x<=baseline_start+T)), ref_spikes(idx)));
    else
        ref_count(k) = sum(cellfun(@(x) length(find(x>interval_start & x<=interval_start+T)), ref_spikes(idx)));
    end
    idx = randperm(n_trials,N);
    count(k) = sum(cellfun(@(x) length(find(x>interval_start & x<=interval_start+T)), spikes(idx)));
end
ref_pd = fitdist(ref_count,'binomial','N',N);
pd = fitdist(count,'binomial','N',N);
ref_start = max(0,floor(ref_pd.mean-5*ref_pd.std));
ref_stop = max(ref_start+10,ceil(ref_pd.mean+5*ref_pd.std));
start = max(0,floor(pd.mean-5*pd.std));
stop = max(start+10,ceil(pd.mean+5*pd.std));
start = min(ref_start,start);
stop = max(ref_stop,stop);
ref_x = start : stop;
x = start : stop;
n_interp = 50;
if strcmpi(mode,'increase')
    ref_y = cdf(ref_pd,ref_x);
    ref_y_int = interp(ref_y,n_interp);
    i = find(ref_y_int-0.95>0, 1);
    if i > 1 && 0.95-ref_y_int(i-1) < ref_y_int(i)-0.95
        i = i-1;
    end
    y = cdf(pd,x);
    y_int = interp(y,n_interp);
    p = 1 - y_int(i);
    if DEBUG
        ref_x_int = interp(ref_x,n_interp);
        figure;
        hold on;
        plot(ref_x,ref_pd.pdf(ref_x),'k');
        plot(x,pd.pdf(x),'r');
        plot(ref_x_int,ref_y_int, 'k--');
        plot(ref_x_int(i)+[0,0],ylim, 'k--');
        plot(interp(x,n_interp),1-y_int,'r--');
        grid on;
        keyboard
    end
else
    ref_y = cdf(ref_pd,ref_x);
    i = find(ref_y-0.05>0, 1);
    if i > 1 && 0.05-ref_y(i-1) < ref_y(i)-0.05
        i = i-1;
    end
    p = cdf(pd,ref_x(i));
    if DEBUG
        figure;
        hold on;
        plot(ref_x,ref_pd.pdf(ref_x),'k');
        plot(x,pd.pdf(x),'r');
        plot(ref_x,ref_y, 'k--');
        plot(ref_x(i)+[0,0],ylim, 'k--');
        plot(x,1-cdf(pd,x),'r--');
    end
end
