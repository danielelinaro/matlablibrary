function [t,Vraw,varargout] = loadIBWFiles(varargin)
% loadIBWFiles    Load (a subset of) the Igor files present in the current
% directory, optionally filtering the data in specified frequency bands.
% 
% [t,Vraw] = loadIBWFiles(directory);
% [t,Vraw] = loadIBWFiles(directory,sweeps);
% [t,Vraw] = loadIBWFiles(directory,'ignore',sweeps);
% [t,Vraw,Vband_1,Vband_2,...] = loadIBWFiles(...,'bands',bands);
% [t,Vraw,Vband_1,Vband_2,...] = loadIBWFiles(...,'N',N,'Rp',Rp,'Rs',Rs);
% 
% 
% Parameters:
%   sweeps - an M-elements array containing the indexes of the sweeps that will be
%            loaded. If the 'ignore' option is passed, the specified sweep
%            indexes will be ignored. Pass an empty array if you want to
%            load all files.
%    bands - an Lx2 array containing the frequency bands where the data
%            will optionally be filtered. Each row contains lower and upper
%            limit.
%  N,Rp,Rs - parameters for the elliptic filter. See ellip for an
%            explanation of the meaning of these parameters. Default values
%            are N=2, Rp=0.1 and Rs=40.
% 
% Returns:
%        t - an 1xN time vector, where N is the number of samples.
%     Vraw - an MxN raw voltage vector, where M is the number of trials.
%  Vband_i - the raw voltage filtered in the i-th frequency band specifid
%            in bands(i,:).
% 

% Author: Daniele Linaro - July 2015.
p = inputParser;
p.addOptional('folder', '.', @isstr);
p.addOptional('sweeps', [], @(x) isnumeric(x) && all(x>0));
p.addParamValue('ignore', [], @(x) isnumeric(x) && all(x>0));
p.addParamValue('bands', [], @(x) isnumeric(x) && size(x,2) == 2 && all(diff(x,[],2) > 0));
p.addParamValue('N', 2, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('Rp', 0.1, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('Rs', 40, @(x) isnumeric(x) && isscalar(x) && x>0);
p.parse(varargin{:});

% filter properties
N = p.Results.N;
Rp = p.Results.Rp;
Rs = p.Results.Rs;

% extract the list of all sweeps from the history file
existing_sweeps = extractSweepsFromHistory(p.Results.folder);

if ~ isempty(p.Results.sweeps) && ~ isempty(p.Results.ignore)
    % sweeps numbers are relative: p.Results.sweeps = i indicates the i-th
    % not ignored sweep
    existing_sweeps = setdiff(existing_sweeps,p.Results.ignore);
    sweeps = existing_sweeps(p.Results.sweeps);
else
    if ~ isempty(p.Results.sweeps)
        if all(ismember(p.Results.sweeps,existing_sweeps))
            % sweeps numbers are absolute: p.Results.sweeps = i indicates
            % the i-th sweep
            sweeps = p.Results.sweeps;
        else
            error('Not all requested sweeps exist.');
        end
    elseif ~ isempty(p.Results.ignore)
        sweeps = setdiff(existing_sweeps,p.Results.ignore);
    else
        sweeps = existing_sweeps;
    end
end

%%% Load all the traces
nsweeps = length(sweeps);
fprintf(1, 'Loading files: ');
for i=1:nsweeps
    nchar = fprintf(1, '[%03d/%03d]', i, nsweeps);
    filename = sprintf('%s/ad0_%d.ibw',p.Results.folder,sweeps(i));
    data = IBWread(filename);
    if i == 1
        dt = data.dx * 1e-3;
        t = (0:length(data.y)-1) * dt;
        Vraw = zeros(nsweeps,length(t));
    end
    try
        Vraw(i,:) = data.y;
    catch
        error('Sweep #%d has different duration', sweeps(i));
    end
    if i < nsweeps
        fprintf(1, repmat('\b',[1,nchar]));
    end
end
fprintf(1, ' done.\n');

%%% Filter in the bands passed by the user
if ~ isempty(p.Results.bands)
    dt = diff(t(1:2));
    nbands = size(p.Results.bands,1);
    varargout = cell(nbands,1);
    for i=1:nbands
        [b,a] = ellip(N,Rp,Rs,p.Results.bands(i,:)*2*dt);
        fprintf(1, 'Filtering the data in the band [%g,%g] Hz... ', ...
            p.Results.bands(i,1), p.Results.bands(i,2));
        varargout{i} = filtfilt(b,a,Vraw')';
        fprintf(1, 'done.\n');
    end
end
