function [s,idx] = extractStd(dt,V)

% minimal distance between peaks
mpd = ceil(1e-3/dt);
% initial threshold for detection
th = 20*std(V);
if max(V) < th
    idx = 1:length(V);
    % Quiroga style
    s = median(abs(V(idx))/0.6745);
else
    % find a first approximation of the spike times
    [~,locs] = findpeaks(V,'minpeakheight',th,'minpeakdistance',mpd);
    locs = [1,locs,length(V)];
    isi = diff(locs);
    % find the longest isi
    [~,i] = max(isi);
    % discard some time between the longest ISI
    window = round(50e-3/dt);
    idx = locs(i)+window:locs(i+1)-window;
    % the threshold is given by the std of the voltage in the longest stretch
    % of time without spikes
    s = std(V(idx));
end
