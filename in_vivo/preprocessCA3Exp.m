function preprocessCA3Exp(varargin)
% preprocessCA3Exp    Extract spikes and sharp-waves from a CA3 experiment.
% 
% preprocessCA3Exp(...)
% 
% Possible options are:
%                     Sweeps - which sweeps to load (default all those in the
%                              current folder).
%               IgnoreSweeps - which sweeps to ignore (default none).
%               APFilterBand - frequency band used for filtering the raw data
%                              (default [300,1000] Hz).
%              LFPFilterBand - frequency band used for filtering the raw data
%                              (default [3,20] Hz).
%             BurstThreshold - the threshold for identifying bursts (default 50 Hz).
%        JuxtaBurstThreshold - the threshold for identifying juxtacellular bursts
%                              (default: equal to BurstThreshold).
%        ExtraBurstThreshold - the threshold for identifying extracellular bursts
%                              (default: equal to BurstThreshold).
%     JuxtaAPThresholdFactor - juxtacellular action potential threshold factor (default 20).
%           JuxtaAPThreshold - juxtacellular action potential threshold
%                              (default NaN and JuxtaAPThresholdFactor is used).
%  juxtaAPMinThresholdFactor - juxtacellular action potential minimal
%                              threshold factor (default 6).
%        JuxtaMaxAPAmplitude - juxtacellular action potential maximal
%                              amplitude (default 50 mV).
%   JuxtaNumAPToMinThreshold - juxtacellular number of action potentials to
%                              reach minimal threshold (default 4).
%     ExtraAPThresholdFactor - extracellular action potential threshold factor (default 5).
%          SWThresholdFactor - sharp-wave threshold factor (default 5).
%         MinNoSpikesInBurst - specify the minimum number of spikes that make up
%                              a burst (default 3).
%                SpikeWindow - extracted window before and after (extracellular)
%                              spike peak (default [1e-3,2e-3] sec).
%                   SWWindow - (default [250e-3,250e-3] sec).
%                 SortSpikes - whether to sort extracellular spikes (default 0) DEPRECATED.
%                   DeadTime - the time after a juxtacellular spike during which
%                              extracellular spikes are discarded (default 10e-3
%                              sec).
%            AnalyzedLFPFile - path of a file that contains a previous
%                              classification of LFP events.
%                Interactive - ask the user about SWs with bursts nearby.
%

% Author: Daniele Linaro - July 2015

p = inputParser;
% which sweeps to load
p.addParamValue('sweeps', [], @isnumeric);
% which sweeps to ignore
p.addParamValue('ignoreSweeps', [], @isnumeric);
% bands for the filtering
p.addParamValue('APFilterBand', [300,1000], @(x) isnumeric(x) && length(x) == 2 && all(x>0));
p.addParamValue('LFPFilterBand', [3,20], @(x) isnumeric(x) && length(x) == 2 && all(x>0));
% minimum rate of spikes in a burst
p.addParamValue('burstThreshold', 50, @(x) isnumeric(x) && all(x>0));
% minimum rate of spikes in a juxtacellular burst
p.addParamValue('juxtaBurstThreshold', -1, @(x) isnumeric(x) && isscalar(x) && x>0);
% minimum rate of spikes in a burst
p.addParamValue('extraBurstThreshold', -1, @(x) isnumeric(x) && isscalar(x) && x>0);
% threshold factors
p.addParamValue('juxtaAPThresholdFactor', 20, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('juxtaAPThreshold', nan, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('juxtaMaxAPAmplitude', 50, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('extraAPThresholdFactor', 5, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('juxtaAPMinThresholdFactor', 6, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('juxtaNumAPToMinThreshold', 4, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('SWThresholdFactor', 5, @(x) isnumeric(x) && isscalar(x) && x>0);
% minimum number of spikes in a burst
p.addParamValue('minNoSpikesInBurst', 3, @(x) isnumeric(x) && isscalar(x) && x>1);
% window to extract around the peak of the (extracellular) spikes
p.addParamValue('APWindow', [1e-3,2e-3], @(x) isnumeric(x) && length(x) == 2 && all(x>0));
% window to extract around the peak of the LFP events
p.addParamValue('SWWindow', [250e-3,250e-3], @(x) isnumeric(x) && length(x) == 2 && all(x>0));
% dead time after a juxtacellular spike
p.addParamValue('deadTime', 10e-3, @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParamValue('analyzedLFPFile', '', @(x) ischar(x));
p.addParamValue('interactive', 1, @(x) isnumeric(x) && isscalar(x) && (x==0 || x==1));

parse(p, varargin{:});
args = p.Results;

if ~ isempty(args.analyzedLFPFile) && ~ exist(args.analyzedLFPFile,'file')
    error('%s: no such file', args.analyzedLFPFile);
end

%%% the names of the output files that will be saved by this function
output_files = struct('juxta','CA3_experiment_juxtacellular_data.mat',...
    'extra','CA3_experiment_extracellular_data.mat',...
    'lfp','CA3_experiment_LFP_data.mat');

if args.juxtaBurstThreshold == -1
    args.juxtaBurstThreshold = args.burstThreshold;
end
if args.extraBurstThreshold == -1
    args.extraBurstThreshold = args.burstThreshold;
end

% general filter properties
N = 2;
Rp = 0.1;
Rs = 40;

% load and filter the data
if isempty(args.ignoreSweeps)
    [t,Vraw,VAP,VLFP] = loadIBWFiles('.', args.sweeps, 'Bands', ...
        [args.APFilterBand ; args.LFPFilterBand], 'N', N, 'Rp', Rp, ...
        'Rs', Rs);
else
    [t,Vraw,VAP,VLFP] = loadIBWFiles('.', 'Ignore', args.ignoreSweeps, ...
        'Bands', [args.APFilterBand ; args.LFPFilterBand], 'N', N, ...
        'Rp', Rp, 'Rs', Rs);
end

%%% Extract juxtacellular spikes and bursts
fprintf(1, 'Extracting juxtacellular spikes... ');
[tspike_juxta,Vspike_juxta] = findJuxtacellularSpikes(t,VAP,...
    args.juxtaBurstThreshold,args.juxtaAPThresholdFactor,args.juxtaAPThreshold,...
    args.juxtaAPMinThresholdFactor,args.juxtaNumAPToMinThreshold);
fprintf(1, 'done.\n');
fprintf(1, 'Locating bursts... ');
[tburst_juxta,Vburst_juxta,tburst_juxta_full,Vburst_juxta_full] = ...
    locateBursts(tspike_juxta,Vspike_juxta,args.juxtaBurstThreshold,...
    args.minNoSpikesInBurst);
fprintf(1, 'done.\n');

%%% Extract extracellular spikes and bursts
fprintf(1, 'Extracting extracellular spikes... ');
[tspike_extra,Vspike_extra] = findExtracellularSpikes(t,VAP,...
    args.extraAPThresholdFactor,tspike_juxta,...
    tburst_juxta_full,args.deadTime,args.APWindow);
fprintf(1, 'done.\n');
fprintf(1, 'Locating bursts... ');
[tburst_extra,Vburst_extra,tburst_extra_full,Vburst_extra_full] = ...
    locateBursts(tspike_extra,Vspike_extra,args.extraBurstThreshold,...
    args.minNoSpikesInBurst,[],1);
fprintf(1, 'done.\n');

%%% Extract single spikes
[tsingle_juxta,idx] = spikediff(tspike_juxta,tburst_juxta_full);
Vsingle_juxta = cellfun(@(x,y) x(y), Vspike_juxta, idx, 'UniformOutput', 0);
%%% Remove spikes that are too large
n = length(Vspike_juxta);
for i=1:n
    idx = Vsingle_juxta{i}<=args.juxtaMaxAPAmplitude;
    tsingle_juxta{i} = tsingle_juxta{i}(idx);
    Vsingle_juxta{i} = Vsingle_juxta{i}(idx);
    idx = Vburst_juxta{i}<=args.juxtaMaxAPAmplitude;
    tburst_juxta{i} = tburst_juxta{i}(idx);
    Vburst_juxta{i} = Vburst_juxta{i}(idx);
    tburst_juxta_full{i} = tburst_juxta_full{i}(idx);
    Vburst_juxta_full{i} = Vburst_juxta_full{i}(idx);
    tspks = [tsingle_juxta{i}, flattenCellArray(tburst_juxta_full{i},'full')'];
    Vspks = [Vsingle_juxta{i}, flattenCellArray(Vburst_juxta_full{i},'full')'];
    [tspike_juxta{i},idx] = sort(tspks);
    Vspike_juxta{i} = Vspks(idx);
end
%%% Extract additional juxtacellular quantities
n_juxta = cellfun(@(x) iff(isempty(x),0,length(x)), tspike_juxta);
n_total_juxta = sum(n_juxta);
n_bursts_juxta = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tburst_juxta));
n_single_spikes_juxta = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tsingle_juxta));
n_spikes_in_bursts_juxta = sum(cellfun(@(x) sum(cellfun(@(y) length(y), x)), tburst_juxta_full));
firing_rate_juxta = cellfun(@(x) iff(isempty(x), 0, length(x)), tspike_juxta)/t(end);

%%% Extract juxtacellular spike features
fprintf(1, 'Extracting juxtacellular AP features... ');
[~,Vspikes_juxta,Tspikes_juxta] = ETSA(t,VAP,tspike_juxta,[4e-3,6e-3]);
AP_type_juxta = flattenCellArray(assignAPType(tspike_juxta,tburst_juxta_full),'full');
Vpeaks_juxta = Vspikes_juxta(:,Tspikes_juxta==0);
tpeaks = zeros(size(Vpeaks_juxta));
[Vhw_juxta,hw_juxta,hw_interval_juxta,tbhp_juxta,Vbhp_juxta,tahp_juxta,Vahp_juxta,tadp_juxta,Vadp_juxta] = ...
    extractJuxtacellularAPFeatures(Tspikes_juxta,Vspikes_juxta,tpeaks,Vpeaks_juxta,1,AP_type_juxta);
fprintf(1, 'done.\n');

%%% Save juxtacellular data
fprintf(1, 'Saving preprocessed juxtacellular data... ');
save(output_files.juxta, 'N', 'Rp', 'Rs', 'Vspike_juxta', 'Vburst_juxta', ...
    'Vburst_juxta_full', 'Vsingle_juxta', 'firing_rate_juxta', ...
    'n_bursts_juxta', 'n_juxta', 'n_single_spikes_juxta', ...
    'n_spikes_in_bursts_juxta', 'n_total_juxta', 'tburst_juxta', ...
    'tburst_juxta_full', 'tsingle_juxta', 'tspike_juxta', ...
    'AP_type_juxta', 'Tspikes_juxta', 'Vadp_juxta', 'Vahp_juxta', ...
    'Vbhp_juxta', 'Vhw_juxta', 'Vpeaks_juxta', 'Vspikes_juxta', ...
    'hw_juxta', 'hw_interval_juxta', 'tadp_juxta', 'tahp_juxta', ...
    'tbhp_juxta', 'args');
fprintf(1, 'done.\n');

%%% Extract additional extracellular quantities
[tsingle_extra,idx] = spikediff(tspike_extra,tburst_extra_full);
Vsingle_extra = cellfun(@(x,y) x(y), Vspike_extra, idx, 'UniformOutput', 0);
n_extra = cellfun(@(x) iff(isempty(x),0,length(x)), tspike_extra);
n_total_extra = sum(n_extra);
n_bursts_extra = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tburst_extra));
n_single_spikes_extra = sum(cellfun(@(x) iff(~isempty(x),length(x),0), tsingle_extra));
n_spikes_in_bursts_extra = sum(cellfun(@(x) sum(cellfun(@(y) length(y), x)), tburst_extra_full));
firing_rate_extra = cellfun(@(x) iff(isempty(x), 0, length(x)), tspike_extra)/t(end);

%%% Extract extracellular spike features
fprintf(1, 'Extracting extracellular AP features... ');
[~,Vspikes_extra,Tspikes_extra] = ETSA(t,VAP,tspike_extra,[2e-3,2e-3]);
Vpeaks_extra = flattenCellArray(Vspike_extra,'full');
tpeaks = zeros(size(Vpeaks_extra));
[halfwidth_extra,Vh_extra,tp_extra,Vp_extra,asymmetry_extra,duration_extra,Vd_extra] = ...
    extractExtracellularAPFeatures(Tspikes_extra,Vspikes_extra,tpeaks,Vpeaks_extra,1);
fprintf(1, 'done.\n');

%%% Save extracellular data
fprintf(1, 'Saving preprocessed extracellular data... ');
save(output_files.extra, 'N', 'Rp', 'Rs', 'Vspike_extra', 'Vburst_extra', ...
    'Vburst_extra_full', 'Vsingle_extra', 'firing_rate_extra', ...
    'n_bursts_extra', 'n_extra', 'n_single_spikes_extra', ...
    'n_spikes_in_bursts_extra', 'n_total_extra', 'tburst_extra', ...
    'tburst_extra_full', 'tsingle_extra', 'tspike_extra', ...
    'Tspikes_extra','Vh_extra','Vp_extra','Vpeaks_extra', ...
    'Vspikes_extra', 'asymmetry_extra', 'halfwidth_extra', ...
    'tp_extra', 'duration_extra','Vd_extra','args');
fprintf(1, 'done.\n');

full = 1;
% keyboard

%%% Extract events from the LFP trace
if full
    juxta.tsingle = tsingle_juxta;
    juxta.tburst = tburst_juxta;
    juxta.tburst_full = tburst_juxta_full;
    extra.tsingle = tsingle_extra;
    extra.tburst = tburst_extra;
    extra.tburst_full = tburst_extra_full;
    
    if ~ isempty(args.analyzedLFPFile)
        fprintf(1, 'Loading LFP file %s... ', args.analyzedLFPFile);
        data = load(args.analyzedLFPFile);
        fprintf(1, 'done.\n');
    else
        data = struct([]);
    end
    fprintf(1, 'Extracting sharp-waves...\n');
    if args.interactive
        [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,VLFP,juxta,extra,...
            args.SWThresholdFactor,args.SWWindow,Vraw,VAP,data);
    else
        [SW,SW_no_activity,DS,artifacts,unknown] = extractSharpWaves(t,VLFP,juxta,extra,...
            args.SWThresholdFactor,args.SWWindow,[],[],data);
    end
    
    %%% Save LFP data
    fprintf(1, 'Saving preprocessed LFP data... ');
    save(output_files.lfp, 'DS', 'N', 'Rp', 'Rs', 'SW', ...
        'SW_no_activity', 'artifacts', 'unknown', 'args');
    fprintf(1, 'done.\n');
end

%%%%%% PLOTTING

%%% Juxtacellular data
fprintf(1, 'Generating summary figures... ');
make_AP_figure(t,VAP,tspike_juxta,tsingle_juxta,tburst_juxta,tburst_juxta_full,...
    Vburst_juxta_full,n_total_juxta, n_spikes_in_bursts_juxta,firing_rate_juxta);
print('-dpdf','juxtacellular_spikes_CA3_exp.pdf');
%%% Extracellular data
make_AP_figure(t,VAP,tspike_extra,tsingle_extra,tburst_extra,tburst_extra_full,...
    Vburst_extra_full,n_total_extra, n_spikes_in_bursts_extra,firing_rate_extra);
print('-dpdf','extracellular_spikes_CA3_exp.pdf');
if full
    %%% Sharp waves
    if size(SW.V) > 1
        make_events_figure(SW.tp,SW.Vp,SW.durations,SW.t,SW.V,tburst_juxta,tsingle_juxta,tspike_extra);
        print('-dpdf','sharp-waves_CA3_exp.pdf');
    end
    %%% Sharp waves with no activity
    if size(SW_no_activity.V) > 1
        make_events_figure(SW_no_activity.tp,SW_no_activity.Vp,SW_no_activity.durations,...
            SW_no_activity.t,SW_no_activity.V,tburst_juxta,tsingle_juxta,tspike_extra);
        print('-dpdf','sharp-waves_no_activity_CA3_exp.pdf');
    end
    %%% Dentate spikes
    if size(DS.V,1) > 1
        make_events_figure(DS.tp,DS.Vp,DS.durations,DS.t,DS.V,tburst_juxta,tsingle_juxta,tspike_extra);
        print('-dpdf','dentate_spikes_CA3_exp.pdf');
    end
    %%% Artifacts
    if size(artifacts.V,1) > 1
        make_events_figure(artifacts.tp,artifacts.Vp,artifacts.durations,...
            artifacts.t,artifacts.V,tburst_juxta,tsingle_juxta,tspike_extra);
        print('-dpdf','artifacts_CA3_exp.pdf');
    end
    %%% Unknown events
    if size(unknown.V,1) > 1
        make_events_figure(unknown.tp,unknown.Vp,unknown.durations,...
            unknown.t,unknown.V,tburst_juxta,tsingle_juxta,tspike_extra);
        print('-dpdf','unknown_events_CA3_exp.pdf');
    end
end
fprintf(1, 'done.\n');

end

function make_AP_figure(t,V,tspike,tsingle,tburst,tburst_full,Vburst_full,n_total,n_spikes_in_bursts,firing_rate)
n_trials = length(tspike);
args.APWindow = [2e-3,3e-3];
[~,Vspikes,T] = ETSA(t,V,tsingle,args.APWindow);

figure;
axes('Position',[0.02,0.5,0.2,0.44],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
title('Individual spikes');
plot(T,Vspikes,'k','LineWidth',0.7);
axis tight;
yl = ylim;
if yl(2) < 0.75
    yl(2) = 0.75;
    axis([xlim,yl]);
end
placeScaleBars(args.APWindow(2)-1e-3,yl(2)/2,1e-3,0.5,'1 ms','500 \muV','k','LineWidth',1);
axis off;

if ~ all(cellfun(@(x) isempty(x), tburst))
    burst_window = [20e-3,100e-3];
    [~,Vbursts,T] = ETSA(t,V,tburst,burst_window);

    ax = zeros(2,1);
    ax(1) = axes('Position',[0.32,0.54,0.33,0.4],'NextPlot','Add','TickDir','Out',...
        'LineWidth',1,'FontSize',11);
    title('Bursts');
    plot(T*1e3,Vbursts,'k','LineWidth',0.8);
    axis([burst_window.*[-1,1]*1e3,min(min(Vbursts))*1.1,max(max(Vbursts))*1.1]);
    set(gca,'XTick',0:50:burst_window(2)*1e3,'YTick',-3:5);
    xlabel('Time since burst (ms)');
    ylabel('Voltage (mV)');
    
    ax(2) = axes('Position',[0.75,0.54,0.2,0.4],'NextPlot','Add','TickDir','Out',...
        'LineWidth',1,'FontSize',11);
    for i=1:n_trials
        for j=1:length(tburst_full{i})
            plot(ax(1), (tburst_full{i}{j}-tburst_full{i}{j}(1))*1e3, ...
                Vburst_full{i}{j}, 's', 'Color', [1,.5,0], ...
                'MarkerFaceColor', 'w', 'MarkerSize', 3, 'LineWidth', 0.8);
            plot(ax(2), Vburst_full{i}{j}/Vburst_full{i}{j}(1),'ko-',...
                'LineWidth',1,'MarkerFaceColor','w', 'MarkerSize', 5);
        end
    end
    n = max(flattenCellArray(cellfun(@(x) max(cellfun(@(y) length(y), x)), ...
        Vburst_full, 'UniformOutput', 0), 'full'));
    axis([0.8,n+0.1,0,1.1]);
    set(gca,'XTick',1:n,'YTick',0:.25:1);
    xlabel('Spike #');
    ylabel('Normalized spike height');
end

axes('Position',[0.08,0.1,0.31,0.28],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
rasterplot(tsingle,'k','LineWidth',1);
rasterplot(tburst,'r','LineWidth',3);
axis([t([1,end]),0,n_trials+1]);
xlabel('Time (s)');
ylabel('Trial #');
set(gca,'XTick',0:5:t(end),'YTick',[1,n_trials]);
title(sprintf('Fraction of spikes in bursts: %.3f', n_spikes_in_bursts/n_total));

axes('Position',[0.47,0.1,0.2,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
plot(firing_rate, 'ko-','MarkerFaceColor','w','MarkerSize',4,'LineWidth',0.75);
axis([0,n_trials+1,0,ceil(max(firing_rate))]);
if max(firing_rate) <= 5
    ticks = 0:ceil(max(firing_rate));
else
    ticks = 0:3:ceil(max(firing_rate));
end
set(gca,'XTick',[1,n_trials],'YTick',ticks);
xlabel('Trial #');
ylabel('Firing rate (spikes/s)');

axes('Position',[0.75,0.1,0.2,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',1,'FontSize',11);
binwidth = 3e-3;
edges = 0:binwidth:100*binwidth;
isi = flattenCellArray(cellfun(@(x) diff(x), tspike, 'UniformOutput', ...
    0),'full');
n = histc(isi,edges);
bar(edges*1e3,n,'FaceColor','k','EdgeColor','k','LineWidth',0.8);
if max(n) == 0
    axis([edges([1,end])*1e3,0,1]);
else
    axis([edges([1,end])*1e3,0,max(n)*1.1]);
end
yl = ylim;
set(gca,'XTick',0:100:binwidth*100*1e3);
if yl(2) > 10
    set(gca,'YTick',round(linspace(0,floor(yl(2)/10)*10,5)));
else
    set(gca,'YTick',0:2:10);
end
xlabel('ISI (ms)');
ylabel(sprintf('ISIs in %.0f-ms bins',binwidth*1e3));

sz = [8,4.5];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);

end

function make_events_figure(tp,Vp,durations,t,V,tburst_juxta,tsingle_juxta,tspike_extra,window,binwidth)

if ~ exist('binwidth','var')
    binwidth = 20e-3;
end
if ~ exist('window','var') || isempty(window)
    window = [0.5,0.5];
end

SW_triggered_tburst_juxta = ETE(tburst_juxta,tp,window);
SW_triggered_tsingle_juxta = ETE(tsingle_juxta,tp,window);
SW_triggered_tspike_extra = ETE(tspike_extra,tp,window);

figure;
axes('Position',[0.05,0.6,0.25,0.35],'NextPlot','Add','FontSize',11);
plot(t,V,'k','LineWidth',0.5);
plot(t,nanmean(V),'r','LineWidth',2);
axis tight;
yl = ylim;
text(t(1),yl(2),sprintf('N = %d', size(V,1)),...
    'VerticalAlignment','Top','HorizontalAlignment','Left','FontSize',11);
placeScaleBars(t(end)-0.1,yl(2)-0.05,0.1,[],'100 ms','','k',...
    'LineWidth',1);
axis off;

axes('Position',[0.35,0.6,0.1,0.35],'TickDir','Out','NextPlot','Add',...
    'XAxisLocation','Top','View',[-90,90],'LineWidth',0.8,'FontSize',11);
axis([xlim,yl]);
edges = yl(1):0.1:yl(2);
n = histc(flattenCellArray(Vp,'full'),edges);
n = n/sum(n);
b = bar(edges,n,0.75,'histc');
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
xlabel('Peak voltage (mV)');
ylabel('Fraction');
box off;
axis tight;

durations = flattenCellArray(durations,'full');
edges = 0:5:ceil(max(durations*1e3)/50)*50;
n = histc(durations*1e3,edges);
n = n/sum(n);
ax = axes('Position',[0.1,0.5,0.2,0.075],'TickDir','Out','NextPlot','Add',...
    'LineWidth',0.8,'FontSize',11);
b = bar(edges,n,0.75,'histc');
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
axis([edges([1,end]),ylim]);
yl = ylim;
set(ax, 'XTick',edges(1):50:edges(end), 'YTick', [0,yl(2)]);
xlabel('SW duration (ms)');
ylabel('Fraction');

ISWI = flattenCellArray(cellfun(@(x) diff(x), tp, 'UniformOutput', 0), 'full');
edges = logspace(-2,2,20);
n = histc(ISWI,edges);
n = n/sum(n);
ax = axes('Position',[0.1,0.1,0.4,0.3],'NextPlot','Add','TickDir','Out',...
    'LineWidth',0.8,'FontSize',11);
b = bar(edges,n,0.75,'histc');
delete(findobj('marker','*'));
set(b,'FaceColor',[.3,.3,.3],'LineWidth',1);
set(gca,'XScale','Log','XTick',10.^(-2:2),'XTickLabel',10.^(-2:2));
if ~ isempty(ISWI)
    axis([0.01,100,0,max(n)*1.1]);
else
    axis([0.01,100,0,1]);
end
xlabel('Interval (s)');
ylabel('Fraction');

IBI = flattenCellArray(cellfun(@(x) diff(x), tburst_juxta, 'UniformOutput', 0), 'full');
edges = logspace(-2,2,20);
n = histc(IBI,edges);
n = n/sum(n);
yl = ylim;
b = bar(ax,edges,n,0.3,'histc');
set(b,'FaceColor',[1,.5,0],'LineWidth',1);
delete(findobj('marker','*'));
if yl(2) < max(n)*1.1
    axis([0.01,100,0,max(n)*1.1]);
end
h = legend('Sharp-waves','Bursts','Location','NorthWest');
set(h,'Box','Off');

n = 3;
above = 0.07;
below = 0.05;
offset = 0.05;
h = (1-offset-(above+below)*n)/n;
ax = arrayfun(@(i) axes('Position',[0.625,offset+below*i+(i-1)*(above+h),0.3,h],...
    'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',11), 1:n);

[~,edges,count] = psth(SW_triggered_tburst_juxta,binwidth,window.*[-1,1]);
count = sum(count);
b = bar(ax(end),edges,count,0.75,'histc');
set(b,'FaceColor',[1,.5,.0],'LineWidth',1);
if all(count == 0)
    axis(ax(end),[edges([1,end]),0,1]);
else
    axis(ax(end),[edges([1,end]),0,1.1*max(count)]);
end
ylabel(ax(end),'Burst count');
title(ax(end),'SW-triggered burst count');

[nu,edges] = psth(SW_triggered_tsingle_juxta,binwidth,window.*[-1,1]);
b = bar(ax(end-1),edges,nu,0.75,'histc');
set(b,'FaceColor',[.8,0,.3],'LineWidth',1);
if all(nu == 0)
    axis(ax(end-1),[edges([1,end]),0,1]);
else
    axis(ax(end-1),[edges([1,end]),0,1.1*max(nu)]);
end
ylabel(ax(end-1),'Spike rate (Hz)');
title(ax(end-1),'SW-triggered single spike rate');

[nu,edges] = psth(SW_triggered_tspike_extra,binwidth,window.*[-1,1]);
b = bar(ax(end-2),edges,nu,0.75,'histc');
set(b,'FaceColor',[0,.3,.8],'LineWidth',1);
if all(nu == 0)
    axis(ax(end-2),[edges([1,end]),0,1]);
else
    axis(ax(end-2),[edges([1,end]),0,1.1*max(nu)]);
end
xlabel(ax(end-2),'Time since SW (s)');
ylabel(ax(end-2),'Spike rate (Hz)');
title(ax(end-2),'SW-triggered MUA spike rate');

sz = [8,4.5];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);

end

