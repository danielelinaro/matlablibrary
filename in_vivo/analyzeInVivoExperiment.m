function analyzeInVivoExperiment(varargin)

narginchk(0,2);

%
%  Process the individual sweeps
%
info = tdfread('history.txt');
if nargin == 0
    sweeps = unique(info.Sweep_Index);
elseif nargin == 1
    sweeps = varargin{1};
elseif strcmp(varargin{1},'remove')
    sweeps = setdiff(unique(info.Sweep_Index),varargin{2});
else
    error('Wrong inputs');
end
nsweeps = length(sweeps);
has_stimulus = zeros(nsweeps,1);
stim.delay = -1 + zeros(nsweeps,1);     % [s]
stim.duration = -1 + zeros(nsweeps,1);  % [s]
stim.pulse_rate = -1 + zeros(nsweeps,1);      % [Hz]
stim.pulse_duration = -1 + zeros(nsweeps,1);  % [s]

for i_sweep=1:nsweeps
    filename = sprintf('ad0_%d.ibw',sweeps(i_sweep));
    idx = find(info.Sweep_Index == sweeps(i_sweep));
    if length(idx) == 3  % stimulus was on
        has_stimulus(i_sweep) = 1;
%         stim_name = info.Wave_Base_Name(idx(end),:);
%         stim_file = sprintf('DP_Sweeper/ttlWaves/%s.ibw',stim_name(1:end-1));
%         stimulus = IBWread(stim_file);
        stim_pars = info.Builder_Parameters(idx(end),:);
        start = strfind(stim_pars,'=') + 1;
        stop = strfind(stim_pars,';') - 1;
        stim.delay(i_sweep) = str2double(stim_pars(start(1):stop(1))) * 1e-3;
        stim.duration(i_sweep) = str2double(stim_pars(start(2):stop(2))) * 1e-3;
        stim.pulse_rate(i_sweep) = str2double(stim_pars(start(3):stop(3)));
        stim.pulse_duration(i_sweep) = str2double(stim_pars(start(4):stop(4))) * 1e-3;
    end
%     if iSweep==102 ,
%         keyboard
%     end
    [~,~,SW_this,MUA_this] = extractSharpWavesAndSpikes(filename);
    SW(i_sweep)=SW_this; %#ok<AGROW>
    MUA(i_sweep)=MUA_this;     %#ok<AGROW>
end

has_stimulus = logical(has_stimulus);

save('preprocessed.mat','MUA','SW','has_stimulus','stim');




%
% Make figures
%

% Some common processing for first two figures
t0 = max(stim.delay);
dur = max(stim.duration);
unsorted_spike_times = arrayfun(@(x) x.unsorted_spike_times, MUA, 'UniformOutput', 0);
sharp_wave_times = arrayfun(@(x) x.sharp_wave_times, SW, 'UniformOutput', 0);

%%  Histogram of unsorted spike times for unstimulated trials
[nu,edges_before] = psth(unsorted_spike_times(~has_stimulus),0.5,[0,20]);
figure;
height = 5;
if any(has_stimulus)
    height = 8;
    subplot(211);
end
hndl = bar(edges_before,nu,'histc');
set(hndl,'FaceColor','k','EdgeColor','w');
axis([0,20,0,max(nu)+2]);
box off;
title('No stimulation');
ylabel('Firing rate (Hz)');
set(gca,'TickDir','Out','XTick',0:5:20,'YTick',0:2:max(nu)+2);
if any(has_stimulus)
    [nu,edges_before] = psth(unsorted_spike_times(has_stimulus),0.5,[0,20]);
    subplot(212);
    hold on;
    hndl = bar(edges_before,nu,'histc');
    set(hndl,'FaceColor',[.5,.5,1],'EdgeColor','w');
    axis([0,20,0,max(nu)+2]);
    plot(t0+[0,0],ylim,'b','LineWidth',2);
    plot(t0+dur+[0,0],ylim,'b','LineWidth',2);
    set(gca,'TickDir','Out','XTick',0:5:20,'YTick',0:2:max(nu)+2);
    box off;
    title('With stimulation');
    ylabel('Unsorted spike rate (Hz)');
end
xlabel('Time (s)');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,height],...
    'PaperSize',[8,height]);
print('-dpdf','firing_rate_hist.pdf');

%% Histogram of sharp wave times for unstimulated trials
[nu,edges_before] = psth(sharp_wave_times(~has_stimulus),0.5,[0,20]);
figure;
height = 5;
if any(has_stimulus)
    height = 8;
    subplot(211);
end
hndl = bar(edges_before,nu,'histc');
set(hndl,'FaceColor','k','EdgeColor','w');
axis([0,20,0,max(nu)+0.2]);
box off;
title('No stimulation');
ylabel('SW rate (Hz)');
set(gca,'TickDir','Out','XTick',0:5:20,'YTick',0:.2:max(nu)+0.2);
if any(has_stimulus)
    [nu,edges_before] = psth(sharp_wave_times(has_stimulus),0.5,[0,20]);
    subplot(212);
    hold on;
    hndl = bar(edges_before,nu,'histc');
    set(hndl,'FaceColor',[.5,.5,1],'EdgeColor','w');
    axis([0,20,0,.8]);
    plot(t0+[0,0],ylim,'b','LineWidth',2);
    plot(t0+dur+[0,0],ylim,'b','LineWidth',2);
    set(gca,'TickDir','Out','XTick',0:5:20,'YTick',0:.2:.8);
    box off;
    ylabel('SW rate (Hz)');
end
xlabel('Time (s)');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,height],...
    'PaperSize',[8,height]);
print('-dpdf','sharp_wave_rate_hist.pdf');





%% Rastergram of ALL spikes triggered by the sharp wave
swt_unsorted_spike_times = cell(0,1);  % col cell array, one element per SW (across all sweeps)
for i_sweep=1:length(MUA) ,
    swt_unsorted_spike_times_per_sw_this_sweep = MUA(i_sweep).swt_unsorted_spike_times ; % col cell array, one element per SW in the sweep
    swt_unsorted_spike_times = [swt_unsorted_spike_times ; ...
                                swt_unsorted_spike_times_per_sw_this_sweep]; %#ok<AGROW>
end
swt_unsorted_fig_h = figure_sharp_wave_triggered_raster(swt_unsorted_spike_times,'Unsorted spikes');
set(swt_unsorted_fig_h,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_firing_rate_all_spikes.pdf');
%%





%% Rastergram of juxta spikes triggered by the sharp wave, but including all sweeps, even if they have no juxta spikes
swt_juxta_spike_times_per_sw = cell(0,1);  % col cell array, one element per SW (across all sweeps)
for i_sweep=1:length(MUA) ,
    % Note that we leave in SWs with zero juxta spikes.
    swt_juxta_spike_times_per_sw_this_sweep = MUA(i_sweep).swt_juxta_spike_times;  % col cell array, one element per SW in the sweep
    swt_juxta_spike_times_per_sw = [swt_juxta_spike_times_per_sw ; ...
                                    swt_juxta_spike_times_per_sw_this_sweep]; %#ok<AGROW>
end
swt_juxta_fig_h = figure_sharp_wave_triggered_raster(swt_juxta_spike_times_per_sw,'Juxtacellular spikes');
set(swt_juxta_fig_h,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_firing_rate_juxta_spikes.pdf');
%%





%% Rastergram of exta spikes triggered by the sharp wave, but including all sweeps, even if they have no extra spikes
swt_extra_spike_times_per_sw = cell(0,1);  % col cell array, one element per SW (across all sweeps)
for i_sweep=1:length(MUA) ,
    % Note that we leave in SWs with zero extra spikes.
    swt_extra_spike_times_per_sw_this_sweep = MUA(i_sweep).swt_extra_spike_times;  % col cell array, one element per SW in the sweep
    swt_extra_spike_times_per_sw = [swt_extra_spike_times_per_sw ; ...
                                    swt_extra_spike_times_per_sw_this_sweep]; %#ok<AGROW>
end
swt_extra_fig_h = figure_sharp_wave_triggered_raster(swt_extra_spike_times_per_sw,'Extracellular spikes');
set(swt_extra_fig_h,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_firing_rate_extra_spikes.pdf');
%%





%% Rastergram of the nonburst spikes, triggered by the sharp waves
swt_nonburst_extra_spike_times_per_sw = cell(0,1);
for i_sweep=1:length(MUA) ,
    swt_nonburst_extra_spike_times_per_sw_this_sweep = MUA(i_sweep).swt_nonburst_extra_spike_times ;
    swt_nonburst_extra_spike_times_per_sw = [swt_nonburst_extra_spike_times_per_sw ; ...
                                              swt_nonburst_extra_spike_times_per_sw_this_sweep];  %#ok<AGROW>
end
swt_unsorted_nonburst_spikes_fig_h = ...
    figure_sharp_wave_triggered_raster(swt_nonburst_extra_spike_times_per_sw,'All non-burst extracellular spikes (burstified within classes, then lumped)');
set(swt_unsorted_nonburst_spikes_fig_h,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_firing_rate_nonburst_extra_spikes.pdf');
%%





%% Rastergram of the juxta bursts triggered by the sharp waves
swt_juxta_burst_times_per_sharp_wave = cell(0,1);  % col cell array, one element per SW (across all sweeps)
for i_sweep=1:length(MUA) ,
    swt_juxta_burst_times_per_sharp_wave_this_sweep = MUA(i_sweep).swt_juxta_burst_times ;  % col cell array, one element per SW in the sweep
    swt_juxta_burst_times_per_sharp_wave = [swt_juxta_burst_times_per_sharp_wave ; ...
                                            swt_juxta_burst_times_per_sharp_wave_this_sweep];  %#ok<AGROW>
end
swt_juxta_bursts_fig_h = ...
    figure_sharp_wave_triggered_raster(swt_juxta_burst_times_per_sharp_wave,'Juxtacellular bursts');
set(swt_juxta_bursts_fig_h,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_juxta_burst_rate.pdf');
%%





%% SW-triggered juxta burst rate, and sharp-wave mean waveform
binwidth = 20e-3;  % s
[nonburst_extra_spike_rate_per_bin,edges_for_nonburst_extra_spike_rate_per_bin,centers_for_nonburst_extra_spike_rate_per_bin] = ...
    psth_mod(swt_nonburst_extra_spike_times_per_sw,binwidth,[-3-binwidth/2,+3+binwidth/2]);
[juxta_burst_rate_per_bin,edge_times_for_juxta_burst_rate_per_bin,center_times_for_juxta_burst_rate_per_bin] = ...
    psth_mod(swt_juxta_burst_times_per_sharp_wave,binwidth,[-3-binwidth/2,+3+binwidth/2]);
t_peri = SW(1).T;
V_peri_mean = nanmean(flattenCellArray(arrayfun(@(x) x.V, SW, 'UniformOutput', 0)));

figure('Color','w');

subplot(1,3,1)
hold on;
% hndl = bar(edges,count/total,'histc');
%burst_prob=juxta_burst_rate_per_bin/sum(juxta_burst_rate_per_bin);
plot(edge_times_for_juxta_burst_rate_per_bin,juxta_burst_rate_per_bin,'k');
%axis([edge_times_for_juxta_burst_rate_per_bin([1,end]),ylim]);
xlim([-3 +3]);
%set(gca,'TickDir','Out','XTick',-20:10:20,'YTick',0:.04:.2);
set(gca,'TickDir','Out','XTick',-3:1:3);
box off;
xlabel('Time since SW (s)');
ylabel('Juxta burst rate (Hz)');

subplot(1,3,2:3)
hold on;
% axes('Position',[0.4,0.6,0.2,0.3],'NextPlot','Add');
is_within_peri_range = (t_peri(1)<=edge_times_for_juxta_burst_rate_per_bin & edge_times_for_juxta_burst_rate_per_bin<=t_peri(end));
% x = edges(idx(1:end-1)) + diff(edges(idx))/2;
% y = count(idx(1:end-1));
% plot(x,(y-min(y))/(max(y)-min(y)),'k','LineWidth',2);
juxta_burst_rate_edge_times_close = edge_times_for_juxta_burst_rate_per_bin(is_within_peri_range);
juxta_burst_rate_close=juxta_burst_rate_per_bin(is_within_peri_range);
%mean_juxta_burst_rate_barseries_h= ...
%    bar(mean_burst_rate_edge_times_close,(mean_burst_rate_close-min(mean_burst_rate_close))/(max(mean_burst_rate_close)-min(mean_burst_rate_close)),'histc');


juxta_burst_rate_normed = (juxta_burst_rate_close-min(juxta_burst_rate_close))/(max(juxta_burst_rate_close)-min(juxta_burst_rate_close)) ;
mean_juxta_burst_rate_line_h= ...
    plot(juxta_burst_rate_edge_times_close, ...
         juxta_burst_rate_normed, ...
         'Color',[0 0 0.5], ...
         'LineWidth',2);

idx = find(edges_for_nonburst_extra_spike_rate_per_bin>=t_peri(1) & edges_for_nonburst_extra_spike_rate_per_bin<=t_peri(end));
t_mua_psth_close = edges_for_nonburst_extra_spike_rate_per_bin(idx(1:end-1)) + diff(edges_for_nonburst_extra_spike_rate_per_bin(idx))/2;
nonburst_extra_spike_rate_close = nonburst_extra_spike_rate_per_bin(idx(1:end-1));
nonburst_extra_spike_rate_normed = ...
    (nonburst_extra_spike_rate_close-min(nonburst_extra_spike_rate_close))/(max(nonburst_extra_spike_rate_close)-min(nonburst_extra_spike_rate_close)) ;
unsorted_spike_rate_h= ...
    plot(t_mua_psth_close,nonburst_extra_spike_rate_normed,'Color',[0,0.5,1],'LineWidth',2);
plot([0 0],[0 1],'--','Color',[.6,.6,.6]);  % t==0 line
V_peri_mean_normed = (V_peri_mean-min(V_peri_mean))/(max(V_peri_mean)-min(V_peri_mean));
mean_sharp_wave_h = ...
    plot(t_peri,V_peri_mean_normed,'Color',[1,.5,0],'LineWidth',2);
xlabel('Time since SW (s)');
set(gca,'TickDir','Out');
placeScaleBars(t_peri(end)-0.5,0.75,0.5,[],'0.5 s','','k');
%axis off;
legend_h=legend([mean_juxta_burst_rate_line_h unsorted_spike_rate_h mean_sharp_wave_h], ...
                {'Juxta burst rate' 'Nonburst extra spike rate' 'Mean sharp wave'});
pos=get(legend_h,'Position');
sz=pos(3:4);
set(legend_h,'Position',[0.67 0.8 sz]);

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,6],...
    'PaperSize',[10,6]);
print('-dpdf','sw_triggered_burst_prob.pdf');
%save('-v6','sw_triggered_burst_prob.mat','mean_burst_rate_edge_times','burst_prob','mean_burst_rate_edge_times_close','mean_burst_rate_close','t_mua_psth_close','nu_mua_psth_close','t_peri','V_peri_mean');
%%





%% ISI distribution of the juxtacellular spikes
does_sweeep_have_juxta = logical([MUA.has_juxta]);
if any(does_sweeep_have_juxta) ,
    binwidth = 20e-3; % changed from 50e-3 on 2/6/2014
    spks = arrayfun(@(x) x.unsorted_spike_times(sort(x.classes{1})), MUA(does_sweeep_have_juxta), 'UniformOutput', 0);
    nspks = sum(cellfun(@(x) length(x), spks));
    isi = flattenCellArray(cellfun(@(x) diff(x), spks, 'UniformOutput', 0), 'full');
    edge_times_for_juxta_burst_rate_per_bin = 0:binwidth:ceil(max(isi));
    n = histc(isi,edge_times_for_juxta_burst_rate_per_bin);
    figure;
    hndl = bar(edge_times_for_juxta_burst_rate_per_bin,n/sum(n),'histc');
    set(hndl,'FaceColor','k','EdgeColor','k');
    title(sprintf('# spikes = %d     Bin width = %.0f ms', nspks, binwidth*1e3));
    xlabel('Time (s)');
    ylabel('Juxta ISI fraction');
    set(gca,'TickDir','out');
    box off;
    axes('Position',[0.5,0.6,0.4,0.3]);
    idx = find(edge_times_for_juxta_burst_rate_per_bin<2);
    hndl = bar(edge_times_for_juxta_burst_rate_per_bin(idx),n(idx)/sum(n),'histc');
    axis([0,2,0,5*mean(n(edge_times_for_juxta_burst_rate_per_bin>binwidth & edge_times_for_juxta_burst_rate_per_bin<2)/sum(n))]);
    set(hndl,'FaceColor','k','EdgeColor','k');
    set(gca,'TickDir','out');
    box off;
    ticks = get(gca,'YTick');
    set(gca,'XTick',0:2,'YTick',ticks([1,end]));
    set(gcf,'Color','w','PaperPosition',[0,0,8,5],'PaperUnits','Inch','PaperSize',[8,5]);
    print('-dpdf','ISI_distribution_juxta.pdf');
end
%%
keyboard

