function [thresh,ap_thresh_deriv] = loadThreshold(default_value,cell_type)
% [thresh,ap_thresh_deriv] = loadThreshold(default_value)

if ~ exist('default_value','var')
    default_value = -20;
end

if ~ exist('cell_type','var')
    cell_type = 'PYR';
end

thresh = default_value;
if strcmpi(cell_type,'cult')
    ap_thresh_deriv = 1;
else
    ap_thresh_deriv = 3;
end

filenames = {'spike_threshold','SPIKE_THRESHOLD','spike_thresh','SPIKE_THRESH'};

level = 0;
cwd = [pwd,'/'];
pos = strfind(cwd,'/');
npos = length(pos);
for i=npos:-1:2
    str = cwd(pos(i-1)+1:pos(i)-1);
    level = level+1;
    if length(str) > 3 && strcmp(str(1:3),'201') && str(end-2) >= 'A' && str(end-2) <= 'Z'
        base_folder = cwd(1:pos(i)-1);
        break;
    end
end

if exist('base_folder','var')
    if strcmp(base_folder,pwd)
        directories = {base_folder};
    else
        directories = {'.',base_folder};
    end
    
    for d = directories
        for f = filenames
            file = [d{1},'/',f{1}];
            if exist(file,'file')
                data = load(file);
                thresh = data(1);
                if length(data) == 2
                    ap_thresh_deriv = data(2);
                elseif exist('CELL_TYPE','file')
                    fid = fopen('CELL_TYPE','r');
                    cell_type = fscanf(fid,'%s');
                    fclose(fid);
                    if strcmpi(cell_type,'cult')
                        ap_thresh_deriv = 1;
                    else
                        ap_thresh_deriv = 3;
                    end
                else
                    ap_thresh_deriv = 3;
                end
                return;
            end
        end
    end
end

