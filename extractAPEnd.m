function [tend,vend] = extractAPEnd(T,V,thresh,min_dV_thresh_coeff,tp,Vth,tahp)
% EXTRACTAPEND finds the final point of an action potential.
% 
% [tend,vend] = extractAPEnd(T,V,thresh,min_dV_thresh_coeff,tp,Vth,tahp)
% 
% Parameters:
%   T - time vector, with size (1xN) where N is the number of samples.
%   V - voltage trace(s), with size (MxN), where M is the number of 
%       traces and N is the number of samples.
% thresh - threshold for spike detection: it can either be an (Mx1) array or a scalar.
%
% Return values:
%  tend - a cell array [of size (Mx1)] with the times of the end of the APs.
%  vend - a cell array [of size (Mx1)] with the corresponding voltage values.
%

% Daniele Linaro - May 2013

cell_type = loadCellType;
if strcmp(cell_type,'CULT')
    use_min_dV_thresh_coeff = false;
else
    use_min_dV_thresh_coeff = true;
end

if ~ exist('thresh','var')
    thresh = [];
end
if ~ exist('min_dV_thresh_coeff','var') || isempty(min_dV_thresh_coeff)
    min_dV_thresh_coeff = 20;
end
if ~ exist('tahp','var') || isempty(tahp)
    tahp = extractAPAHP(T,V,[],thresh);
end
if ~ exist('Vth','var') || isempty(Vth)
    [~,vend] = extractAPThreshold(T,V,thresh);
else
    vend = Vth;
end
if ~ exist('tp','var') || isempty(tp)
    tp = extractAPPeak(T,V,thresh);
end

nexp = size(V,1);
tend = cell(nexp,1);
nspks = cellfun(@(x)length(x), tp);
dt = diff(T(1:2));

for ii=1:nexp

    if nspks(ii) == 0
        continue;
    end
    
    tend{ii} = zeros(1,nspks(ii));

    for jj=1:nspks(ii)
        if jj<nspks(ii) && vend{ii}(jj) < vend{ii}(jj+1)
            vend{ii}(jj) = vend{ii}(jj+1);
        end
        ndx = find(T>=tp{ii}(jj)+0.5e-3 & T<=tahp{ii}(jj));
        if isempty(ndx)
            ndx = find(T>=tp{ii}(jj) & T<=tahp{ii}(jj));
        end
        if use_min_dV_thresh_coeff
            dV = (V(ii,ndx(3:end)) - V(ii,ndx(1:end-2))) / (2*dt);
%             g = fittype('a*exp(-t/tau)','indep','t','coeff',{'a','tau'});
%             x = T(ndx(3:end))' - T(ndx(3));
%             y = dV'*1e-3;
%             model = fit(x,y,g,'StartPoint',[min(y),2e-3]);
%             min_dV = model.a*1e3;
%             min_dV_thresh_coeff = 4;
%             kk = find(dV > min_dV/min_dV_thresh_coeff, 1);
            kk = find(dV > min(dV)/min_dV_thresh_coeff, 1);
            if vend{ii}(jj) < V(ii,ndx(kk)+1)
                vend{ii}(jj) = V(ii,ndx(kk)+1);
            end
        end
        idx = find(V(ii,ndx) <= vend{ii}(jj), 1);
        if ~ isempty(idx)
            tend{ii}(jj) = T(ndx(idx));
            vend{ii}(jj) = V(ii,ndx(idx));
        else
            try
            tend{ii}(jj) = T(ndx(end));
            vend{ii}(jj) = V(ii,ndx(end));
            catch
                keyboard
            end
        end
    end
    
end
