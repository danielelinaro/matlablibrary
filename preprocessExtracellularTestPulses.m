function preprocessExtracellularTestPulses(DEBUG)

if ~exist('DEBUG','var')
    DEBUG = false;
end

files = listH5Files;
nfiles = length(files);
Fc = 1000;

EPSP_taur = nan(nfiles,1);
EPSP_taud = nan(nfiles,1);
EPSP_amplitude = nan(nfiles,1);
EPSP_slope = nan(nfiles,1);
EPSP_delay = nan(nfiles,1);

fit_duration = 0.5;

figure;
hndl = subplot(2,6,[1:3,7:9]);

for i=1:nfiles
    [entities,info] = loadH5Trace(files{i});
    t = (0:length(entities(1).data)-1) * info.dt;
    nent = length(entities);
    for j=1:nent
        switch entities(j).name
            case 'AnalogInput'
                V = filterTrace(entities(j).data,1/info.dt,Fc,2);
            case 'Waveform'
                t0 = entities(j).metadata(1,1);
                F = abs(entities(j).metadata(2,4));
                stim_dur = entities(j).metadata(2,1);
                N = stim_dur * F;
        end
    end
    if N == 1 && max(V(t>t0 & t<t0+fit_duration)) < -40
        fprintf(1, '[%02d/%02d] %s contains 1 pulse: fit window = %g ms.\n', ...
            i, nfiles, files{i}, fit_duration*1e3);
        [EPSP_taur(i),EPSP_taud(i),EPSP_amplitude(i),EPSP_slope(i),EPSP_delay(i)] = ...
            fitEPSP(t,V,t0,fit_duration,[],hndl,DEBUG);
    else
        if N > 1
            fprintf(1, '[%02d/%02d] skipping %s because it contains %d pulses.\n', ...
                i, nfiles, files{i}, N);
        elseif max(V) >= -40
            fprintf(1, '[%02d/%02d] skipping %s because it likely contains spikes (max Vm = %g mV).\n', ...
                i, nfiles, files{i}, max(V));
        end
    end
end

idx = ~isnan(EPSP_amplitude) & EPSP_amplitude >= 0.5 & EPSP_amplitude < 10 & ...
    EPSP_taud > 1e-3 & EPSP_taur >= 0.001e-3 & EPSP_delay < 20e-3 & ...
    (EPSP_taud./EPSP_taur >= 7 | (EPSP_taur > 10e-3 & EPSP_taud./EPSP_taur >= 3));
EPSP_amplitude = EPSP_amplitude(idx);
EPSP_slope = EPSP_slope(idx);
EPSP_taud = EPSP_taud(idx);
EPSP_taur = EPSP_taur(idx);
EPSP_delay = EPSP_delay(idx);

save('extracellular.mat','EPSP_*');

subplot(2,6,4);
hold on;
plot(0.5+[0,1],mean(EPSP_amplitude)+[0,0],'k','LineWidth',2);
plotSpread(EPSP_amplitude);
ylim([0,10]);
ylabel('EPSP amplitude (mV)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',[]);
box off;
title(sprintf('n=%d',length(EPSP_amplitude)));
subplot(2,6,5);
plot(0.5+[0,1],mean(EPSP_taur*1e3)+[0,0],'k','LineWidth',2);
plotSpread(EPSP_taur*1e3);
ylabel('EPSP tau_r (ms)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',[]);
box off;
title(sprintf('n=%d',length(EPSP_taur)));
subplot(2,6,6);
plot(0.5+[0,1],mean(EPSP_taud*1e3)+[0,0],'k','LineWidth',2);
plotSpread(EPSP_taud*1e3);
ylabel('EPSP tau_d (ms)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',[]);
box off;
title(sprintf('n=%d',length(EPSP_taud)));
subplot(2,6,10);
plot(0.5+[0,1],mean(EPSP_delay*1e3)+[0,0],'k','LineWidth',2);
plotSpread(EPSP_delay*1e3);
ylabel('EPSP delay (ms)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',[]);
box off;
title(sprintf('n=%d',length(EPSP_delay)));
subplot(2,6,11);
plot(0.5+[0,1],mean(EPSP_slope)*1e-3+[0,0],'k','LineWidth',2);
plotSpread(EPSP_slope*1e-3);
ylabel('EPSP slope (mV/ms)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',[]);
box off;
title(sprintf('n=%d',length(EPSP_slope)));

set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,6,4],'PaperSize',[6,4]);
print('-dpdf','extracellular.pdf');
