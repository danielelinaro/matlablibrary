function acc = computeAccommodationIndex(tspks,k)
% acc = computeAccommodationIndex(tspks,k)

ntrials = length(tspks);
acc = nan(ntrials,1);
all_isi = cellfun(@(x) diff(x), tspks, 'UniformOutput', 0);
for i=1:ntrials
    if length(all_isi{i}) < k+2
        continue;
    end
    isi = all_isi{i};
    isi = isi(:)';
    N = length(isi);
    num = diff(isi);
    den = sum([isi(2:end) ; isi(1:end-1)]);
    idx = k+1:N-1;
    acc(i) = sum(num(idx) ./ den(idx)) / (N-k-1);
end

