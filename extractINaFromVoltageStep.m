function [INa,Vh,I0] = extractINaFromVoltageStep(t,I,V,t0,dur)
% [INa,Vh,I0] = extractINaFromVoltageStep(t,I,V,t0,dur)

idx = find(t>t0 & t<t0+min(dur,20e-3));
Vh = V(:,idx(1));
I0 = mean(mean(I(:,t<20e-3)));
INa = min(I(:,idx),[],2);
[Vh,idx] = sort(Vh);
INa = INa(idx);
