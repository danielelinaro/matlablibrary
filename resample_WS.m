function [newTrace, newTime] = resample_WS(oldTrace, oldSamplingRate, newSamplingRate, varargin)
% This function will use the Whittaker�Shannon interpolation formula to
% resample your data :  
%             inf
% x_out(t) =  SUM { x_input[n] * sinc((t-nT)/T) }  
%             n=-inf
%
% , where n is the number of sinc celements for reconstruction, T is the 
% input sampling interval (1/oldSamplingRate) and t time (continuous or
% time instants in the newly sampled trace). "n" is also the number of samples
% available in x_input and is akso called kernel size, since it defines 
% how many sinc elements make up the kernel. 
%
% NOTE : 
%
% - how this works : basically, for each sample point in x_input, x_input(i), one
% calculates one sinc function, based on all other avaliable sample points
% This sinc function is peak-centered at the time of x_input(i) and has 
% its zero-crossings at the times of all other sample points. 
% Each sinc function is scaled with the current sample point x_input(i) 
% (it gets its peak height from the sample point x_input(i)). 
% Once this has been performed for all sample points, these sinc functions 
% are added up to result in the output trace (or continuous function, in limit of infinity).
%
% - reconstruction of an infinite number of samples is needed to recreate a
% perfect, continuous function x_out(t), as one can see above. However,
% since one only has a limited number of samples in x_input, one can only
% use these, meaning the best one can do is to calculate one sinc function
% for each sample point x_input, having thus "n" such functions. Thus "n" 
% is the kernel size, which equals the number of sample points.
%
% - You need to have at least n=100 sample points in your input x_input, in
% order to have enough sinc functions to result in smooth and realistic
% output, maching the input signal well. You may add some values to each
% side of your input signal to make it longer, and cut away the results of
% these additions afterwards. Like this you also get rid of edge-effects.
% The mean of the first and last few data points in your sample, repeated a
% few times and added to the beginning and the end of your signal,
% respectively, should do the job. See "padding" in the algorithm below.
%
% - The output will contain a finer resolution, but NOT more
% information, i.e. the aquisition frequency will still determine the
% maximal signal frequency you are able to observe, despite up-sampling
% here artificially.

% INPUTS:
% - oldTrace : data points of your trace, sampled at time intervals of
% 1/oldSamplingRate.
% - newSamplingRate :  1/newSamplingRate gives the time intervals of
% the returned and upsampled trace in "newTrace".
% - VARARGIN : < <Name>, <value> >  pairs, as < 'plotFitHandle', 67 > , withouth the <>.

% Istvan Biro, 2013.03.01.



%% PARAMETERS and DEFAULTS
paddingNoOfPoints = 50;    % padding number of data points to each side of your trace to avoid edge effects, as described above.


%% EVALUATE VARAGIN
% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected. These will than be usable in the script, as if they were defined
% in it, by their names. Use the same to overwrite default values defined before this point
% in the script (function). E.g. : the above example is equivalent to having
%  plotFitHandle = 67; in your script (function). 
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%% PREPARE

% create times and signal vectors
oldTrace = [ mean(oldTrace(1:5)).*ones(1,paddingNoOfPoints)  oldTrace  mean(oldTrace(end-5:end)).*ones(1,paddingNoOfPoints) ] ;  % padding if needed
oldTime = [0:1/oldSamplingRate:1/oldSamplingRate*(length(oldTrace)-1)];  % time vector of old trace

newSampleSize = floor( length(oldTrace) * newSamplingRate/oldSamplingRate ); % how many data points the resampled trace will have
newTime = [0:1/newSamplingRate:1/newSamplingRate*(newSampleSize-1)]; % time vector for new trace
newTrace = zeros(1,newSampleSize);  % initialized trace

%% PROCESSING INTERPOLATION
for i=1:length(oldTrace)
    newTrace =  newTrace + oldTrace(i) * sinc((newTime-oldTime(i))*oldSamplingRate);
end


%% REMOVE PADDING IF NEEDED
overFlow = floor(paddingNoOfPoints*newSamplingRate/oldSamplingRate) + 1; % ampount we need to cut away due to padding
usefulSize = floor(  (length(oldTrace) - 2*paddingNoOfPoints ) * (newSamplingRate/oldSamplingRate)  );
newTrace = newTrace(overFlow:overFlow+usefulSize-1);
newTime = newTime(overFlow:overFlow+usefulSize-1); 
newTime = newTime - newTime(1);  % make it start from 0 


end