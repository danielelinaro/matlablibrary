function [tau_rise, tau_decay, amplitudes, event_times, Vwndw, Vmodel] = extractEPSPs(t,V,threshold_factor,wndw,interactive)
% [tau_rise, tau_decay, amplitudes, event_times, Vwndw, Vmodel] = extractEPSPs(t,V,threshold_factor,wndw,interactive)

warning('OFF', generatemsgid('noPeaks'));

if nargout == 6
    Vwndw = [];
    Vmodel = [];
end

if ~ exist('threshold_factor','var') || isempty(threshold_factor)
    threshold_factor = 5;
end

if ~ exist('wndw','var') || isempty(wndw)
    wndw = [5e-3,200e-3];
end

if ~ exist('interactive','var')
	interactive = 0;
end

dt = diff(t(1:2));
n = size(V,1);
before = round(wndw(1)/dt);
after = round(wndw(2)/dt);
options = optimset('TolX', 1e-12, 'TolFun', 1e-12, 'Display', 'off');

tau_rise = [];
tau_decay = [];
amplitudes = [];
event_times = cell(n,1);

for i=1:n
    m = mean(V(i,:));
    s = computeThreshold(V(i,:)-m);
    thresh = m+threshold_factor*s;
    
    if max(V(i,:)) <= thresh
        continue
    end
    try
        [~,locs] = findpeaks(V(i,:), 'MinPeakHeight', thresh, 'MinPeakDistance', round(20e-3/diff(t(1:2))));
    catch
        continue
    end
    locs = locs(V(i,locs) < -30);
    for j=1:length(locs)
        idx = locs(j)-before:locs(j)+after;
        idx = idx(idx > 0 & idx <= length(V));
        epsp = V(i,idx) - V(i,idx(1));
        amp = max(epsp);
        epsp = epsp / amp;
        T = (0:length(epsp)-1)*dt;
        pars  = [0.001, 0.01, 0.001];
        opt = 1e12;
        for jj=1:5
            [x,fval] = fminsearch(@(x) EPSPCost(x,T,epsp), pars .* (1 + 0.1*rand(size(pars))), options);
            if fval < opt
                pars = x;
                opt = fval;
            end
        end
        pars(1:2) = sort(pars(1:2));
        accept = 1;
        if interactive
            model = doubleexp(pars(1),pars(2),pars(3),T);
            if pars(2)<1e-3 || pars(2)>1
                continue;
            end
%             figure(hndl);
            clf;
            hold on;
            plot(T*1e3,epsp,'k');
            plot(T*1e3,model,'r');
            xlabel('t (ms)');
            ylabel('Normalized V');
            axis tight;
            box off;
            set(gca, 'YTick', [0,1]);
            set(gcf, 'Color', [1,1,1]);
            title(sprintf('Tau_{decay} = %.2f ms', pars(2)*1e3));
            response = input(sprintf('[%02d/%02d] [%03d/%03d] Accept EPSP? (y/N) ', i, n, j, length(locs)),'s');
            if isempty(response) || (~strcmpi(response,'yes') && ~strcmpi(response,'y'))
                accept = 0;
            end
        end
        if accept
            event_times{i} = [event_times{i}, t(locs(j))];
            amplitudes = [amplitudes, amp];
            tau_rise = [tau_rise, pars(1)];
            tau_decay = [tau_decay, pars(2)];
            if exist('Vwndw','var')
                Vwndw = [Vwndw; epsp*amp + V(i,idx(1))];
                Vmodel = [Vmodel; model*amp + V(i,idx(1))];
            end
        end
    end
end

% if ~ interactive
%     m = mean(tau_decay);
%     s = std(tau_decay);
%     idx = find(tau_decay > m-s & tau_decay < m+s);
%     tau_decay = tau_decay(idx);
%     tau_rise = tau_rise(idx);
%     amplitudes = amplitudes(idx);
%     
%     m = mean(tau_rise);
%     s = std(tau_rise);
%     idx = find(tau_rise > m-s & tau_rise < m+s);
%     tau_decay = tau_decay(idx);
%     tau_rise = tau_rise(idx);
%     amplitudes = amplitudes(idx);
% end

warning('ON', generatemsgid('noPeaks'));
