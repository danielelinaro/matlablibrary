function plotTracesAndStimuli(t,V,stim)
% plotTracesAndStimuli(t,V,stim)

dt = diff(t([1,end]))/10;
dV = (max(max(V))-min(min(V)))/10;
if size(V,1) == 1 || all(stim(1,:) == stim(2,:))
    dI = (max(max(stim))-min(min(stim)))/10;
    singleStim = 1;
else
    dI = max(stim(2,:)) - max(stim(1,:));
    singleStim = 0;
end
base = -70;

n = size(V,1);
cmap = jet(n);
axes('Position',[0.1,0.1,0.5,0.8],'NextPlot','Add',...
    'FontName','Arial','FontSize',9);
plot(t([1,end]),base+[0,0],'--','Color',[.4,.4,.4],'LineWidth',2);
text(-dt/3,base,sprintf('%.0f mV',base),'HorizontalAlignment','Right');
for k=1:n
    plot(t,V(k,:),'Color',cmap(k,:));
end
axis tight;
xl = xlim;
yl = ylim;
axis([xl,yl(1)-2*dV,yl(2)]);
xl(1) = xl(2)-dt;
yl = ylim;
yl(2) = yl(1)+dV;
plot(xl,yl(1)+[0,0],'k','LineWidth',2);
plot(xl(1)+[0,0],yl,'k','LineWidth',2);
axis off;
text(xl(1)-dt/3,sum(yl)/2,sprintf('%.0f mV',dV),'HorizontalAlignment','Right');
text(sum(xl)/2,yl(1)-dV/2,sprintf('%.2f s',dt),'HorizontalAlignment','Center');

axes('Position',[0.7,0.25,0.2,0.4],'NextPlot','Add',...
    'FontName','Arial','FontSize',9);
if singleStim
    plot(t,stim(1,:),'k','LineWidth',2);
else
    for k=1:n
        plot(t,stim(k,:),'Color',cmap(k,:),'LineWidth',2);
    end
end
axis tight;
xl = xlim;
yl = ylim;
axis([xl,yl(1)-2*dI,yl(2)]);
xl(1) = xl(2)-dt;
yl = ylim;
yl(2) = yl(1)+dI;
plot(xl,yl(1)+[0,0],'k','LineWidth',2);
plot(xl(1)+[0,0],yl,'k','LineWidth',2);
axis off;
text(xl(1)-dt/2,sum(yl)/2,sprintf('%.0f pA',dI),'HorizontalAlignment','Right');
text(sum(xl)/2,yl(1)-dI/2,sprintf('%.2f s',dt),'HorizontalAlignment','Center');
    