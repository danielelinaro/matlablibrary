function C = computeCapacitance(t,V,offset,R,doPlot)
% C = computeCapacitance(t,V,offset,R)

if ~ exist('doPlot','var')
    doPlot = 0;
end

Vm = mean(V(max(V,[],2) < -20,:));
[m,i] = max(Vm);
idx = find(t >= offset & t <= t(i));
x = t(idx) - offset;
y = -(Vm(idx) - Vm(idx(end)));
y = y/y(1);
idx = find(y > 0.1);
p = polyfit(x(idx),log(y(idx)),1);
C = -1/(R*p(1));

if doPlot
    figure;
    hold on;
    plot(x(idx),y(idx),'r.');
    plot(x(idx),exp(p(2))*exp(p(1)*x(idx)),'k');
end
