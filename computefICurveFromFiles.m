function [I,f,gain,rheobase,a,b,c,Iint,CV,CVm] = computefICurveFromFiles ...
    (files,Ke,thresh,ttran,doPlot,onlyStimulus)
% [I,f,slope,rheobase,a,b,c,Iint,CV] = 
%   computefICurveFromFiles(files,Ke,thresh,ttran,doPlot,onlyStimulus)
%
% Arguments:
%        files - the files to read.
%           Ke - the kernel electrode to use. If Ke = [], then the traces are
%                assumed to be already compensated.
%       thresh - the threshold for spike detection.
%        ttran - transient duration to discard when counting spikes
%                (default 1 second, and values < 0 instruct the function to
%                compute the frequency as the inverse of the first ISI.
%       doPlot - whether to make a plot of the results. The default is no.
% onlyStimulus - whether to use only the additional stimulus current or all
%                the currents that were injected into the neuron. The
%                default is yes.
% 
% Returns:
%         I - the values of injected current.
%         f - the corresponding firing rate, after ttran seconds.
%      gain - the gain of the f-I curve in Hz/pA.
%  rheobase - the current threshold for sustained firing in pA.
%     a,b,c - the coefficients of a power-law fit to the f-I curve of the
%             form f = a*I^b + c.
%      Iint - the interval of current used to compute the fit of the f-I
%             curve.
%        CV - the coefficient of variation of the ISIs, for each value of 
%             injected current.
%       CVm - the mean CV for those values of injected current used in the
%             fit of the f-I curve.
% 
% See also computefICurve.
%

% Author: Daniele Linaro - January 2012

if ~ exist('thresh','var')
    thresh = -20;
end
if ~ exist('ttran','var')
    ttran = 1;
end
if ~ exist('doPlot','var')
    doPlot = 0;
end
if ~ exist('onlyStimulus','var') || strcmpi(onlyStimulus,'y') || strcmpi(onlyStimulus,'yes')
    onlyStimulus = 1;
else
    onlyStimulus = 0;
end

if isstruct(files(1))
    files =  arrayfun(@(x) x.name, files, 'UniformOutput', 0);
end

if length(files) == 1 && strcmp(files{1},'recording.mat')
    data = load('recording.mat');
    t = (0:size(data.V,2)-1) * data.dt;
    Vc = data.V;
    Iinj = data.I;
    stim = cell(size(Vc,1),1);
    j = find(t > data.tstart,1);
    for i=1:length(stim)
        stim{i} = zeros(3,12);
        stim{i}(:,2) = 1;
        stim{i}(1,1) = data.tstart;
        stim{i}(2,1) = data.dur;
        stim{i}(3,1) = t(end) - data.tstart - data.dur + data.dt;
        stim{i}(2,3) = data.I(i,j);
    end
else
    if ~ isempty(Ke)
        [t,Vr,Iinj,stim] = loadH5Traces(files);
        Vc = AECoffline(Vr,Iinj,Ke);
    else
        n = length(files);
        stim = cell(n,1);
        for i=1:n
            [entities,info] = loadH5Trace(files{i});
            if i == 1
                Vc = repmat(entities(1).data,[n,1]);
                Iinj = repmat(entities(end).data,[n,1]);
            else
                Vc(i,:) = entities(1).data;
                Iinj(i,:) = entities(end).data;
            end
            stim{i} = entities(end).metadata;
        end
        t = (0:size(Vc,2)-1) * info.dt;
    end
end

% Voff = zeros(size(Vc,1),1);
% for k=1:size(Vc,1)
%     Voff(k) = mean(Vc(k,t < stim{1}(1)));
%     Vc(k,:) = Vc(k,:) - Voff(k);
% end
% Vc = Vc + mean(Voff);
Fc = 1000;
Fs = 1/diff(t(1:2));
N = Fs/Fc*2;
padding = repmat(Vc(:,1),[1,N]);
Vc = filterTrace(Vc,Fs,Fc,2);
Vc(:,1:N) = padding;
if onlyStimulus
    [I,f,~,~,CV] = computefICurve(t,Vc,[],stim,thresh,ttran);
else
    [I,f,~,~,CV] = computefICurve(t,Vc,Iinj,stim,thresh,ttran);
end

if sum(f(I>0) > 0) > 1
    [Iu,~,idx] = unique(I);
    fu = zeros(size(Iu));
    CVu = zeros(size(Iu));
    for i=1:length(Iu)
        if std(f(idx==i)) > mean(f(idx==i))/3
            jdx = find(idx == i);
            [fu(i),j] = max(f(jdx));
            CVu(i) = CV(jdx(j));
        else
            jdx = idx == i;
            fu(i) = mean(f(jdx));
            CVu(i) = mean(CV(jdx));
        end
    end
    idx = find(fu > 0);
    if idx(1) == 1
        rheobase_guess = Iu(idx(1));
    else
        rheobase_guess = sum(Iu(idx(1)-1:idx(1)))/2;
    end
    Iu = Iu(idx);
    fu = fu(idx);
    CVu = CVu(idx);
    idx = 1:length(I);
    if length(Iu) > 1
        while 1
            jdx = [1;find(diff(fu)./fu(1:end-1) > -0.1)+1];
            Iu = Iu(jdx);
            fu = fu(jdx);
            CVu = CVu(jdx);
            if length(idx) == length(jdx) || length(jdx) == 1
                break
            end
            idx = jdx;
        end
    end
    CVm = nanmean(CVu);
    [a,b,c,II,ff,rb,rss] = fitFICurve(Iu,fu,[rheobase_guess,rheobase_guess*0.9]);
    if isreal(rb)
        rheobase = rb;
    else
        rheobase = (-c/a)^(1/b);
    end
    gain = a*b*mean(II)^(b-1);
    if isnan(gain)
        gain = 0;
    end
    Iint = II([1,end]);
else
    a = nan;
    b = nan;
    c = nan;
    rheobase = nan;
    gain = nan;
    rss = nan;
    Iint = [nan,nan];
    CVm = nan;
end

if doPlot
    figure;
    
    axes('Position',[0.1,0.1,0.85,0.3],'NextPlot','Add','TickDir','Out',...
        'LineWidth',0.8,'FontName','Arial','FontSize',10);
    plot(I,f,'ro','MarkerFaceColor','r','MarkerSize',4);
    if exist('II','var')
        plot(II,ff,'k','LineWidth',1);
        plot(Iu,fu,'ks','MarkerFaceColor','k','MarkerSize',5);
    end
    axis tight;
    axis([xlim,0,max(max(f)*1.05,1)]);
    if ~isnan(rss)
        title(sprintf('CV = %.2f', CVm));
    end
    plot(rheobase+[0,0],ylim,'g--','LineWidth',1);
    set(gcf,'Color','w');
    xlabel('I (pA)');
    ylabel('f (spikes/s)')
    Istep = max(diff(sort(cellfun(@(x) x(end-1,3), stim))));
        
    axes('Position',[0.1,0.6,0.85,0.35],'NextPlot','Add','TickDir','Out',...
         'LineWidth',0.8,'FontName','Arial','FontSize',9);
    plot(t,Vc,'k');
    axis tight;
    axis([xlim,ylim+[-5,5]]);
    axis off;
    placeScaleBars(t(end)-0.2,max(max(Vc))-50,0.2,40,'200 ms','40 mV','k','LineWidth',1);
    
    axes('Position',[0.1,0.475,0.85,0.1],'NextPlot','Add','TickDir','Out',...
         'LineWidth',0.8,'FontName','Arial','FontSize',9);
    plot(t,Iinj,'k');
    placeScaleBars(t(end)-0.2,max(max(Iinj))-1.2*4*Istep,[],4*Istep,'',...
        sprintf('%d pA',4*Istep),'k','LineWidth',1);
    axis tight;
    axis off;

    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,9,6],...
        'PaperSize',[9,6]);
    print('-dpdf','fI_curve.pdf');
end
