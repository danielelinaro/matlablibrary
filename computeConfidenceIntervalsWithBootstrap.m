function CI = computeConfidenceIntervalsWithBootstrap(events, binwidth, interval, n_bootstrap, n_subset)
% CI = computeConfidenceIntervalsWithBootstrap(events, binwidth, interval, n_bootstrap, n_subset)

if length(events) < n_subset
    error('n_subset must be smaller than the number of events');
end

[nu,~,count] = psth(events,binwidth,interval);

N = size(count,1);
COUNT = zeros(n_bootstrap,size(count,2));
for i=1:n_bootstrap
    idx = randperm(N);
    COUNT(i,:) = sum(count(idx(1:n_subset),:));
    if mod(i,100) == 0
        fprintf(1, '.');
    end
    if mod(i,2000) == 0
        fprintf(1, '\n');
    end
end

coeff = nanmean(nanmean(COUNT ./ nu));
CI = zeros(2,size(nu,2));
for i=1:size(nu,2)-1
    pd = fitdist(COUNT(:,i),'poisson');
    x = 0:3*pd.lambda;
    y = cdf(pd,x);
    try CI(1,i) = find(y > 0.05, 1, 'first'); catch, end
    try CI(2,i) = find(y <= 0.95, 1, 'last'); catch, end
end
CI = CI / coeff;
