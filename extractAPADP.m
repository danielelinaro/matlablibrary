function [tadp,vadp] = extractAPADP(T,V,max_adp_dur,thresh,tth,tahp)
% EXTRACTAPADP finds the value of the ADP.
% 
% [tahp,vahp] = extractAPAHP(T,V,max_adp_dur,thresh,tth,tahp)
% 
% Parameters:
%            T - time vector, with size (1xN) where N is the number of samples.
%            V - voltage trace(s), with size (MxN), where M is the number of 
%                traces and N is the number of samples.
%  max_adp_dur - maximal duration of the ADP, from the preceding AHP (in seconds, default 15e-3).
%       thresh - threshold for spike detection: it can either be an (Mx1) array or a scalar.
% 
% Return values:
%  tadp - a cell array [of size (Mx1)] with the times of the AHP voltages.
%  vadp - a cell array [of size (Mx1)] with the AHP voltages.
%

% Daniele Linaro - December 2014

if ~ exist('max_adp_dur','var') || isempty(max_adp_dur)
    max_adp_dur = 15e-3;
end
if ~ exist('thresh','var')
    thresh = [];
end
if ~ exist('tth','var') || isempty(tth)
    tth = extractAPThreshold(T,V,thresh);
end
if ~ exist('tahp','var') || isempty(tahp)
    tahp = extractAPAHP(T,V,[],thresh);
end

nexp = size(V,1);
tadp = cell(nexp,1);
vadp = cell(nexp,1);
nspks = cellfun(@(x)length(x), tth);

if ~ all(cellfun(@(x) length(x), tth) == cellfun(@(x) length(x), tahp))
    keyboard
end

for ii=1:nexp

    if nspks(ii) == 0
        continue;
    end
    
    tadp{ii} = zeros(1,nspks(ii));
    vadp{ii} = zeros(1,nspks(ii));

    for jj=1:nspks(ii)-1
        stop = min(tth{ii}(jj+1)-tahp{ii}(jj),max_adp_dur);
        idx = find(T >= tahp{ii}(jj) & T <= tahp{ii}(jj)+stop);
        [vadp{ii}(jj),k] = max(V(ii,idx));
        tadp{ii}(jj) = T(idx(k));
    end
    idx = find(T > tahp{ii}(end) & T < tahp{ii}(end)+max_adp_dur);
    if isempty(idx)
        vadp{ii}(end) = V(ii,end);
        tadp{ii}(end) = tahp{ii}(end);
    else
        [vadp{ii}(end),k] = max(V(ii,idx));
        tadp{ii}(end) = T(idx(k));
    end
end
