function neuron = makeNeuronStruct(varargin)
% makeNeuronStruct creates and fills a structure whose fields
% correspond to the fields in a record in the database.
% 
% neuron = makeNeuronStruct(options)
% 
% Required options are:
%  CellSpecies - a string that identifies the species of the cell.
%   AnimalType - a string that identifies the animal used in the experiment.
%                Presently accepted values are 'wistar', 'bl6', 'nod/scid'.
%    AnimalAge - the age of the animal at the time of the experiment.
%   NeuronType - a string that identifies the type of neuron (e.g., 'pyramidal').
%  BrainRegion - a string that identifies the brain region (e.g., 'cortex').
%  ProjectName - the name of the project the experiment belongs to.
% 
% Optional values are:
%       Folder - where to look for data relative to the cell. (default '.').
%    BrainArea - a string that identifies the brain area (e.g., 'S1').
%     Location - a string that identifies the location in the brain area (e.g.,
%                'L5' or 'L2/3' in the cortex).
%       CellId - a string used as identifier of the cell. If no value is passed,
%                the name of the current directory is used.
%     CellDate - the date in which the experiment was performed, in the form
%                YYYY/MM/DD. If no value is passed, the date of creation of
%                the working directory is used.
% Experimenter - name and surname of the experimentalist. If no value is
%                passed, the name of the author of this function is used
%                (Daniele Linaro).
%  Preparation - the type of preparation ('culture', 'in vivo' or 'in vitro', which is
%                the default).
%        Virus - the virus used to infect the cell.
% Differentiation - the ID of the differentiation.
% 
% Returns:
%    A structure that can be passed to the function addNeuronToDB for
% inclusion in the database.
% 
% Example:
% 
%    neuron = makeNeuronStruct('Experimenter','Daniele Linaro',...
%         'AnimalType','Wistar','AnimalAge',18,...
%         'NeuronType','Pyramidal','BrainRegion','Cortex','BrainArea','S1',...
%         'Location','L5','CellId','20130226A01','CellDate','20130226',...
%         'ProjectName','sinusoids','Preparation','In vitro');
% 
% See also makeBaseNeuronStruct, addNeuronToDB.
% 

% Author: Daniele Linaro - February 2013.

p = inputParser;
p.addParameter('CellSpecies', '', @ischar);
p.addParameter('AnimalType', '', @ischar);
p.addParameter('AnimalAge', [], @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('CellAge', [], @(x) isnumeric(x) && isscalar(x) && x>0);
p.addParameter('NeuronType', '---', @ischar);
p.addParameter('ProjectName', 'misc', @ischar);
p.addParameter('Folder', '.', @ischar);
p.addParameter('Experimenter', 'Daniele Linaro', @ischar);
p.addParameter('BrainRegion', '---', @ischar);
p.addParameter('BrainArea', '---', @ischar);
p.addParameter('Location', '---', @ischar);
p.addParameter('Preparation', 'In vitro', @ischar);
p.addParameter('CellID', '', @ischar);
p.addParameter('CellDate', '', @ischar);
p.addParameter('Virus', '', @ischar);
p.addParameter('Treatment', '', @ischar);
p.addParameter('Differentiation', '', @ischar);
p.parse(varargin{:});

if isempty(p.Results.CellSpecies)
    error('You must specify the species of the cell.');
end

if isempty(p.Results.CellAge) && isempty(p.Results.AnimalAge)
    error('You must specify at least one of AnimalAge and CellAge.');
end

cell_age = p.Results.CellAge;
if isempty(cell_age)
    cell_age = p.Results.AnimalAge;
end

if isempty(p.Results.CellID)
    wd = pwd;
    idx = strfind(wd,'/');
    p.Results.CellID = wd(idx(end)+1:end);
end
if isempty(p.Results.CellDate)
    listing = dir('.');
    p.Results.CellDate = datestr(listing(1).date,'yyyymmdd',26);
end

neuron = makeBaseNeuronStruct(p.Results.CellID, p.Results.CellDate, p.Results.CellSpecies, p.Results.Experimenter, ...
    p.Results.AnimalType, p.Results.AnimalAge, cell_age, p.Results.BrainRegion, p.Results.BrainArea, ...
    p.Results.Location, p.Results.NeuronType, p.Results.ProjectName, p.Results.Preparation, ...
    p.Results.Virus, p.Results.Treatment, p.Results.Differentiation);

text_fields = {'normalized_spike_times_at_max_rate',...
    'normalized_spike_amplitudes_at_max_rate',...
    'normalized_spike_times',...
    'normalized_spike_amplitudes'};

if exist([p.Results.Folder,'/cell.mat'],'file')
    data = load([p.Results.Folder,'/cell.mat']);
    for fld = fieldnames(data)'
        if isnan(data.(fld{1}))
            neuron.(fld{1}) = [];
        else
            if any(cellfun(@(x) strcmp(x,fld{1}), text_fields)) || isnumeric(data.(fld{1})) && ~isscalar(data.(fld{1}))
                % serialize double arrays
                neuron.(fld{1}) = sprintf('%02X',getByteStreamFromArray(data.(fld{1})));
                if strcmp(p.Results.CellID,'20161201C01') && strcmp(fld{1},'sag')
                    keyboard
                end
            else
                neuron.(fld{1}) = data.(fld{1});
            end
        end
    end
else
    try
        data = load([p.Results.Folder,'/vi/01/VI_curve.mat'],'R','V0','sag_ratio','I');
        neuron.Rin = data.R.value;
        neuron.Vm = data.V0.value;
        neuron.sag_ratio = nanmean(data.sag_ratio(data.I<0));
    catch
    end
    
    try
        data = load([p.Results.Folder,'/tau/01/tau.mat'],'taus');
        neuron.tau = max(data.taus)*1e3;
    catch
    end
    
    try
        data = load([p.Results.Folder,'/ap/01/ap.mat'],'features');
        neuron.AP_amplitude = data.features.amplitude.Vm;
        neuron.AP_peak = data.features.peak.Vm;
        neuron.AP_duration = data.features.halfwidth.wm;
        neuron.AP_threshold = data.features.threshold.Vm;
        neuron.AP_rise_time = data.features.rise_time.tm;
        neuron.AP_rise_rate = data.features.rise_rate.mean;
    catch
    end
    
    try
        data = load([p.Results.Folder,'/fi/01/fI_curve.mat']);
    catch
        try
            data = load([p.Results.Folder,'/steps/01/fI_curve.mat']);
        catch
            data = [];
        end
    end
    if ~ isempty(data)
        if isfield(data,'models')
            neuron.fI_gain = data.models{1}.gain;
        elseif isfield(data,'model')
            neuron.fI_gain = data.model{1}.gain;
        elseif isfield(data,'slope')
            neuron.fI_gain = data.slope.value;
        elseif isfield(data,'p')
            neuron.fI_gain = data.p(1);
        else
            cwd = pwd;
            idx = strfind(cwd,'/');
            fprintf(1, 'Unknown data format in [./%s/fi/01/fI_curve.mat].\n', cwd(idx(end)+1:end));
        end
    end
    
    try
        data = load([p.Results.Folder,'/ramp/01/ramp.mat'],'Ithresh');
        neuron.threshold = mean(data.Ithresh);
    catch
    end
    
    try
        data = load([p.Results.Folder,'/spontaneous/01/spontaneous.mat']);
        if isfield(data,'AP')
            neuron.AP_amplitude = data.AP.amplitude.Vm;
            neuron.AP_peak = data.AP.peak.Vm;
            neuron.AP_duration = data.AP.halfwidth.wm;
            neuron.AP_threshold = data.AP.threshold.Vm;
            neuron.AP_rise_time = data.AP.rise_time.tm;
            neuron.AP_rise_rate = data.AP.rise_rate.mean;
        end
        neuron.Vm = data.Vm.mean;
    catch
    end
    
    try
        data = load([p.Results.Folder,'/rin/01/Rin.mat']);
        neuron.Rin = data.R;
    catch
    end
end
