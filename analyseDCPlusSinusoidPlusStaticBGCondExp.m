function [F,r0,r1,phi,conf,r0_shuffled,r1_shuffled,phi_shuffled,TF] = ...
    analyseDCPlusSinusoidPlusStaticBGCondExp(...
    ttran, nPeriods, nBins, saveSpikeTimes, loadSpikeTimes, do_plot, spikesFile)
% 
% ANALYSEDCPLUSSINUSOIDPLUSSTATICBGCONDEXP performs the analysis of an
% experiment in which a DC + sinusoidal current where injected into a
% neuron when also injection balanced background conductances. This
% function reads all the files in the current directory, extracts the spike
% times and constructs PSTHs which are then fit (over a given number of periods)
% by a function of the form
% 
% f(t) = r0 + r1*sin(2*pi*f*t + phi)
% 
% where f is the frequency of the sinusoidal component of the injected
% current.
% 
% Usage:
% [F,r0,r1,phi,conf,r0_shuffled,r1_shuffled,phi_shuffled,conf_shuffled] = ...
%     analyseDCPlusSinusoidPlusStaticBGCondExp( ...
%     ttran, nPeriods, nBins, saveSpikeTimes, loadSpikeTimes, do_plot)
% 
% Arguments:
%          ttran - the duration of the transient after the onset of the
%                  stimulation. Spikes in the transient will be discarded
%                  (default is 2 sec).
%       nPeriods - the number of periods of the sinusoidal component over
%                  which the PSTH should be computed (default is 1).
%          nBins - the number of bins used for computing the PSTH (default is
%                  10*nPeriods).
% saveSpikeTimes - whether to save the spike times extracted from the data
%                  files (default is yes).
% loadSpikeTimes - whether to load the spike times previously saved to file
%                  (default is no).
%        do_plot - whether to plot the results of the analysis (default is yes).
%     spikesFile - name of the file containing the spike times (default
%                  spikes_ap_peak.mat, used only if loadSpikeTimes is 1).
% 
% Returns:
%              F - an array of the frequencies of the sinusoidal component
%                   used in the experiment.
%             r0 - the mean rate of firing of the neuron.
%             r1 - the amplitude of the modulation of the PSTH.
%            phi - the phase difference between the PSTH and the modulating
%                  sinusoidal current.
%           conf - the confidence intervals on r0, r1 and phi.
%    r0_shuffled - same as r0, but for shuffled spike times.
%    r1_shuffled - same as r1, but for shuffled spike times.
%   phi_shuffled - same as phi, but for shuffled spike times.
%             TF - a struct that contains the parameters of the rational
%                  transfer function used to fit the frequency response data.
%                  Its fields are A, Z and P.
% 
% See also fitTransferFunction.
% 

% Author: Daniele Linaro - October 2012
% Modified in June 2013 to include the fit of the tranfer function.

%%% default values for the arguments
if ~ exist('ttran','var')
    ttran = 2;
end
if ~ exist('nPeriods','var')
    nPeriods = 1;
end
if ~ exist('nBins','var')
    nBins = nPeriods*10;
end
if ~ exist('do_plot','var')
    do_plot = 1;
end
if ~ exist('saveSpikeTimes','var') || isempty(saveSpikeTimes)
    saveSpikeTimes = 1;
end
if ~ exist('loadSpikeTimes','var') || isempty(loadSpikeTimes)
    loadSpikeTimes = 1;
end
if ~ exist('spikesFile','var')
    SPIKES_FILE = 'spikes_ap_peak.mat';
else
    SPIKES_FILE = spikesFile;
end

fitMeanRate = 1;

if ~ loadSpikeTimes || ~ exist(SPIKES_FILE,'file')

    files = listH5Files;

    if ~ isempty(files)
        
        %%%%
        %%%% DANIELE'S DATA FORMAT
        %%%%

        nfiles = length(files);
        F = zeros(nfiles,1);
        dur = zeros(nfiles,1);
        spikes = cell(nfiles,1);
        for k=1:nfiles
            % load the data
            [entities,info] = loadH5Trace(files{k});
            Ke = [];
            for ii=1:length(entities)
                if strcmp(entities(ii).name,'RealNeuron')
                    % the voltage is already compensated
                    Vm = entities(ii).data;
                elseif strcmp(entities(ii).name,'AnalogInput')
                    kernels = dir('*_kernel.dat');
                    kernels_secs = arrayfun(@(x) datevec(x.name(1:14),'yyyymmddHHMMSS') ...
                        * [0;0;0;3600;60;1], kernels);
                    file_secs = datevec(files{k}(1:14),'yyyymmddHHMMSS') * [0;0;0;3600;60;1];
                    time_dst = abs(kernels_secs - file_secs);
                    [~,jj] = min(time_dst);
                    % the kernel closest in time
                    Ke = load(kernels(jj).name);
                    % the uncompensated voltage
                    Vr = entities(ii).data;
                elseif strcmp(entities(ii).name,'Waveform')
                    %%% let's check whether there is a sinusoidal modulation
                    idx = find(entities(ii).metadata(:,2) == 3, 1); % find the sine wave
                    if isempty(idx) && min(entities(ii).metadata(:,2)) < 0 % there are composite waveforms
                        idx = find(entities(ii).metadata(:,end-2) == 3, 1); % find the sine wave
                    end
                    if ~ isempty(idx) && entities(ii).metadata(idx,4) > 0
                        F(k) = entities(ii).metadata(idx,4);
                    end
                    if dur(k) == 0
                        dur(k) = max(entities(ii).metadata(:,1));
                    end
                    if strcmp(entities(ii).units,'pA')
                        % the current
                        I = entities(ii).data;
                        % the duration of the preamble
                        t0 = sum(entities(ii).metadata(1:5,1));
                    elseif strcmp(entities(ii).units,'nS')
                        dur(k) = max(entities(ii).metadata(:,1));
                    end
                end
            end
            if ~ isempty(Ke)
                % we need to compensate
                Vm = AECoffline(Vr,I,Ke);
            end
            t = (0:length(entities(1).data)-1) * info.dt;
%             idx = find(t < 0.5);
%             V0 = mean(Vm(idx));
%             if V0 < -80 || V0 > -40
%                 Vm = fixOffset(Vm,idx,-55);
%                 fprintf(1, 'Fixing the offset in the trace in file [%s].\n', files{k});
%             end
            % find the spikes
            events = extractAPPeak(t,Vm,-20);
            % remove the time at which the simulation started
            events = events{1}' - t0;
            % compute the correct transient time and remove the preceding spikes
            tr = round(ttran*F(k))/F(k);
            if tr ~= ttran
                fprintf(1, ['Using transient equal to %g seconds ', ...
                    '(rounded from %g seconds for a frequency of %g Hz).\n'], tr, ttran, F(k));
            end
            dur(k) = dur(k) - tr;
            % spike times minus the transient duration: note that the spike times
            % are also relative to the beginning of the sinusoidal modulation
            spikes{k} = events(events > tr) - tr;
        end
    else
        
        %%%%
        %%%% ISTVAN'S DATA FORMAT
        %%%%
        
        files = dir('*.mat');
        nfiles = length(files);
        F = zeros(nfiles,1);
        dur = zeros(nfiles,1);
        spikes = cell(nfiles,1);
        fprintf(1, 'Loading MAT files ');
        for k=1:nfiles
            data = load(files(k).name,'current','conductance','recorded','model');
            fprintf(1, '.');
            if length(find([data.current.I_omega,data.conductance.omega_exc,data.conductance.omega_inh] > 0)) == 1
                if data.current.I_omega ~= 0
                    F(k) = data.current.I_omega;
                elseif data.conductance.omega_exc ~= 0
                    F(k) = data.conductance.omega_exc;
                elseif data.conductance.omega_inh ~= 0
                    F(k) = data.conductance.omega_inh;
                end
            elseif data.conductance.omega_exc == data.conductance.omega_inh
                F(k) = data.conductance.omega_exc;
            else
                error('Don''t know which frequency to use.');
            end
            t = (0:length(data.recorded.voltage_compensated)-1) * data.model.dt;
            dur(k) = t(end);
            events = extractAPPeak(t,data.recorded.voltage_compensated',-10);
            tr = round(ttran*F(k))/F(k);
            if tr ~= ttran
                fprintf(1, ['Using transient equal to %g seconds ', ...
                    '(rounded from %g seconds for a frequency of %g Hz).\n'], tr, ttran, F(k));
            end
            dur(k) = dur(k) - tr;
            spikes{k} = (events{1}(events{1} > tr) - tr)';
        end
        fprintf(1, ' done.\n'); 
    end
    
    % sort the frequencies and accordingly the groups of spikes times and the durations
    [F,idx] = sort(F);
    spikes = spikes(idx);
    dur = dur(idx);
    
    % lump together all spikes that correspond to stimulations with the same
    % modulating frequency
    [F,~,j] = unique(F);
    nF = length(F);
    stimulusDurations = zeros(nF,1);
    spikeTimes = cell(nF,1);
    ISIs = cell(nF,1);
    for k=1:nF
        spikeTimes{k} = [];
        ISIs{k} = [];
    end
    for k=1:nfiles
        stimulusDurations(j(k)) = stimulusDurations(j(k)) + dur(k);
        spikeTimes{j(k)} = [spikeTimes{j(k)}; spikes{k}];
        ISIs{j(k)} = [ISIs{j(k)}; diff(spikes{k})];
    end
    
    % mean firing rate, given simply by the total number of spikes divided
    % by the duration of the stimulation
    meanRate = zeros(nF,1);
    % coefficient of variation of the ISIs
    CV = zeros(nF,1);
    for k=1:nF
        meanRate(k) = length(spikeTimes{k}) / stimulusDurations(k);
        CV(k) = std(ISIs{k}) / mean(ISIs{k});
    end
    
    if do_plot
        hndl = zeros(nF,1);
        lgnd = cell(nF,1);
        cmap = jet(nF);
        figure;
        hold on;
        for k=1:nF
            [n,x] = hist(ISIs{k},20);
            n = n / sum(n);
            hndl(k) = plot(x*1e3,n,'o','Color',cmap(k,:));
            lgnd{k} = sprintf('F = %d Hz CV = %.2f', F(k), CV(k));
        end
        legend(hndl, lgnd);
        xlabel('ISI (ms)');
        ylabel('Fraction of spikes');
        box off;
        set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,6,4]);
        print('-depsc2', 'ISIs.eps');
    end
    
    if saveSpikeTimes
        save(SPIKES_FILE,'F','spikeTimes','meanRate','CV','stimulusDurations');
    end

else
    fprintf(1, 'Loading spikes file [%s].\n', SPIKES_FILE);
    load(SPIKES_FILE);
    [F,idx] = sort(double(F));
    CV = CV(idx);
    meanRate = meanRate(idx);
    spikeTimes = spikeTimes(idx);
    stimulusDurations = stimulusDurations(idx);
    nF = length(F);
end

%%% extract the sinusoidal modulation from the original spike times
r0 = zeros(nF,1);
r1 = zeros(nF,1);
phi = zeros(nF,1);

ncol = ceil(nF/2);
if do_plot
    figure;
    subplot(2,ncol,1);
end
[r0(1),r1(1),phi(1),conf] = extractSinusoidalModulation(spikeTimes{1}, ...
                        F(1), nBins, stimulusDurations(1), nPeriods, fitMeanRate, do_plot);
conf = repmat(conf, [nF,1]);

for k=2:nF
    if do_plot
        subplot(2,ncol,k);
    end
    [r0(k),r1(k),phi(k),conf(k)] = extractSinusoidalModulation(...
            spikeTimes{k}, F(k), nBins, stimulusDurations(k), nPeriods, fitMeanRate, do_plot);
end
if do_plot
    set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,12,6]);
    print('-depsc2','fit_histograms.eps');
end

% adjust the values of phi to have them in a nice looking way when plotted
phi = adjustPhase(F,phi,pi/2,do_plot);

%%% extract the sinusoidal modulation from the shuffled spike times

shuffle_iter = 100;
spikeTimesShuffled = cell(nF,1);

r0_shuffled = zeros(nF,shuffle_iter);
r1_shuffled = zeros(nF,shuffle_iter);
phi_shuffled = zeros(nF,shuffle_iter);

fprintf(1, 'Shuffling the spike times %d times ', shuffle_iter);
for ii=1:shuffle_iter
    % shuffle the spike times
    for jj=1:nF
        isi = diff(spikeTimes{jj});
        idx = randperm(length(isi));
        spikeTimesShuffled{jj} = cumsum(isi(idx));
    end
    
    [r0_shuffled(1,ii),r1_shuffled(1,ii),phi_shuffled(1,ii)] = ...
        extractSinusoidalModulation(spikeTimesShuffled{1}, ...
        F(1), nBins, stimulusDurations(1), nPeriods, fitMeanRate, 0);
    
    for jj=2:nF
        [r0_shuffled(jj,ii),r1_shuffled(jj,ii),phi_shuffled(jj,ii)] = ...
            extractSinusoidalModulation(spikeTimesShuffled{jj}, ...
            F(jj), nBins, stimulusDurations(jj), nPeriods, fitMeanRate, 0);
    end
    fprintf(1, '.');
end
fprintf(1, ' done.\n');

r0_shuffled = mean(r0_shuffled,2);
r1_shuffled = mean(r1_shuffled,2);
phi_shuffled = mean(phi_shuffled,2);

% fit a rational transfer function
idx = find(F<=1000);
% w = [diff(reshape([conf.r1],[2,length(conf)]))', diff(reshape([conf.phi],[2,length(conf)]))'];
% [A,Z,P,Dt,offset] = fitTransferFunction(F(idx), r1(idx)./r0(idx), phi(idx), 1, [2,3], 1./w(idx,:), [], do_plot);
w = repmat(r1./r0-r1_shuffled./r0_shuffled,[1,2]);
w(w(:,1)<=0,1) = min(w(w(:,1)>0,1))/10;
w(w(:,2)<=0,2) = 0;
[A,Z,P,Dt,offset] = fitTransferFunction(F(idx), r1(idx)./r0(idx), phi(idx),     1, [2,3], w(idx,:), [], do_plot);
% if length(P) == 3 && max(P) > 1e3
%     [A,Z,P,Dt,offset] = fitTransferFunction(F(idx), r1(idx)./r0(idx), phi(idx), 1, 2, w(idx,:), [], do_plot);
% end
print('-depsc2','fit_transfer_function.eps');
TF = struct('A',A,'Z',sort(Z),'P',sort(P),'Dt',Dt,'offset',offset);

if do_plot
    figure;
    subplot(2,1,1);
    loglog(F,abs(r1)./r0,'ko-','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    hold on;
    loglog(F,abs(r1_shuffled)./r0_shuffled,'bo','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    ylabel('r_1/r_0');
    legend('Real spike times', 'Shuffled spike times','Location','SouthWest');
    subplot(2,1,2);
    semilogx(F,180/pi*phi,'ko-','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    xlabel('F (Hz)');
    ylabel('\Phi');
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,8,8]);
    print('-depsc2','modulation_phase_shift_ap_peak.eps');
end

end


