function spikesOut = reshapeSpikes(spikesIn, frequency)
% spikesOut = reshapeSpikes(spikesIn, frequency)

n = length(spikesIn);
spks = mod(spikesIn, 1./frequency);
block = zeros(n,1);

spikesOut = {};

jj = 0;
kk = 1;
while 1
    ii = 1;
    jj = jj+1;
    block(ii) = spks(jj);
    while jj < n && spks(jj+1) > spks(jj)
        ii = ii+1;
        jj = jj+1;
        block(ii) = spks(jj);
    end
    spikesOut{kk} = block(1:ii);
    kk = kk+1;
    if jj == n
        break;
    end
end

