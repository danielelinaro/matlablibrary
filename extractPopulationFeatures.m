function varargout = extractPopulationFeatures(cells,varargin)
% [prop_1,...,prop_n] = extractPopulationFeatures(cells,'prop_1',...,'prop_n')

ncells = length(cells);
nfeatures = length(varargin);
varargout = cell(nfeatures,1);
for i=1:nfeatures
    switch varargin{i}
        case {'normalized_spike_times',...
              'normalized_spike_amplitudes',...
              'normalized_spike_times_at_max_rate',...
              'normalized_spike_amplitudes_at_max_rate'},
            varargout{i} = cell(ncells,1);
        otherwise,
            varargout{i} = nan(ncells,1);
    end
end

for i=1:ncells
    f = [cells{i},'/cell.mat'];
    if exist(f,'file')
        data = load(f);
        for j=1:nfeatures
            if isfield(data,varargin{j})
                if ~ isempty(data.(varargin{j}))
                    try
                        varargout{j}(i) = data.(varargin{j});
                    catch
                        varargout{j}{i} = data.(varargin{j});
                    end
                else
                    varargout{j}(i) = nan;
                end
            else
                fprintf(1, 'Cell %s does not contain field %s.\n', cells{i}, varargin{j});
            end
        end
    else
        fprintf(1, 'No cell.mat file in %s.\n', cells{i});
    end
end
