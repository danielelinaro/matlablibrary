function preprocessHPCEcodeExp

ss = textscan(pwd,'%s','Delimiter','/');
d = textscan(ss{1}{end},'%s','Delimiter','-');
suffix = textscan(ss{1}{end-1},'%s','Delimiter','_');
id = sprintf('20%s%02d%02d%s', d{1}{end}, str2double(d{1}{1}), str2double(d{1}{2}), suffix{1}{1});

info = tdfread('history.txt');
sweeps = unique(info.Sweep_Index);
nsweeps = length(sweeps);
wave = IBWread('DP_Sweeper/dacWaves/stepPulse.ibw');
dt = wave.dx * 1e-3;
duration = 0.5; %sum(wave.y>0)*dt;
t0 = 0.125;
V = [];
amplitudes = [];
for i=1:nsweeps
    filename = sprintf('ad0_%d.ibw',sweeps(i));
    data = IBWread(filename);
    if data.dx*1e-3 == dt
        jdx = find(info.Sweep_Index == sweeps(i));
        str = info.Builder_Parameters(jdx(end),:);
        tokens = textscan(str,'%s','delimiter',';');
        for j=1:length(tokens{1})
            [key,value] = strread(tokens{1}{j},'%s%f','delimiter','=');
            if strcmp(key,'duration')
                dur = value*1e-3;
            elseif strcmp(key,'amplitude')
                amp = value * info.Multiplier(jdx(end));
            end
        end
        if abs(dur - duration) < 1e-10
            if size(V,1) > 0 && length(data.y) ~= size(V,2)
                continue;
            end
            V = [V ; data.y'];
            amplitudes = [amplitudes; amp];
        end
    end
end
t = (0:size(V,2)-1) * dt;
[amplitudes,idx] = sort(amplitudes);
V = V(idx,:);
stim = zeros(1,size(V,2));
stim(t>=t0 & t<=t0+duration) = 1;
nsweeps = size(V,1);
I = repmat(stim,[nsweeps,1]) .* repmat(amplitudes,[1,length(t)]);

figure;
axes('Position',[0.1,0.3,0.8,0.65]);
plot(t,V,'k');
axis([t([1,end]),-100,50]);
box off;
set(gca,'XTick',[],'YTick',-100:50:50,'TickDir','Out');
ylabel('Membrane voltage (mV)');
axes('Position',[0.1,0.1,0.8,0.175]);
plot(t,I,'k');
axis([t([1,end]),min(min(I))-20,max(max(I))+20]);
box off;
set(gca,'XTick',0:0.25:t(end),'TickDir','Out');
xlabel('Time (s)');
ylabel('Current (pA)');
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,8],'PaperSize',[10,8]);
print('-dpdf',[id,'_traces.pdf']);

max_ahp_dur = 10e-3;
max_adp_dur = 20e-3;
max_peak_to_end = 5e-3;
figure;
[tth,Vth,tp,Vp,tend,Vend,tahp,Vahp,tadp,Vadp,thalf,Vhalf,halfwidth,TT,VV] = ...
    extractSpikeFeatures(t, V, max_ahp_dur, max_adp_dur, max_peak_to_end, 1);
delete('spike_features.pdf');
print('-dpdf',[id,'_spikes.pdf']);
dt = dt*1e3;
tth = cellfun(@(x) x*1e3, tth, 'UniformOutput', 0);
tp = cellfun(@(x) x*1e3, tp, 'UniformOutput', 0);
tend = cellfun(@(x) x*1e3, tend, 'UniformOutput', 0);
tahp = cellfun(@(x) x*1e3, tahp, 'UniformOutput', 0);
tadp = cellfun(@(x) x*1e3, tadp, 'UniformOutput', 0);
thalf = cellfun(@(x) x*1e3, thalf, 'UniformOutput', 0);
halfwidth = cellfun(@(x) x*1e3, halfwidth, 'UniformOutput', 0);

filename = [id,'.h5'];
if exist(filename,'file')
    delete(filename);
end
h5create(filename, '/V', size(V), 'Datatype', 'double', ...
    'Shuffle', 1, 'Deflate', 9, 'ChunkSize', [size(V,1),1000]);
h5write(filename, '/V', V);
h5create(filename, '/I', size(I), 'Datatype', 'double', ...
    'Shuffle', 1, 'Deflate', 9, 'ChunkSize', [size(I,1),1000]);
h5write(filename, '/I', I);
h5writeatt(filename,'/','dt',dt);
nspikes = cellfun(@(x) length(x), tth);
for suffix = {'th','p','end','ahp','adp','half'}
    for prefix = ['t','V']
        for i=1:length(nspikes)
            grp = sprintf('/%c%s/%d',prefix,suffix{1},i-1);
            if nspikes(i)
                if strcmp(suffix,'half')
                    sz = [nspikes(i),2];
                else
                    sz = nspikes(i);
                end
                h5create(filename, grp, sz, 'Datatype', 'double');
                expr = sprintf('h5write(filename, grp, %c%s{i});',prefix,suffix{1});
                eval(expr);
            else
                h5create(filename, grp, inf, 'Datatype', 'double', 'ChunkSize', 10);
            end
        end
    end
end







