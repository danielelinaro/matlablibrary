function preprocessSpontaneousRecording(threshold, event_threshold_factor, discard)
% preprocessSpontaneousRecording(threshold, event_threshold_factor, discard)

if ~ exist('discard','var') || isempty(discard)
    discard = 5;
end

if ~ exist('event_threshold_factor','var') || isempty(event_threshold_factor)
    event_threshold_factor = 5;
end

if ~ exist('threshold','var') || isempty(threshold)
    threshold = loadThreshold(0);
end

if ~ exist('recording.mat','file')
    files = listH5Files;
    V = [];
    I = [];
    for i=1:length(files)
        [entities,info] = loadH5Trace(files{i});
        for j=1:length(entities)
            switch entities(j).units
                case 'mV'
                    V = [V, entities(j).data];
                case 'pA'
                    I = [I, entities(j).data];
            end
        end
    end
    dt = info.dt;
else
    data = load('recording.mat');
    if isfield(data,'V')
        V = data.V';
        V = V(:)';
        dt = data.dt;
        I = [];
    else
%         I = data.I';
%         I = I(:)';
        I = data.I(1,:);
        dt = data.dt;
        V = [];
        idx = find(I == I(end));
        stop = find(diff(fliplr(idx)) ~= -1, 1, 'first');
        if ~ isempty(stop)
           idx = idx(end-stop:end);
        end
        I = I(setdiff(1:length(I),idx));
    end
end
if ~ isempty(V)
    t = (0:length(V)-1) * dt;
    V0 = V(1);
    Fc = 1000;
    Fs = 1/diff(t(1:2));
    V = filterTrace(V,Fs,Fc,2);
    N = ceil(Fs/Fc*2);
    V(1:N) = V0;
    idx = t >= discard;
    V = V(:,idx);
    t = t(idx) - discard;
    do_plot = 1;
    [Vm,AP] = analyseSpontaneous(t,V,threshold,do_plot,10e-3,20e-3);
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,9,5],...
        'PaperSize',[9,5]);
    print('-dpdf','spontaneous.pdf');
%     [tau_rise, tau_decay, amplitudes, event_times, Vwndw, Vmodel] = extractEPSPs(...
%         t,V,event_threshold_factor,[10e-3,100e-3],1);
    recording_mode = 'current_clamp';
    recording_duration = t(end);
    save('spontaneous.mat','recording_mode','Vm','AP','recording_duration');
%     save('spontaneous.mat','recording_mode','Vm','AP','tau_rise','tau_decay',...
%         'amplitudes','event_times','Vwndw','Vmodel','event_threshold_factor','recording_duration');
end
if ~ isempty(I)
    t = (0:length(I)-1) * dt;
    idx = t >= discard;
    I = I(:,idx);
    t = t(idx) - discard;
    [event_times, amplitudes, tau_decay, EPSC, RMSE, threshold, suspicious, Ifiltered, discarded] = ...
        extractEPSCs(t,I,event_threshold_factor,[5e-3,50e-3],50e-3);
    event_times = event_times{1};
    amplitudes = amplitudes{1};
    tau_decay = tau_decay{1};
    EPSC = EPSC{1};
    RMSE = RMSE{1};
    suspicious = suspicious{1};
    md = nanmean(tau_decay);
    sd = nanstd(tau_decay);
    ma = nanmean(amplitudes);
    sa = nanstd(amplitudes);
    
%     td = tau_decay(~isnan(tau_decay))*1e3;
%     edges = .05:.05:20;
%     n = histc(td,edges);
%     n = n/trapz(edges,n);
%     %%% log-normal distribution
%     g = fittype('1/(x*sigma*sqrt(2*pi))*exp(-((log(x)-mu)^2)/(2*sigma^2))','indep','x','coeff',{'mu','sigma'});
%     model = fit(edges(:),n(:),g,'StartPoint',[mean(td),std(td)]);
%     start = fminsearch(@(x) abs(f(x,model.mu,model.sigma)-0.01), mean(td));
%     stop = fminsearch(@(x) abs(f(x,model.mu,model.sigma)-0.99), model.mu+3*model.sigma);
    
    idx = find(~suspicious & ~isnan(tau_decay) & tau_decay >= max(1e-3,md-3*sd) & tau_decay <= min(md+3*sd,0.1) & ...
        amplitudes >= ma-7*sa & amplitudes <= ma+3*sa);
    event_times = event_times(idx);
    amplitudes = amplitudes(idx);
    tau_decay = tau_decay(idx);
    EPSC = EPSC(idx,:);
    T = (0:size(EPSC,2)-1) * dt;
    RMSE = RMSE(idx);
%     suspicious = suspicious(idx);
    recording_mode = 'voltage_clamp';
    recording_duration = t(end)-sum(discarded);
    save('spontaneous_VC.mat','recording_mode','tau_decay','amplitudes',...
        'event_times','event_threshold_factor','recording_duration');
    
    if ~ isempty(event_times)
        col = [1,.7,.3];
        figure;
        ax = betterSubplot(2,2,'Offset',[0.1,0.1;0.05,0.3]);
        axes('Position',[0.1,0.75,0.85,0.225],'NextPlot','Add',...
            'TickDir','Out','LineWidth',0.8);
        plot(t,I,'Color',[.6,.6,.6]);
        plot(t([1,end]),threshold+[0,0],'r--','LineWidth',1);
        plot(t(t>t(1)+discarded(1) & t<t(end)-discarded(2)),Ifiltered,'k');
        plot(event_times, amplitudes, 'kv', 'MarkerFaceColor', col, ...
            'LineWidth', 0.5, 'MarkerSize', 4);
%         plot(event_times(suspicious), amplitudes(suspicious), 'ko', 'MarkerFaceColor', 'r', ...
%             'LineWidth', 0.8, 'MarkerSize', 4);
        axis tight;
        xlim([t(1)+discarded(1),t(end)-discarded(2)]);
        yl = ylim;
        placeScaleBars(5,yl(1)+5,10,20,'10 s','20 pA','Color','k','LineWidth',1);
        axis off;
        
        da = 2;
        edges = 0:da:ceil(min(1000,max(-amplitudes))/da)*da;
        n = histc(-amplitudes,edges);
        b = bar(ax(1,1),edges,n,0.8,'histc');
        set(b,'FaceColor',col,'LineWidth',1);
        xlabel(ax(1,1),'EPSC amplitude (pA)');
        ylabel(ax(1,1),'Count');
        
        dt = 1e-3;
        edges = 0:dt:ceil(min(100e-3,max(tau_decay))/dt)*dt;
        n = histc(tau_decay,edges);
        b = bar(ax(1,2),edges*1e3,n,0.8,'histc');
        set(b,'FaceColor',col,'LineWidth',1);
        xlabel(ax(1,2),'\tau_{decay} (ms)');
        ylabel(ax(1,2),'Count');
        
        if length(event_times) > 1
            iei = diff(event_times);
            dt = 100e-3;
            edges = 0:dt:ceil(min(5,max(iei))/dt)*dt;
            n = histc(iei,edges);
            b = bar(ax(2,1),edges,n,0.8,'histc');
            set(b,'FaceColor',col,'LineWidth',1);
        end
        xlabel(ax(2,1),'Inter-EPSC interval (s)');
        ylabel(ax(2,1),'Count');
        
        plot(ax(2,2),-amplitudes,tau_decay*1e3,'ko','MarkerFaceColor',col,'MarkerSize',4,'LineWidth',1);
        xlabel(ax(2,2),'EPSC amplitude (pA)');
        ylabel(ax(2,2),'\tau_{decay} (ms)');
        
        sz = [8,5];
        set(gcf, 'Color', 'w', 'PaperUnits', 'Inch', 'PaperPosition', [0,0,sz], 'PaperSize', sz);
        print('-dpdf','EPSCs.pdf');
    end
    
end

