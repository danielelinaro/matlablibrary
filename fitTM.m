function [A,U,tau_rec,tau_facil,tau_rise,tau_decay] = fitTM(t,V,events)
% [A,U,tau_rec,tau_facil,tau_rise,tau_decay] = fitTM(t,V,events)

DEBUG = 1;

[r,c] = size(V);
if r > c
    V = V';
    n = c;
else
    n = r;
end
if n > 1
    V = mean(V);
end

dt = diff(t(1:2));
idx = round((events-t(1)) / dt);
nev = length(events);
baseline = mean(V(1:idx(1)));

%%% let's first fit a model EPSP to the last EPSP in the trace

EPSP = V(idx(end):end);
EPSP = EPSP - baseline;      % set the baseline to zero
amplitude = max(EPSP);
EPSP = EPSP / amplitude;    % normalise the amplitude
tEPSP = t(idx(end):end) - t(idx(end));  % start EPSP time at zero

EPSPpars  = [0.001 0.01 0.001];     % initial conditions for the rise time, the
                                % decay time, and the axonal propagation
                                % delay
opt = optimset('TolX', 1e-12, 'TolFun', 1e-12, 'Display', 'off');
optVal = 1e12;
for k=1:5                       % Let's repeat it few times 
    [x,fval] = fminsearch(@(x) EPSPCost(x,tEPSP,EPSP), EPSPpars .* (1 + 0.1*rand(size(EPSPpars))), opt);
    if fval < optVal
        EPSPpars = x;
        optVal = fval;
    end
end
tau_rise = EPSPpars(1);
tau_decay = EPSPpars(2);

if DEBUG
    modelEPSP = doubleexp(EPSPpars(1),EPSPpars(2),EPSPpars(3),tEPSP) * amplitude + baseline;
    EPSP = EPSP * amplitude + baseline;
    tEPSP = tEPSP + t(idx(end));
    figure;
    hold on;
    plot(tEPSP,EPSP,'k');
    plot(tEPSP,modelEPSP,'r');
end

%%% let's now find the amplitudes of the EPSPs
nsamples = round((30*EPSPpars(1)+EPSPpars(3))/dt);
peakAmplitudes = zeros(1,nev);
peakTimes = zeros(1,nev);
if DEBUG
    figure;
    hold on;
    plot(t,V,'k');
end
for k=1:nev
    [peakAmplitudes(k),i] = max(V(idx(k):idx(k)+nsamples));
    peakTimes(k) = t(idx(k)+i-1);
    if DEBUG
        plot(peakTimes(k),peakAmplitudes(k),'ro');
    end
end
peakAmplitudes = peakAmplitudes - baseline;

%%% let's fit the parameters of the TM model
TMpars = [0.3 0.05 0.1 0.2];      % Initial conditions for A, U, tau_rec, and tau_facil
optVal = 1e12;
ntrials = 5;
opt = optimset('TolX', 1e-12, 'TolFun', 1e-12, 'Display', 'off');
disp('Fitting the parameters of the Tsodyks-Markram model...');
for k=1:ntrials
    [x,fval] = fminsearch(@(x) EPSPTrainCost(x,EPSPpars,t,events,peakTimes,peakAmplitudes), ...
        TMpars .* (1 + 0.1*rand(size(TMpars))), opt);
    fprintf(1, 'Trial %d/%d => P: [%g,%g,%g,%g] Cost: %g ', k, ntrials, x(1), x(2), x(3), x(4), fval);
    if fval < optVal && x(2) < 1 && all(TMpars(2:4) > 0)
        TMpars = x;
        optVal = fval;
        fprintf(1, 'OK.\n');
    else
        fprintf(1, 'rejected.\n');
    end
end

[~,Vmodel] = modeltrain(TMpars,EPSPpars,t,events,peakTimes);
figure;
hold on;
plot(t,V,'k');
hold on;
plot(t,Vmodel+baseline,'r');

A = TMpars(1);
U = TMpars(2);
tau_rec = TMpars(3);
tau_facil = TMpars(4);

