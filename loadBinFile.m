function [Vr,Vm,I] = loadBinFile(filename)
% [Vr,Vm,I] = loadBinFile(filename)

if ~ exist(filename,'file')
    error('%s: no such file.', filename);
end

fid = fopen(filename);
data = fread(fid,inf,'double');
Vr = data(1:3:end);
Vm = data(2:3:end);
I = data(3:3:end);
fclose(fid);

