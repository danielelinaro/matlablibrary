function [T,C] = computeMovingCorrelation(t,x,y,window_dur)
% [T,C] = computeMovingCorrelation(t,x,y,window_dur)

[r,c] = size(x);
if c > r
    x = x';
    y = y';
    c = size(x,2);
end

dt = diff(t(1:2));
window_length = round(window_dur/dt);
steps = ceil(t(end)/window_dur)*2-1;
T = zeros(steps,2);
C = zeros(steps,c);

offset = ceil(window_length/2);
idx = -offset+1;
for k=1:steps
    start = idx(1) + offset;
    stop = start + window_length - 1;
    if stop > length(t)
        stop = length(t);
    end
    idx = start:stop;
    T(k,:) = t(idx([1,end]));
    C(k,:) = diag(corr(x(idx,:),y(idx,:)))';
end

