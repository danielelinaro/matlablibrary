function threshold = adjustThreshold(V,thresh)

if ~ exist('thresh','var') || isempty(thresh)
    m = mean(V,2);
    s = std(V,[],2);
    M = max(V,[],2);
    threshold = -20 + zeros(size(V,1),1);
    idx = find(M > m+5*s);
    threshold(idx) = M(idx)-20;
elseif isscalar(thresh)
    threshold = repmat(thresh,[size(V,1),1]);
elseif length(thresh) ~= size(V,1)
    error('length(thresh) must be equal to size(V,1)');
else
    threshold = thresh;
end
