function [nmda_to_ampa_ratio,taus,Ipeak,I,coeff,model] = ...
    preprocessAMPANMDAExperiment(folders, currents, hold_values)
% [nmda_to_ampa_ratio,Ipeak,I,coeff,model] = ...
%       preprocessAMPANMDAExperiment(folders, currents, hold_values)

DEBUG = 0;

if ~ exist('folders','var') || isempty(folders)
    folders = {'01','02'};
end

if ~ exist('currents','var') || isempty(currents)
    currents = {'AMPA+NMDA','NMDA'};
end

if ~ exist('hold_values','var') || isempty(hold_values)
    hold_values = [-70,40];
end

ampa = nan;
nmda = nan;

for i=1:length(currents)
    switch currents{i}
        case 'AMPA+NMDA'
            both = i;
        case 'AMPA'
            ampa = i;
        case 'NMDA'
            nmda = i;
        otherwise
            error('Unknown current: %s.', currents{i});
    end
end

if ~ exist('both','var')
    error('One folder must contain AMPA + NMDA currents.');
end

n_folders = length(folders);
n_hold_values = length(hold_values);

I_raw = cell(n_folders,1);

%%% load all the data
for i=1:n_folders
    files = listH5Files(folders{i});
    fprintf(1, 'Loading data from [%s]... ', folders{i});
    [t,I,Vh_raw] = loadH5TracesBis(files,1,2);
    fprintf(1, 'done.\n');
    I_raw{i} = zeros(n_hold_values,size(I,2));

    hold_stim = load([folders{i},'/hold.stim']);
    pulse_stim = load([folders{i},'/pulse.stim']);
    t_pulse = pulse_stim(1,1);
    t_hold_stim = hold_stim(1,1);
    dur_hold_stim = hold_stim(2,1);
    Vh = Vh_raw(:,t==t_hold_stim+dur_hold_stim/2);

    dt = diff(t(1:2));
    Fs = 1/dt;
    Fc = 2000;
    N = 2;

    for j=1:n_hold_values
        idx = Vh == hold_values(j) & ...
            min(I(:,t>t_hold_stim+10e-3 & ...
            t<t_hold_stim+dur_hold_stim-10e-3),[],2)>-1000;
        I_raw{i}(j,:) = filterTrace(mean(I(idx,:)),Fs,Fc,N);
    end
    
end

%%% fit the current traces
coeff = zeros(n_folders,n_hold_values);
Ipeak = zeros(n_folders,n_hold_values);
g = fittype('a1*exp(b1*x)+a2*exp(b2*x)','coeff',{'a1','b1','a2','b2'});
baseline_dur = 10e-3;
ss_dur = 200e-3;
I = cell(n_folders,1);
coeff = zeros(n_folders,2);
Ipeak = zeros(n_folders,2);
M = zeros(n_folders,2);
model = cell(n_folders,2);
for i=1:n_folders
    I{i} = zeros(2,round((baseline_dur+ss_dur)/dt));
    for j=1:n_hold_values
        coeff(i,j) = hold_values(j)/abs(hold_values(j));
        I0 = mean(I_raw{i}(j,t > t_pulse-baseline_dur & t < t_pulse-1e-3));
        idx = t > t_pulse+2e-3 & t < t_pulse+ss_dur;
        Itmp = (I_raw{i}(j,idx) - I0)*coeff(i,j);
        [Ipeak(i,j),M(i,j)] = max(Itmp);
        idx = find(t > t_pulse-baseline_dur & t < t_pulse+ss_dur);
        M(i,j) = M(i,j) + round((baseline_dur+2e-3)/dt);
        I{i}(j,:) = (I_raw{i}(j,idx) - I0)*coeff(i,j);

        x = t(idx(M(i,j):end))' - t(idx(M(i,j)));
        y = I{i}(j,M(i,j):end)';
        x0 = [max(y)/2,0,max(y)/2,0];
        x0(2) = -1/5e-3;
        x0(4) = -1/50e-3;
        try
        model{i,j} = fit(x,y,g,'StartPoint',x0);
        catch
            keyboard
        end

        if DEBUG
            figure;
            hold on;
            plot(t(idx),I{i}(j,:)*coeff(i,j),'k');
            plot(t(idx(M(i,j):end)),y*coeff(i,j),'k');
            plot(t(idx(M(i,j):end)),model{i,j}(x)*coeff(i,j),'r');
            plot(t(idx(M(i,j))),Ipeak(i,j)*coeff(i,j),'gs','MarkerFaceColor','w');
        end
    end
end

%%% compute AMPA and NMDA time constants
taus = zeros(1,2);
[~,i] = min(hold_values);
if -1/model{both,i}.b1 > 0 && ...
        (-1/model{both,i}.b1 < -1/model{both,i}.b2 || -1/model{both,i}.b2 < 0)
    taus(1) = -1/model{both,i}.b1;
else
    taus(1) = -1/model{both,i}.b2;
end
[~,i] = max(hold_values);
taus(2) = max(-1/model{both,i}.b1, -1/model{both,i}.b2);

%%% compute NMDA to AMPA ratio
T = t(idx) - t_pulse;
ampa_window = 1e-3;
nmda_delay = 40e-3;
%nmda_delay = 5*taus(1);
nmda_window = 10e-3;
% peak of AMPA currents at -70 mV
t0 = T(M(both,1));
ampa_current = mean(I{both}(2,T>=t0 & T<=t0+ampa_window));
nmda_current = mean(I{both}(2,T>=nmda_delay & T<=nmda_delay+nmda_window));
nmda_to_ampa_ratio = nmda_current/ampa_current;

yl = zeros(1,2);
figure;
axes('Position',[0.02,0.02,0.96,0.96],'NextPlot','Add')
highlight = [1,1,.7];
col = [.3,.3,.3 ; .6,.6,.6];
red = [1,0,0];
patch([t0,t0+ampa_window,t0+ampa_window,t0],[-200,-200,200,200],highlight,...
    'EdgeColor',highlight);
patch([nmda_delay,nmda_delay+nmda_window,nmda_delay+nmda_window,nmda_delay],...
    [-200,-200,200,200],highlight,'EdgeColor',highlight);
for i=1:n_hold_values
    plot(T,I{both}(i,:)*coeff(both,i),'Color',col(i,:),'LineWidth',1.5);
    x = T(M(both,i):end) - T(M(both,i));
    plot(T(M(both,i):end),model{both,i}(x)*coeff(both,i),'Color',...
        red,'LineWidth',1);
    plot(T(M(both,i)),I{both}(i,M(both,i))*coeff(both,i),'o','Color',red,...
        'MarkerSize',5,'MarkerFaceColor','w','LineWidth',1);
    yl(i) = I{both}(i,M(both,i))*coeff(both,i);
end
text(T(end)*0.6,2/10*yl(1),sprintf('NMDA/AMPA = %.1f',...
    nmda_to_ampa_ratio),'FontSize',10);
text(T(end)*0.6,3.5/10*yl(1),sprintf('tau_{AMPA} = %.1f ms',...
    taus(1)*1e3),'FontSize',10);
text(T(end)*0.6,5/10*yl(1),sprintf('tau_{NMDA} = %.1f ms',...
    taus(2)*1e3),'FontSize',10);
set(gca,'XLim',T([1,end]),'YLim',yl+[-10,10]);
placeScaleBars(T(end)-50e-3,yl(1),50e-3,50,'50 ms','50 pA','k','LineWidth',1);
axis off;
sz = [4,3];
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
print('-dpdf','ampa_nmda.pdf');
