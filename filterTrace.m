function y = filterTrace(x, Fs, Fc, N) 
% y = filterTrace(x, Fs, Fc, N) 

if ~ exist('Fc','var')
    Fc = 500;  % cut frequency of the filter
end
if ~ exist('N','var')
    N = 2; % order of the filter
end

[r,c] = size(x);
if r > c
    x = x';
    r = c;
end

lpf  = fdesign.lowpass('N,F3dB', N, Fc, Fs);
fltr = design(lpf, 'butter');

y = zeros(size(x));
for k=1:r
    y(k,:) = filter(fltr, x(k,:));
end

if r == c
    y = y';
end


