function p = computeDetectionProbability(spikes, step_time, T, N, n_reps, mode)
% COMPUTEDETECTIONPROBABILITY computes the probability that a step in the
% input to a neuron is detected as a change in the output spike rate.
% 
% p = computeDetectionProbability(spikes, step_time, T, N, n_reps, mode)
% 
% Parameters:
%      spikes - a cell array containing the spike times.
%   step_time - the time at which a change in the input was applied.
%           T - the interval over which the probability should be computed.
%           N - the number of neurons to consider.
%      n_reps - the number of ''bootstrap'' repetitions.
%        mode - either 'increase' or 'decrease', for positive and negative
%               steps, respectively.
% 
% Returns:
%           p - the probability of detection of the step in the input.
% 
% The algorithm is taken from the following paper:
% 
% Tchumatchenko, T., Malyshev, A., Wolf, F., & Volgushev, M. (2011).
% Ultrafast Population Encoding by Cortical Neurons.
% The Journal of neuroscience, 31(34), 12171?12179.
% 

% Author: Daniele Linaro - July 2013

if ~strcmpi(mode,'increase') && ~strcmpi(mode,'decrease')
    error('mode must be either ''increase'' or ''decrease''.');
end
n_trials = length(spikes);
count_pre = zeros(n_reps,1);
count_post = zeros(n_reps,1);
for k=1:n_reps
    idx = randperm(n_trials,N);
    count_pre(k) = sum(cellfun(@(x) length(find(x>step_time-T & x<=step_time)), spikes(idx)));
    count_post(k) = sum(cellfun(@(x) length(find(x>step_time & x<=step_time+T)), spikes(idx)));
end
pd_pre = fitdist(count_pre,'binomial','N',N);
pd_post = fitdist(count_post,'binomial','N',N);
x_pre = floor(pd_pre.mean-5*pd_pre.std):ceil(pd_pre.mean+5*pd_pre.std);
% x_post = floor(pd_post.mean-5*pd_post.std):ceil(pd_post.mean+5*pd_post.std);
if strcmpi(mode,'increase')
    y_pre = cdf(pd_pre,x_pre);
    i = find(y_pre-0.95>0, 1);
    if 0.95-y_pre(i-1) < y_pre(i)-0.95
        i = i-1;
    end
    p = 1 - cdf(pd_post,x_pre(i));
    
%    figure;
%    hold on;
%    plot(x_pre,pd_pre.pdf(x_pre),'k');
%    plot(x_post,pd_post.pdf(x_post),'r');
%    plot(x_pre,y_pre, 'k--');
%    plot(x_pre(i)+[0,0],ylim, 'k--');
%    plot(x_post,1-cdf(pd_post,x_post),'r--');

else
    y_pre = cdf(pd_pre,x_pre);
    i = find(y_pre-0.05>0, 1);
    if 0.05-y_pre(i-1) < y_pre(i)-0.05
        i = i-1;
    end
    p = cdf(pd_post,x_pre(i));
end
