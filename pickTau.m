function tau = pickTau(taus)

if isscalar(taus)
    tau = taus;
else
    taus = sort(taus);
    if taus(2)/taus(1) <= 3
        tau = mean(taus);
    elseif taus(1) < 10e-3
        tau = taus(2);
    elseif taus(2) > 100e-3
        tau = taus(1);
    else
        tau = taus(1);
    end
end
