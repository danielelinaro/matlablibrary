function [gsyn,isyn,Vlif] = simulateTMGSynapseAndLIF( ...
    t,Vreal,Vth,syn,lif)
% SIMULATETMGSYNAPSEANDLIF simulates the time course of a Tsodyks-Markram
% synapse connected to a presynaptic real neuron and a postsynaptic LIF
% neuron.
% 
% [gsyn,isyn,Vlif] = simulateTMGSynapseAndLIF(t,Vreal,Vth,syn,lif)
% 
% Parameters:
%      t - (1xN) time vector.
%  Vreal - (MxN) real neuron voltage traces.
%    Vth - spike detection threshold for the real neuron
%    syn - a structure containing the parameters of the synapse
%          (E, tau1, tauRec, tauFacil, U, weight, delay).
%    lif - a structure containing the parameters of the neuron
%          (C, tau, tarp, Er, E0, Vth, Iext).
% 
% Returns:
%   gsyn - the time course of the synaptic conductance.
%   isyn - the corresponding synaptic current.
%   Vlif - the time course of the membrane potential of the LIF
%          neuron.
% 

% Author: Daniele Linaro - December 2012

dt = diff(t(1:2));  % [s]
synCoeff = 1/((syn.tau1/syn.tauRec)-1);
Rl = lif.tau/lif.C;

[m,n] = size(Vreal);

gsyn = zeros(m,n);  % [nS]
isyn = zeros(m,n);  % [pA]
Vlif = -60 + zeros(m,n);  % [mV]

for ii=1:m
    y = 0;
    z = 0;
    u = syn.U;
    tPrevSpikePyr = -1e3;
    tPrevSpikeLIF = -1e3;
    tmr = 0;
    for jj=2:n
        
        %%%% evolution of the synapse %%%%
        
        gsyn(ii,jj) = gsyn(ii,jj-1) * exp(-dt/syn.tau1);
        
        tmr = tmr - dt;
        if tmr <= 0 && tmr+dt > 0
            % the time since last spike
            isi = t(jj) - tPrevSpikePyr;
            % calculate z at event, based on prior y and z
            z = z * exp(-isi/syn.tauRec);
            z = z + y * (exp(-isi/syn.tau1) - exp(-isi/syn.tauRec)) * synCoeff;
            % calculate y at event
            y = y * exp(-isi/syn.tau1);
            x = 1 - y - z;
            % calculate u at event
            if syn.tauFacil > 0
                u = u * exp(-isi/syn.tauFacil);
                u = u + syn.U * (1-u);
            else
                u = syn.U;
            end
            gsyn(ii,jj) = gsyn(ii,jj-1) + syn.weight * x * u;
            y = y + x * u;
            tPrevSpikePyr = t(jj);
        end
        
        if Vreal(ii,jj) >= Vth && Vreal(ii,jj-1) < Vth
            tmr = syn.delay - dt;
        end
        
        isyn(ii,jj) = gsyn(ii,jj) * (syn.E - Vlif(ii,jj-1));
        
        %%%% evolution of the neuron %%%%
        % refractory period
        if (t(jj) - tPrevSpikeLIF) < lif.tarp
            Vlif(ii,jj) = lif.Er;
        else
            Vinf = Rl*isyn(ii,jj-1) + lif.E0;
            Vlif(ii,jj) = Vinf - (Vinf - Vlif(ii,jj-1)) * exp(-dt/lif.tau);
            % spike in the LIF neuron
            if Vlif(ii,jj) > lif.Vth
                Vlif(ii,jj) = -lif.Vth;
%                 Vlif(ii,jj) = lif.E0;
                tPrevSpikeLIF = t(jj);
            end
        end
        
    end
end
