function c = crosscorr(x,y,tau)
% CROSSCORR computes the crosscorrelation between two signals. NaNs are
% treated as missing values.
% 
% c = crosscorr(x,y,tau)
% 
% Parameters:
%    x,y - the signals.
%    tau - the time lag, in steps. The default is 0.
% 
% Returns:
%      c - the correlation coefficient.
% 

% Author: Daniele Linaro - September 2013

if ~ exist('tau','var')
    tau = 0;
end

if tau < 0
    error('tau must be positive.');
end

y = y(1+tau:end);
L = min(length(x),length(y));
x = x(1:L);
y = y(1:L);
mx = nanmean(x);
my = nanmean(y);
sx = nanstd(x);
sy = nanstd(x);

c = nansum((x-mx).*(y-my))/(sx*sy*L);
