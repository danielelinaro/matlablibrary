function preprocessZapExperiment(filter_data,M,N,input_resistance)
% preprocessZapExperiment(filter_data,M,N,input_resistance)

if ~exist('N','var') || isempty(N)
    N = 2;
end
if ~exist('M','var') || isempty(M)
    M = 1;
end
if ~exist('filter_data','var') || isempty(filter_data)
    filter_data = 1;
end

files = listH5Files;
[t,Vr,I,info] = loadH5Traces(files);
kernels = dir('*_kernel.dat');
try
    Ke = load(kernels(end).name);
catch
    kernels = dir('../../steps/01/*_kernel.dat');
    Ke = load(['../../steps/01/',kernels(1).name]);
end
Vm = AECoffline(Vr,I,Ke);
I = I(1,:);
Vm = mean(Vm);
[dur,i] = max(info{1}(:,1));
t0 = sum(info{1}(1:i-1,1));
idx = find(t>t0 & t<t0+dur);
V0 = mean(Vm(idx));
fmin = info{1}(end-1,4);
fmax = info{1}(end-1,5);
Fs = 1/diff(t(1:2));
if filter_data
    [b,a] = butter(2,[fmin/2,2*fmax] * 2/Fs);
    Vm = filtfilt(b,a,Vm);
end

L = size(I,2);
n = 2^nextpow2(L);
X = fft((Vm(idx)-mean(Vm(idx)))*1e-3,n)/L;
Y = fft((I(idx)-mean(I(idx)))*1e-12,n)/L;
R = sqrt(real(X./Y).^2+imag(X./Y).^2);
phase = atan(imag(X./Y)./real(X./Y));
f = Fs/2*linspace(0,1,n/2+1);
idx = find(f >= fmin & f <= fmax);
f = f(idx);
R = R(idx)*1e-6;
phase = phase(idx);
f = f(:);
R = R(:);
phase = phase(:);
idx = find(f > 10);
jdx = find(phase(idx) > 0);
phase(idx(jdx)) = phase(idx(jdx)) - pi;

if R(1) > mean(R(f<=1))*2
    f = f(2:end);
    R = R(2:end);
    phase = phase(2:end);
end

if exist('input_resistance','var')
    f = [f(1)/10; f];
    R = [input_resistance; R];
    phase = [0; phase];
end

[A,Z,P,Dt,offset] = ...
    fitTransferFunction(f, R, phase, M, N, [], [1,0], 0);

save('impedance.mat','f','R','phase','M','N','A','Z','P','Dt','offset','V0');
if exist('input_resistance','var')
    save('impedance.mat','input_resistance','-append');
end

[w,mag,phi] = evalTransferFunction(A,Z*2*pi,P*2*pi,Dt,0.5*f(1)*2*pi,2*f(end)*2*pi,100);

% do some plotting
fnt = 11;
green = [0,.6,0];
blue = [0,0,.6];

idx = round(logspace(0,ceil(log10(length(f))),500));
idx = idx(idx<=length(f));
f_ds = f(idx);
R_ds = R(idx);
phase_ds = phase(idx);

figure;
axes('Position',[0.12,0.83,0.85,0.15],'NextPlot','Add');
plot(t,Vm,'k','LineWidth',0.8);
axis tight;
axis off;
dVm = round((max(Vm) - min(Vm))/4);
placeScaleBars(t(1),max(Vm)-dVm,0.5,dVm,'0.5 s',sprintf('%.0f mV',dVm),'k','LineWidth',1);

axes('Position',[0.12,0.8,0.85,0.03],'NextPlot','Add');
plot(t,I,'r','LineWidth',0.8);
axis tight;
axis off;
placeScaleBars(t(1),0,[],max(I),'',sprintf('%.0f pA',max(I)),'k','LineWidth',1);

axes('Position',[0.12,0.1,0.85,0.31],'NextPlot','Add');
set(gca,'FontSize',fnt);
plot(f_ds,phase_ds,'k','LineWidth',1);
plot(w/(2*pi),phi,'Color',[1,0,.5],'LineWidth',2);
xlabel('Frequency (Hz)');
ylabel('Phase (rad)');
axis([w([1,end])/(2*pi),ylim]);
set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8,'YTick',-2:1);

axes('Position',[0.12,0.45,0.85,0.31],'NextPlot','Add');
set(gca,'FontSize',fnt);
plot(f_ds,R_ds,'k','LineWidth',1);
plot(w/(2*pi),mag,'Color',[1,0,.5],'LineWidth',2);
if exist('input_resistance','var')
    plot(f(1),R(1),'ks','MarkerFaceColor',[1,.5,0],'MarkerSize',8);
    text(10^log10(0.8*f(1)),0.95*R(1),sprintf('R_{in} = %.0f MOhm', input_resistance),...
        'VerticalAlignment','Top','FontSize',fnt);
end
text(f(end),max(R),sprintf('V_m = %.1f mV', mean(V0)),'HorizontalAlignment','Right',...
    'FontSize',fnt,'VerticalAlignment','Top');
ylabel('Impedance (MOhm)');
axis([w([1,end])/(2*pi),ylim]);
set(gca,'XScale','Log','TickDir','Out','LineWidth',0.8,'XTickLabel',[]);

axes('Position',[0.12,0.1,0.85,0.66],'NextPlot','Add');
for i=1:M
    plot(Z(i)+[0,0],[0,1],'Color',blue,'LineWidth',1);
    text(10^log10(0.85*Z(i)),0.01,sprintf('Z_%d = %.2f Hz', i, Z(i)),'Rotation',90,...
        'Color',blue,'FontSize',fnt);
end
for i=1:N
    plot(P(i)+[0,0],[0,1],'Color',green,'LineWidth',1);
    text(10^log10(1.5*P(i)),0.01,sprintf('P_%d = %.2f Hz', i, P(i)),'Rotation',90,...
        'Color',green,'FontSize',fnt);
end
set(gca,'XScale','Log');
axis([w([1,end])/(2*pi),0,1]);
axis off;

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,7],'PaperSize',[6,7]);
print('-dpdf','impedance.pdf');
