function [y,edges,n] = binarizeSpikeTrain(spks, binwidth, interval, T, step)
% binarizeSpikeTrain transforms a spike train into a vector composed
% of zeros and ones.
% 
% [y,edges,n] = binarizeSpikeTrain(spks, binwidth, interval, T)
% 
% Arguments:
%      spks - the spike times, as an array of cells.
%  binwidth - the width of the histogram bin (in seconds).
%  interval - a 2-elements vector containing the onset and end times of the
%             stimulus. If this argument is not given, the first and the
%             last spike times are used.
%         T - the duration of the window used for computing spike counts.
%      step - the time increment of the window used for computing spike
%             counts (default: binwidth).
% Returns:
%         y - a binary vector indicating with 1 the occurrence of a spike.
%     edges - the edges of the binary vector.
%         n - the spike counts, computed with window T.
% 

% Author: Daniele Linaro - April 2013.

if ~ exist('step','var')
    step = binwidth;
end

if ~ exist('T','var') && nargout == 3
    error('You must specify the window duration for computing spike counts.')
end

if ~ exist('interval','var') || isempty(interval)
    interval = [min(cellfun(@(x) x(1), spks)), max(cellfun(@(x) x(end), spks))];
end

edges = interval(1) : binwidth : interval(2);
nedges = length(edges);

ntrials = length(spks);
y = zeros(ntrials,nedges);
for k=1:ntrials
    if sum(spks{k})
        y(k,:) = histc(spks{k},edges);
    end
end

y = sparse(y);

if nargout == 3
    increment = round(step/binwidth);
    window = round(T/binwidth);
    n = zeros(ntrials,round((nedges-window)/increment));
    for k=1:increment:nedges-window
        n(:,(k-1)/increment+1) = sum(y(:,k:k+window-1),2);
    end
    n = sparse(n);
end
