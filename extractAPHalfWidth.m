function [Vhalf,width,interval] = extractAPHalfWidth(T,V,thresh,tp,Vp,tth,Vth,tend)
% EXTRACTAPHALFWIDTH extracts the half-width of action potentials.
%
% [Vhalf,width,interval] = extractAPHalfWidth(T,V,thresh,tp,Vp,tth,Vth,tend)
%
% Parameters:
%   T - time vector, with size (1xN) where N is the number of samples.
%   V - voltage trace(s), with size (MxN), where M is the number of 
%       traces and N is the number of samples.
% thresh - threshold for spike detection.
% 
% Return values:
%     Vhalf - the voltage value at which the half width is measured.
%     width - a cell array [of size (Mx1)] with the AP half-width for each
%             voltage trace.
%  interval - time instants at which the half width is measured.
%

% Daniele Linaro - April 2012

if ~ exist('thresh','var')
    thresh = [];
end

if ~ exist('tp','var') || ~ exist('Vp','var') || isempty(tp) || isempty(Vp)
    [tp,Vp] = extractAPPeak(T,V,thresh);
end

if ~ exist('tth','var') || ~ exist('Vth','var') || isempty(tth) || isempty(Vth)
    [tth,Vth] = extractAPThreshold(T,V,thresh,tp);
end

if ~ exist('tend','var') || isempty(tend)
    tend = extractAPEnd(T,V,thresh,[],tp,Vth);
end

nexp = size(V,1);
Vhalf = cell(nexp,1);
width = cell(nexp,1);
interval = cell(nexp,1);
nspks = cellfun(@(x)length(x), tp);

for ii=1:nexp
    if nspks(ii) == 0
        continue;
    end
    Vhalf{ii} = zeros(1,nspks(ii));
    interval{ii} = zeros(2,nspks(ii));
    width{ii} = zeros(1,nspks(ii));
    for jj=1:nspks(ii)
        Vhalf{ii}(jj) = (Vp{ii}(jj) + Vth{ii}(jj))/2;
        idx = find(T > tth{ii}(jj) & T < tp{ii}(jj));
        above = find(V(ii,idx) >= Vhalf{ii}(jj), 1, 'first');
        interval{ii}(1,jj) = polyval(polyfit(V(ii,idx(above)-1:idx(above)),...
            T(idx(above)-1:idx(above)),1),Vhalf{ii}(jj));
        idx = find(T > tp{ii}(jj) & T < tend{ii}(jj));
        above = find(V(ii,idx) > Vhalf{ii}(jj), 1, 'last');
        interval{ii}(2,jj) = polyval(polyfit(V(ii,idx(above):idx(above)+1),...
            T(idx(above):idx(above)+1),1),Vhalf{ii}(jj));
        width{ii}(jj) = diff(interval{ii}(:,jj));
    end
end
