function plotHistc(n,edges,varargin)
% PLOTHISTC Produces a histogram bar plot of data. 
% 
% plotHistc(n,edges)
% 
% Arguments:
%       n - the number of elements in each bin.
%   edges - the edges of the histogram
% 

% Author: Daniele Linaro - January 2010

nedges = length(edges);
v = varargin{:};

hold on;
plot(edges(1)+[0,0], [0,n(1)], v);
plot(edges(1:2), n(1)+[0,0], v);
for k=2:nedges-1
    plot(edges(k)+[0,0], [n(k-1),n(k)], v);
    plot(edges(k:k+1), n(k)+[0,0], v);
end
plot(edges(end)+[0,0],n(end-1:end), v);

