function c = costMISE(t, w)
% c = costMISE(t, w)

N = length(t);
c = 0;
coeff = 2*sqrt(2);

for j = 1:N
    for i = 1:j-1
        c = c + exp(-(t(i)-t(j))^2/(4*w^2)) - coeff * exp(-(t(i)-t(j))^2/(2*w^2));
    end
end

c = N/w + 2/w*c;
