function [xa,xwndw,T] = ETSA(t,x,events,wndw)
% [xa,xwndw] = ETSA(t,x,events,wndw)
% 
%       t - time.
%       x - stimulus.
%  events - events' times.
%    wndw - time window: two elements vector.
% 
%      xa - event-triggered stimulus average.
% 

dt = t(2) - t(1);
% wndw = wndw + dt/10;

% find the number of events
if iscell(events)
    if length(events) == size(x,1)
        nev = sum(cellfun(@(y) length(y), events));
    else
        error('length(events) must be equal to size(x,1).');
    end
elseif isvector(events)
    nev = length(events);
else
    error('events must be either a vector or a cell with length(events) equal to size(x,1).');
end

% compute the number of samples per event
before = round(wndw(1)/dt);
after = round(wndw(2)/dt);
T = -wndw(1) : dt : wndw(2);
N = length(T);

% allocate enough memory to store all the events
xwndw = zeros(nev,N);

if iscell(events)
    k = 1;
    nev = cellfun(@(y) iff(isempty(y),0,length(y)), events);
    for i=1:length(events)
        pos = round((events{i}-t(1))/dt)+1;
        for j=1:nev(i)
            xwndw(k,:) = nan;
            idx = 1:N;
            jdx = pos(j)-before:pos(j)+after-1;
            start = find(jdx > 0, 1, 'first');
            stop = find(jdx <= size(x,2), 1, 'last');
            jdx = jdx(start:stop);
            idx = idx(start:stop);
            xwndw(k,idx) = x(i,jdx);
            k = k+1;
        end
    end
else
    for k=1:nev
        ndx = find(t > events(k) - (wndw(1)+dt/2)  & ...
            t < events(k) + (wndw(2)+dt/2));
        if length(ndx) < N
            if events(k) < wndw(1)
                xwndw(k,:) = x(ndx(1));
                xwndw(k,N-length(ndx)+1:end) = x(ndx);
            elseif t(end)-events(k) < wndw(2)
                xwndw(k,:) = x(ndx(end));
                xwndw(k,1:length(ndx)) = x(ndx);
            else
                keyboard
            end
        else
            xwndw(k,:) = x(ndx(1:N));
        end
    end
end

xa = nanmean(xwndw);
