function analyseFrequencyClampExperiment(t,V,I,f,target)
% analyseFrequencyClampExperiment plots the results of a
% frequency clamp experiment.
% 
% analyseFrequencyClampExperiment(t,V,I,f,target)
% 
% Arguments:
%        t - time vector.
%        V - voltage vector.
%        I - injected current vector.
%        f - estimated frequency vector.
%   target - target frequency (scalar).
% 
% Example:
% 
% files = listH5Files;
% [t,V,I,target,f] = loadH5TracesBis(files{end},1,2,3,4);
% analyseFrequencyClampExperiment(t,V,I,f,target(end));
%

% Author: Daniele Linaro - March 2013

dt = 2;
dV = 20;

ttran = t(end)/2;
Im = mean(I(t>ttran));

figure;
axes('Position', [0.1,0.6,0.8,0.4],'NextPlot','Add');
plot(t,V,'k', 'LineWidth', 1);
placeScaleBars(t(end)-dt-1, min(V), dt, [], ...
    sprintf('%d s', dt), [], 'k');
placeScaleBars(t(end)-dt-1.1, min(V)-dV/2, [], dV, [], ...
    sprintf('%d mV', dV), 'k');
axis([t([1,end]),ylim]);
axis off;

axes('Position', [0.1,0.4,0.8,0.2], 'NextPlot', 'Add');
plot(t([1,end]),target+[0,0],'m--','LineWidth',2);
plot(t,f,'Color',[0,.6,0],'LineWidth',2);
axis([t([1,end]),min(f)-5,max(f)+5]);
hndl = legend('Target','Estimated','Location','SouthEast');
set(hndl,'Box','Off');
ylabel('Frequency (Hz)');

axes('Position', [0.1,0.1,0.8,0.2], 'NextPlot', 'Add');
plot([ttran,t(end)],Im+[0,0],'y--','LineWidth',3);
plot(t,I,'Color',[0,0,.6],'LineWidth',1);
axis([t([1,end]),min(I)-100,max(I)+100]);
xlabel('Time (s)');
ylabel('Current (pA)');
hndl = legend(sprintf('I_m = %.0f pA', Im), 'Injected','Location','SouthEast');
set(hndl,'Box','Off');

set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,10,7]);

print('-depsc2', 'fclamp.eps');
