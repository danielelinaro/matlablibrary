function preprocessConductanceStepsExperiment(bin_width, frequency_range, do_plot, trial_order)
% preprocessConductanceStepsExperiment(bin_width, frequency_range, do_plot, trial_order)

if ~exist('bin_width','var')
    bin_width = 50e-3;
end
if ~exist('frequency_range','var')
    frequency_range = [0,10];
end
if ~exist('do_plot','var')
    do_plot = 1;
end
if ~exist('trial_order','var')
    trial_order = 'original';
end

if do_plot && strcmp(trial_order,'original') == 0 && strcmp(trial_order,'shuffled') == 0
    error('trial_order must be either ''original'' or ''shuffled''.');
end

Eexc = 0;
Einh = -80;

files = listH5Files;
[entities,info] = loadH5Trace(files{1});
dt = info.dt;
idx = find(entities(2).metadata(:,3) ~= 0); 
t0 = entities(2).metadata(idx(1)-1,1);
steps_dur = entities(2).metadata(idx,1);
stim_times = cumsum(entities(2).metadata(:,1));
steps = entities(2).metadata(idx,3) / entities(2).metadata(idx(1),3);
step = -(diff(steps(end:-1:1)))*100;
ntrials = length(files);

[t,V,gexc,ginh] = loadH5TracesBis(files,1,2,3);
I = mean(gexc .* (Eexc-V) + ginh .* (Einh-V));
I0 = mean(mean(I(t>t0+0.1 & t<t0+steps_dur(1))));
I1 = mean(mean(I(t>t0+steps_dur(1)+0.1 & t<t0+sum(steps_dur))));
spikes = extractAPPeak(t,V,-20);
[n,edges] = psth(spikes,bin_width,[t0,stim_times(end-1)]);
mean_rate = cellfun(@(x) length(x)/sum(steps_dur), spikes);
save('conductance_steps.mat','dt','t0','stim_times','steps_dur','steps',...
    'ntrials','bin_width','spikes','n','edges','mean_rate','step')
save('current.mat','I','I0','I1','step');

if do_plot
    if strcmp(trial_order,'shuffled')
        idx = randperm(length(spikes));
    else
        idx = 1:length(spikes);
    end
    figure;
    
    axes('Position',[0.1,0.35,0.6,0.6],'NextPlot','Add');
    rasterplot(spikes(idx),'k');
    for k=1:length(steps)+1
        plot(stim_times(k)+[0,0],ylim,'r','LineWidth',2);
    end
    axis([0,stim_times(end),1,ntrials]);
    set(gca,'YTick',[1,ntrials],'TickDir','out',...
        'XTick',0:stim_times(end),'XTickLabel',[]);
    ylabel('Trial #');
    
    x = repmat(stim_times(1:end-1)',[2,1]);
    x = x(2:end-1);
    y = repmat(steps',[2,1]);
    y = y(:)';
    
    axes('Position',[0.75,0.35,0.2,0.6],'NextPlot','Add');
    plot(mean_rate(idx),1:ntrials,'ko');
    plot(frequency_range(1)+[0,0],[1,ntrials],'r','LineWidth',2);
    plot(frequency_range(2)+[0,0],[1,ntrials],'r','LineWidth',2);
    lim = [max(0,floor(min(mean_rate)/5)*5), ceil(max(mean_rate)/5)*5];
    axis([lim, 1, ntrials]);
    axis off;
    plot(lim,[1,1],'k');
    text(lim(1),-ntrials/50,num2str(lim(1)),'HorizontalAlignment','Center','VerticalAlignment','Top');
    text(lim(2),-ntrials/50,num2str(lim(2)),'HorizontalAlignment','Center','VerticalAlignment','Top');
    
    axes('Position',[0.1,0.1,0.6,0.2],'NextPlot','Add');
    bar(edges,n/mean(n(edges>stim_times(2)-0.2 & edges<stim_times(2))),'histc');
    plot(x,y,'r','LineWidth',2);
    axis([0,stim_times(end),0,2]);
    xlabel('Time (s)');
    ylabel('Normalized firing rate');
    set(gca,'YTick',0:2,'TickDir','out','XTick',0:stim_times(end));
    
    axes('Position',[0.75,0.1,0.2,0.2],'NextPlot','Add');
    n = histc(mean_rate, lim(1):lim(2));
    bar(lim(1):lim(2),n,'histc');
    plot(frequency_range(1)+[0,0],[0,max(n)],'r','LineWidth',2);
    plot(frequency_range(2)+[0,0],[0,max(n)],'r','LineWidth',2);
    axis([lim, 0, max(n)]);
    xlabel('Firing rate (Hz)');
    
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,6,7]);
    print('-depsc2','conductance_steps.eps');
end
