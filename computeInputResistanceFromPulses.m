function Rin = computeInputResistanceFromPulses(t,Vm,I,spike_threshold,doPlot)
% COMPUTEINPUTRESISTANCEFROMPULSES computes the input resistance of
% a cell given a recording of membrane potential in which hyperpolarizing
% current pulses have been applied.
% 
% Rin = computeInputResistanceFromPulses(t,Vm,I,doPlot)
% 
% Parameters:
%             t - time vector.
%            Vm - membrane potential vector.
%             I - applied current vector.
%  spike_thresh - the spike threshold to use for discarding traces with
%                 spikes just after the stimulation.
%        doPlot - tells whether a figure should be generated.
% 
% Returns:
%           Rin - the input resistance of the cell, in MOhm.
% 

% Author: Daniele Linaro - February 2013

narginchk(5,5);

before_stimulus = 0.2;

dt = diff(t(1:2));
dI = min(I);
idx = find(I < 0);
if isempty(idx)
    error('The applied current is always positive: I cannot continue.');
end
pulse_dur = find(diff(idx)>1,1) * dt;
nsamples = pulse_dur/dt;
steady_state = pulse_dur/2;
idx = reshape(idx, [nsamples,length(idx)/nsamples]);
[r,c] = size(idx);
before = round(before_stimulus/dt);
ndx = zeros(r+before,c);
for k=1:c
    ndx(1:before,k) = idx(1,k)-before+1:idx(1,k);
    ndx(before+1:end,k) = idx(:,k);
end
ndx = ndx(:)';
V = Vm(ndx);
V = reshape(V, [nsamples+before,c])';
T = (0:nsamples+before-1) * dt;
[~,Vth] = extractAPPeak(T,V,spike_threshold);
if ~ isempty(flattenCellArray(Vth,'full'))
    V = V(max(V,[],2)<nanmean(cellfun(@(x) nanmean(x), Vth)),:);
end
s = std(V(:,T>before_stimulus),[],2);
V = V(s < mean(s)+0.5*std(s),:);
dV = mean(V(:,T>T(end)-steady_state),2) - mean(V(:,T<before_stimulus),2);
R = dV/dI*1e3;
idx = find(R>0);
R = R(idx);
V = V(idx,:);
m = mean(R);
s = std(R);
Rin = mean(R(R > m-2*s & R < m+2*s));

if doPlot

    Vmin = min(min(V));
    orange = [1,.5,0];
    
    figure;
    axes('Position',[0.1,0.55,0.85,0.4],'NextPlot','Add');
    plot(t,Vm,'Color',[.3,.3,.3]);
    plot(t([1,end]),[0,0],'m--');
    plot(t([1,end]),-70+[0,0],'m--');
    text(0,0,'0 mV','HorizontalAlignment','Right','VerticalAlignment','Middle');
    text(0,-70,'-70 mV','HorizontalAlignment','Right','VerticalAlignment','Middle');
    placeScaleBars(-t(end)/30,-30,[],20,'','20 mV','k','LineWidth',2);
    placeScaleBars(t(end/2)-10,min(Vm)-5,20,[],'20 sec','','k','LineWidth',2);
    axis([-t(end)/30-2,t(end),min(-75,min(Vm)-10),20]);
    axis off;
    
    axes('Position',[0.1,0.1,0.4,0.4],'NextPlot','Add');
    RR = R;
    [R,idx] = sort(R);
    V = V(idx,:);
    cmap = hot(2*length(R));
    start = round(length(R)/2);
    stop = start + length(R) - 1;
    cmap = cmap(start:stop,:);
    for k=1:length(R)
        plot(T,V(k,:),'Color',cmap(k,:));
    end
    plot(T,mean(V),'k','LineWidth',4);
    plot([T(1),before_stimulus,before_stimulus,T(end)],...
         [Vmin,Vmin,Vmin-5,Vmin-5],'Color',orange,'LineWidth',2);
    axis([T([1,end]),[Vmin,max(max(V))]+[-10,5]]);
    placeScaleBars(T(1),Vmin-8,0.1,5,'100 ms', ...
        {'5 mV','k'; sprintf('%.0f pA',abs(dI)), orange}, 'k','LineWidth',1);
    axis off;
    title(sprintf('trials = %d', length(R)));
    
    axes('Position',[0.6,0.1,0.35,0.4],'NextPlot','Add');
%     for k=1:length(R)
%         plot(k,RR(k),'o','Color',cmap(k,:),'MarkerFaceColor',cmap(k,:));
%     end
    plot([0,length(R)],m+[0,0],'k--');
    plot([0,length(R)],m-2*s+[0,0],'k--');
    plot([0,length(R)],m+2*s+[0,0],'k--');
    plot([0,length(R)],Rin+[0,0],'g','LineWidth',2);
    plot(RR,'o','Color','k','MarkerFaceColor','w','MarkerSize',5);
    text(length(R),1.25*Rin,sprintf('R_{in} = %.0f MOhm', Rin),...
        'HorizontalAlignment','Right','VerticalAlignment','Top');
    xlabel('Trial #');
    ylabel('R_{in} (MOhm)');
    axis([0,length(R),m-3*s,m+3*s]);
%     set(gca,'XTick',[-1,2],'YTick',0:50:150);
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,9,7],'PaperSize',[9,7]);
    print('-dpdf','Rin.pdf');
end
