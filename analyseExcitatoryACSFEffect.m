function analyseExcitatoryACSFEffect
% analyseExcitatoryACSFEffect

% Author: Daniele Linaro - December 2013

if ~exist('01','dir') || ~exist('02','dir')
    error('Directories 01 and 02 must be present.');
end

Fc = 5000;
N = 2;

if exist('00','dir')
    files = listH5Files('00');
    [entities,info] = loadH5Trace(files{end});
    for i=1:length(entities)
        t = (0:length(entities(i).data)-1) * info.dt;
        V = removeSpikes(t,filterTrace(entities(i).data,1/info.dt,Fc,N),-20,[2e-3,4e-3]);
        Vm = nanmean(V);
        Vs = nanstd(V);
        neuron(i).Control.Vm = Vm;
        neuron(i).Control.Vs = Vs;
        neuron(i).Control.edges = floor(Vm-3*Vs) : 0.1 : ceil(Vm+3*Vs);
        neuron(i).Control.n = histc(V,neuron(i).Control.edges);
        neuron(i).Control.fraction = neuron(i).Control.n/sum(neuron(i).Control.n);
    end
else
    disp('00: no such directory.');
end

files = listH5Files('01');
[entities,info] = loadH5Trace(files{end});
for i=1:length(entities)
    t = (0:length(entities(i).data)-1) * info.dt;
    V = removeSpikes(t,filterTrace(entities(i).data,1/info.dt,Fc,N),-20,[2e-3,4e-3]);
    Vm = nanmean(V);
    Vs = nanstd(V);
    neuron(i).Exc.Vm = Vm;
    neuron(i).Exc.Vs = Vs;
    neuron(i).Exc.edges = floor(Vm-3*Vs) : 0.1 : ceil(Vm+3*Vs);
    neuron(i).Exc.n = histc(V,neuron(i).Exc.edges);
    neuron(i).Exc.fraction = neuron(i).Exc.n/sum(neuron(i).Exc.n);
end

files = listH5Files('02');
[entities,info] = loadH5Trace(files{end});
for i=1:length(entities)
    t = (0:length(entities(i).data)-1) * info.dt;
    V = removeSpikes(t,filterTrace(entities(i).data,1/info.dt,Fc,N),-20,[2e-3,4e-3]);
    Vm = nanmean(V);
    Vs = nanstd(V);
    neuron(i).Washout.Vm = Vm;
    neuron(i).Washout.Vs = Vs;
    neuron(i).Washout.edges = floor(Vm-3*Vs) : 0.1 : ceil(Vm+3*Vs);
    neuron(i).Washout.n = histc(V,neuron(i).Washout.edges);
    neuron(i).Washout.fraction = neuron(i).Washout.n/sum(neuron(i).Washout.n);
end

bar_color = [.6,.6,.6];

for i=1:length(neuron)
    figure;
    hold on;
    hndl = zeros(1,3);
    
    if isfield(neuron(i),'Control')
        h = bar(neuron(i).Control.edges,neuron(i).Control.fraction,'histc');
        set(h,'FaceColor',bar_color,'EdgeColor',bar_color);
        x = linspace(neuron(i).Control.edges(1),neuron(i).Control.edges(end),200);
        y = 1/(neuron(i).Control.Vs*sqrt(2*pi)) * exp(-(x-neuron(i).Control.Vm).^2/(2*neuron(i).Control.Vs^2));
        y = y/max(y)*max(neuron(i).Control.fraction);
        hndl(1) = plot(x,y,'b','LineWidth',2);
    end
    
    h = bar(neuron(i).Exc.edges,neuron(i).Exc.fraction,'histc');
    set(h,'FaceColor',bar_color,'EdgeColor',bar_color);
    x = linspace(neuron(i).Exc.edges(1),neuron(i).Exc.edges(end),200);
    y = 1/(neuron(i).Exc.Vs*sqrt(2*pi)) * exp(-(x-neuron(i).Exc.Vm).^2/(2*neuron(i).Exc.Vs^2));
    y = y/max(y)*max(neuron(i).Exc.fraction);
    hndl(2) = plot(x,y,'r','LineWidth',2);

    h = bar(neuron(i).Washout.edges,neuron(i).Washout.fraction,'histc');
    set(h,'FaceColor',bar_color,'EdgeColor',bar_color);
    x = linspace(neuron(i).Washout.edges(1),neuron(i).Washout.edges(end),200);
    y = 1/(neuron(i).Washout.Vs*sqrt(2*pi)) * exp(-(x-neuron(i).Washout.Vm).^2/(2*neuron(i).Washout.Vs^2));
    y = y/max(y)*max(neuron(i).Washout.fraction);
    hndl(3) = plot(x,y,'Color',[1,.5,0],'LineWidth',2);
    
    axis tight;
    yl = ylim;
    yl(2) = ceil(yl(2)/0.05) * 0.05;
    xl = xlim;
    xl = [floor(xl(1)), ceil(xl(2))];
    axis([xl,yl]);
    set(gca,'XTick',xl(1):2:xl(2),'YTick',0:0.05:yl(2),'TickDir','Out');
    
    xlabel('Voltage (mV)');
    ylabel('Fraction');
    
    if ~isfield(neuron(i),'Control')
        legend(hndl(2:3),{'Excitatory ACSF','Washout'},'Location','NorthWest');
    else
        legend(hndl,{'Control','Excitatory ACSF','Washout'},'Location','NorthWest');
    end

    set(gcf,'Color','w','PaperUnits','Centimeter','PaperPosition',[0,0,16,9]);
    print('-depsc2',sprintf('cell_%02d.eps',i)); 
end
