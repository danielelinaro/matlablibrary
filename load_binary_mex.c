#include <stdio.h>
#include <stdlib.h>
#include "mex.h"

/* [srate N M time data] = load_binary_mex(fname) */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    char *filename;
    FILE *fp;
    double srate, dt;
    double *data, *time, *tmp;
    unsigned long N, M;
    int n, i, j, flag;
    
    
    n = mxGetN(prhs[0]) + 1;
    filename = mxCalloc(n, sizeof(char));
    mxGetString(prhs[0], filename, n);
    
    fp = fopen(filename, "r");
    if(fp == NULL) {
        printf("Unable to open %s\n", filename);
        free(filename);
        return;
    }
    
    flag = fread(&srate, sizeof(double), 1, fp);
    if(flag != 1) {
        printf("Error reading sampling rate.\n");
        free(filename);
        return;
    }
    printf("Sampling rate: %.1f Hz.\n", srate);
    dt = 1.0/srate;
    
    flag = fread(&N, sizeof(unsigned long), 1, fp);
    if(flag != 1) {
        printf("Error reading number of channels.\n");
        free(filename);
        return;
    }
    printf("Number of channels: %ld.\n", N);
    
    do {
        flag = fread(&M, sizeof(unsigned long), 1, fp);
        if(flag != 1) {
            printf("Error reading channel length.\n");
            free(filename);
            return;
        }
    } while (M == 0);
    printf("Number of elements in each channel: %ld.\n", M);
    
    plhs[0] = mxCreateDoubleScalar(srate);
    plhs[1] = mxCreateDoubleScalar(N);
    plhs[2] = mxCreateDoubleScalar(M);
    
    plhs[3] = mxCreateDoubleMatrix(M, 1, mxREAL);
    time = mxGetPr(plhs[3]);
    for(i=0; i<M; i++)
        time[i] = i*dt;
    
    plhs[4] = mxCreateDoubleMatrix(M, N, mxREAL);
    data = mxGetPr(plhs[4]);
    flag = fread(data, sizeof(double), M*N, fp);
    mexPrintf("Read %d items.\n", flag);
    
    mxFree(filename);
    fclose(fp);
    
    return;
}





