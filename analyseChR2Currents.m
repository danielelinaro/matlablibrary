function [Ipeak,Iss] = analyseChR2Currents(t,I,tstart,dur)

Fc = 500;
Ifilt = filterTrace(I,1/diff(t(1:2)),Fc,2);
I0 = mean(Ifilt(:,t>5/Fc & t<tstart-10e-3),2);
Ipeak = abs(min(Ifilt(:,t>tstart & t<tstart+50e-3),[],2) - I0);
Iss = abs(mean(Ifilt(:,t>tstart+dur/2 & t<tstart+dur-50e-3),2) - I0);

[~,i] = min(I(1,:));
