function [t,V,I,stim] = loadH5Traces(files)
% [t,V,I,stim] = loadH5Traces(files)

if isstruct(files(1))
    files =  arrayfun(@(x) x.name, files, 'UniformOutput', 0);
end

n = length(files);
[entities,info] = loadH5Trace(files{1});
m = length(entities(1).data);
t = (0:m-1) * info.dt;
V = zeros(n,m);
I = zeros(n,m);

if isempty(entities(1).metadata)
    vidx = 1;
    iidx = 2;
else
    vidx = 2;
    iidx = 1;
end
V(1,:) = entities(vidx).data';
I(1,:) = entities(iidx).data';

stim = cell(n,1);
stim{1} = entities(iidx).metadata;

for k=2:n
    entities = loadH5Trace(files{k});
    V(k,:) = entities(vidx).data';
    I(k,:) = entities(iidx).data';
    stim{k} = entities(iidx).metadata;
end
