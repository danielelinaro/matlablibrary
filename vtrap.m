function z = vtrap(x, y) 
if abs(x/y) < 1e-6
    z = y.*(1. - x./y/2.);
else
    z = x./(exp(x./y) - 1.);
end

