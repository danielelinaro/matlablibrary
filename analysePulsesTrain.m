function [tau_rise,tau_decay,amplitude,pp_ratio] = analysePulsesTrain(t,V,events,do_plot)
% analysePulsesTrain analyses data from an extracellular pulses experiment.
% 
% [tau_rise,tau_decay,amplitude,pp_ratio] = analysePulsesTrain(t,V,events,do_plot)
% 
% Parameters:
%          t - time vector, size (1xN).
%          V - voltage matrix, size (MxN), where M is the number of trials.
%     events - time instants at which the extracellular stimulation was
%              applied.
%    do_plot - whether to plot the results, default 0.
% 
% Returns:
%   tau_rise - rise time constant, in s.
%  tau_decay - decay time constant, in s.
%  amplitude - the amplitude of the first EPSP.
%   pp_ratio - the paired pulse ratio of the first and second EPSPs.
%          
% See also analysePulsesTrainFromFiles.

% Author: Daniele Linaro - April 2013

if ~ exist('do_plot','var')
    do_plot = 1;
end

[r,c] = size(V);
if r > c
    V = V';
    n = c;
else
    n = r;
end
if n > 1
    V = mean(V);
end

dt = diff(t(1:2));
idx = round((events-t(1)) / dt);

%%% let's first fit a model EPSP to the last EPSP in the trace
baseline = mean(V(idx(end)-round(0.1/dt):idx(end)));
ndx = idx(end):idx(end)+round(0.35/dt);
EPSP = V(ndx);
EPSP = EPSP - baseline;      % set the baseline to zero
amplitude = max(EPSP);
EPSP = EPSP / amplitude;    % normalise the amplitude
tEPSP = t(ndx) - t(idx(end));  % start EPSP time at zero

% initial conditions for the rise time, the decay time, and the axonal propagation delay
EPSPpars  = [0.01,0.1,0.001];
opt = optimset('TolX', 1e-6, 'TolFun', 1e-6, 'Display', 'off');
optVal = 1e12;
for k=1:5                       % Let's repeat it few times
    [x,fval] = fminsearch(@(x) EPSPCost(x,tEPSP,EPSP), EPSPpars .* (1 + 0.1*rand(size(EPSPpars))), opt);
    if fval < optVal
        EPSPpars = x;
        optVal = fval;
    end
end
tau_rise = min(EPSPpars(1:2));
tau_decay = max(EPSPpars(1:2));
delay = EPSPpars(3);

%%% let's fit the model EPSP to the first EPSP in the trace
baseline = mean(V(1:idx(1)));
EPSP1 = V(idx(1):idx(2)) - baseline;
amplitude1 = max(EPSP1);
offset = doubleexp(tau_rise, tau_decay, delay, t(idx(2):idx(3)) - t(idx(1))) * amplitude1;
EPSP2 = V(idx(2):idx(3)) - baseline - offset;
amplitude2 = max(EPSP2);
pp_ratio = amplitude2 / amplitude1;

amplitude = amplitude1;

if do_plot
    modelEPSP = doubleexp(EPSPpars(1),EPSPpars(2),EPSPpars(3),tEPSP) * amplitude + baseline;
    EPSP = EPSP * amplitude + baseline;
    tEPSP = tEPSP + t(idx(end));
    figure;
    axes('Position',[0.1,0.6,0.8,0.3],'NextPlot','Add');
    plot(tEPSP,EPSP,'k','LineWidth',3);
    plot(tEPSP,modelEPSP,'r--','LineWidth',2);
    axis tight;
    placeScaleBars(tEPSP(end)-0.1,baseline+amplitude-0.5,0.1,0.5,'100 ms','0.5 mV','k');
    axis off;
    title('Fit of the last EPSP');

    axes('Position',[0.1,0.1,0.8,0.3],'NextPlot','Add');
    ndx = idx(1)-round(50e-3/dt) : idx(3)+round(50e-3/dt);
    plot(t(ndx), V(ndx),'k','LineWidth',3);
    plot(t(idx(1):idx(3)), doubleexp(tau_rise, tau_decay, EPSPpars(3), t(idx(1):idx(3))-t(idx(1))) * amplitude1 + baseline, ...
        '--','Color',[.6,.6,.6],'LineWidth',2);
    plot(t(idx(2):idx(3)), V(idx(2):idx(3))-offset,'r','LineWidth',3);
    plot(t(idx(2):idx(3)), doubleexp(tau_rise, tau_decay, EPSPpars(3), t(idx(2):idx(3))-t(idx(2))) * amplitude2 + baseline, ...
        '--','Color',[1,.6,.6],'LineWidth',2);
    placeScaleBars(t(idx(1))-50e-3, max(V(ndx))-1, 25e-3, 1, '25 ms', '1 mV', 'k');
    axis tight;
    axis off;
    title('Fit of the first two EPSPs');
    set(gcf, 'Color', [1,1,1]);

    print('-depsc2','EPSPs.eps')
end
