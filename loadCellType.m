function type = loadCellType(default_type)
% type = loadCellType(default_type)

if ~ exist('default_type','var')
    default_type = 'PYR';
end

filenames = {'cell_type','CELL_TYPE'};
type = default_type;

level = 0;
cwd = [pwd,'/'];
pos = strfind(cwd,'/');
npos = length(pos);
for i=npos:-1:2
    str = cwd(pos(i-1)+1:pos(i)-1);
    level = level+1;
    if length(str) > 3 && strcmp(str(1:3),'201') && str(end-2) >= 'A' && str(end-2) <= 'Z'
        base_folder = cwd(1:pos(i)-1);
        break;
    end
end

if ~ exist('base_folder','var')
    type = default_type;
    return;
end

if strcmp(base_folder,pwd)
    directories = {base_folder};
else
    directories = {'.',base_folder};
end

for d = directories
    for f = filenames
        file = [d{1},'/',f{1}];
        if exist(file,'file')
            fid = fopen(file,'r');
            type = fscanf(fid,'%s');
            fclose(fid);
            return;
        end
    end
end
