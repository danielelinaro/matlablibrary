function [seconds,Rin,V0,slope,amplitude,area] = analyseLTPLTD3(t, Vm, Vstim, Istim, stim_freq, max_window, do_plot)

if ~iscell(Vm) || length(Vm) ~= 3
    error('Vm must be a 3-element cell array.');
end

if ~exist('do_plot','var') || isempty(do_plot)
    do_plot = 0;
end

if ~exist('max_window','var') || isempty(max_window)
    max_window = [10e-3,200e-3];
%     max_window = [1e-3,20e-3];
end

dt = diff(t(1:2));
seconds = cellfun(@(x) (1:size(x,1))/stim_freq, Vm, 'UniformOutput', 0);
minutes = cellfun(@(x) x/60, seconds, 'UniformOutput', 0);

%%% these quantities are computed for all conditions
Rin = cell(3,1);
V0 = cell(3,1);

%%% these quantities are computed only before and after the pairing
% tau_rise = cell(2,1);
% tau_decay = cell(2,1);
amplitude = cell(2,1);
slope = cell(2,1);
area = cell(2,1);

before_window = 0.02;
before_step = t > Istim.step.tstart-before_window & t < Istim.step.tstart;
during_step = t > Istim.step.tstart+Istim.step.dur-before_window & ...
    t < Istim.step.tstart+Istim.step.dur;

before_extra = find(t > Vstim.tstart-before_window & t < Vstim.tstart);
after_extra = find(t > Vstim.tstart+max_window(1) & t < Vstim.tstart+max_window(2));
after_extra_large = find(t>Vstim.tstart & t < Vstim.tstart+max_window(2));

decay_area = find(t>Vstim.tstart+max_window(1) & t<Istim.step.tstart);

for i=1:3
    Rin{i} = abs(mean(Vm{i}(:,before_step),2) - mean(Vm{i}(:,during_step),2)) / abs(Istim.step.amplitude) * 1e3;
    V0{i} = mean(Vm{i}(:,t<min(0.1,Istim.pulses.tstart)),2);
    if i ~= 2
        j = max(i-1,1);
        baseline = mean(Vm{i}(:,before_extra),2);
        amplitude{j} = max(Vm{i}(:,after_extra),[],2) - baseline;
        area{j} = sum(Vm{i}(:,decay_area) - repmat(baseline,[1,length(decay_area)]),2)*dt;
        slope{j} = nan(size(amplitude{j}));
        for k=1:size(Vm{i},1)
            if amplitude{j}(k) > 0
                a = find(Vm{i}(k,after_extra_large) > baseline(k)+0.1*amplitude{j}(k),1,'first');
                b = find(Vm{i}(k,after_extra) > baseline(k)+0.9*amplitude{j}(k),1,'first');
%                 slope{j}(k) = amplitude{j}(k)*0.8 / ((b-a)*dt*1e3);
                slope{j}(k) = amplitude{j}(k)*0.8 / ((t(after_extra(b)) - t(after_extra_large(a)))*1e3);
                
%                 if slope{j}(k) < 1
%                     clf;
%                     hold on;
%                     plot(t(before_extra(1):after_extra(end)),Vm{i}(k,before_extra(1):after_extra(end)),'k');
%                     plot([0.48,0.5],baseline(k)+[0,0],'r');
%                     plot(t(after_extra_large(a))+[0,0],ylim,'g--');
%                     plot(t(after_extra(b))+[0,0],ylim,'g--');
%                     keyboard;
%                 end
            end
        end
        % remove those trials in which the cell spiked
        idx = max(Vm{i}(:,t>Vstim.tstart-0.1 & t<Vstim.tstart+0.2),[],2) > -50;
        amplitude{j}(idx) = nan;
        area{j}(idx) = nan;
        slope{j}(idx) = nan;
        slope{j}(isinf(slope{j})) = nan;
    end
end

if do_plot
    figure;
    ax = betterSubplot(5,1,'Delta',[0.05,0.05]);
    for i=1:length(ax)
        pos = get(ax(i),'Position');
        pos(3) = 0.6;
        set(ax(i),'Position',pos);

    end
    ax(6) = axes('Position', [pos(1)+pos(3)+0.05,0.6,0.2,0.35], ...
        'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',10);
    ax(7) = axes('Position', [pos(1)+pos(3)+0.05,0.1,0.2,0.45], ...
        'NextPlot','Add','TickDir','Out','LineWidth',0.8,'FontSize',10);
    
    idx = max(Vm{1}(:,t>Vstim.tstart-0.1 & t<Vstim.tstart+0.2),[],2) < -50;
    jdx = t>Vstim.tstart-5e-3 & t<Vstim.tstart+0.25;
    V = mean(Vm{1}(idx,jdx));
    ymin1 = min(-0.5, min(V(round(10e-3/dt):end))-mean(V(1:100)));
    plot(ax(end-1),t(jdx),V-mean(V(1:100)),'k','LineWidth',1);
    idx = max(Vm{3}(:,t>Vstim.tstart-0.1 & t<Vstim.tstart+0.2),[],2) < -50;
    V = mean(Vm{3}(minutes{3}>=minutes{3}(end)-5 & idx',jdx));
    ymin2 = min(-0.5, min(V(round(10e-3/dt):end))-mean(V(1:100)));
    plot(ax(end-1),t(jdx),V-mean(V(1:100)),'r','LineWidth',1);
    axes(ax(end-1));
    axis tight;
    yl = ylim(ax(end-1));
    axis([xlim(ax(end-1)),min(ymin1,ymin2),yl(2)*1.05]);
    axis off;
    placeScaleBars(0.58,yl(2)-2,0.02,2,'20 ms','2 mV','k','LineWidth',0.8);
    
    idx = find(t>min(Vstim.tstart,Istim.pulses.tstart)-5e-3 & t<Vstim.tstart+0.1);
    lim = [min(Vm{2}(1,idx)),max(Vm{2}(1,idx))];
    plot(ax(end),[0,0],lim,'Color',[1,.7,.2],'LineWidth',2);
    for i=1:Istim.pulses.n
        plot(ax(end),(Istim.pulses.tstart-Vstim.tstart+(i-1)/Istim.pulses.freq)*1e3+[0,0],...
            lim,'Color',[.2,.7,1],'LineWidth',2)
    end
    plot(ax(end),(t(idx)-Vstim.tstart)*1e3,Vm{2}(1,idx),'k','LineWidth',1);
    xlabel(ax(end), 'Time since extra stim (ms)');
    set(ax(end),'YTick',[]);
    axes(ax(end));
    axis tight;
    placeScaleBars(80,-20,[],40,'','40 mV','k','LineWidth',0.8);

    n = 30*stim_freq;
    secs = seconds;
    secs{2} = secs{2} + secs{1}(end);
    secs{3} = secs{3} + secs{2}(end);
    time = (flattenCellArray(secs,'full')/60 - secs{3}(1)/60)';
    set(ax(1:5), 'XLim',[time(1),ceil(time(end)/5)*5]);
    xl = get(ax(1),'XLim');
    set(ax(1:5), 'XTick', xl(1):5:xl(2), 'XTickLabel', {});
    RMP = flattenCellArray(V0,'full');
    stop = floor(length(RMP)/n)*n;
    time_ds = time(1:n:stop);
    RMP_ds = mean(reshape(RMP(1:stop),n,stop/n));
    RMPm = mean(V0{1});
    axis(ax(5), [xl,RMPm-20,RMPm+20]);
    plot(ax(5), time_ds, RMP_ds, 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 4, ...
        'LineWidth', 1);
    xlabel(ax(5), 'Time after induction (min)');
    ylabel(ax(5), 'RMP (mV)');
    set(ax(5), 'XTickLabel', xl(1):5:xl(2));

    R = flattenCellArray(Rin,'full');
    R_ds = mean(reshape(R(1:stop),n,stop/n));
    plot(ax(4), time_ds, R_ds, 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 4, ...
        'LineWidth', 1);
    Rm = mean(Rin{1});
%     axis(ax(4), [xl,Rm*0.5,Rm*1.5]);
    axis(ax(4), [xl,0,Rm*2]);
    plot(ax(4), xlim(ax(3)),Rm*0.7+[0,0],'r','LineWidth',2);
    plot(ax(4), xlim(ax(3)),Rm*1.3+[0,0],'r','LineWidth',2);
    ylabel(ax(4), 'R_{in} (MOhm)');
    
    ar = nan(size(time));
    ar(1:length(area{1})) = area{1};
    ar(end-length(area{2})+1:end) = area{2};
    plot(ax(3), time, ar, 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 4, ...
        'LineWidth', 1);
    plot(ax(3), time([1,length(area{1})]), nanmean(area{1})+[0,0], 'Color', ...
        [0,.7,0], 'LineWidth', 2);
    plot(ax(3), [time(end)-5,time(end)], nanmean(area{2}(...
        minutes{3}>=minutes{3}(end)-5))+[0,0], 'Color', [0,.7,0], 'LineWidth', 2);
    M = 1.2*nanmax(ar(ar <= nanmean(ar)+3*nanstd(ar)));
    axis(ax(3), [xlim(ax(3)),0,M]);
%     yl = ylim(ax(3));
%     set(ax(3), 'YTick', 0:0.1:yl(2));
    ylabel(ax(3), 'Area (mV*s)');

    amp = nan(size(time));
    amp(1:length(amplitude{1})) = amplitude{1};
    amp(end-length(amplitude{2})+1:end) = amplitude{2};
    plot(ax(2), time, amp, 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 4, ...
        'LineWidth', 1);
    plot(ax(2), time([1,length(amplitude{1})]), nanmean(amplitude{1})+[0,0], 'Color', ...
        [0,.7,0], 'LineWidth', 2);
    plot(ax(2), [time(end)-5,time(end)], nanmean(amplitude{2}(...
        minutes{3}>=minutes{3}(end)-5))+[0,0], 'Color', [0,.7,0], 'LineWidth', 2);
    M = 1.2*nanmax(amp(amp <= nanmean(amp)+3*nanstd(amp)));
    axis(ax(2), [xlim(ax(3)),0,M]);
%     yl = ylim(ax(2));
%     set(ax(2), 'YTick', 0:3:yl(2));
    ylabel(ax(2), 'Amplitude (mV)');

    slp = nan(size(time));
    slp(1:length(slope{1})) = slope{1};
    slp(end-length(slope{2})+1:end) = slope{2};
    plot(ax(1), time, slp, 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 4, ...
        'LineWidth', 1);
    plot(ax(1), time([1,length(slope{1})]), nanmean(slope{1})+[0,0], 'Color', ...
        [0,.7,0], 'LineWidth', 2);
    plot(ax(1), [time(end)-5,time(end)], nanmean(slope{2}(...
        minutes{3}>=minutes{3}(end)-5))+[0,0], 'Color', [0,.7,0], 'LineWidth', 2);
    M = 1.2*nanmax(slp(slp <= nanmean(slp)+3*nanstd(slp)));
    axis(ax(1), [xlim(ax(3)),0,M]);
%     yl = ylim(ax(1));
%     set(ax(1), 'YTick', 0:yl(2));
    ylabel(ax(1), 'Slope (mV/ms)');
    
    sz = [7,6];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);

end
