function c = computeSpikeCountsCovariance(Ni,Nj,L,T)
% y = computeSpikeCountsCovariance(Ni,Nj,L,T)

Ri = length(Ni);
Rj = length(Nj);
N = min(min(cellfun(@(x) size(x,1), Ni)),min(cellfun(@(x) size(x,1), Nj)));
for j=1:Rj
    if size(Nj{j},1) == N
        Nj{j} = [Nj{j} ; Nj{j}(1,:)];
    end
end

c = 0;
for i=1:Ri
    for j=1:Rj
        for k=1:N
            c = c + Ni{i}(k,:)*Nj{j}(k,:)' - Ni{i}(k,:)*Nj{j}(k+1,:)';
        end
    end
end
c = c / (Ri*Rj*N*(L-T));
