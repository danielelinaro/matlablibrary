function y = fixOffset(x, start, y0)
% y = fixOffset(x, start, y0)

if isscalar(start)
    idx = 1:start;
else
    idx = start;
end

x0 = median(x(idx));

y = x - (x0 - y0);
