function Vout = removeArtifacts(Vin,I)
% Vout = removeArtifacts(Vin,I)

[r,c] = size(Vin);
if r > c
    Vin = Vin';
    n = c;
else
    n = r;
end

Vout = Vin;

if size(I,1) == 1 && n > 1
    I = repmat(I,[n,1]);
end

for ii=1:n
    idx = find(I(ii,:) ~= 0);
    idx = idx([1,find(diff(idx)>1)+1]);
    npulses = length(idx);
    for jj=1:npulses
%         [pks,locs] = findpeaks(-Vin(ii,idx(jj):end),'npeaks',2);
%         ndx = [idx(jj)-100:idx(jj),idx(jj)+locs(2):idx(jj)+locs(2)+50];
%         sp = csaps(ndx,Vin(ii,ndx));
%         ndx = idx(jj):idx(jj)+locs(2);
%         Vout(ii,ndx) = fnval(sp,ndx);
        m = mean(Vin(ii,idx(jj)-100:idx(jj)-5));
        stop = find(Vin(ii,idx(jj)+10:end)>m,1,'first') - 2;
%         Vout(ii,idx(jj):idx(jj)+10+stop) = m;
        Vout(ii,idx(jj):idx(jj)+10+stop) = Vout(ii,idx(jj)-10-stop:idx(jj));
    end
end

if n == c
    Vout = Vout';
end
