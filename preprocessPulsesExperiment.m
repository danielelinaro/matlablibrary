function preprocessPulsesExperiment(threshold)
% preprocessPulsesExperiment
if ~ exist('threshold','var')
    threshold = [];
end

ok = 0;
if exist('recording.mat','file')
    load recording.mat
    if exist('V','var') && exist('VLED','var')
        ok = 1;
        t = (0:size(V,2)-1) * dt;
    end
    t0 = tstart;
    stim_dur = npulses/freq;
    pulses_dur = dur;
    if exist('VLED','var')
        stimulation_type = 'light';
    else
        stimulation_type = 'current';
    end
else
    files = listH5Files;
    entities = loadH5Trace(files{1});
    rec_idx = nan;
    waveform_idx = nan;
    for i=1:length(entities)
        if strcmp(entities(i).units,'mV')
            rec_idx = i;
            rec_type = 'V';
        elseif strcmp(entities(i).units,'pA')
            rec_idx = i;
            rec_type = 'I';
        elseif strcmp(entities(i).name,'Waveform') && any(entities(i).metadata(:,2) == 8)
            idx = find(entities(i).metadata(:,2) == 8,1,'first');
            if entities(i).metadata(idx,3) == 5
                stimulation_type = 'current';
            else
                stimulation_type = 'light';
            end
            waveform_idx = i;
            pulses_idx = find(entities(i).metadata(:,2) == 8);
        end
    end
    if all(~isnan([rec_idx,waveform_idx]))
        if rec_type == 'V'
            if exist('kernel.dat','file')
                [t,Vr,Istim] = loadH5Traces(files);
                Ke = load('kernel.dat');
                V = AECoffline(Vr,Istim,Ke);
            else
                [t,V] = loadH5TracesBis(files,rec_idx);
            end
        else
            [t,I] = loadH5TracesBis(files,rec_idx);
        end
        freq = -entities(waveform_idx).metadata(pulses_idx,4);
        pulses_dur = entities(waveform_idx).metadata(pulses_idx,5);
        t0 = sum(entities(waveform_idx).metadata(1:pulses_idx-1,1));
        stim_dur = entities(waveform_idx).metadata(pulses_idx,1);
        npulses = ceil(stim_dur * freq);
        ok = 1;
        dt = diff(t(1:2));
    end
end
if ok
    if rec_type == 'V'
        V0 = mean(V(:,round(5e-3/dt)),2);
        Fc = 1000;
        Fs = 1/diff(t(1:2));
        V = filterTrace(V,Fs,Fc,2);
        N = Fs/Fc*2;
        V(:,1:N) = repmat(V0,[1,N]);
        [tp,Vp] = extractAPPeak(t,V,threshold,5e-3);
        nspikes = cellfun(@(x) length(x), tp);
        save('pulses.mat','freq','tp','Vp','npulses','nspikes','t0',...
            'stim_dur','pulses_dur','stimulation_type','rec_type');
        
        offset = 0.02;
        idx = find(t>t0-offset & t<t0+stim_dur+0.2);
        t = t(idx) - t(idx(1));
        V = V(:,idx);
        figure;
        axes('Position',[0.15,0.75,0.8,0.2],'NextPlot','Add');
        plot(t,V(1,:),'k');
        plot(t,V(end,:),'r');
        axis([t([1,end]),min(min(V([1,end],:)))-2,max(max(V([1,end],:)))+2]);
        axis off;
        placeScaleBars(t(end)-0.105,-20,0.1,40,'100 ms','40 mV','k','LineWidth',0.8);
        title(sprintf('%.0f ms @ %.0f Hz', pulses_dur, freq));
        
        axes('Position',[0.15,0.72,0.8,0.025],'NextPlot','Add');
        for i=0:npulses-1
            patch(offset+i/freq+[0,pulses_dur,pulses_dur,0]*1e-3,[0,0,1,1],...
                [0,0,0],'EdgeColor','k')
        end
        axis([t([1,end]),0,1]);
        axis off;
        
        tp = extractAPPeak(t-offset,V,threshold,5e-3);
        axes('Position',[0.15,0.15,0.8,0.5],'NextPlot','Add','TickDir','Out','LineWidth',0.8);
        rasterplot(tp,'k','LineWidth',1);
        axis([t([1,end])-offset,0.5,size(V,1)+0.5]);
        set(gca,'YTick',[1,size(V,1)]);
        xlabel('Time (s)');
        ylabel('Trial #');
    else
        I0 = mean(I(:,round(5e-3/dt)),2);
        Fc = 1000;
        Fs = 1/diff(t(1:2));
        I = filterTrace(I,Fs,Fc,2);
        N = Fs/Fc*2;
        I(:,1:N) = repmat(I0,[1,N]);
%         [tp,Vp] = extractAPPeak(t,V,threshold,5e-3);
%         nspikes = cellfun(@(x) length(x), tp);
        save('pulses.mat','freq','npulses','t0','stim_dur','pulses_dur',...
            'stimulation_type','rec_type');
        
        offset = 0.02;
        idx = find(t>t0-offset & t<t0+stim_dur+0.2);
        t = t(idx) - t(idx(1));
        I = I(:,idx);
        figure;
        axes('Position',[0.15,0.75,0.8,0.2],'NextPlot','Add');
        plot(t,I(1,:),'k');
        plot(t,I(end,:),'r');
        axis([t([1,end]),min(min(I([1,end],:)))-2,max(max(I([1,end],:)))+2]);
        axis off;
%         placeScaleBars(t(end)-0.105,-120,0.1,100,'100 ms','100 pA','k','LineWidth',0.8);
        title(sprintf('%.0f ms @ %.0f Hz', pulses_dur, freq));
        
        axes('Position',[0.15,0.72,0.8,0.025],'NextPlot','Add');
        for i=0:npulses-1
            patch(offset+i/freq+[0,pulses_dur,pulses_dur,0]*1e-3,[0,0,1,1],...
                [0,0,0],'EdgeColor','k')
        end
        axis([t([1,end]),0,1]);
        axis off;
        
%         tp = extractAPPeak(t-offset,V,threshold,5e-3);
        axes('Position',[0.15,0.15,0.8,0.5],'NextPlot','Add','TickDir','Out','LineWidth',0.8);
%         rasterplot(tp,'k','LineWidth',1);
        axis([t([1,end])-offset,0.5,size(I,1)+0.5]);
        set(gca,'YTick',[1,size(I,1)]);
        xlabel('Time (s)');
        ylabel('Trial #');
    end
    
    sz = [4.5,4];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','pulses.pdf');
    
end
