function [t,Vm,tau_rise,tau_decay,amplitude,pp_ratio,offset,varargout] = ...
    analysePulsesTrainFromFiles(files, fit_TM, fig_hndl)
% analysePulsesTrainFromFiles loads files containing data from an
% extracellular pulses experiment and analyses them.
% 
% [t,Vm,tau_rise,tau_decay,amplitude,pp_ratio,offset,A,U,tau_rec,tau_facil] = ...
%     analysePulsesTrainFromFiles(files, fit_TM, fig_hndl)
% 
% Parameters:
%      files - a cell array containing the list of files to be loaded.
%     fit_TM - whether to fit a Tsodyks-Markram model to the data (default 0).
%   fig_hndl - a optional figure handle where to plot the results.
% 
% Returns:
%          t - time vector.
%         Vm - average voltage response.
%   tau_rise - rise time constant, in s.
%  tau_decay - decay time constant, in s.
%  amplitude - the amplitude of the first EPSP.
%   pp_ratio - the paired pulse ratio of the first and second EPSPs.
%     offset - removed offset from the voltage traces.
%          A - the parameter A of the TM model.
%          U - the parameter U of the TM model.
%    tau_rec - recovery from depression time constant, in s.
%  tau_facil - facilitation time constant, in s.
%          
% See also analysePulsesTrain.

% Author: Daniele Linaro - April 2013

if ~ exist('fit_TM','var')
    fit_TM = 0;
end

if ~ exist('fig_hndl','var')
    fig_hndl = 0;
end

% load the raw data
[t,Vraw,I,stim] = loadH5Traces(files);
dt = diff(t(1:2));
% take only those trials where no spikes were emitted
Vraw = Vraw(max(Vraw,[],2) < -40, :);
% filter the traces
Fs = 1/diff(t(1:2));
Fc = 500;
N = 2;
V = filterTrace(Vraw, Fs, Fc, N);
% remove the stimulation artifacts and the first 100 ms
V = removeArtifacts(V,I);
idx = find(t > 0.1);
t = t(idx);
t = t - t(1);
V = V(:,idx);
I = I(:,idx);
% take the mean of the traces
if length(files) > 1
    [Vm,~,offset] = meanWithOffset(V,round(stim{1}(1)*0.9/dt));
else
    Vm = V;
    offset = nan;
end

events = find(I(1,:) ~= 0);
events = events([1,find(diff(events)>1)+1]);
events = t(events);

[tau_rise,tau_decay,amplitude,pp_ratio] = analysePulsesTrain(t,Vm,events);

if fit_TM
    [A,U,tau_rec,tau_facil,tau_rise,tau_decay] = fitTM(t,Vm,events);
    varargout = cell(4,1);
    varargout{1} = A;
    varargout{2} = U;
    varargout{3} = tau_rec;
    varargout{4} = tau_facil;
end

if exist('fig_hndl','var')
    figure(fig_hndl);
    hold on;
    ttran = 0;
    idx = find(t>ttran);
    plot(t(idx),Vm(idx),'k');
    stimtimes = cumsum(stim{1}(:,1));
    for k=1:2:length(stimtimes)-1
        patch([stimtimes(k),stimtimes(k+1),stimtimes(k+1),stimtimes(k)]-0.1,...
            [-2,-2,-1.5,-1.5],[1,.5,.5],'EdgeColor',[1,.5,.5]);
    end
    if max(Vm) > 2
        placeScaleBars(t(end)-0.6,max(Vm)-1.5,0.5,[],'0.5 s','','k');
        placeScaleBars(t(end)-0.1,max(Vm)-1.5,[],1,'','-1 mV','k');
    else
        placeScaleBars(t(end)-0.1,0.5,[],1,'','-1 mV','k');
    end
    axis off;
    axis tight;
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,6,4]);
    print('-depsc2','pulses_train.eps');
end
