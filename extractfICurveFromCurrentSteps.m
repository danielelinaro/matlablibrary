function [I,f] = extractfICurveFromCurrentSteps(t,V,stim,ttran)
% [I,f] = extractfICurveFromCurrentSteps(t,V,stim,ttran)

if ~ exist('ttran','var')
    ttran = 0.5;
end

spks = extractSpikes(t,V,-20,1);
n = length(spks);
stimtimes = cumsum(stim(:,1));
[dur,i] = max(stim(:,1));
start = i-1;
nsteps = length(find(stim(:,1) == dur));
I = repmat(stim(i:i+nsteps-1,3)',[n,1]);
f = zeros(n,nsteps);
for ii=1:n
    for jj=1:nsteps
        idx = find(spks{ii} >= stimtimes(start+jj-1)+ttran & ...
                   spks{ii} <= stimtimes(start+jj));
        f(ii,jj) = length(idx) / (dur-ttran);
    end
end

I = I(:);
f = f(:);

