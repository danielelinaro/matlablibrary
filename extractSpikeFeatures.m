function [tth,Vth,tp,Vp,tend,Vend,tahp,Vahp,tadp,Vadp,thalf,Vhalf,halfwidth,rapidness,T,V] = ...
    extractSpikeFeatures(t, v, threshold, max_ahp_dur, max_adp_dur, max_peak_to_end, ...
    max_threshold_to_peak, min_ISI, min_dV_threshold_coeff, dV_threshold, ...
    rapidness_window, rapidness_max_no_spikes, deriv, do_plot)
% [tth,Vth,tp,Vp,tend,Vend,tahp,Vahp,tadp,Vadp,thalf,Vhalf,halfwidth,rapidness,T,V] =
%      extractSpikeFeatures(t,v,threshold,max_ahp_dur,max_adp_dur,max_peak_to_end,
%      max_threshold_to_peak,min_ISI,min_dV_threshold_coeff,dV_threshold,...
%      rapidness_window,rapidness_max_no_spikes, deriv, do_plot)

if ~ exist('threshold','var') || isempty(threshold)
    % find the threshold
    threshold = adjustThreshold(v);
end

if ~ exist('max_ahp_dur','var') || isempty(max_ahp_dur)
    max_ahp_dur = 10e-3;
end

if ~ exist('max_adp_dur','var') || isempty(max_adp_dur)
    max_adp_dur = 20e-3;
end

if ~ exist('max_peak_to_end','var') || isempty(max_peak_to_end)
    % reasonable value for pyramidal cells
    max_peak_to_end = 5e-3;
    % reasonable value for interneurons
    %max_peak_to_end = 3e-3;
end

if ~ exist('max_threshold_to_peak','var') || isempty(max_threshold_to_peak)
    % reasonable value for pyramidal cells
    max_threshold_to_peak = 5e-3;
    % reasonable value for interneurons
    %max_threshold_to_peak = 2e-3;
end

if ~ exist('min_ISI','var') || isempty(min_ISI)
    % reasonable value for pyramidal cells
    min_ISI = 4e-3;
    % reasonable value for interneurons
    %min_ISI = 3e-3;
end

if ~ exist('dV_threshold','var')
    dV_threshold = [];
end

if ~ exist('rapidness_window','var')
    rapidness_window = [];
end

if ~ exist('rapidness_max_no_spikes','var')
    rapidness_max_no_spikes = [];
end

if ~ exist('deriv','var')
    deriv = 3;
end

if ~ exist('do_plot','var')
    do_plot = 1;
end

%%%% find the spike peaks
[tp,Vp] = extractAPPeak(t,v,threshold,min_ISI);
% remove those spikes that are too close to the beginning or end of the
% recording
Vp = cellfun(@(x,y) y(x>5e-3 & x<t(end)-10e-3), tp, Vp, 'UniformOutput', 0);
tp = cellfun(@(x) x(x>5e-3 & x<t(end)-10e-3), tp, 'UniformOutput', 0);
% find the ''true'' number of spikes
nspikes = cellfun(@(x) length(x), tp);

%%%% find the threshold
[tth,Vth] = extractAPThreshold(t,v,threshold,tp,deriv);
if any(cellfun(@(x) length(x), tth) ~= nspikes)
    % something went very wrong
    keyboard
end

threshold_to_peak = cellfun(@(x,y) y-x, tth, tp, 'UniformOutput', 0);
if any(cellfun(@(x) min(x), threshold_to_peak(nspikes>0)) <= 0) || ...
   any(cellfun(@(x) max(x), threshold_to_peak(nspikes>0)) > max_threshold_to_peak)
    % something went very wrong
    keyboard
end

%%%% find the fast AHP
tahp = extractAPAHP(t,v,max_ahp_dur,threshold,tp,tth);
if any(cellfun(@(x) length(x), tahp) ~= nspikes)
    % something went very wrong
    keyboard
end

peak_to_ahp = cellfun(@(x,y) y-x, tp, tahp, 'UniformOutput', 0);
if any(cellfun(@(x) min(x), peak_to_ahp(nspikes>0)) <= 0) || ...
   any(cellfun(@(x) max(x), peak_to_ahp(nspikes>0)) > max_ahp_dur)
    % something went very wrong
    keyboard
end

%%%% find the ADP
[tadp,Vadp] = extractAPADP(t,v,max_adp_dur,threshold,tth,tahp);
if any(cellfun(@(x) length(x), tadp) ~= nspikes)
    % something went very wrong
    keyboard
end

peak_to_adp = cellfun(@(x,y) y-x, tp, tadp, 'UniformOutput', 0);
if any(cellfun(@(x) min(x), peak_to_adp(nspikes>0)) <= 0)
    % something went very wrong
    keyboard
end

%%%% find the fast AHP again, this time using additional information on the
%%%% time of the ADP
[tahp,Vahp] = extractAPAHP(t,v,max_ahp_dur,threshold,tp,tth,tadp);
if any(cellfun(@(x) length(x), tahp) ~= nspikes)
    % something went very wrong
    keyboard
end

% peak_to_ahp = cellfun(@(x,y) y-x, tp, tahp, 'UniformOutput', 0);
% if any(cellfun(@(x) min(x), peak_to_ahp(nspikes>0)) <= 0) || ...
%    any(cellfun(@(x) max(x), peak_to_ahp(nspikes>0)) > max_ahp_dur+eps)
%     % something went very wrong
%     keyboard
% end
% 
% ahp_to_adp = cellfun(@(x,y) y-x, tahp, tadp, 'UniformOutput', 0);
% if any(cellfun(@(x) min(x), ahp_to_adp(nspikes>0)) <= 0) || ...
%    any(cellfun(@(x) max(x), ahp_to_adp(nspikes>0)) > max_adp_dur)
%     % something went very wrong
%     keyboard
% end

%%%% find the end of the spikes
[tend,Vend] = extractAPEnd(t,v,threshold,min_dV_threshold_coeff,tp,Vth,tahp);
if any(cellfun(@(x) length(x), tend) ~= nspikes)
    % something went very wrong
    keyboard
end

% if any(cellfun(@(x) max(x), Vend(nspikes>0)) > 0)
%     keyboard
% end

peak_to_end = cellfun(@(x,y) y-x, tp, tend, 'UniformOutput', 0);
if any(cellfun(@(x) min(x), peak_to_end(nspikes>0)) <= 0) || ...
   any(cellfun(@(x) max(x), peak_to_end(nspikes>0)) > max_peak_to_end)
    % something went very wrong
    keyboard
end

[Vh,halfwidth,interval] = extractAPHalfWidth(t,v,threshold,tp,Vp,tth,Vth,tend);
thalf = cellfun(@(x) x', interval, 'UniformOutput', 0);
Vhalf = cellfun(@(x) repmat(x',[1,2]), Vh, 'UniformOutput', 0);

%%% remove all the spikes that have duration < 0.1 ms, they are probably just
%%% glitches in the recording
min_halfwidth = 0.1e-3;
idx = find(nspikes > 0);
for i = find(cellfun(@(x) min(x), halfwidth(idx)) < min_halfwidth)'
    if isempty(i)
        break
    end
    jdx = setdiff(1:nspikes(idx(i)), find(halfwidth{idx(i)} < min_halfwidth));
    n_removed = nspikes(idx(i))-length(jdx);
    if n_removed == 1
        fprintf(1, 'Removing 1 spike in trial %d that has halfwidth shorter than %g ms.\n', ...
            idx(i), min_halfwidth*1e3);
    else
        fprintf(1, 'Removing %d spikes in trial %d that have halfwidth shorter than %g ms.\n', ...
            n_removed, idx(i), min_halfwidth*1e3);
    end
    tth{idx(i)} = tth{idx(i)}(jdx);
    Vth{idx(i)} = Vth{idx(i)}(jdx);
    tp{idx(i)} = tp{idx(i)}(jdx);
    Vp{idx(i)} = Vp{idx(i)}(jdx);
    tend{idx(i)} = tend{idx(i)}(jdx);
    Vend{idx(i)} = Vend{idx(i)}(jdx);
    tahp{idx(i)} = tahp{idx(i)}(jdx);
    Vahp{idx(i)} = Vahp{idx(i)}(jdx);
    tadp{idx(i)} = tadp{idx(i)}(jdx);
    Vadp{idx(i)} = Vadp{idx(i)}(jdx);
    thalf{idx(i)} = thalf{idx(i)}(jdx,:);
    Vhalf{idx(i)} = Vhalf{idx(i)}(jdx,:);
    halfwidth{idx(i)} = halfwidth{idx(i)}(jdx);
end
nspikes = cellfun(@(x) length(x), tp);
threshold_to_peak = cellfun(@(x,y) y-x, tth, tp, 'UniformOutput', 0);
peak_to_end = cellfun(@(x,y) y-x, tp, tend, 'UniformOutput', 0);
peak_to_ahp = cellfun(@(x,y) y-x, tp, tahp, 'UniformOutput', 0);
peak_to_adp = cellfun(@(x,y) y-x, tp, tadp, 'UniformOutput', 0);

rapidness = extractAPRapidness(t,v,tp,tth,Vth,dV_threshold,rapidness_window,...
    rapidness_max_no_spikes,do_plot);

dt = diff(t(1:2));
before = round((1e-3 + max(cellfun(@(x) max(x), threshold_to_peak(nspikes>0))))/dt);
after = round((1e-3 + max(cellfun(@(x) max(x), peak_to_adp(nspikes>0))))/dt);

T = (-before : after) * dt;
V = nan(sum(nspikes),before+after+1);
for i=1:length(nspikes)
    for j=1:nspikes(i)
        idx = round((tp{i}(j)-t(1))/dt)-before : round((tp{i}(j)-t(1))/dt)+after;
        idx = idx+1;
        ndx = find(idx > 0 & idx <= size(v,2));
        V(sum(nspikes(1:i-1))+j,ndx) = v(i,idx(ndx));
    end
end
    
if do_plot
    figure;
    hold on;
    plot(T([1,end]),-65+[0,0],'--','Color',[.6,.6,.6],'LineWidth',2);
    plot(T([1,end]),[0,0],'--','Color',[.6,.6,.6],'LineWidth',2);
    cnt = 1;
    for i=1:length(nspikes)
        for j=1:nspikes(i)
            plot(thalf{i}(j,:) - tp{i}(j), Vhalf{i}(j,:), 'b', 'LineWidth', 1);
            stop = tadp{i}(j) - tp{i}(j);
            idx = find(T<stop+2*dt);
            plot(T(idx),V(cnt,idx),'k','LineWidth',1);
            cnt = cnt+1;
        end
    end
    plot(zeros(size(V,1),1),flattenCellArray(Vp,'full'),'ko',...
        'MarkerFaceColor','m','MarkerSize',4);
    plot(-flattenCellArray(threshold_to_peak,'full'),flattenCellArray(Vth,'full'),...
        'ko','MarkerFaceColor','g','MarkerSize',4);
    plot(flattenCellArray(peak_to_ahp,'full'),flattenCellArray(Vahp,'full'),...
        'ko','MarkerFaceColor','b','MarkerSize',4);
    plot(flattenCellArray(peak_to_end,'full'),flattenCellArray(Vend,'full'),...
        'ko','MarkerFaceColor','r','MarkerSize',4);
    plot(flattenCellArray(peak_to_adp,'full'),flattenCellArray(Vadp,'full'),...
        'ko','MarkerFaceColor','y','MarkerSize',4);
    text(T(1), 2, '0 mV', 'HorizontalAlignment','Left');
    text(T(1), -69, '-65 mV', 'HorizontalAlignment','Left');
    axis tight;
    dt = floor(max_ahp_dur*1e3/2)*1e-3;
    placeScaleBars(T(end)-dt-0.5e-3,-30,dt,20,...
        {sprintf('%.0f ms',dt*1e3),[1,.5,0]},{'20 mV',[1,.5,0]},...
        'Color',[1,.5,0],'LineWidth',2);
    axis off
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,14,9],'PaperSize',[14,9]);
    print('-dpdf','spike_features.pdf');
end
