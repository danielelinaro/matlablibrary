function [entities,info,comments] = loadH5TraceV2(filename,entities_index)
% [entities,info,comments] = loadH5TraceV2(filename,entities_index)

if ~ exist('entities_index','var')
    entities_index = [];
end

entities_index = entities_index(:)';

try
    version = hdf5read(filename, '/Info/version');
    if version ~= 2
        error('Unknown version in H5 file.');
    end
catch
    error('Unknown version in H5 file.');
end

comments = {};
info = hdf5info(filename);
ngroups = length(info.GroupHierarchy.Groups);

for ii=1:ngroups
    if strcmp(info.GroupHierarchy.Groups(ii).Name,'/Entities')
        if isempty(entities_index)
            entities_index = 1:length(info.GroupHierarchy.Groups(ii).Groups);
        elseif max(entities_index) > length(info.GroupHierarchy.Groups(ii).Groups)
            error('Not enough entities in file %s', filename);
        end
        nentities = length(entities_index);
        entities = repmat( ...
            struct('id',[],'data',[],'events',struct([]),'metadata',[],'units','','name','',...
            'parameters',struct([])),[nentities,1]);
        for jj=1:nentities
            name = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Name;
            idx = strfind(name, '/');
            entities(jj).id = str2double(name(idx(end)+1:end));
            try
                entities(jj).data = hdf5read(filename, [name,'/Data']);
                [m,n] = size(entities(jj).data);
                if m>n
                    entities(jj).data = entities(jj).data';
                end
            catch
            end
            try
                entities(jj).metadata = hdf5read(filename, [name,'/Metadata'])';
            catch
            end
            nattrs = length(info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Attributes);
            for kk=1:nattrs
                name = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Attributes(kk).Name;
                idx = strfind(name, '/');
                name = lower(name(idx(end)+1:end));
                value = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Attributes(kk).Value;
                if isa(value, 'hdf5.h5string')
                    value = value.Data;
                end
                entities(jj).(name) = value;
            end
            nsubgroups = length(info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Groups);
            for kk=1:nsubgroups
                name = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Groups(kk).Name;
                idx = strfind(name, '/');
                name = name(idx(end)+1:end);
                if strcmpi(name, 'parameters')
                    npars = length(info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Groups(kk).Attributes);
                    for ll=1:npars
                        name = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Groups(kk).Attributes(ll).Name;
                        idx = strfind(name, '/');
                        name = lower(name(idx(end)+1:end));
                        value = info.GroupHierarchy.Groups(ii).Groups(entities_index(jj)).Groups(kk).Attributes(ll).Value;
                        if isa(value, 'hdf5.h5string')
                            value = value.Data;
                        end
                        entities(jj).parameters(1).(name) = value;                            
                    end
                end
            end
        end
    elseif strcmp(info.GroupHierarchy.Groups(ii).Name,'/Events')
        for dset = info.GroupHierarchy.Groups(ii).Datasets
            events(1).(dset.Name(9:end)) = hdf5read(filename, dset.Name);
        end
    elseif strcmp(info.GroupHierarchy.Groups(ii).Name,'/Comments')
        for attr = info.GroupHierarchy.Groups(ii).Attributes
            data = hdf5read(filename,attr.Name(1:length('/Comments')),...
                attr.Name(length('/Comments')+2:end));
            comments = [comments ; data.Data];
        end
    end
end

clear('info');
info.tend = hdf5read(filename,'/Info/tend');
info.dt = hdf5read(filename,'/Info/dt');
try
    nsec = double(hdf5read(filename,'/Info/startTimeSec'))*1e9 + double(hdf5read(filename,'/Info/startTimeNsec'));
    info.date = [datestr(datenum('1970','yyyy')+nsec/8.64e13, 'yyyymmdd HH:MM:SS.FFF'), ' UTC'];
catch
end
info.srate = 1.0 / info.dt;
info.version = 2;

try
    ts = hdf5read(filename,'/Timestamp');
    info.timestamp = ts.Data;
catch
end

if exist('events','var')
    for ii=1:length(entities)
        idx = find(events.Sender == entities(ii).id);
        entities(ii).events(1).times = double(events.Timestamp(idx)) * info.dt;
        entities(ii).events(1).codes = double(events.Code(idx));
    end
end
