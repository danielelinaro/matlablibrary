function [z,y,offset] = meanWithOffset(x, preamble)
% [z,y,offset] = meanWithOffset(x, preamble)

offset = mean(x(:,1:preamble),2);
y = x - repmat(offset, [1,size(x,2)]);
z = mean(y);
