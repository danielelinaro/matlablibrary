function [Itrimmed,Vsubthresh,Vrest,Vth] = ...
    extractCurrents(ref_point, time_after_ref, ttran)
% [Itrimmed,Vsubtrhesh,Vrest,Vth] = extractCurrents(ref_point, time_after_ref, ttran)

narginchk(2,5);

if ~ exist('ttran','var')
    ttran = 0.1;
end

if isempty(listH5Files)
    dirs = listDirectories;
else
    dirs = {'.'};
end

baseDir = pwd;
Ee = 0;
Ei = -80;

% consider only the first repetition of the stimulation
cd(dirs{1});

files = listH5Files;
entities = loadH5Trace(files{1});
t0 = entities(2).metadata(1);
dur = entities(2).metadata(2);

try
    [t,V,gampa,gampac,ggaba,ggabac,gnmda,gnmdac] = loadH5TracesBis(files,1,2,4,3,5,6,7);
    gnmda = max(gnmda, zeros(size(gnmda)));
    gnmdac = max(gnmdac, zeros(size(gnmdac)));
    with_nmda = 1;
catch
    [t,V,gampa,gampac,ggaba,ggabac] = loadH5TracesBis(files,1,2,4,3,5);
    with_nmda = 0;
end
idx = find(t >= t0+ttran & t <= t0+dur);
idx = idx(2:end-1);
if isempty(idx)
    keyboard
end
threshold = max(V,[],2) - 20;
threshold(threshold < -40) = 0;

%%%% remove spikes from the voltage trace and compute the currents
%%%% corresponding to the trimmed voltage traces
%%%% START

data = load('spike_features.mat','tth','tend');
Vtrimmed = trimSpikes(t,V,data.tth,data.tend,ref_point,time_after_ref);

Vtrimmed = Vtrimmed(:,idx)';
Vtrimmed = Vtrimmed(:);
Vsubthresh = nanmean(Vtrimmed);
Vrest = V(:,t<t0);
Vrest = mean(mean(Vrest,2));
[~,Vth] = extractAPThreshold(t,V,threshold);
Vth = mean(flattenCellArray(Vth,'full'));

%%% total conductances
Ge = gampa(:,idx) + gampac(:,idx);
if with_nmda
    Ge = Ge + gnmda(:,idx) + gnmdac(:,idx);
end
Gi = ggaba(:,idx) + ggabac(:,idx);
Ge = Ge';
Ge = Ge(:);
Gi = Gi';
Gi = Gi(:);
%%% total injected current TRIMMED
Itrimmed = Ge .* (Ee-Vtrimmed) + ...
    Gi .* (Ei-Vtrimmed);
%%%% END
cd(baseDir);
