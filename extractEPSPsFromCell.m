function extractEPSPsFromCell
cwd = pwd;
dirs = {'spontaneous','ap','fi','steps','tau','vi'};
ndirs = length(dirs);
tau_rise = [];
tau_decay = [];
amplitudes = [];
for i=1:ndirs
    if exist(dirs{i},'dir')
        cd(dirs{i});
        d = dir('0*');
        for j=1:length(d)
            cd(d(j).name)
            fprintf(1, 'Analysing data in %s/%s...\n', dirs{i}, d(j).name);
            [tr,td,amp] = extractEPSPsFromDirectory;
            if ~isempty(tr)
                tau_rise = [tau_rise, tr];
                tau_decay = [tau_decay, td];
                amplitudes = [amplitudes, amp];
            end
            cd('..')
        end
        cd(cwd);
    end
end
if ~isempty(amplitudes)
    save('epsp.mat','tau_rise','tau_decay','amplitudes');
end
end

function [tr,td,amp] = extractEPSPsFromDirectory
tr = [];
td = [];
amp = [];
if exist('fI_PID.xml','file')
    return;
end
ttran = 5e-3;
files = listH5Files;
if ~ isempty(files)
    entities = loadH5Trace(files{1});
    if ~ any(cellfun(@(x) strcmp(x,'mV'), {entities.units}))
        return;
    end
    kernels = dir('*_kernel.dat');
    if ~ isempty(kernels)
        Ke = load(kernels(1).name);
%         [t,Vr,I] = loadH5TracesBis(files,2,1);
        [t,Vr,I] = loadH5Traces(files);
        V = AECoffline(Vr,I,Ke);
        stop = [];
        k = 1;
        while isempty(stop)
            stop = find(I(k,:)~=0,1);
            k = k+1;
        end
    else
        try
            [t,V,I] = loadH5TracesBis(files,2,1);
            stop = find(I(1,:)~=0,1);
            if isempty(stop)
                stop = length(t);
            end
        catch
            % spontaneous recording
            [t,V] = loadH5TracesBis(files,1);
            stop = length(t);
        end
    end
elseif exist('recording.mat','file')
    data = load('recording.mat');
    if ~ isfield(data,'V')
        tr = [];
        td = [];
        amp = [];
        return;
    end
    V = data.V;
    if isfield(data,'I')
        I = data.I;
        if size(I,1) ~= size(V,1)
            I = repmat(I,[size(V,1),1]);
        end
    end
    t = (0:size(V,2)-1) * data.dt;
    if isfield(data,'tstart')
        stop = round(data.tstart/data.dt);
    else
        stop = length(t);
    end
else
    tr = [];
    td = [];
    amp = [];
    return;
end

V = filterTrace(V, 1/diff(t(1:2)), 500, 2);
idx = find(t > ttran & t < t(stop));
if exist('I','var')
    ndx = all(I(:,idx)==0,2);
else
    ndx = 1:size(V,1);
end
try
t = t(idx) - t(idx(1));
catch
    keyboard
end
V = V(ndx,idx);
[tr,td,amp] = extractEPSPs(t,V,5,[5e-3,100e-3],1);
end
