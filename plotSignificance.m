function plotSignificance(x,y,dx,dy,n)
% plotSignificance(x,y,dx,dy,n)

hold on;
plot([x,x+dx],[y,y],'k','LineWidth',1.2);
plot([x,x],[y-dy,y],'k','LineWidth',1.2);
plot([x,x]+dx,[y-dy,y],'k','LineWidth',1.2);
if n == 0
    str = 'ns';
else
    str = '';
    for k=1:n
        str = [str, '*'];
    end
end
text(x+dx/2,y+2.3*dy,str,'HorizontalAlignment','Center','VerticalAlignment','Top');

