function dirs = listDirectories(baseDir,pattern)
% listDirectories returns a (non-recursive) list of the directories
% contained in a base path, ignoring hidden files.
% 
% dirs = listDirectories(baseDir,pattern)
% 
% Arguments:
%      baseDir - the base directory for the search (default: .).
%      pattern - a pattern to append to the base directory (default: '*').
% 
% Returns:
%  A cell array containing all the non-hidden directory names.
% 

% Author: Daniele Linaro - May 2013

if ~ exist('baseDir','var')
    baseDir = '.';
end

if ~ exist('pattern','var')
    pattern = '*';
end

d = dir([baseDir,'/',pattern]);
dirs = {};
for k=1:length(d)
    if d(k).isdir && d(k).name(1) ~= '.'
        dirs = [dirs; d(k).name];
    end
end

