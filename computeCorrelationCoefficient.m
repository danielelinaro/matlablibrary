function [rhoT,covT,varT] = computeCorrelationCoefficient(Ni,Nj)
% computeCorrelationCoefficient computes the spike count correlation
% between blocks of trials.
% 
% rhoT = computeCorrelationCoefficient(Ni,Nj)
% 
% Arguments:
%     Ni,Nj - 2 cell arrays containing the repetitions of the stimulation
%             blocks performed on each cell. The number of repetitions
%             needs not be the same for each cell, but the number of trials
%             in each block must be.
%
% Returns:
%      rhoT - correlation between spike counts.
%      covT - covariance between spike counts.
%      varT - variance between spike counts.
% 

% Author: Daniele Linaro with fix by Gabriel Ocker

Ri = length(Ni);
Rj = length(Nj);
rhoT = 0;
covT = 0;
varT = 0;
for i=1:Ri % sum over repetitions
    for j=1:Rj
        rhoT = rhoT + compute_rho(Ni{i},Nj{j});
        covT = covT + compute_covariance(Ni{i},Nj{j});
        var_i = compute_covariance(Ni{i},Ni{i});
        var_j = compute_covariance(Nj{j},Nj{j});
        varT = varT + sqrt(var_i.*var_j);
    end
end
rhoT = rhoT / (Ri*Rj);
covT = covT / (Ri*Rj);
varT = varT / (Ri*Rj);

end

function r = compute_rho(Ni,Nj)
cov_ij = compute_covariance(Ni,Nj);
var_i = compute_covariance(Ni,Ni);
var_j = compute_covariance(Nj,Nj);
r =  cov_ij / sqrt(var_i * var_j);
end

function c = compute_covariance(Ni,Nj)
N = min(size(Ni,1),size(Nj,1));
if size(Ni,1) ~= size(Nj,1)
    fprintf('Using only %d trials instead of %d.\n', ...
        N, max(size(Ni,1),size(Nj,1)));
end
if size(Nj,1) == N
    Nj = [Nj ; Nj(1,:)];
end
c = 0;
for k=1:N % sum over trials
    c = c + Ni(k,:)*Nj(k,:)' - Ni(k,:)*Nj(k+1,:)';
end
c = c / (N*(size(Ni,2))/2); % divide by number of windows
end

