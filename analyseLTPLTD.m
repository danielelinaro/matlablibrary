function [seconds,Rin,slope,tau_rise,tau_decay,amplitude] = analyseLTPLTD(data_dirs, fit_window, do_plot)
% analyseLTPLTD performs an analysis of an experimental protocol for the
% induction of LTP or LTD.
% 
% [seconds,Rin,slope,tau_rise,tau_decay,amplitude] = analyseLTPLTD(data_dirs, fit_window, do_plot)
% 
% Parameters:
%   data_dirs - a 3-elements array of cells containing the paths of the
%               directories where the files are stored, before, during and after the pairing.
%  fit_window - the duration of the window used to fit the EPSP.
%     do_plot - whether to plot the results, default is 1.
% 
% Returns:
%     seconds - the instants at which the extracellular pulses were applied.
%         Rin - the input resistance of the cell.
%       slope - the slope of the first 2.5 ms of the EPSP.
%    tau_rise - EPSP rise time constant.
%   tau_decay - EPSP decay time constant.
%   amplitude - amplitude of the EPSPs.
% 
% See also fitEPSP.

% Author: Daniele Linaro - April 2013.

DATA_FILE = 'ltp_ltd.mat';

if length(data_dirs) ~= 3 && ~ exist(DATA_FILE,'file')
    error('You must specify the path of the three directories that contain the data.');
end

if ~ exist('fit_window','var')
    fit_window = 0.2;
end

if ~ exist('do_plot','var')
    do_plot = 1;
end

cwd = pwd;

%%% these quantities are computed for all conditions
seconds = cell(3,1);
minutes = cell(3,1);
Rin = cell(3,1);

%%% these quantities are computed only before and after the pairing
tau_rise = cell(2,1);
tau_decay = cell(2,1);
amplitude = cell(2,1);
slope = cell(2,1);
Vmean = cell(2,1);

pairing_dir = data_dirs{2};
data_dirs{2} = data_dirs{3};
data_dirs{3} = pairing_dir;

if ~ exist(DATA_FILE, 'file')
    for k=1:3
        cd(data_dirs{k});
        fprintf(1, 'Analysing data in directory [%s]...\n', data_dirs{k});
        files = listH5Files;
        nFiles = length(files);
        seconds{k} = cellfun(@(x) datevec(x(1:14),'yyyymmddHHMMSS') * ...
            [0;0;0;3600;60;1], files);
        kernels = dir('*_kernel.dat');
        nKernels = length(kernels);
        if nKernels > 0
            fprintf(1, 'Loading data... ');
            [t,Vr,Vext,I] = loadH5TracesBis(files,1,2,3);
            fprintf(1, 'done.\n');
            dt = diff(t(1:2));
            block = round(nFiles / nKernels);
            Vc = zeros(size(Vr));
            for ii=1:nKernels
                Ke = load(kernels(ii).name);
                idx = (ii-1)*block+1 : ii*block;
                Vc(idx,:) = AECoffline(Vr(idx,:),I(idx,:),Ke);
            end
            idx = idx(end)+1:nFiles;
            Vc(idx,:) = AECoffline(Vr(idx,:),I(idx,:),Ke);
            Vm = removeArtifacts(Vc,Vext);
            %%% estimate the input resistance
            fprintf(1, 'Estimating the input resistance of all trials... ');
            stim = find(I(1,:) < 0);
            if ~ isempty(stim)
                pre_stim = stim(1) - (round(0.3/dt):-1:1);
                stim = stim(end-round(50e-3/dt):end);
                dI = I(1,stim(1)) * 1e-12;  % [A]
                dV = (mean(Vm(:,stim),2) - mean(Vm(:,pre_stim),2)) * 1e-3;   % [V]
                Rin{k} = dV / dI * 1e-6;    % [MOhm]
            end
            fprintf(1, 'done.\n');
        else
            [t,Vr,Vext] = loadH5Traces(files);
            dt = diff(t(1:2));
            Vm = removeArtifacts(Vr,Vext);
        end
        if k < 3     % pre- and post-pairing
            t0 = t(find(Vext(1,:)>0,1));
            tmp = meanWithOffset(Vm,round(t0/2/dt));
            Vmean{k} = tmp(:,t>t0-30e-3 & t<t0+fit_window);
            fprintf(1, 'Extracting the parameters of the EPSPs... ');
            [tau_rise{k},tau_decay{k},amplitude{k},slope{k}] = fitEPSP(t,Vm,t0,fit_window);
            fprintf(1, 'done.\n');
            [seconds{k},idx] = sort(seconds{k});
            if ~ isempty(Rin{k})
                Rin{k} = Rin{k}(idx);
            end
            tau_rise{k} = tau_rise{k}(idx);
            tau_decay{k} = tau_decay{k}(idx);
            amplitude{k} = amplitude{k}(idx);
            idx = find(amplitude{k} > mean(amplitude{k}) - 2*std(amplitude{k}) & ...
                amplitude{k} < mean(amplitude{k}) + 2*std(amplitude{k}));
            seconds{k} = seconds{k}(idx);
            if ~ isempty(Rin{k})
                Rin{k} = Rin{k}(idx);
            end
            tau_rise{k} = tau_rise{k}(idx);
            tau_decay{k} = tau_decay{k}(idx);
            amplitude{k} = amplitude{k}(idx);
            slope{k} = slope{k}(idx);
            T = -30e-3 + (0:length(Vmean{1})-1) * dt;
        end
        cd(cwd);
    end
    for k=3:-1:1
        seconds{k} = seconds{k} - seconds{1}(1);
        minutes{k} = seconds{k} / 60;
    end
    fprintf(1, 'Saving data... ');
    save(DATA_FILE, 'T', 'seconds', 'minutes', 'Rin', 'slope', ...
        'tau_rise', 'tau_decay', 'amplitude', 'Vmean');
    fprintf(1, 'done.\n');
else
    fprintf(1, 'Loading data... ');
    load(DATA_FILE);
    fprintf(1, 'done.\n');
end

if do_plot
    fprintf(1, 'Plotting the results...\n');
    orange = [1,0.5,0];
    
    figure;

    axes('Position',[0.1,0.55,0.5,0.35],'NextPlot','Add');
    for k=1:2
        plot(minutes{k},slope{k},'ko');
        plot(minutes{k}([1,end]),mean(slope{k})+[0,0],'--','Color',orange,'LineWidth',2);
    end
    xlabel('Time (minutes)');
    ylabel('PSP slope (mV/ms)');
    axis([0,minutes{2}(end),0,max(mean(slope{1}),mean(slope{2})) + ...
        max(std(slope{1}),std(slope{2}))*2]);
    
    axes('Position',[0.1,0.1,0.5,0.35],'NextPlot','Add');
    if ~ isempty(Rin{1})
        for k=1:3
            plot(minutes{k},Rin{k},'ko');
            plot(minutes{k}([1,end]),mean(Rin{k})+[0,0],'--','Color',orange,'LineWidth',2);
        end
        plot([minutes{1}(1),minutes{2}(end)],mean(Rin{1})*1.15+[0,0],'r--','LineWidth',2);
        plot([minutes{1}(1),minutes{2}(end)],mean(Rin{1})*0.85+[0,0],'r--','LineWidth',2);
        ylabel('Input resistance (MOhm)');
        axis([0,minutes{2}(end),0,1.5*max(cellfun(@(x) mean(x), Rin)) + ...
            max(cellfun(@(x) std(x), Rin))*2]);
    else
        for k=1:2
            plot(minutes{k},amplitude{k},'ko');
            plot(minutes{k}([1,end]),mean(amplitude{k})+[0,0],'r--','LineWidth',2);
        end
        ylabel('PSP amplitude (mV)');
        axis([0,minutes{2}(end),0,max(mean(amplitude{1}),mean(amplitude{2})) + ...
            max(std(amplitude{1}),std(amplitude{2}))*2]);
    end
    
    axes('Position',[0.7,0.55,0.2,0.35],'NextPlot','Add');
    plot(T,Vmean{1},'k');
    plot(T,Vmean{2},'r');
    axis tight;
    axis off;
    placeScaleBars(100e-3,0.5,50e-3,1,'50 ms','1 mV','k');
    hndl = legend('Pre','Post');
    set(hndl, 'Box', 'Off');

    [h,p,ci] = ttest2(amplitude{1},amplitude{2});
    axes('Position',[0.7,0.1,0.1,0.35],'NextPlot','Add');
    barwitherr(cellfun(@(x) std(x), amplitude), ...
        cellfun(@(x) mean(x), amplitude), 0.8, ...
        'FaceColor', [0.6,0.6,1], 'EdgeColor', 'w');
    plotSignificance(1,ceil(max(mean(amplitude{1})+2*std(amplitude{1}),...
        mean(amplitude{2})+2*std(amplitude{2}))),1,0.1,h);
    yl = ylim;
    axis([0.5,2.5,0,yl(2)]);
    ylabel('PSP amplitude (mV)');
    set(gca, 'XTick', [1,2], 'XTickLabel', {'Pre','Post'}, 'TickDir', 'Out');
    
    [h,p,ci] = ttest2(slope{1},slope{2});
    axes('Position',[0.875,0.1,0.1,0.35],'NextPlot','Add');
    barwitherr(cellfun(@(x) std(x), slope), ...
        cellfun(@(x) mean(x), slope), 0.8, ...
        'FaceColor', [0.6,0.6,1], 'EdgeColor', 'w');
    plotSignificance(1,ceil(10*max(mean(slope{1})+2*std(slope{1}),...
        mean(slope{2})+2*std(slope{2})))/10,1,0.05,h);
    yl = ylim;
    axis([0.5,2.5,0,yl(2)]);
    ylabel('PSP slope (mV/ms)');
    set(gca, 'XTick', [1,2], 'XTickLabel', {'Pre','Post'}, 'TickDir', 'Out');
    
    set(gcf, 'Color', 'w', 'PaperUnits', 'Inch', 'PaperPosition', [0,0,8,6]);
    print('-depsc2','ltp_ltd.eps');
end
