function addToDatabase(db, table, columns, values)
% addToDatabase adds an entry into the database.
% 
% addToDatabase(db, table, columns, values)
% 
% Parameters:
%       db - a string containing the name of the database.
%    table - a string containing the name of the table where the search will
%            be performed.
%  columns - a cell array with the names of the columns to be inserted.
%   values - a cell array with the corresponding values.
% 
% Example:
% 
% addToDatabase('electrophysiology','experimenters',{'name','surname'},{'Daniele','Linaro'});
% 
% See also findInDatabase, updateInDatabase.
%

% Author: Daniele Linaro - February 2013

if ~ iscell(columns)
    columns = {columns};
end
if ~ iscell(values)
    values = {values};
end

if length(columns) ~= length(values)
    error('Columns and values must have the same length.');
end
n = length(columns);

try
    hndl = mysql('open','localhost','daniele','ilenia');
catch err
    error('Unable to connect to the database [%s].', err.identifier);
end

foo = mysql(sprintf('use %s', db));

query = sprintf('INSERT INTO %s (', table);
for k=1:n
    query = [query, columns{k}];
    if k < n
        query = [query, ', '];
    end
end
query = [query, ') VALUES ('];
for k=1:n
    if ischar(values{k})
        query = [query, '''', values{k}, ''''];
    else
        query = [query, num2str(values{k},'%.10f')];
    end
    if k < n
        query = [query, ', '];
    end
end
query = [query, ');'];

try
foo = mysql(query);
catch
    keyboard
end

mysql('close');
