function [AP, voltageTrace, dt, myFigure]  = analyzeAP(voltageTrace, dt, varargin)
% Extracts spike properties of all spikes in this trace and returns it in AP. 
% INPUT
% 
% - dt, voltageTrace : timestep and voltage in [s] and [mV]. ONE TRACE AT A TIME.
% - varargin : use to overwrite default values (see "DEFAULT PARAMETERS") defined below, BEFORE the
% part of code that evaluates Varargin. Varargin is evaluated like this: 
% provide (Name, value) pairs in argument list, as ('figHandle', 67).
% 
% OUTPUT
% 
% - new_voltageTrace : resampled voltage trace  (see resampling below), if resampling not diabled.
% - new_dt : new timestep (resampled)
% 
% - AP. ... :   (in all fileds below, one entry per spike, NaN if not measurable)
% * amplitudes : [mV] spike aplitudes from spike onset till peak in 
% * steepestUpstrokeSpeeds : [mV/s] maximal upstroke speed, defined as the maximum of the first derivative
% * steepestUpstrokeIndex : index, where this is on the voltage trace
% * averageUpstrokeSpeeds : [mV] upstroke speed as the slope of the line connecting spike base to its peak.
% * steepestDownstrokeIndex : index, where this is on the voltage trace
% * steepestDownstrokeSpeeds : [mV/s] maximal downstroke speed, defined as the maximum of the first derivative
% * averageDownstrokeSpeeds : [mV] downstroke speed in as the slope of the line connecting spike peak to its end.
% * halfwidths : [mV], full width of the spike at its half amplitude
% * upstrokeTime : [s], time from spike base to its peak.
% * downstrokeTime : [s], time from spike peak to its end.
% * onsetIndex : spike bases are at voltageTrace(baseIndex) at time timeTrace(baseIndex)
% * endIndex : spike ends are at voltageTrace(baseIndex) at time timeTrace(baseIndex)
% * peakIndex : spike peaks are at voltageTrace(baseIndex) at time timeTrace(baseIndex)
% * traces : [mV] actual spike traces, peak-alligned (to traces_peakIndex),
% cut out from voltage trace from prePeakTime to postPeakTime around each
% spike peak. Each ROW is a trace.
% * traces_peakIndex : ... see above .
% * sagVs : [mV] absolute value of voltage of the sag (the minimum voltage between 2 spikes. For the last spike it is the min voltage 100ms after the spike's ending point).
% * sagIndex : index of this point on the trace 
%
% - myFigure.xx contains the figure properties, if plotting enbled, as:
% * figHandle : handle of figure
% * traceHandles : handles of plot objects of traces


% PROCEDURE
%  Comments are provided in code, but in a nutshell:
% * spikes are detected by threshold crossing and peaks are extracted
% * For each spike:
% ** spike onset is detected from the location of fist peak of the voltage's 3dr derivative
% ** other characteristics are determined as described in the OUTPUT section.

% Istvan Biro, 2013.04.09.


%% DEFAULT PARAMETERS - can be overwritten by varargin as described below

onlyTimeInterval = [-Inf Inf]; % [s <to> s] process only selected part of the trace - from ... to.
onlySpikeInterval = [1 Inf]; %  only process spikes from spike number onlySpikeInterval(1) to onlySpikeInterval(2). Upper limit set to Inf means all.
figHandle = 0;   % 0 -> no plotting, a valid hadle -> plot on it, otherwise create this handle
prePeakTime = 4/1000;  % [s] time to consider, before each peak, when analizing spike properties.
postPeakTime = 8/1000; % [s] time to consider, after each peak, when analizing spike properties.
threshold = -10; % [mV] spike detection threshold
fetchTraces = 1; % [1 or 0] return single spike traces in AP.traces or not (MEMORY!)
resampleRate = -1; % [Hz] resampling of traces before processing to this sampling rate. If <=0 than no resampling will be done.
stopPointAboveOnsetPoint = 2; % [mV] defines when the spike is considered to have ended --> when looking for end of an Ap, this is found by the voltage crossing the level of AP onset (auto-detected via 2rd derivative) + stopPointAboveOnsetPoint. 
colormapName =  'jet';  % EITHER ONE color like [1 0 0]  OR name of colormap to use when plotting spikes, as 'jet' 'lines' 'autumn',...
plotExtractFeatures = 0; % [1 or 0] add extraced features as slopes, halfwidth, etc to the plot or not. (could make plot crowded!).

%% EVALUATE VARAGIN
%% evaluating varargin; (Name, value) pairs, as ('plotFitHandle', 67)
% expected
noVars = length(varargin);
for i=1:2:noVars
    if isnumeric(varargin{i+1})
        if (isscalar(varargin{i+1})), commandName=sprintf('%s%s%f%s',varargin{i},'=',varargin{i+1},';');
        else commandName=sprintf('%s%s%s%s',varargin{i},'=',mat2str(varargin{i+1}),';');
        end
    else commandName=sprintf('%s%s%s%s',varargin{i},'=''',varargin{i+1},''';');
    end
    eval(commandName);
end


%% make sure it is a ROW vector
if size(voltageTrace,1)>size(voltageTrace,2), voltageTrace = voltageTrace'; end


%% resample if necessary
if resampleRate>0 && dt~=1/resampleRate
    disp('* resampling trace.. this could take a while...');
    dt_new = 1/resampleRate;
    [voltageTrace, time] = resample_WS(voltageTrace, 1/dt, 1/dt_new);
    dt = dt_new;
else
    % keep voltageTrace and dt unchanged and generate time only
    time = 0:dt:dt*(length(voltageTrace)-1);
end

%% cut desired part of trace of needed
ind = find( time>=onlyTimeInterval(1) & time<=onlyTimeInterval(2) );
time = time(ind);
voltageTrace = voltageTrace(ind);
ind = []; % free up

%% detect spikes
peakIndexes = extract_peaks(voltageTrace, threshold);
fromspike = max([1 onlySpikeInterval(1)]);
tillspike = min([length(peakIndexes) onlySpikeInterval(2)]);

%% calculate dependencies and prepare
prePeakTimeIndex = ceil(prePeakTime / dt);  % index corresponding to time
postPeakTimeIndex = ceil(postPeakTime / dt); % index corresponding to time
AP = [];
% prepare figure to plot on if needed
if figHandle>0,  figure(figHandle); hold all; end
% generate colormap
spikeNo = length(peakIndexes([fromspike:tillspike]));
if isnumeric(colormapName)
     colors = repmat(colormapName,spikeNo,1);
else
    commandName=sprintf('%s%s%s%d%s','colors=',colormapName,'(', spikeNo,');');
    eval(commandName);
end
myFigure = [];
myFigure.figHandle = figHandle; 

%% some memoty allocation
AP.amplitudes = NaN * ones(1,spikeNo);
AP.steepestUpstrokeSpeeds  = NaN * ones(1,spikeNo);
AP.steepestUpstrokeIndex  = NaN * ones(1,spikeNo);
AP.averageUpstrokeSpeeds  = NaN * ones(1,spikeNo);
AP.steepestDownstrokeIndex  = NaN * ones(1,spikeNo);
AP.steepestDownstrokeSpeeds  = NaN * ones(1,spikeNo);
AP.averageDownstrokeSpeeds  = NaN * ones(1,spikeNo);
AP.halfwidths  = NaN * ones(1,spikeNo);
AP.upstrokeTime  = NaN * ones(1,spikeNo);
AP.downstrokeTime  = NaN * ones(1,spikeNo);
AP.onsetIndex  = NaN * ones(1,spikeNo);
AP.endIndex  = NaN * ones(1,spikeNo);
AP.peakIndex  = NaN * ones(1,spikeNo);
AP.sagVs = NaN * ones(1,spikeNo);
AP.sagIndex = NaN * ones(1,spikeNo);
if fetchTraces ==1
    AP.traces = NaN.* ones(spikeNo, postPeakTimeIndex+prePeakTimeIndex+1 );
    AP.traces_peakIndex = NaN * ones(1,spikeNo);
end
 


%% go peak by peak and analize
for i=fromspike:tillspike
    ii = i-fromspike+1;    % index fo output structure AP.xx
    
    
    %%%%%%%%% obtain right interval %%%%%%%%%%%%%
    % start looking prePeakTimeIndex before a peak OR at 
    % 1/3 interspike-interval - whichever is closest to our current spike.
    % Similar strategy for after the peak.
    % Also memorize peak location on this interval.
    if i==1, ISIbefore = Inf;   else ISIbefore = peakIndexes(i) - peakIndexes(i-1); end
    if i==length(peakIndexes), ISIafter = Inf;   else ISIafter = peakIndexes(i+1) - peakIndexes(i); end
    v_peakAtI = min([ ceil(0.3*ISIbefore)  prePeakTimeIndex ]); % index of peak in the interval [startLookingFromI stopLookingAtI] : it is the index corresponding to the pre-spike time or the ISI imposed limit (whichever is smaller)
    startLookingFromI = peakIndexes(i) - v_peakAtI;
    stopLookingAtI =    peakIndexes(i) + min([ ceil(0.6*ISIafter)  postPeakTimeIndex]);
    % save peak index
    AP.peakIndex(ii) = peakIndexes(i);
    
    
    %%%%%%%%% obtain trace of this spike only %%%%%%%%%%%%%
    % WE MAKE A SUB-TRACE HERE TO ONLY DEAL WITH THIS ONE SPIKE AT A TIME
    % (note: consequent index adjustments for full trace reference will be needed)
    % calculate derivates and offset them to correct for differentiation-induced
    % time(length)-error(offset), by repeating the first and last points
    v = voltageTrace(startLookingFromI:stopLookingAtI);
    t = time(startLookingFromI:stopLookingAtI);
    dvdt = (v(3:end)-v(1:end-2)) ./ (2*dt);
    dvdt = [ dvdt(1) dvdt dvdt(end) ]; % offset and length correction by artificial repetition of first and last element
    d2vdt2 = (dvdt(3:end)-dvdt(1:end-2)) ./ (2*dt);
    d2vdt2 = [ d2vdt2(1) d2vdt2 d2vdt2(end) ];
    d3vdt3 = (d2vdt2(3:end)-d2vdt2(1:end-2)) ./ (2*dt);
    d3vdt3 = [ d3vdt3(1) d3vdt3 d3vdt3(end) ];
    d2vdt2 = []; % not needed, free up    

    
    % Steepest UPSTROKE point... from the 1st derivative, before the peak value
    [max_upstroke max_upstroke_ind ] = max( dvdt(1:v_peakAtI) );
    AP.steepestUpstrokeIndex(ii) = max_upstroke_ind + startLookingFromI - 1;
    % index in the entire trace is:   index in subtrace of this spike +
    % index in full trace where this subtrace is starting from, -1 because
    % the subtrace (in which did the search) indexing starts at 1 not 0.
    AP.steepestUpstrokeSpeeds(ii) = max_upstroke;
    
    % spike base threshold  
    % Obtain it from the 3rd deriavtive, as its first peak 
    % (Henze and Buzsaki,Neuroscience, 2001). Note that the first peak must
    % be before the maxima of the first derivative of the voltage, which is
    % in turn before the peak value.
    [d3val v_onset_ind ] = max( d3vdt3(1:max_upstroke_ind) );  
    v_onset = v(v_onset_ind);
    AP.onsetIndex(ii) = v_onset_ind + startLookingFromI - 1; %   indexes, as explained above
    
    % spike amplitude : difference from base to peak
    AP.amplitudes(ii) = v(v_peakAtI) - v(v_onset_ind);
    
    % average upstroke and rise time
    AP.upstrokeTime(ii) = t(v_peakAtI)-t(v_onset_ind);
    AP.averageUpstrokeSpeeds(ii) = AP.amplitudes(ii) / AP.upstrokeTime(ii);
    
    % spike stop location - this shall be where the voltage crosses the
    % upstroke base level + stopPointAboveOnsetPoint mV , after the spike peak
    v_end_v_ind = find( v(v_peakAtI:end) < v_onset+stopPointAboveOnsetPoint );
    if isempty(v_end_v_ind)
        v_end_v_ind = length(v);  % the end of the trace if not found before
        disp(' ******* WARNING : AP detection ! ! ! -- the end of the spike could not be found - USING END OF WINDOW AS VALUE! Please consider enlarging the spike detection window by adjusing <postPeakTime> or <stopPointAboveOnsetPoint> ');
    else
        v_end_v_ind = v_end_v_ind + v_peakAtI - 1; 
        %  v_peakAtI is added because we search from this starting point :
        %  v(v_peakAtI:end) and -1 is due to the fact that indexing in
        %  v(v_peakAtI:end) starts from 1 not 0.
        v_end_v_ind = v_end_v_ind(1); 
    end
    AP.endIndex(ii) = v_end_v_ind + startLookingFromI - 1; %   indexes, as explained above
    
    % maximum DOWNSTROKE value - min of dvdt, after the spike peak
    [max_downstroke max_downstroke_ind ] =  min( dvdt(v_peakAtI:end) ) ;
    max_downstroke_ind = max_downstroke_ind + v_peakAtI - 1;  %   indexes, as explained above
    AP.steepestDownstrokeIndex(ii) = max_downstroke_ind + startLookingFromI - 1; %   indexes, as explained above
    AP.steepestDownstrokeSpeeds(ii) = max_downstroke;

    
    % average downstroke speed, similar to upstroke
    AP.downstrokeTime(ii) = t(v_end_v_ind) - t(v_peakAtI);
    AP.averageDownstrokeSpeeds(ii) = AP.amplitudes(ii) / AP.downstrokeTime(ii);

    
    % halfwidth... get 2 closest points to the half maximul level, on both sides 
    % of the peak and fit 1-1 line on these, than extract exact values.
    v_half = AP.amplitudes(ii)/2 +  v_onset;
    ind_u_1 = find( v(1:v_peakAtI)>v_half );
    ind_u_1 = ind_u_1(1);
    ind_u_0 = ind_u_1 - 1; 
    x1u = v(ind_u_0); x2u=v(ind_u_1);
    y1u = t(ind_u_0); y2u=t(ind_u_1);
    mu = (y2u-y1u)/(x2u-x1u);
    t1 = mu*v_half + y1u-mu*x1u;
    
    
    ind_d_1 = find( v(v_peakAtI:end)<v_half );
    ind_d_1 = ind_d_1(1) + v_peakAtI -1;
    ind_d_0 = ind_d_1 - 1; 
    x1d = v(ind_d_0); x2d=v(ind_d_1);
    y1d = t(ind_d_0); y2d=t(ind_d_1);
    md = (y2d-y1d)/(x2d-x1d);
    t2 = md*v_half + y1d-md*x1d;
    
    AP.halfwidths(ii) = t2-t1;
    
    
    % SAG
    % * sagVs : [mV] absolute value of voltage of the sag 
    % (the minimum voltage between 2 spikes. For the last spike it is the
    % min voltage 100ms after the spike's ending point).
    % * sagIndex : index of this point on the trace
    if i < length(peakIndexes)  % all spikes, except the last one
        [max_downstroke max_downstroke_ind ] =  min( dvdt(v_peakAtI:end) ) ;
        [AP.sagVs(ii) AP.sagIndex(ii) ] = min(voltageTrace( [ peakIndexes(i):peakIndexes(i+1) ] ));
        AP.sagIndex(ii) = AP.sagIndex(ii) + peakIndexes(i) -1; % index correction as explained above
    else % last spike
        endi = min([ peakIndexes(i)+ceil(0.1/dt) length(voltageTrace) ]); % index of end : 100ms after last peak OR at end of available total trace
        [AP.sagVs(ii) AP.sagIndex(ii) ] = min( voltageTrace( [ peakIndexes(i):endi ] ) );
        AP.sagIndex(ii) = AP.sagIndex(ii) + peakIndexes(i) -1; % index correction as explained above
    end
    
    if i==1, ISIbefore = Inf;   else ISIbefore = peakIndexes(i) - peakIndexes(i-1); end
    if i==length(peakIndexes), ISIafter = Inf;   else ISIafter = peakIndexes(i+1) - peakIndexes(i); end

 
    % save trace  
    if fetchTraces == 1
        AP.traces(ii,:) = voltageTrace([ AP.peakIndex(ii)-prePeakTimeIndex : AP.peakIndex(ii)+postPeakTimeIndex ]);
    end
    
    
    % plot if needed
    if figHandle>0
        figure(figHandle); % make current
        pt = plot(t-t(v_peakAtI),v,'-', 'color', colors(ii,:));
        myFigure.traceHandles(ii) = pt; 
        
        if plotExtractFeatures==1  % plot spike features if needed
            pd = plot(t-t(v_peakAtI),dvdt*2*dt,'s--', 'color', colors(ii,:));
            pddd = plot(t-t(v_peakAtI),d3vdt3*(2*dt)^3,'d-.', 'color', colors(ii,:)); % /2/dt to make it visible on the AP scale
            legend([pt, pd, pddd] , {'v' '(dvdt)*dt' '(d3vdt3)*dt^3'});
            
            plot(time(AP.peakIndex(ii))-t(v_peakAtI) , voltageTrace(AP.peakIndex(ii)), 'kd' );
            plot(time(AP.onsetIndex(ii))-t(v_peakAtI) , voltageTrace(AP.onsetIndex(ii)), 'ks' );
            plot(time(AP.endIndex(ii))-t(v_peakAtI) , voltageTrace(AP.endIndex(ii)), 'ks' );
            plot(time(AP.steepestDownstrokeIndex(ii))-t(v_peakAtI) , voltageTrace(AP.steepestDownstrokeIndex(ii)), 'gd' );
            plot(time(AP.steepestUpstrokeIndex(ii))-t(v_peakAtI) , voltageTrace(AP.steepestUpstrokeIndex(ii)), 'gd' );
            
            plot(t(v_peakAtI)-t(v_peakAtI) , v_half, 'bo' );
            line( [t1 t2]-t(v_peakAtI) , [v_half v_half]);
        end
        
    end
    
    
end

%% save other tings 
AP.traces_peakIndex = prePeakTimeIndex + 1;

end
