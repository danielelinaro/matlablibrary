function neuron = makeBaseNeuronStruct(id, date, cell_species, experimenter, ...
                                       animal_type, animal_age, cell_age,...
                                       brain_region, brain_area, location, ...
                                       neuron_type, project_name, ...
                                       preparation_type, virus_name, ...
                                       treatment_name, differentiation_name)
% makeBaseNeuronStruct creates a (mostly empty) structure whose fields
% correspond to the fields in a record in the database.
% 
% neuron = makeBaseNeuronStruct(cell_id, cell_date, neuron_type, brain_area,
%                               animal_type, animal_age, experimenter,
%                               project_name, preparation_type, virus_name,
%                               treatment_name, differentiation_name)
% 
% Parameters:
%               id - a string used as identifier of the cell.
%             date - the date in which the experiment was performed, in the form
%                    YYYYMMDD.
%     cell_species - the species of the cell.
%      animal_type - a string that identifies the animal used in the experiment.
%                    Presently accepted values are 'wistar', 'bl6', 'nod/scid'.
%       animal_age - the age of the animal at the time of the experiment.
%         cell_age - the age of the cell at the time of the experiment.
%     brain_region - a string that identifies the brain region (e.g., 'cortex').
%       brain_area - a string that identifies the brain area (e.g., 'S1').
%         location - a string that identifies the location in the brain area (e.g.,
%                    'L5' or 'L2/3' in the cortex).
%      neuron_type - a string that identifies the type of neuron (e.g., 'pyramidal').
%     project_name - the name of the project the experiment belongs to.
% preparation_type - the type of preparation used ('in vitro' or 'in vivo').
%       virus_name - the virus used to infect the cells (default: '').
%   treatment_name - the treatment applied to the cells (default: '').
% differentiation_name - the differentiation (default: '').
% 
% Returns:
% A structure that needs to be completed and then can be passed to the function
% addNeuronToDB for inclusion in the database.
% 
% Example:
% 
% neuron = makeBaseNeuronStruct('20130226A01','20130226','Daniele Linaro',...
%           'Wistar', 18, 'Cortex', 'S1', 'L5', 'Pyramidal', 'sinusoids');
% 
% 
% See also makeNeuronStruct, addNeuronToDB.
% 

% Author: Daniele Linaro - February 2013.

db = 'electrophysiology';

if ~exist('virus_name','var') || isempty(virus_name)
    virus_name = '';
end

if ~exist('treatment_name','var') || isempty(treatment_name)
    treatment_name = '';
end

%%% find animal type in the DB
if ~ isempty(animal_type)
    animal_id = findInDatabase(db, 'animals', 'id', 'strain', animal_type);
    if isempty(animal_id)
        reply = input(sprintf('Animal type [%s] not found. Do you want to add it? Y/N [N]: ', animal_type), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            species = input('Enter animal species (rat, mouse, ...): ','s');
            strain = input('Enter animal strain (Wistar, C57BL/6, ...): ','s');
            addToDatabase('electrophysiology', 'animals', {'species','strain'}, {species,strain});
        else
            neuron = [];
            return;
        end
        animal_id = findInDatabase(db, 'animals', 'id', 'strain', strain);
    end
else
    animal_id = [];
end

%%% find cell species in the DB
species_id = findInDatabase(db, 'species', 'id', 'name', cell_species);
if isempty(species_id)
    reply = input(sprintf('Species [%s] not found. Do you want to add it? Y/N [N]: ', cell_species), 's');
    if isempty(reply)
        reply = 'N';
    end
    if strcmpi(reply,'y')
        addToDatabase('electrophysiology', 'species', 'name', cell_species);
    else
        neuron = [];
        return;
    end
    species_id = findInDatabase(db, 'species', 'id', 'name', cell_species);
end

%%% find experimenter in the DB
C = textscan(experimenter,'%s');
if length(C{1}) ~= 2
    error('Experimenter name should be in the form ''Name Surname''.');
end
experimenter_id = findInDatabase(db, 'experimenters', 'id', {'name','surname'},C{1});
if isempty(experimenter_id)
    reply = input(sprintf('Experimenter [%s %s] not found. Do you want to add it? Y/N [N]: ', C{1}{1}, C{1}{2}), 's');
    if isempty(reply)
        reply = 'N';
    end
    if strcmpi(reply,'y')
        name = input('Enter experimenter name: ','s');
        surname = input('Enter experimenter surname: ','s');
        addToDatabase('electrophysiology', 'experimenters', {'name','surname'}, {name,surname});
    else
        neuron = [];
        return;
    end
    experimenter_id = findInDatabase(db, 'experimenters', 'id', {'name','surname'},{name,surname});
end

%%% find brain region in the DB
if ~ strcmpi(preparation_type,'culture')
    region_id = findInDatabase(db, 'regions', 'id', 'name', brain_region);
    if isempty(region_id)
        reply = input(sprintf('Brain region [%s] not found. Do you want to add it? Y/N [N]: ', brain_region), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            addToDatabase('electrophysiology', 'regions', 'name', brain_region);
        else
            neuron = [];
            return;
        end
        region_id = findInDatabase(db, 'regions', 'id', 'name', brain_region);
    end
    
    %%% find brain area in the DB
    area_id = findInDatabase(db, 'areas', 'id', 'code', brain_area);
    if isempty(area_id)
        reply = input(sprintf('Brain area [%s] not found. Do you want to add it? Y/N [N]: ', brain_area), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            code = input('Enter brain area code (S1, V1, ...): ','s');
            description = input('Enter a brief description of the area: ','s');
            addToDatabase('electrophysiology', 'areas', {'code','description'}, {code,description});
        else
            neuron = [];
            return;
        end
        area_id = findInDatabase(db, 'areas', 'id', 'code', code);
    end
    
    %%% find location in the DB
    location_id = findInDatabase(db, 'locations', 'id', 'name', location);
    if isempty(location_id)
        reply = input(sprintf('Location [%s] not found. Do you want to add it? Y/N [N]: ', location), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            name = input('Enter location name (L2/3, L5, ...): ','s');
            description = input('Enter a brief description of the location: ','s');
            addToDatabase('electrophysiology', 'locations', {'name','description'}, {name,description});
        else
            neuron = [];
            return;
        end
        location_id = findInDatabase(db, 'locations', 'id', 'name', name);
    end
else
    region_id = [];
    area_id = [];
    location_id = [];
end

%%% find cell type in the DB
type_id = findInDatabase(db, 'cell_types', 'id', 'name', neuron_type);
if isempty(type_id)
    reply = input(sprintf('Neuron type [%s] not found. Do you want to add it? Y/N [N]: ', neuron_type), 's');
    if isempty(reply)
        reply = 'N';
    end
    if strcmpi(reply,'y')
        name = input('Enter neuron type (pyramidal, Purkinje, ...): ','s');
        description = input('Enter a brief description of the cell type: ','s');
        addToDatabase('electrophysiology', 'cell_types', {'name','description'}, {name,description});
    else
        neuron = [];
        return;
    end
    type_id = findInDatabase(db, 'cell_types', 'id', 'name', name);
end

%%% find project name in the DB
project_id = findInDatabase(db, 'projects', 'id', 'name', project_name);
if isempty(project_id)
    reply = input(sprintf('Project [%s] not found. Do you want to add it? Y/N [N]: ', project_name), 's');
    if isempty(reply)
        reply = 'N';
    end
    if strcmpi(reply,'y')
        name = input('Enter project name: ','s');
        description = input('Enter a brief description of the project: ','s');
        addToDatabase('electrophysiology', 'projects', {'name','description'}, {name,description});
    else
        neuron = [];
        return;
    end
    project_id = findInDatabase(db, 'projects', 'id', 'name', name);
end

%%% find preparation type in the DB
preparation_id = findInDatabase(db, 'preparations', 'id', 'type', preparation_type);
if isempty(preparation_id)
    reply = input(sprintf('Preparation type [%s] not found. Do you want to add it? Y/N [N]: ', preparation_type), 's');
    if isempty(reply)
        reply = 'N';
    end
    if strcmpi(reply,'y')
        addToDatabase('electrophysiology', 'preparations', {'type'}, {preparation_type});
    else
        neuron = [];
        return;
    end
    preparation_id = findInDatabase(db, 'preparations', 'id', 'type', preparation_type);
end

%%% find virus in the DB
if ~ isempty(virus_name)
    virus_id = findInDatabase(db, 'viruses', 'id', 'name', virus_name);
    if isempty(virus_id)
        reply = input(sprintf('Virus [%s] not found. Do you want to add it? Y/N [N]: ', virus_name), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            addToDatabase('electrophysiology', 'viruses', {'name'}, {virus_name});
        else
            neuron = [];
            return;
        end
        virus_id = findInDatabase(db, 'viruses', 'id', 'name', virus_name);
    end
    infected = 1;
else
    virus_id = [];
    infected = 0;
end

%%% find treatment in the DB
if ~ isempty(treatment_name)
    treatment_id = findInDatabase(db, 'treatments', 'id', 'name', treatment_name);
    if isempty(treatment_id)
        reply = input(sprintf('treatment [%s] not found. Do you want to add it? Y/N [N]: ', treatment_name), 's');
        if isempty(reply)
            reply = 'N';
        end
        if strcmpi(reply,'y')
            addToDatabase('electrophysiology', 'treatments', {'name'}, {treatment_name});
        else
            neuron = [];
            return;
        end
        treatment_id = findInDatabase(db, 'treatments', 'id', 'name', treatment_name);
    end
else
    treatment_id = [];
end

%%% find differentiation in the DB
if ~ isempty(differentiation_name)
    differentiation_id = findInDatabase(db, 'differentiations', 'id', 'name', differentiation_name);
    if isempty(differentiation_id)
        fprintf(1, 'differentiation [%s] not found.\n', differentiation_name);
    end
else
    differentiation_id = [];
end

neuron = struct('id', id, ...
        'date', date, ...
        'species_id', species_id, ...
        'animal_age', int32(animal_age), ...
        'cell_age', int32(cell_age), ...
        'experimenter_id', experimenter_id, ...
        'animal_id', animal_id, ...
        'region_id', region_id, ...
        'area_id', area_id, ...
        'location_id', location_id, ...
        'type_id', type_id, ...
        'project_id', project_id, ...
        'preparation_id', preparation_id, ...
        'infected', infected, ...
        'virus_id', virus_id, ...
        'treatment_id', treatment_id, ...
        'differentiation_id', differentiation_id);

