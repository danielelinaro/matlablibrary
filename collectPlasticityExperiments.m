function [time,amplitudes,areas,slopes,Rin,RMP] = collectPlasticityExperiments(cells)

data = cellfun(@(c) load([c,'/ltp_ltd/ltp_ltd.mat']), cells, ...
    'UniformOutput', 0);

ncells = length(data);
time = cell(ncells,1);
for i=1:ncells
    secs = data{i}.seconds;
    secs{2} = secs{2} + secs{1}(end);
    secs{3} = secs{3} + secs{2}(end);
    time{i} = (flattenCellArray(secs,'full')/60 - secs{3}(1)/60)';
end

limits = [-20,35];
dt = 1;
TIME = limits(1) : dt : limits(2);
amplitudes = nan(ncells,length(TIME));
areas = nan(ncells,length(TIME));
slopes = nan(ncells,length(TIME));
Rin = nan(ncells,length(TIME));
RMP = nan(ncells,length(TIME));
for i=1:ncells
    amp = flattenCellArray({data{i}.amplitude{1}, nan(length(time{i})-length(data{i}.amplitude{1})-...
        length(data{i}.amplitude{2}),1), data{i}.amplitude{2}},'full');
    amp = amp/nanmean(data{i}.amplitude{1});
    ar = flattenCellArray({data{i}.area{1}, nan(length(time{i})-length(data{i}.area{1})-...
        length(data{i}.area{2}),1), data{i}.area{2}},'full');
    ar = ar/nanmean(data{i}.area{1});
    slp = flattenCellArray({data{i}.slope{1}, nan(length(time{i})-length(data{i}.slope{1})-...
        length(data{i}.slope{2}),1), data{i}.slope{2}},'full');
    slp = slp/nanmean(data{i}.slope{1});
    R = flattenCellArray(data{i}.Rin);
    R = R/nanmean(data{i}.Rin{1});
    V0 = flattenCellArray(data{i}.V0);
    [~,bin] = histc(time{i},TIME);
    for j=unique(bin)
        idx = bin == j;
        amplitudes(i,j) = nanmean(amp(idx));
        areas(i,j) = nanmean(ar(idx));
        slopes(i,j) = nanmean(slp(idx));
        Rin(i,j) = nanmean(R(idx));
        RMP(i,j) = nanmean(V0(idx));
    end
    Rin(i,Rin(i,:)>1.5) = nan;
end


make_struct = @(x) struct('values',x,'mean',nanmean(x),'std',nanstd(x),...
    'sem',nanstd(x)/sqrt(size(x,1)));

amplitudes = make_struct(amplitudes);
areas = make_struct(areas);
slopes = make_struct(slopes);
Rin = make_struct(Rin);
RMP = make_struct(RMP);
time = TIME;

