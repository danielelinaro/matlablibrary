function preprocessVoltageStepsProtocol
% preprocessVoltageStepsProtocol

files = listH5Files;
if isempty(files) && exist('recording.mat','file')
    load('recording.mat');
    t = (0:size(I,2)-1) * dt;
    pre_dur = 200e-3;
    pre_tstart = tstart - pre_dur;
    dV = -10;
    if ~ exist('with_preamble','var')
        with_preamble = true;
    end
else
    entities = loadH5Trace(files{1});
    pre_tstart = entities(2).metadata(1,1);
    pre_dur = entities(2).metadata(2,1);
    tstart = sum(entities(2).metadata(1:2,1));
    dur = entities(2).metadata(3,1);
    [t,I,V] = loadH5TracesBis(files,1,2);
    idx = find(t > tstart, 1);
    dV = -diff(V(1:2,idx));
    with_preamble = true;
end

I = filterTrace(I,1/diff(t(1:2)),2000,2);
Im = mean(I);
I0 = mean(Im(t<20e-3));
if with_preamble
    Ip = min(Im(t>pre_tstart & t<pre_tstart + 5e-3));
    Ra = dV / (Ip-I0) * 1e3;
    [R,C] = extractRinAndCFromVoltageStep(t,I,dV,pre_tstart,pre_dur);
else
    Ra = nan;
    R = nan;
    C = nan;
end
[INa,Vh,I0] = extractINaFromVoltageStep(t,I,V,tstart,dur);

save('Vsteps_VC.mat','R','C','INa','Vh','I0','Ra');

idx = find(t>tstart-10e-3 & t<tstart+dur+10e-3);
m = min(min(I(:,idx)))*1.2;
M = max(max(I(:,t>tstart+50e-3 & tstart+dur)))*1.1;
figure;
subplot(2,1,1);
plot((t(idx)-tstart)*1e3,I(:,idx),'k','LineWidth',1);
axis([(t(idx([1,end]))-tstart)*1e3,m,M]);
box off;
xlabel('Time (ms)');
ylabel('Current (pA)');
set(gca,'TickDir','Out','LineWidth',0.8,'XTick',0:50:500);

[V,~,idx] = unique(Vh);
Im = zeros(size(V));
Is = zeros(size(V));
subplot(2,1,2);
hold on;
plot(V([1,end]),I0+[0,0],'r--','LineWidth',1);
for i=1:length(V)
    Im(i) = mean(INa(idx == i));
    Is(i) = std(INa(idx == i));
    plot(V(i)+[0,0],Im(i)+Is(i)*[-1,1],'k','LineWidth',1);
end
plot(V,Im,'ko-','LineWidth',1,'MarkerFaceColor','w','MarkerSize',4);
xlabel('Holding voltage (mV)');
ylabel('Peak inward current (pA)');
box off;
set(gca,'TickDir','Out','LineWidth',0.8);
text(-65,0.7*min(Im),sprintf('R_{a} = %.0f MOhm\n', Ra));
text(-65,0.8*min(Im),sprintf('R_{in} = %.0f MOhm\n', R));
text(-65,0.9*min(Im),sprintf('C = %.0f pF\n', C));
sz = [6,7];
set(gcf,'PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz,'Color','w');
print('-dpdf','Vsteps_VC.pdf');
