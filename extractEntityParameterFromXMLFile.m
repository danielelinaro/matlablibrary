function p = extractEntityParameterFromXMLFile(filename, entityName, parameterName)
% p = extractEntityParameterFromXMLFile(filename, entityName, parameterName)

p = [];

s = parseXML(filename);

for i=1:length(s.Children)
    if strcmp(s.Children(i).Name, 'entities') == 1
        for j=1:length(s.Children(i).Children)
            if strcmp(s.Children(i).Children(j).Name, 'entity') == 1
                for k=1:length(s.Children(i).Children(j).Children)
                    if strcmp(s.Children(i).Children(j).Children(k).Name, 'name') == 1 && ...
                            strcmp(s.Children(i).Children(j).Children(k).Children.Data, entityName) == 0
                        continue;
                    end
                    if strcmp(s.Children(i).Children(j).Children(k).Name, 'parameters') == 1
                        for l=1:length(s.Children(i).Children(j).Children(k).Children)
                            if strcmp(s.Children(i).Children(j).Children(k).Children(l).Name, parameterName) == 1
                                p = [p ; s.Children(i).Children(j).Children(k).Children(l).Children.Data];
                                break
                            end
                        end
                    end
                end
            end
        end
        break
    end
end
