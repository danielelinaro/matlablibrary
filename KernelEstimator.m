function varargout = KernelEstimator(varargin)
% KERNELESTIMATOR M-file for KernelEstimator.fig
%      KERNELESTIMATOR, by itself, creates a new KERNELESTIMATOR or raises the existing
%      singleton*.
%
%      H = KERNELESTIMATOR returns the handle to a new KERNELESTIMATOR or the handle to
%      the existing singleton*.
%
%      KERNELESTIMATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in KERNELESTIMATOR.M with the given input arguments.
%
%      KERNELESTIMATOR('Property','Value',...) creates a new KERNELESTIMATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before KernelEstimator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to KernelEstimator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help KernelEstimator

% Last Modified by GUIDE v2.5 25-Jan-2012 19:06:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @KernelEstimator_OpeningFcn, ...
                   'gui_OutputFcn',  @KernelEstimator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before KernelEstimator is made visible.
function KernelEstimator_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to KernelEstimator (see VARARGIN)

% Choose default command line output for KernelEstimator
handles.output = hObject;

disp(varargin)

handles.pathname = [pwd, '/'];

handles.bg_col = [0.7,0.7,0.7];
set(handles.Vaxes,'Color',handles.bg_col);%,'XTickLabel',[],'YTickLabel',[]);
axes(handles.Vaxes);
xlabel('t (s)'); ylabel('V (mV)');
set(handles.Iaxes,'Color',handles.bg_col);%,'XTickLabel',[],'YTickLabel',[]);
axes(handles.Iaxes);
xlabel('t (s)'); ylabel('I (pA)');
set(handles.Kaxes,'Color',handles.bg_col);%,'XTickLabel',[],'YTickLabel',[]);
axes(handles.Kaxes);
xlabel('t (ms)'); ylabel('R (MOhm)');
set(handles.Vlinaxes,'Color',handles.bg_col);%,'XTickLabel',[],'YTickLabel',[]);
axes(handles.Vlinaxes);
xlabel('t (s)'); ylabel('V (mV)');
set(handles.Vcompaxes,'Color',handles.bg_col);%,'XTickLabel',[],'YTickLabel',[]);
axes(handles.Vcompaxes);
xlabel('t (s)'); ylabel('V (mV)');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes KernelEstimator wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = KernelEstimator_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function filename_edit_Callback(hObject, eventdata, handles)
% hObject    handle to filename_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filename_edit as text
%        str2double(get(hObject,'String')) returns contents of filename_edit as a double


% --- Executes during object creation, after setting all properties.
function filename_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filename_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in browse_btn.
function browse_btn_Callback(hObject, eventdata, handles)
% hObject    handle to browse_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename,pathname] = uigetfile( ...
    {'*.h5','H5 files'; ...
     '*.dat','Dat files'}, ...
     'Pick a data file...','MultiSelect','On');
if filename == 0
    return;
end
handles.pathname = pathname;
localdir = pwd;
if strcmp(handles.pathname(1:end-1),localdir)
    set(handles.filename_edit,'String',filename);
    set(handles.output_edit,'String',[filename(1:end-4),'_kernel.dat']);
else
    set(handles.filename_edit,'String',[handles.pathname,filename]);
    set(handles.output_edit,'String',[handles.pathname,filename(1:end-4),'_kernel.dat']);
end
guidata(hObject,handles);

function fnames = makeFilenames(data_file)
if strcmp(data_file(end-1:end),'h5') == 1
    fnames = {data_file, [data_file(1:end-3),'_kernel.dat']};
else
    fnames = {data_file, ...
              [data_file(1:end-4),'_stim.dat'], ...
              [data_file(1:end-4),'_prot.stim'], ...
              [data_file(1:end-4),'_kernel.dat']};
end

% --- Executes on button press in load_btn.
function load_btn_Callback(hObject, eventdata, handles)
% hObject    handle to load_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(get(handles.filename_edit,'String'))
    disp('Error');
    return;
end
cla(handles.Vaxes);
cla(handles.Iaxes);
cla(handles.Kaxes);
cla(handles.Vlinaxes);
cla(handles.Vcompaxes);
if isfield(handles,'filenames')
    handles = rmfield(handles,'filenames');
end
filenames = makeFilenames(get(handles.filename_edit,'String'));
set(handles.output_edit,'String',filenames{end});
for k=1:length(filenames)-1
    if ~ exist(filenames{k},'file')
        set(handles.filename_edit,'String','');
        disp('Error');
        return
    end
end
handles.filenames = filenames;
if strcmp(handles.filenames{1}(end-1:end),'h5') == 1
    out = loadH5Trace(handles.filenames{1});
    handles.dt = 1./out.srate;
    handles.t = 0 : handles.dt : handles.dt*(length(out.data.ds1)-1);
    handles.V = out.data.ds1;
    handles.I = out.data.ds2;
    p = out.metadata.ds1;
else
    data = loadTrace(handles.filenames{1});
    handles.dt = 1/data.srate;
    handles.t = 0 : handles.dt : handles.dt*(length(data.ch1)-1);
    handles.V = data.ch1';
    data = loadTrace(handles.filenames{2});
    handles.I = data.ch1';
    p = load(handles.filenames{3});
end
handles.t0 = sum(p(1:end-2,1));
handles.t1 = sum(p(1:end-1,1));
set(handles.t_interval, 'String', sprintf('[%g,%g]', handles.t0, handles.t1));
draw_V_and_I_axes(handles);
guidata(hObject,handles);

function draw_V_and_I_axes(handles)
cla(handles.Vaxes);
cla(handles.Iaxes);
axes(handles.Vaxes)
plot(handles.t,handles.V,'k');
hold on;
plot(handles.t0+[0,0],ylim,'r--','LineWidth',2);
plot(handles.t1+[0,0],ylim,'r--','LineWidth',2);
xlabel('t (s)');
ylabel('V (mV)');
set(handles.Vaxes,'Color',handles.bg_col);
box off;
axes(handles.Iaxes)
plot(handles.t,handles.I,'k');
hold on;
plot(handles.t0+[0,0],ylim,'r--','LineWidth',2);
plot(handles.t1+[0,0],ylim,'r--','LineWidth',2);
xlabel('t (s)');
ylabel('I (pA)');
set(handles.Iaxes,'Color',handles.bg_col);
box off;

function kernel_dur_Callback(hObject, eventdata, handles)
% hObject    handle to kernel_dur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kernel_dur as text
%        str2double(get(hObject,'String')) returns contents of kernel_dur as a double


% --- Executes during object creation, after setting all properties.
function kernel_dur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kernel_dur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function t_interval_Callback(hObject, eventdata, handles)
% hObject    handle to t_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of t_interval as text
%        str2double(get(hObject,'String')) returns contents of t_interval as a double


% --- Executes during object creation, after setting all properties.
function t_interval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to t_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in compute_full_btn.
function compute_full_btn_Callback(hObject, eventdata, handles)
% hObject    handle to compute_full_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'Ke')
    handles = rmfield(handles,'Ke');
end
t0t1 = sscanf(get(handles.t_interval,'String'),'[%f,%f]');
if length(t0t1) ~= 2 || t0t1(1) > t0t1(2)
    disp('Error');
    return;
end
handles.t0 = t0t1(1);
handles.t1 = t0t1(2);
% draw_V_and_I_axes(handles);
dur = str2double(get(handles.kernel_dur,'String')) * 1e-3;
if isnan(dur)
    disp('Error');
end
handles.kdur = dur;
handles.ksize = round(handles.kdur / handles.dt);
idx = find(handles.t > handles.t0 & handles.t < handles.t1);
v = handles.V(idx) * 1e-3;
v(end-handles.ksize:end) = 0;
i = handles.I(idx) * 1e-12;
[handles.K,handles.v0] = fullKernel(v,i,handles.ksize);
handles.kt = 0 : handles.dt : handles.dt * (length(handles.K)-1);
plotKernels(handles);

cla(handles.Vlinaxes);
axes(handles.Vlinaxes);
vlin = handles.v0 + conv(handles.K,i);
vlin = (vlin(1:end-handles.ksize+1))*1e3;
plot(handles.t(idx),vlin,'k');
axis([get(handles.Vaxes,'xlim'),get(handles.Vaxes,'ylim')]);
% hold on;
% plot(handles.t0+[0,0],ylim,'r--','LineWidth',2);
% plot(handles.t1+[0,0],ylim,'r--','LineWidth',2);
% plot(xlim,min(handles.V(idx))+[0,0],'r--','LineWidth',2);
% plot(xlim,max(handles.V(idx))+[0,0],'r--','LineWidth',2);
xlabel('t (s)'); ylabel('V (mV)');
set(handles.Vlinaxes,'Color',handles.bg_col);
box off;

guidata(hObject,handles);

% --- Executes on button press in compute_electrode_btn.
function compute_electrode_btn_Callback(hObject, eventdata, handles)
% hObject    handle to compute_electrode_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clean = 0;
if get(handles.clean_ckbox,'Value') == get(handles.clean_ckbox,'Max')
   clean = 1;
end
tail = str2double(get(handles.start_tail,'String'));
if isnan(tail)
    disp('Error');
    return;
end
handles.tail = tail;
% [handles.Ke,handles.Re] = electrodeKernel(handles.K, handles.dt, handles.tail, clean);
[handles.Ke,handles.Re] = electrodeKernel(handles.K, handles.tail);
plotKernels(handles);

Ue = conv(handles.Ke,handles.I*1e-12)*1e3;
Vc = handles.V - Ue(1:end-handles.ksize+1);
cla(handles.Vcompaxes);
axes(handles.Vcompaxes);
plot(handles.t,Vc,'k');
axis([get(handles.Vaxes,'xlim'),get(handles.Vaxes,'ylim')]);
% hold on;
% plot(handles.t0+[0,0],ylim,'r--','LineWidth',2);
% plot(handles.t1+[0,0],ylim,'r--','LineWidth',2);
% plot(xlim,min(handles.V)+[0,0],'r--','LineWidth',2);
% plot(xlim,max(handles.V)+[0,0],'r--','LineWidth',2);
xlabel('t (s)'); ylabel('V (mV)');
set(handles.Vcompaxes,'Color',handles.bg_col);
box off;

guidata(hObject,handles);

function plotKernels(handles)
cla(handles.Kaxes);
axes(handles.Kaxes);
if isfield(handles,'Ke')
    plot(handles.kt*1e3,handles.K*1e-6,'k','LineWidth',2);
    hold on;
    plot(handles.kt*1e3,handles.Ke*1e-6,'r','LineWidth',2);
    plot(handles.kt*1e3,(handles.K-handles.Ke)*1e-6,'b','LineWidth',2);
    legend('Full','Electrode','Membrane');
    text(handles.kt(end)*1e3,max(handles.K)*1e-6/2.5,sprintf('R_e = %g MOhm', handles.Re*1e-6),...
    'HorizontalAlignment','Right');
else
    plot(handles.kt*1e3,handles.K*1e-6,'k-o','LineWidth',1);
    legend('Full');
end
text(handles.kt(end)*1e3,max(handles.K)*1e-6/2,sprintf('V_0 = %g mV', handles.v0*1e3),...
    'HorizontalAlignment','Right');
xlabel('t (ms)');
ylabel('R (MOhm)');
set(handles.Kaxes,'Color',handles.bg_col);
% set(handles.Kaxes,'XTickLabel',get(handles.Kaxes,'XTick'),'YTickLabel',get(handles.Kaxes,'YTick'));
% box off;

function start_tail_Callback(hObject, eventdata, handles)
% hObject    handle to start_tail (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of start_tail as text
%        str2double(get(hObject,'String')) returns contents of start_tail as a double


% --- Executes during object creation, after setting all properties.
function start_tail_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_tail (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on mouse press over axes background.
function Kaxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Kaxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over browse_btn.
function browse_btn_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to browse_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on start_tail and none of its controls.
function start_tail_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to start_tail (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over t_interval.
function t_interval_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to t_interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on t_interval and none of its controls.
function t_interval_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to t_interval (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
% if strcmp(eventdata.Key,'return')
%     t0t1 = sscanf(get(handles.t_interval,'String'),'[%f,%f]');
%     handles.t0 = t0t1(1);
%     handles.t1 = t0t1(2);
%     draw_V_and_I_axes(handles);
%     guidata(hObject,handles);
% end


% --- Executes on button press in save_btn.
function save_btn_Callback(hObject, eventdata, handles)
% hObject    handle to save_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~ isfield(handles,'Ke')
    disp('Error');
    return;
end
outfile = get(handles.output_edit,'String');
data = handles.Ke(:);
save('-ascii',outfile,'data');


function output_edit_Callback(hObject, eventdata, handles)
% hObject    handle to output_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_edit as text
%        str2double(get(hObject,'String')) returns contents of output_edit as a double


% --- Executes during object creation, after setting all properties.
function output_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in clean_ckbox.
function clean_ckbox_Callback(hObject, eventdata, handles)
% hObject    handle to clean_ckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of clean_ckbox
