function files = listH5Files(directory, ignoreKernels)
% LISTH5FILES lists H5 files in a directory, optionally
% ignoring those that correspond to protocols used for the
% computation of active electrode compensation kernels.
% 
% files = listH5Files(directory, ignoreKernels)
% 
% Arguments:
%      directory - the directory containing the H5 files (default: .).
%  ignoreKernels - whether to ignore kernel protocol files (default: yes).
% 
% Returns:
%  A cell array containing all the file names.
% 
% See also loadH5Trace
% 

% Author: Daniele Linaro - November 2012

if ~ exist('directory','var')
    directory = '.';
end

if ~ exist('ignoreKernels','var')
    ignoreKernels = 1;
end

if ignoreKernels
    h5files = dir([directory,'/*.h5']);
    files = [];
    for jj=1:length(h5files)
        if ~ exist([directory,'/',h5files(jj).name(1:end-3),'_kernel.dat'],'file')
            files = [files ; h5files(jj)];
        end
    end
    %%%%% remove this part afterwards
%     H5files = dir([directory,'/*.H5']);
%     if ~isempty(H5files)
%         files = [H5files; files];
%     end
    %%%%%
else
    files = dir([directory,'/*.h5']);
end

if ~ isempty(files)
    if strcmp(directory,'.')
        files = arrayfun(@(x) x.name, files, 'UniformOutput', 0);
    else
        files = arrayfun(@(x) sprintf('%s/%s', directory, x.name), files, 'UniformOutput', 0);
    end
end
