function [Ke,Re,Rm,taum] = electrodeKernel(K,dt,startTail,clean)
% [Ke,Re,Rm,taum] = electrodeKernel(K,dt,startTail,clean)
%
% Extracts the electrode kernel Ke from the raw kernel K
% by removing the membrane kernel, estimated from the
% indexes >= startTail of the raw kernel.
%
% K-Ke is the membrane kernel filtered by the electrode
%
% Parameters:
%         K - the full kernel (membrane + electrode)
%        dt - the acquisition time step
% startTail - the beginning of the exponential decay of the full kernel
%     clean - zero the beginning of the electrode kernel
% 
% Return values:
%        Ke - the electrode kernel
%        Re - the kernel resistance
%        Rm - the membrane resistance
%      taum - the membrane time constant
% 
% Author: R. Brette, Sep 2005
% Minor modifications by: D. Linaro, Dec 2011
%  

    function [err,Kel]=removeKm(RawK,x,tau,dt,tail)
    % Solves Ke = RawK - Km * Ke/Re
    % for an exponential Km
    % Returns the error on the tail
    
        alpha = x*dt/tau;
        lambda = exp(-dt/tau);
        Y = 0*RawK;
        Y(1) = alpha/(alpha+1)*RawK(1); % K(1) == 0 ?
        for i=2:length(RawK)
            Y(i) = (alpha*RawK(i)+lambda*Y(i-1))/(1+alpha);
        end
        Kel = RawK - Y;
        err = Kel(tail)'*Kel(tail);
    end
    
    if ~ exist('clean','var')
        clean = 0;
    end
    
    % Exponential fit of the tail
    tail = startTail:length(K);
    t = 0 : dt : dt*(length(K)-1);
    t = t';

    X = t(tail(1:30)) - t(tail(1));
    Y = log(K(tail(1:30)));
    p = polyfit(X,Y,1);
    rfit.a = exp(p(2));
    rfit.b = p(1);
    
    % Membrane time constant and resistance
    taum = -1/rfit.b;
    Rm = rfit.a*taum/dt - sum(rfit.a*exp(rfit.b*t(1:2)));
    % NB: the first two steps are ignored

    % Clean the beginning (optional)
    if clean
        K(1:2) = 0;
    end

    % Electrode resistance
%     Re = sum(K(3:startTail))-sum(rfit.a*exp(rfit.b*t(3:startTail)));
    Re = sum(K(1:startTail))-sum(rfit.a*exp(rfit.b*t(3:startTail)));
    
    % Replace tail by fit
    K(tail) = rfit.a * exp(rfit.b * t(tail));
    
    % Optimize the membrane kernel
    z = fminbnd(@(x) removeKm(K,x,taum,dt,tail),0.5*Rm/Re,2*Rm/Re);
    [err,Ke] = removeKm(K,z,taum,dt,tail);

    % Clean the electrode kernel (remove negative numbers)
    %Ke=Ke.*(Ke>0);

    % Display electrode resistance
%     Re = sum(Ke);
end
