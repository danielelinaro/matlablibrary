function features = analyseDCPulse(t,V,tstart,stepdur,stepamp,thresh,min_isi)
% features = analyseDCPulse(t,V,tstart,stepdur,stepamp,thresh)

if ~ exist('min_isi','var')
    min_isi = [];
end

if ~ exist('thresh','var')
    thresh = max(V,[],2) - 20;
    thresh(thresh < -40) = 0;
end

if isscalar(thresh)
    thresh = repmat(thresh,[size(V,1),1]);
end

srate = 1/diff(t(1:2));
nexp = size(V,1);

if isscalar(tstart)
    tstart = repmat(tstart, [nexp,1]);
end
if isscalar(stepdur)
    stepdur = repmat(stepdur, [nexp,1]);
end
tstop = tstart + stepdur;

[tp,Vp] = extractAPPeak(t,V,thresh,min_isi);
[tth,Vth] = extractAPThreshold(t,V,thresh,tp);
[Vhalf,width,interval] = extractAPHalfWidth(t,V,thresh,tp,Vp,tth,Vth);
[tahp,Vahp] = extractAPAHP(t,V,20e-3,thresh,tp,tth);
[tend,Vend] = extractAPEnd(t,V,thresh,tp,Vth,tahp);

Vfiltered = filterTrace(V,srate,500,2);
cnt = 1;
features = [];

for k=1:nexp
    
    if ~isempty(tp{k})
        features(cnt).nspikes = length(tp{k});
        features(cnt).stepdur = stepdur(k);
        features(cnt).stepamp = stepamp(k);
        idx = find(t>tstop(k) & t<tstop(k)+0.2);
        [m,i] = min(Vfiltered(k,idx));
        try
            out = fitExp(Vfiltered(k,idx(i):end),srate,2);
            features(cnt).ahp = struct('deflection', m - mean(V(k,t < tp{k}(1) - 0.1)), ...
                'taus', [out.F.b0,out.F.b1]);
        catch
            features(cnt).ahp = struct('deflection', nan, 'taus', [nan,nan]);
        end
%         figure;
%         hold on;
%         plot(t(t>tstop(k)),V(k,t>tstop(k)),'k');
%         plot(t(t>tstop(k)),Vfiltered(k,t>tstop(k)),'r');
%         plot(t(idx(i):end),out.F(t(idx(i):end)-t(idx(i)))+m,'g');
        features(cnt).tth = tth{k};
        features(cnt).Vth = Vth{k};
        features(cnt).tp = tp{k};
        features(cnt).Vp = Vp{k};
        features(cnt).Vhalf = Vhalf{k};
        features(cnt).width = width{k};
        features(cnt).tahp = tahp{k};
        features(cnt).Vahp = Vahp{k};
        features(cnt).tend = tend{k};
        features(cnt).Vend = Vend{k};
        if length(tp{k}) > 1
            features(cnt).AP_amplitude = Vp{k}(1:2) - Vth{k}(1:2);
            features(cnt).AP_duration = tend{k}(1:2) - tth{k}(1:2);
            features(cnt).AP_half_width = width{k}(1:2);
            features(cnt).AP_rise_time = tp{k}(1:2) - tth{k}(1:2);
            features(cnt).AP_fall_time = tend{k}(1:2) - tp{k}(1:2);
            features(cnt).AP_ahp_time = tahp{k}(1:2) - tend{k}(1:2);
            features(cnt).AP_rise_rate = features(cnt).AP_amplitude ./ features(cnt).AP_rise_time;
            features(cnt).AP_fall_rate = features(cnt).AP_amplitude ./ features(cnt).AP_fall_time;
            features(cnt).AP_fast_ahp = Vend{k}(1:2) - Vahp{k}(1:2);
            features(cnt).AP_amplitude_change = diff(features(cnt).AP_amplitude)/features(cnt).AP_amplitude(2);
            features(cnt).AP_duration_change = diff(features(cnt).AP_duration)/features(cnt).AP_duration(2);
            features(cnt).AP_half_width_change = diff(features(cnt).AP_half_width)/features(cnt).AP_half_width(2);
            features(cnt).AP_ahp_time_change = diff(features(cnt).AP_ahp_time)/features(cnt).AP_ahp_time(2);
            features(cnt).AP_rise_rate_change = diff(features(cnt).AP_rise_rate)/features(cnt).AP_rise_rate(2);
            features(cnt).AP_fall_rate_change = diff(features(cnt).AP_fall_rate)/features(cnt).AP_fall_rate(2);
            features(cnt).AP_fast_ahp_change = diff(features(cnt).AP_fast_ahp)/features(cnt).AP_fast_ahp(2);
            features(cnt).AP0_ahp_to_AP1_th_time = tahp{k}(1) - tth{k}(2);
            features(cnt).AHP_symmetry = features(cnt).AP_ahp_time / (features(cnt).AP_ahp_time+features(cnt).AP0_ahp_to_AP1_th_time);
        end
        cnt = cnt+1;
    end
end
