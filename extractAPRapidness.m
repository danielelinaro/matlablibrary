function [rapidness,Vthresh,dVthresh,Vr,dVr] = extractAPRapidness(t,V,tp,tth,Vth,min_dVthresh,window,no_spikes,do_plot)
% extractAPRapidness extracts the rapidness of AP increase around the
% threshold.
% 
% rapidness = extractAPRapidness(t,V,tp,tth,Vth,min_dVthresh,window,no_spikes,do_plot)
% 
% Parameters:
%            t - 1xN time vector, where N is the number of samples, in seconds.
%            V - MxN voltage matrix, where M is the number of repetitions.
%           tp - Mx1 cell array containing the times of the spike peaks, as
%                returned by extractAPPeak. It can be empty, in which case
%                extractAPPeak is called with a value of threshold = -20
%                mV.
%          tth - Mx1 cell array containing the times of the spike thresholds, as
%                returned by extractAPThreshold. It can be empty, in which case
%                a fixed threshold is used (specified by dVthresh).
% min_dVthresh - minimum voltage threshold for the computation of the rapidness
%                (default 15 mV/ms).
%       window - a 2x1 vector representing the time window around each spike to
%                consider for the computation of the voltage derivative
%                (default [3e-3,5e-3] seconds).
%    no_spikes - the maximum number of spikes to consider in each trial
%                (default: all spikes).
%      do_plot - whether to plot a summary figure (default: yes).
% 
% Returns:
%    rapidness - Mx1 cell containing all the values of rapidness.
% 
% See also:
%    extractAPPeak extractAPThreshold
% 

% Author: Daniele Linaro - November 2016

if ~exist('min_dVthresh','var') || isempty(min_dVthresh)
    min_dVthresh = 15; % [mV/ms]
end

if ~exist('tp','var') || isempty(tp)
    tp = extractAPPeak(t,V,-20);
end

if ~exist('tth','var') || isempty(tth) || ~exist('Vth','var') || isempty(Vth)
    [tth,Vth] = extractAPThreshold(t,V,-20,tp);
end

idx = cellfun(@(x) ~isempty(x), tp);
V = V(idx,:);
tp = tp(idx);
tth = tth(idx);
Vth = Vth(idx);

if ~exist('window','var') || isempty(window)
    window = [3e-3,5e-3];
end

if exist('no_spikes','var') && ~isempty(no_spikes)
    tp = cellfun(@(x) x(1:min(no_spikes,length(x))), tp, ...
        'UniformOutput', 0);
    tth = cellfun(@(x) x(1:min(no_spikes,length(x))), tth, ...
        'UniformOutput', 0);
    Vth = cellfun(@(x) x(1:min(no_spikes,length(x))), Vth, ...
        'UniformOutput', 0);
end
rapidness = cellfun(@(x) nan(size(x)), tp, 'UniformOutput', 0);
Vthresh = rapidness;
dVthresh = rapidness;

if ~exist('do_plot','var') || isempty(do_plot)
    do_plot = 1;
end

bound = [1e-3,1e-3];
window = window+bound;

p = 2;
[~,Vwndw] = ETSA(t,V,tp,window);
nspikes = size(Vwndw,1);
Vr = resample(Vwndw',p,1)';
dt = diff(t(1:2));
dtr = dt/p;
Tr = (0:size(Vr,2)-1) * dtr - window(1);
Vr = Vr(:,Tr>=-window(1)+bound(1) & Tr<=window(2)-bound(2));
dVr = (Vr(:,3:end)-Vr(:,1:end-2)) / (2*dtr) * 1e-3;

if do_plot
    figure;
    hold on;
    cmap = lines(max(cellfun(@(x) length(x), tp)));
end

j = 1;
k = 1;
for i=1:nspikes
    if max(dVr(i,:)) > min_dVthresh
        start = round((window(1)-bound(1)-tp{j}(k)+tth{j}(k))/dtr);
        stop = start+5;
        offset = Vr(i,start-1);
        x = Vr(i,start:stop) - offset;
        y = dVr(i,start-1:stop-1);
        if y(1) > min_dVthresh/5
            dVthresh{j}(k) = y(1);
            try
                model = fit(x',y','power2');
                Vthresh{j}(k) = ((dVthresh{j}(k) - model.c) / model.a)^(1/model.b);
                rapidness{j}(k) = model.b * model.a * Vthresh{j}(k) ^ (model.b-1);
                if ~ isreal(rapidness{j}(k)) || rapidness{j}(k) < 0
                    rapidness{j}(k) = nan;
                    if do_plot
                        plot(Vr(i,2:end-1),dVr(i,:),'Color',cmap(k,:));
                    end
                elseif do_plot
                    q = dVthresh{j}(k) - rapidness{j}(k)*Vthresh{j}(k);
                    plot(Vr(i,2:end-1),dVr(i,:),'Color',cmap(k,:));
                    plot(Vthresh{j}(k)+[-5,5]+offset,rapidness{j}(k)*(Vthresh{j}(k)+[-5,5])+q,...
                        'k','LineWidth',1);
                    plot(Vthresh{j}(k)+offset,dVthresh{j}(k),'ks','MarkerFaceColor','w',...
                        'MarkerSize',4,'LineWidth',1);
                end
                Vthresh{j}(k) = Vthresh{j}(k)+offset;
            catch
                fprintf(1, 'Could not fit data.\n');
                rapidness{j}(k) = nan;
            end
        end
    else
        rapidness{j}(k) = nan;
        if do_plot
            plot(Vr(i,2:end-1),dVr(i,:),'Color',cmap(k,:));
        end
    end
    
    k = k+1;
    if k > length(rapidness{j})
        j = j+1;
        k = 1;
    end
end

r = flattenCellArray(rapidness,'full');
r = sort(r(~isnan(r)));
lower_bound = quantile(r,0.25) - 1.5*iqr(r);
upper_bound = quantile(r,0.75) + 1.5*iqr(r);
discarded = 0;
for i=1:length(rapidness)
    for j=1:length(rapidness{i})
        if rapidness{i}(j) < lower_bound || rapidness{i}(j) > upper_bound
            rapidness{i}(j) = nan;
            discarded = discarded + 1;
        end
    end
end
if discarded == 1
    fprintf(1, 'Discarded 1 value of rapidness because it looks like an outlier.\n');
elseif discarded > 1
    fprintf(1, 'Discarded %d values of rapidness because they look like outliers.\n', discarded);
end

if do_plot
    r = flattenCellArray(rapidness,'full');
    axis tight;
    set(gca,'TickDir','Out','LineWidth',1,'FontSize',10);
    title(sprintf('Rapidness = %.2f +- %.2f ms', nanmean(r), nanstd(r)));
    xlabel('Vm (mV)');
    ylabel('dVm/dt (mV/ms)');
    sz = [5,4];
    set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,sz],'PaperSize',sz);
    print('-dpdf','AP_rapidness.pdf');
end
