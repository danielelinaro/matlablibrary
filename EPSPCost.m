function c = EPSPCost(pars,T,Vref)
% c = EPSPcost(pars,T,Vref)
V = doubleexp(pars(1), pars(2), pars(3), T);
c = norm(Vref - V)/length(T);
